<?php include_once "staffinfo.php" ?>
<?php

// Create page object
if (!isset($EnquiryCallAsset_grid)) $EnquiryCallAsset_grid = new cEnquiryCallAsset_grid();

// Page init
$EnquiryCallAsset_grid->Page_Init();

// Page main
$EnquiryCallAsset_grid->Page_Main();
?>
<?php if ($EnquiryCallAsset->Export == "") { ?>
<script type="text/javascript">

// Page object
var EnquiryCallAsset_grid = new ew_Page("EnquiryCallAsset_grid");
EnquiryCallAsset_grid.PageID = "grid"; // Page ID
var EW_PAGE_ID = EnquiryCallAsset_grid.PageID; // For backward compatibility

// Form object
var fEnquiryCallAssetgrid = new ew_Form("fEnquiryCallAssetgrid");

// Validate form
fEnquiryCallAssetgrid.Validate = function(fobj) {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	fobj = fobj || this.Form;
	this.PostAutoSuggest();	
	if (fobj.a_confirm && fobj.a_confirm.value == "F")
		return true;
	var elm, aelm;
	var rowcnt = (fobj.key_count) ? Number(fobj.key_count.value) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // rowcnt == 0 => Inline-Add
	var addcnt = 0;
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = (fobj.key_count) ? String(i) : "";
		var checkrow = (fobj.a_list && fobj.a_list.value == "gridinsert") ? !this.EmptyRow(infix) : true;
		if (checkrow) {
			addcnt++;
		elm = fobj.elements["x" + infix + "_assetSerialNumber"];
		if (elm && !ew_HasValue(elm))
			return ew_OnError(this, elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($EnquiryCallAsset->assetSerialNumber->FldCaption()) ?>");
		elm = fobj.elements["x" + infix + "_vendorName"];
		if (elm && !ew_HasValue(elm))
			return ew_OnError(this, elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($EnquiryCallAsset->vendorName->FldCaption()) ?>");

		// Set up row object
		ew_ElementsToRow(fobj, infix);

		// Fire Form_CustomValidate event
		if (!this.Form_CustomValidate(fobj))
			return false;
		} // End Grid Add checking
	}
	return true;
}

// Check empty row
fEnquiryCallAssetgrid.EmptyRow = function(infix) {
	var fobj = this.Form;
	if (ew_ValueChanged(fobj, infix, "assetSerialNumber", false)) return false;
	if (ew_ValueChanged(fobj, infix, "assetDescription", false)) return false;
	if (ew_ValueChanged(fobj, infix, "vendorName", false)) return false;
	if (ew_ValueChanged(fobj, infix, "vendorDetails", false)) return false;
	return true;
}

// Form_CustomValidate event
fEnquiryCallAssetgrid.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fEnquiryCallAssetgrid.ValidateRequired = true;
<?php } else { ?>
fEnquiryCallAssetgrid.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fEnquiryCallAssetgrid.Lists["x_assetSerialNumber"] = {"LinkField":"x_serialNumber","Ajax":null,"AutoFill":false,"DisplayFields":["x_serialNumber","x_assetDescription","",""],"ParentFields":[],"FilterFields":[],"Options":[]};

// Form object for search
</script>
<?php } ?>
<?php
if ($EnquiryCallAsset->CurrentAction == "gridadd") {
	if ($EnquiryCallAsset->CurrentMode == "copy") {
		$bSelectLimit = EW_SELECT_LIMIT;
		if ($bSelectLimit) {
			$EnquiryCallAsset_grid->TotalRecs = $EnquiryCallAsset->SelectRecordCount();
			$EnquiryCallAsset_grid->Recordset = $EnquiryCallAsset_grid->LoadRecordset($EnquiryCallAsset_grid->StartRec-1, $EnquiryCallAsset_grid->DisplayRecs);
		} else {
			if ($EnquiryCallAsset_grid->Recordset = $EnquiryCallAsset_grid->LoadRecordset())
				$EnquiryCallAsset_grid->TotalRecs = $EnquiryCallAsset_grid->Recordset->RecordCount();
		}
		$EnquiryCallAsset_grid->StartRec = 1;
		$EnquiryCallAsset_grid->DisplayRecs = $EnquiryCallAsset_grid->TotalRecs;
	} else {
		$EnquiryCallAsset->CurrentFilter = "0=1";
		$EnquiryCallAsset_grid->StartRec = 1;
		$EnquiryCallAsset_grid->DisplayRecs = $EnquiryCallAsset->GridAddRowCount;
	}
	$EnquiryCallAsset_grid->TotalRecs = $EnquiryCallAsset_grid->DisplayRecs;
	$EnquiryCallAsset_grid->StopRec = $EnquiryCallAsset_grid->DisplayRecs;
} else {
	$bSelectLimit = EW_SELECT_LIMIT;
	if ($bSelectLimit) {
		$EnquiryCallAsset_grid->TotalRecs = $EnquiryCallAsset->SelectRecordCount();
	} else {
		if ($EnquiryCallAsset_grid->Recordset = $EnquiryCallAsset_grid->LoadRecordset())
			$EnquiryCallAsset_grid->TotalRecs = $EnquiryCallAsset_grid->Recordset->RecordCount();
	}
	$EnquiryCallAsset_grid->StartRec = 1;
	$EnquiryCallAsset_grid->DisplayRecs = $EnquiryCallAsset_grid->TotalRecs; // Display all records
	if ($bSelectLimit)
		$EnquiryCallAsset_grid->Recordset = $EnquiryCallAsset_grid->LoadRecordset($EnquiryCallAsset_grid->StartRec-1, $EnquiryCallAsset_grid->DisplayRecs);
}
?>
<p style="white-space: nowrap;"><span id="ewPageCaption" class="ewTitle ewTableTitle"><?php if ($EnquiryCallAsset->CurrentMode == "add" || $EnquiryCallAsset->CurrentMode == "copy") { ?><?php echo $Language->Phrase("Add") ?><?php } elseif ($EnquiryCallAsset->CurrentMode == "edit") { ?><?php echo $Language->Phrase("Edit") ?><?php } ?>&nbsp;<?php echo $Language->Phrase("TblTypeVIEW") ?><?php echo $EnquiryCallAsset->TableCaption() ?></span></p>
</p>
<?php $EnquiryCallAsset_grid->ShowPageHeader(); ?>
<?php
$EnquiryCallAsset_grid->ShowMessage();
?>
<br>
<table cellspacing="0" class="ewGrid"><tr><td class="ewGridContent">
<div id="fEnquiryCallAssetgrid" class="ewForm">
<div id="gmp_EnquiryCallAsset" class="ewGridMiddlePanel">
<table id="tbl_EnquiryCallAssetgrid" class="ewTable ewTableSeparate">
<?php echo $EnquiryCallAsset->TableCustomInnerHtml ?>
<thead><!-- Table header -->
	<tr class="ewTableHeader">
<?php

// Render list options
$EnquiryCallAsset_grid->RenderListOptions();

// Render list options (header, left)
$EnquiryCallAsset_grid->ListOptions->Render("header", "left");
?>
<?php if ($EnquiryCallAsset->assetSerialNumber->Visible) { // assetSerialNumber ?>
	<?php if ($EnquiryCallAsset->SortUrl($EnquiryCallAsset->assetSerialNumber) == "") { ?>
		<td><span id="elh_EnquiryCallAsset_assetSerialNumber" class="EnquiryCallAsset_assetSerialNumber"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $EnquiryCallAsset->assetSerialNumber->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div><span id="elh_EnquiryCallAsset_assetSerialNumber" class="EnquiryCallAsset_assetSerialNumber">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $EnquiryCallAsset->assetSerialNumber->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($EnquiryCallAsset->assetSerialNumber->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($EnquiryCallAsset->assetSerialNumber->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($EnquiryCallAsset->assetDescription->Visible) { // assetDescription ?>
	<?php if ($EnquiryCallAsset->SortUrl($EnquiryCallAsset->assetDescription) == "") { ?>
		<td><span id="elh_EnquiryCallAsset_assetDescription" class="EnquiryCallAsset_assetDescription"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $EnquiryCallAsset->assetDescription->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div><span id="elh_EnquiryCallAsset_assetDescription" class="EnquiryCallAsset_assetDescription">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $EnquiryCallAsset->assetDescription->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($EnquiryCallAsset->assetDescription->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($EnquiryCallAsset->assetDescription->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($EnquiryCallAsset->vendorName->Visible) { // vendorName ?>
	<?php if ($EnquiryCallAsset->SortUrl($EnquiryCallAsset->vendorName) == "") { ?>
		<td><span id="elh_EnquiryCallAsset_vendorName" class="EnquiryCallAsset_vendorName"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $EnquiryCallAsset->vendorName->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div><span id="elh_EnquiryCallAsset_vendorName" class="EnquiryCallAsset_vendorName">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $EnquiryCallAsset->vendorName->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($EnquiryCallAsset->vendorName->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($EnquiryCallAsset->vendorName->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($EnquiryCallAsset->vendorDetails->Visible) { // vendorDetails ?>
	<?php if ($EnquiryCallAsset->SortUrl($EnquiryCallAsset->vendorDetails) == "") { ?>
		<td><span id="elh_EnquiryCallAsset_vendorDetails" class="EnquiryCallAsset_vendorDetails"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $EnquiryCallAsset->vendorDetails->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div><span id="elh_EnquiryCallAsset_vendorDetails" class="EnquiryCallAsset_vendorDetails">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $EnquiryCallAsset->vendorDetails->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($EnquiryCallAsset->vendorDetails->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($EnquiryCallAsset->vendorDetails->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php

// Render list options (header, right)
$EnquiryCallAsset_grid->ListOptions->Render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
$EnquiryCallAsset_grid->StartRec = 1;
$EnquiryCallAsset_grid->StopRec = $EnquiryCallAsset_grid->TotalRecs; // Show all records

// Restore number of post back records
if ($objForm) {
	$objForm->Index = -1;
	if ($objForm->HasValue("key_count") && ($EnquiryCallAsset->CurrentAction == "gridadd" || $EnquiryCallAsset->CurrentAction == "gridedit" || $EnquiryCallAsset->CurrentAction == "F")) {
		$EnquiryCallAsset_grid->KeyCount = $objForm->GetValue("key_count");
		$EnquiryCallAsset_grid->StopRec = $EnquiryCallAsset_grid->KeyCount;
	}
}
$EnquiryCallAsset_grid->RecCnt = $EnquiryCallAsset_grid->StartRec - 1;
if ($EnquiryCallAsset_grid->Recordset && !$EnquiryCallAsset_grid->Recordset->EOF) {
	$EnquiryCallAsset_grid->Recordset->MoveFirst();
	if (!$bSelectLimit && $EnquiryCallAsset_grid->StartRec > 1)
		$EnquiryCallAsset_grid->Recordset->Move($EnquiryCallAsset_grid->StartRec - 1);
} elseif (!$EnquiryCallAsset->AllowAddDeleteRow && $EnquiryCallAsset_grid->StopRec == 0) {
	$EnquiryCallAsset_grid->StopRec = $EnquiryCallAsset->GridAddRowCount;
}

// Initialize aggregate
$EnquiryCallAsset->RowType = EW_ROWTYPE_AGGREGATEINIT;
$EnquiryCallAsset->ResetAttrs();
$EnquiryCallAsset_grid->RenderRow();
if ($EnquiryCallAsset->CurrentAction == "gridadd")
	$EnquiryCallAsset_grid->RowIndex = 0;
if ($EnquiryCallAsset->CurrentAction == "gridedit")
	$EnquiryCallAsset_grid->RowIndex = 0;
while ($EnquiryCallAsset_grid->RecCnt < $EnquiryCallAsset_grid->StopRec) {
	$EnquiryCallAsset_grid->RecCnt++;
	if (intval($EnquiryCallAsset_grid->RecCnt) >= intval($EnquiryCallAsset_grid->StartRec)) {
		$EnquiryCallAsset_grid->RowCnt++;
		if ($EnquiryCallAsset->CurrentAction == "gridadd" || $EnquiryCallAsset->CurrentAction == "gridedit" || $EnquiryCallAsset->CurrentAction == "F") {
			$EnquiryCallAsset_grid->RowIndex++;
			$objForm->Index = $EnquiryCallAsset_grid->RowIndex;
			if ($objForm->HasValue("k_action"))
				$EnquiryCallAsset_grid->RowAction = strval($objForm->GetValue("k_action"));
			elseif ($EnquiryCallAsset->CurrentAction == "gridadd")
				$EnquiryCallAsset_grid->RowAction = "insert";
			else
				$EnquiryCallAsset_grid->RowAction = "";
		}

		// Set up key count
		$EnquiryCallAsset_grid->KeyCount = $EnquiryCallAsset_grid->RowIndex;

		// Init row class and style
		$EnquiryCallAsset->ResetAttrs();
		$EnquiryCallAsset->CssClass = "";
		if ($EnquiryCallAsset->CurrentAction == "gridadd") {
			if ($EnquiryCallAsset->CurrentMode == "copy") {
				$EnquiryCallAsset_grid->LoadRowValues($EnquiryCallAsset_grid->Recordset); // Load row values
				$EnquiryCallAsset_grid->SetRecordKey($EnquiryCallAsset_grid->RowOldKey, $EnquiryCallAsset_grid->Recordset); // Set old record key
			} else {
				$EnquiryCallAsset_grid->LoadDefaultValues(); // Load default values
				$EnquiryCallAsset_grid->RowOldKey = ""; // Clear old key value
			}
		} elseif ($EnquiryCallAsset->CurrentAction == "gridedit") {
			$EnquiryCallAsset_grid->LoadRowValues($EnquiryCallAsset_grid->Recordset); // Load row values
		}
		$EnquiryCallAsset->RowType = EW_ROWTYPE_VIEW; // Render view
		if ($EnquiryCallAsset->CurrentAction == "gridadd") // Grid add
			$EnquiryCallAsset->RowType = EW_ROWTYPE_ADD; // Render add
		if ($EnquiryCallAsset->CurrentAction == "gridadd" && $EnquiryCallAsset->EventCancelled && !$objForm->HasValue("k_blankrow")) // Insert failed
			$EnquiryCallAsset_grid->RestoreCurrentRowFormValues($EnquiryCallAsset_grid->RowIndex); // Restore form values
		if ($EnquiryCallAsset->CurrentAction == "gridedit") { // Grid edit
			if ($EnquiryCallAsset->EventCancelled) {
				$EnquiryCallAsset_grid->RestoreCurrentRowFormValues($EnquiryCallAsset_grid->RowIndex); // Restore form values
			}
			if ($EnquiryCallAsset_grid->RowAction == "insert")
				$EnquiryCallAsset->RowType = EW_ROWTYPE_ADD; // Render add
			else
				$EnquiryCallAsset->RowType = EW_ROWTYPE_EDIT; // Render edit
		}
		if ($EnquiryCallAsset->CurrentAction == "gridedit" && ($EnquiryCallAsset->RowType == EW_ROWTYPE_EDIT || $EnquiryCallAsset->RowType == EW_ROWTYPE_ADD) && $EnquiryCallAsset->EventCancelled) // Update failed
			$EnquiryCallAsset_grid->RestoreCurrentRowFormValues($EnquiryCallAsset_grid->RowIndex); // Restore form values
		if ($EnquiryCallAsset->RowType == EW_ROWTYPE_EDIT) // Edit row
			$EnquiryCallAsset_grid->EditRowCnt++;
		if ($EnquiryCallAsset->CurrentAction == "F") // Confirm row
			$EnquiryCallAsset_grid->RestoreCurrentRowFormValues($EnquiryCallAsset_grid->RowIndex); // Restore form values

		// Set up row id / data-rowindex
		$EnquiryCallAsset->RowAttrs = array_merge($EnquiryCallAsset->RowAttrs, array('data-rowindex'=>$EnquiryCallAsset_grid->RowCnt, 'id'=>'r' . $EnquiryCallAsset_grid->RowCnt . '_EnquiryCallAsset', 'data-rowtype'=>$EnquiryCallAsset->RowType));

		// Render row
		$EnquiryCallAsset_grid->RenderRow();

		// Render list options
		$EnquiryCallAsset_grid->RenderListOptions();

		// Skip delete row / empty row for confirm page
		if ($EnquiryCallAsset_grid->RowAction <> "delete" && $EnquiryCallAsset_grid->RowAction <> "insertdelete" && !($EnquiryCallAsset_grid->RowAction == "insert" && $EnquiryCallAsset->CurrentAction == "F" && $EnquiryCallAsset_grid->EmptyRow())) {
?>
	<tr<?php echo $EnquiryCallAsset->RowAttributes() ?>>
<?php

// Render list options (body, left)
$EnquiryCallAsset_grid->ListOptions->Render("body", "left", $EnquiryCallAsset_grid->RowCnt);
?>
	<?php if ($EnquiryCallAsset->assetSerialNumber->Visible) { // assetSerialNumber ?>
		<td<?php echo $EnquiryCallAsset->assetSerialNumber->CellAttributes() ?>><span id="el<?php echo $EnquiryCallAsset_grid->RowCnt ?>_EnquiryCallAsset_assetSerialNumber" class="EnquiryCallAsset_assetSerialNumber">
<?php if ($EnquiryCallAsset->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<select id="x<?php echo $EnquiryCallAsset_grid->RowIndex ?>_assetSerialNumber" name="x<?php echo $EnquiryCallAsset_grid->RowIndex ?>_assetSerialNumber"<?php echo $EnquiryCallAsset->assetSerialNumber->EditAttributes() ?>>
<?php
if (is_array($EnquiryCallAsset->assetSerialNumber->EditValue)) {
	$arwrk = $EnquiryCallAsset->assetSerialNumber->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($EnquiryCallAsset->assetSerialNumber->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " selected=\"selected\"" : "";
		if ($selwrk <> "") $emptywrk = FALSE;
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $arwrk[$rowcntwrk][1] ?>
<?php if ($arwrk[$rowcntwrk][2] <> "") { ?>
<?php echo ew_ValueSeparator(1,$EnquiryCallAsset->assetSerialNumber) ?><?php echo $arwrk[$rowcntwrk][2] ?>
<?php } ?>
</option>
<?php
	}
}
if (@$emptywrk) $EnquiryCallAsset->assetSerialNumber->OldValue = "";
?>
</select>
<script type="text/javascript">
fEnquiryCallAssetgrid.Lists["x_assetSerialNumber"].Options = <?php echo (is_array($EnquiryCallAsset->assetSerialNumber->EditValue)) ? ew_ArrayToJson($EnquiryCallAsset->assetSerialNumber->EditValue, 1) : "[]" ?>;
</script>
<input type="hidden" name="o<?php echo $EnquiryCallAsset_grid->RowIndex ?>_assetSerialNumber" id="o<?php echo $EnquiryCallAsset_grid->RowIndex ?>_assetSerialNumber" value="<?php echo ew_HtmlEncode($EnquiryCallAsset->assetSerialNumber->OldValue) ?>">
<?php } ?>
<?php if ($EnquiryCallAsset->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<select id="x<?php echo $EnquiryCallAsset_grid->RowIndex ?>_assetSerialNumber" name="x<?php echo $EnquiryCallAsset_grid->RowIndex ?>_assetSerialNumber"<?php echo $EnquiryCallAsset->assetSerialNumber->EditAttributes() ?>>
<?php
if (is_array($EnquiryCallAsset->assetSerialNumber->EditValue)) {
	$arwrk = $EnquiryCallAsset->assetSerialNumber->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($EnquiryCallAsset->assetSerialNumber->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " selected=\"selected\"" : "";
		if ($selwrk <> "") $emptywrk = FALSE;
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $arwrk[$rowcntwrk][1] ?>
<?php if ($arwrk[$rowcntwrk][2] <> "") { ?>
<?php echo ew_ValueSeparator(1,$EnquiryCallAsset->assetSerialNumber) ?><?php echo $arwrk[$rowcntwrk][2] ?>
<?php } ?>
</option>
<?php
	}
}
if (@$emptywrk) $EnquiryCallAsset->assetSerialNumber->OldValue = "";
?>
</select>
<script type="text/javascript">
fEnquiryCallAssetgrid.Lists["x_assetSerialNumber"].Options = <?php echo (is_array($EnquiryCallAsset->assetSerialNumber->EditValue)) ? ew_ArrayToJson($EnquiryCallAsset->assetSerialNumber->EditValue, 1) : "[]" ?>;
</script>
<?php } ?>
<?php if ($EnquiryCallAsset->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span<?php echo $EnquiryCallAsset->assetSerialNumber->ViewAttributes() ?>>
<?php echo $EnquiryCallAsset->assetSerialNumber->ListViewValue() ?></span>
<input type="hidden" name="x<?php echo $EnquiryCallAsset_grid->RowIndex ?>_assetSerialNumber" id="x<?php echo $EnquiryCallAsset_grid->RowIndex ?>_assetSerialNumber" value="<?php echo ew_HtmlEncode($EnquiryCallAsset->assetSerialNumber->FormValue) ?>">
<input type="hidden" name="o<?php echo $EnquiryCallAsset_grid->RowIndex ?>_assetSerialNumber" id="o<?php echo $EnquiryCallAsset_grid->RowIndex ?>_assetSerialNumber" value="<?php echo ew_HtmlEncode($EnquiryCallAsset->assetSerialNumber->OldValue) ?>">
<?php } ?>
</span><a id="<?php echo $EnquiryCallAsset_grid->PageObjName . "_row_" . $EnquiryCallAsset_grid->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($EnquiryCallAsset->assetDescription->Visible) { // assetDescription ?>
		<td<?php echo $EnquiryCallAsset->assetDescription->CellAttributes() ?>><span id="el<?php echo $EnquiryCallAsset_grid->RowCnt ?>_EnquiryCallAsset_assetDescription" class="EnquiryCallAsset_assetDescription">
<?php if ($EnquiryCallAsset->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<textarea name="x<?php echo $EnquiryCallAsset_grid->RowIndex ?>_assetDescription" id="x<?php echo $EnquiryCallAsset_grid->RowIndex ?>_assetDescription" cols="35" rows="4"<?php echo $EnquiryCallAsset->assetDescription->EditAttributes() ?>><?php echo $EnquiryCallAsset->assetDescription->EditValue ?></textarea>
<input type="hidden" name="o<?php echo $EnquiryCallAsset_grid->RowIndex ?>_assetDescription" id="o<?php echo $EnquiryCallAsset_grid->RowIndex ?>_assetDescription" value="<?php echo ew_HtmlEncode($EnquiryCallAsset->assetDescription->OldValue) ?>">
<?php } ?>
<?php if ($EnquiryCallAsset->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<textarea name="x<?php echo $EnquiryCallAsset_grid->RowIndex ?>_assetDescription" id="x<?php echo $EnquiryCallAsset_grid->RowIndex ?>_assetDescription" cols="35" rows="4"<?php echo $EnquiryCallAsset->assetDescription->EditAttributes() ?>><?php echo $EnquiryCallAsset->assetDescription->EditValue ?></textarea>
<?php } ?>
<?php if ($EnquiryCallAsset->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span<?php echo $EnquiryCallAsset->assetDescription->ViewAttributes() ?>>
<?php echo $EnquiryCallAsset->assetDescription->ListViewValue() ?></span>
<input type="hidden" name="x<?php echo $EnquiryCallAsset_grid->RowIndex ?>_assetDescription" id="x<?php echo $EnquiryCallAsset_grid->RowIndex ?>_assetDescription" value="<?php echo ew_HtmlEncode($EnquiryCallAsset->assetDescription->FormValue) ?>">
<input type="hidden" name="o<?php echo $EnquiryCallAsset_grid->RowIndex ?>_assetDescription" id="o<?php echo $EnquiryCallAsset_grid->RowIndex ?>_assetDescription" value="<?php echo ew_HtmlEncode($EnquiryCallAsset->assetDescription->OldValue) ?>">
<?php } ?>
</span><a id="<?php echo $EnquiryCallAsset_grid->PageObjName . "_row_" . $EnquiryCallAsset_grid->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($EnquiryCallAsset->vendorName->Visible) { // vendorName ?>
		<td<?php echo $EnquiryCallAsset->vendorName->CellAttributes() ?>><span id="el<?php echo $EnquiryCallAsset_grid->RowCnt ?>_EnquiryCallAsset_vendorName" class="EnquiryCallAsset_vendorName">
<?php if ($EnquiryCallAsset->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="text" name="x<?php echo $EnquiryCallAsset_grid->RowIndex ?>_vendorName" id="x<?php echo $EnquiryCallAsset_grid->RowIndex ?>_vendorName" size="30" maxlength="50" value="<?php echo $EnquiryCallAsset->vendorName->EditValue ?>"<?php echo $EnquiryCallAsset->vendorName->EditAttributes() ?>>
<input type="hidden" name="o<?php echo $EnquiryCallAsset_grid->RowIndex ?>_vendorName" id="o<?php echo $EnquiryCallAsset_grid->RowIndex ?>_vendorName" value="<?php echo ew_HtmlEncode($EnquiryCallAsset->vendorName->OldValue) ?>">
<?php } ?>
<?php if ($EnquiryCallAsset->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<input type="text" name="x<?php echo $EnquiryCallAsset_grid->RowIndex ?>_vendorName" id="x<?php echo $EnquiryCallAsset_grid->RowIndex ?>_vendorName" size="30" maxlength="50" value="<?php echo $EnquiryCallAsset->vendorName->EditValue ?>"<?php echo $EnquiryCallAsset->vendorName->EditAttributes() ?>>
<?php } ?>
<?php if ($EnquiryCallAsset->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span<?php echo $EnquiryCallAsset->vendorName->ViewAttributes() ?>>
<?php echo $EnquiryCallAsset->vendorName->ListViewValue() ?></span>
<input type="hidden" name="x<?php echo $EnquiryCallAsset_grid->RowIndex ?>_vendorName" id="x<?php echo $EnquiryCallAsset_grid->RowIndex ?>_vendorName" value="<?php echo ew_HtmlEncode($EnquiryCallAsset->vendorName->FormValue) ?>">
<input type="hidden" name="o<?php echo $EnquiryCallAsset_grid->RowIndex ?>_vendorName" id="o<?php echo $EnquiryCallAsset_grid->RowIndex ?>_vendorName" value="<?php echo ew_HtmlEncode($EnquiryCallAsset->vendorName->OldValue) ?>">
<?php } ?>
</span><a id="<?php echo $EnquiryCallAsset_grid->PageObjName . "_row_" . $EnquiryCallAsset_grid->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($EnquiryCallAsset->vendorDetails->Visible) { // vendorDetails ?>
		<td<?php echo $EnquiryCallAsset->vendorDetails->CellAttributes() ?>><span id="el<?php echo $EnquiryCallAsset_grid->RowCnt ?>_EnquiryCallAsset_vendorDetails" class="EnquiryCallAsset_vendorDetails">
<?php if ($EnquiryCallAsset->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="text" name="x<?php echo $EnquiryCallAsset_grid->RowIndex ?>_vendorDetails" id="x<?php echo $EnquiryCallAsset_grid->RowIndex ?>_vendorDetails" size="30" maxlength="250" value="<?php echo $EnquiryCallAsset->vendorDetails->EditValue ?>"<?php echo $EnquiryCallAsset->vendorDetails->EditAttributes() ?>>
<input type="hidden" name="o<?php echo $EnquiryCallAsset_grid->RowIndex ?>_vendorDetails" id="o<?php echo $EnquiryCallAsset_grid->RowIndex ?>_vendorDetails" value="<?php echo ew_HtmlEncode($EnquiryCallAsset->vendorDetails->OldValue) ?>">
<?php } ?>
<?php if ($EnquiryCallAsset->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<input type="text" name="x<?php echo $EnquiryCallAsset_grid->RowIndex ?>_vendorDetails" id="x<?php echo $EnquiryCallAsset_grid->RowIndex ?>_vendorDetails" size="30" maxlength="250" value="<?php echo $EnquiryCallAsset->vendorDetails->EditValue ?>"<?php echo $EnquiryCallAsset->vendorDetails->EditAttributes() ?>>
<?php } ?>
<?php if ($EnquiryCallAsset->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span<?php echo $EnquiryCallAsset->vendorDetails->ViewAttributes() ?>>
<?php echo $EnquiryCallAsset->vendorDetails->ListViewValue() ?></span>
<input type="hidden" name="x<?php echo $EnquiryCallAsset_grid->RowIndex ?>_vendorDetails" id="x<?php echo $EnquiryCallAsset_grid->RowIndex ?>_vendorDetails" value="<?php echo ew_HtmlEncode($EnquiryCallAsset->vendorDetails->FormValue) ?>">
<input type="hidden" name="o<?php echo $EnquiryCallAsset_grid->RowIndex ?>_vendorDetails" id="o<?php echo $EnquiryCallAsset_grid->RowIndex ?>_vendorDetails" value="<?php echo ew_HtmlEncode($EnquiryCallAsset->vendorDetails->OldValue) ?>">
<?php } ?>
</span><a id="<?php echo $EnquiryCallAsset_grid->PageObjName . "_row_" . $EnquiryCallAsset_grid->RowCnt ?>"></a></td>
	<?php } ?>
<?php

// Render list options (body, right)
$EnquiryCallAsset_grid->ListOptions->Render("body", "right", $EnquiryCallAsset_grid->RowCnt);
?>
	</tr>
<?php if ($EnquiryCallAsset->RowType == EW_ROWTYPE_ADD || $EnquiryCallAsset->RowType == EW_ROWTYPE_EDIT) { ?>
<script type="text/javascript">
fEnquiryCallAssetgrid.UpdateOpts(<?php echo $EnquiryCallAsset_grid->RowIndex ?>);
</script>
<?php } ?>
<?php
	}
	} // End delete row checking
	if ($EnquiryCallAsset->CurrentAction <> "gridadd" || $EnquiryCallAsset->CurrentMode == "copy")
		if (!$EnquiryCallAsset_grid->Recordset->EOF) $EnquiryCallAsset_grid->Recordset->MoveNext();
}
?>
<?php
	if ($EnquiryCallAsset->CurrentMode == "add" || $EnquiryCallAsset->CurrentMode == "copy" || $EnquiryCallAsset->CurrentMode == "edit") {
		$EnquiryCallAsset_grid->RowIndex = '$rowindex$';
		$EnquiryCallAsset_grid->LoadDefaultValues();

		// Set row properties
		$EnquiryCallAsset->ResetAttrs();
		$EnquiryCallAsset->RowAttrs = array_merge($EnquiryCallAsset->RowAttrs, array('data-rowindex'=>$EnquiryCallAsset_grid->RowIndex, 'id'=>'r0_EnquiryCallAsset', 'data-rowtype'=>EW_ROWTYPE_ADD));
		ew_AppendClass($EnquiryCallAsset->RowAttrs["class"], "ewTemplate");
		$EnquiryCallAsset->RowType = EW_ROWTYPE_ADD;

		// Render row
		$EnquiryCallAsset_grid->RenderRow();

		// Render list options
		$EnquiryCallAsset_grid->RenderListOptions();
		$EnquiryCallAsset_grid->StartRowCnt = 0;
?>
	<tr<?php echo $EnquiryCallAsset->RowAttributes() ?>>
<?php

// Render list options (body, left)
$EnquiryCallAsset_grid->ListOptions->Render("body", "left", $EnquiryCallAsset_grid->RowIndex);
?>
	<?php if ($EnquiryCallAsset->assetSerialNumber->Visible) { // assetSerialNumber ?>
		<td><span id="el$rowindex$_EnquiryCallAsset_assetSerialNumber" class="EnquiryCallAsset_assetSerialNumber">
<?php if ($EnquiryCallAsset->CurrentAction <> "F") { ?>
<select id="x<?php echo $EnquiryCallAsset_grid->RowIndex ?>_assetSerialNumber" name="x<?php echo $EnquiryCallAsset_grid->RowIndex ?>_assetSerialNumber"<?php echo $EnquiryCallAsset->assetSerialNumber->EditAttributes() ?>>
<?php
if (is_array($EnquiryCallAsset->assetSerialNumber->EditValue)) {
	$arwrk = $EnquiryCallAsset->assetSerialNumber->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($EnquiryCallAsset->assetSerialNumber->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " selected=\"selected\"" : "";
		if ($selwrk <> "") $emptywrk = FALSE;
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $arwrk[$rowcntwrk][1] ?>
<?php if ($arwrk[$rowcntwrk][2] <> "") { ?>
<?php echo ew_ValueSeparator(1,$EnquiryCallAsset->assetSerialNumber) ?><?php echo $arwrk[$rowcntwrk][2] ?>
<?php } ?>
</option>
<?php
	}
}
if (@$emptywrk) $EnquiryCallAsset->assetSerialNumber->OldValue = "";
?>
</select>
<script type="text/javascript">
fEnquiryCallAssetgrid.Lists["x_assetSerialNumber"].Options = <?php echo (is_array($EnquiryCallAsset->assetSerialNumber->EditValue)) ? ew_ArrayToJson($EnquiryCallAsset->assetSerialNumber->EditValue, 1) : "[]" ?>;
</script>
<?php } else { ?>
<span<?php echo $EnquiryCallAsset->assetSerialNumber->ViewAttributes() ?>>
<?php echo $EnquiryCallAsset->assetSerialNumber->ViewValue ?></span>
<input type="hidden" name="x<?php echo $EnquiryCallAsset_grid->RowIndex ?>_assetSerialNumber" id="x<?php echo $EnquiryCallAsset_grid->RowIndex ?>_assetSerialNumber" value="<?php echo ew_HtmlEncode($EnquiryCallAsset->assetSerialNumber->FormValue) ?>">
<?php } ?>
<input type="hidden" name="o<?php echo $EnquiryCallAsset_grid->RowIndex ?>_assetSerialNumber" id="o<?php echo $EnquiryCallAsset_grid->RowIndex ?>_assetSerialNumber" value="<?php echo ew_HtmlEncode($EnquiryCallAsset->assetSerialNumber->OldValue) ?>">
</span></td>
	<?php } ?>
	<?php if ($EnquiryCallAsset->assetDescription->Visible) { // assetDescription ?>
		<td><span id="el$rowindex$_EnquiryCallAsset_assetDescription" class="EnquiryCallAsset_assetDescription">
<?php if ($EnquiryCallAsset->CurrentAction <> "F") { ?>
<textarea name="x<?php echo $EnquiryCallAsset_grid->RowIndex ?>_assetDescription" id="x<?php echo $EnquiryCallAsset_grid->RowIndex ?>_assetDescription" cols="35" rows="4"<?php echo $EnquiryCallAsset->assetDescription->EditAttributes() ?>><?php echo $EnquiryCallAsset->assetDescription->EditValue ?></textarea>
<?php } else { ?>
<span<?php echo $EnquiryCallAsset->assetDescription->ViewAttributes() ?>>
<?php echo $EnquiryCallAsset->assetDescription->ViewValue ?></span>
<input type="hidden" name="x<?php echo $EnquiryCallAsset_grid->RowIndex ?>_assetDescription" id="x<?php echo $EnquiryCallAsset_grid->RowIndex ?>_assetDescription" value="<?php echo ew_HtmlEncode($EnquiryCallAsset->assetDescription->FormValue) ?>">
<?php } ?>
<input type="hidden" name="o<?php echo $EnquiryCallAsset_grid->RowIndex ?>_assetDescription" id="o<?php echo $EnquiryCallAsset_grid->RowIndex ?>_assetDescription" value="<?php echo ew_HtmlEncode($EnquiryCallAsset->assetDescription->OldValue) ?>">
</span></td>
	<?php } ?>
	<?php if ($EnquiryCallAsset->vendorName->Visible) { // vendorName ?>
		<td><span id="el$rowindex$_EnquiryCallAsset_vendorName" class="EnquiryCallAsset_vendorName">
<?php if ($EnquiryCallAsset->CurrentAction <> "F") { ?>
<input type="text" name="x<?php echo $EnquiryCallAsset_grid->RowIndex ?>_vendorName" id="x<?php echo $EnquiryCallAsset_grid->RowIndex ?>_vendorName" size="30" maxlength="50" value="<?php echo $EnquiryCallAsset->vendorName->EditValue ?>"<?php echo $EnquiryCallAsset->vendorName->EditAttributes() ?>>
<?php } else { ?>
<span<?php echo $EnquiryCallAsset->vendorName->ViewAttributes() ?>>
<?php echo $EnquiryCallAsset->vendorName->ViewValue ?></span>
<input type="hidden" name="x<?php echo $EnquiryCallAsset_grid->RowIndex ?>_vendorName" id="x<?php echo $EnquiryCallAsset_grid->RowIndex ?>_vendorName" value="<?php echo ew_HtmlEncode($EnquiryCallAsset->vendorName->FormValue) ?>">
<?php } ?>
<input type="hidden" name="o<?php echo $EnquiryCallAsset_grid->RowIndex ?>_vendorName" id="o<?php echo $EnquiryCallAsset_grid->RowIndex ?>_vendorName" value="<?php echo ew_HtmlEncode($EnquiryCallAsset->vendorName->OldValue) ?>">
</span></td>
	<?php } ?>
	<?php if ($EnquiryCallAsset->vendorDetails->Visible) { // vendorDetails ?>
		<td><span id="el$rowindex$_EnquiryCallAsset_vendorDetails" class="EnquiryCallAsset_vendorDetails">
<?php if ($EnquiryCallAsset->CurrentAction <> "F") { ?>
<input type="text" name="x<?php echo $EnquiryCallAsset_grid->RowIndex ?>_vendorDetails" id="x<?php echo $EnquiryCallAsset_grid->RowIndex ?>_vendorDetails" size="30" maxlength="250" value="<?php echo $EnquiryCallAsset->vendorDetails->EditValue ?>"<?php echo $EnquiryCallAsset->vendorDetails->EditAttributes() ?>>
<?php } else { ?>
<span<?php echo $EnquiryCallAsset->vendorDetails->ViewAttributes() ?>>
<?php echo $EnquiryCallAsset->vendorDetails->ViewValue ?></span>
<input type="hidden" name="x<?php echo $EnquiryCallAsset_grid->RowIndex ?>_vendorDetails" id="x<?php echo $EnquiryCallAsset_grid->RowIndex ?>_vendorDetails" value="<?php echo ew_HtmlEncode($EnquiryCallAsset->vendorDetails->FormValue) ?>">
<?php } ?>
<input type="hidden" name="o<?php echo $EnquiryCallAsset_grid->RowIndex ?>_vendorDetails" id="o<?php echo $EnquiryCallAsset_grid->RowIndex ?>_vendorDetails" value="<?php echo ew_HtmlEncode($EnquiryCallAsset->vendorDetails->OldValue) ?>">
</span></td>
	<?php } ?>
<?php

// Render list options (body, right)
$EnquiryCallAsset_grid->ListOptions->Render("body", "right", $EnquiryCallAsset_grid->RowCnt);
?>
<script type="text/javascript">
fEnquiryCallAssetgrid.UpdateOpts(<?php echo $EnquiryCallAsset_grid->RowIndex ?>);
</script>
	</tr>
<?php
}
?>
</tbody>
</table>
<?php if ($EnquiryCallAsset->CurrentMode == "add" || $EnquiryCallAsset->CurrentMode == "copy") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridinsert">
<input type="hidden" name="key_count" id="key_count" value="<?php echo $EnquiryCallAsset_grid->KeyCount ?>">
<?php echo $EnquiryCallAsset_grid->MultiSelectKey ?>
<?php } ?>
<?php if ($EnquiryCallAsset->CurrentMode == "edit") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridupdate">
<input type="hidden" name="key_count" id="key_count" value="<?php echo $EnquiryCallAsset_grid->KeyCount ?>">
<?php echo $EnquiryCallAsset_grid->MultiSelectKey ?>
<?php } ?>
<?php if ($EnquiryCallAsset->CurrentMode == "") { ?>
<input type="hidden" name="a_list" id="a_list" value="">
<?php } ?>
<input type="hidden" name="detailpage" id="detailpage" value="fEnquiryCallAssetgrid">
</div>
<?php

// Close recordset
if ($EnquiryCallAsset_grid->Recordset)
	$EnquiryCallAsset_grid->Recordset->Close();
?>
<?php if (($EnquiryCallAsset->CurrentMode == "add" || $EnquiryCallAsset->CurrentMode == "copy" || $EnquiryCallAsset->CurrentMode == "edit") && $EnquiryCallAsset->CurrentAction != "F") { // add/copy/edit mode ?>
<div class="ewGridLowerPanel">
</div>
<?php } ?>
</div>
</td></tr></table>
<?php if ($EnquiryCallAsset->Export == "") { ?>
<script type="text/javascript">
fEnquiryCallAssetgrid.Init();
</script>
<?php } ?>
<?php
$EnquiryCallAsset_grid->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<?php
$EnquiryCallAsset_grid->Page_Terminate();
$Page = &$MasterPage;
?>
