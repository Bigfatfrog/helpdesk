<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg9.php" ?>
<?php include_once "ewmysql9.php" ?>
<?php include_once "phpfn9.php" ?>
<?php include_once "EnquiryCallAssetinfo.php" ?>
<?php include_once "staffinfo.php" ?>
<?php include_once "EnquiryMasterinfo.php" ?>
<?php include_once "userfn9.php" ?>
<?php

//
// Page class
//

$EnquiryCallAsset_list = NULL; // Initialize page object first

class cEnquiryCallAsset_list extends cEnquiryCallAsset {

	// Page ID
	var $PageID = 'list';

	// Project ID
	var $ProjectID = "{4F01D370-BA3A-4C91-9445-0AA347261657}";

	// Table name
	var $TableName = 'EnquiryCallAsset';

	// Page object name
	var $PageObjName = 'EnquiryCallAsset_list';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Page URLs
	var $AddUrl;
	var $EditUrl;
	var $CopyUrl;
	var $DeleteUrl;
	var $ViewUrl;
	var $ListUrl;

	// Export URLs
	var $ExportPrintUrl;
	var $ExportHtmlUrl;
	var $ExportExcelUrl;
	var $ExportWordUrl;
	var $ExportXmlUrl;
	var $ExportCsvUrl;
	var $ExportPdfUrl;

	// Update URLs
	var $InlineAddUrl;
	var $InlineCopyUrl;
	var $InlineEditUrl;
	var $GridAddUrl;
	var $GridEditUrl;
	var $MultiDeleteUrl;
	var $MultiUpdateUrl;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			$html .= "<p class=\"ewMessage\">" . $sMessage . "</p>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			$html .= "<table class=\"ewMessageTable\"><tr><td class=\"ewWarningIcon\"></td><td class=\"ewWarningMessage\">" . $sWarningMessage . "</td></tr></table>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			$html .= "<table class=\"ewMessageTable\"><tr><td class=\"ewSuccessIcon\"></td><td class=\"ewSuccessMessage\">" . $sSuccessMessage . "</td></tr></table>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			$html .= "<table class=\"ewMessageTable\"><tr><td class=\"ewErrorIcon\"></td><td class=\"ewErrorMessage\">" . $sErrorMessage . "</td></tr></table>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p class=\"phpmaker\">" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Fotoer exists, display
			echo "<p class=\"phpmaker\">" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language, $UserAgent;

		// User agent
		$UserAgent = ew_UserAgent();
		$GLOBALS["Page"] = &$this;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (EnquiryCallAsset)
		if (!isset($GLOBALS["EnquiryCallAsset"])) {
			$GLOBALS["EnquiryCallAsset"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["EnquiryCallAsset"];
		}

		// Initialize URLs
		$this->ExportPrintUrl = $this->PageUrl() . "export=print";
		$this->ExportExcelUrl = $this->PageUrl() . "export=excel";
		$this->ExportWordUrl = $this->PageUrl() . "export=word";
		$this->ExportHtmlUrl = $this->PageUrl() . "export=html";
		$this->ExportXmlUrl = $this->PageUrl() . "export=xml";
		$this->ExportCsvUrl = $this->PageUrl() . "export=csv";
		$this->ExportPdfUrl = $this->PageUrl() . "export=pdf";
		$this->AddUrl = "EnquiryCallAssetadd.php";
		$this->InlineAddUrl = $this->PageUrl() . "a=add";
		$this->GridAddUrl = $this->PageUrl() . "a=gridadd";
		$this->GridEditUrl = $this->PageUrl() . "a=gridedit";
		$this->MultiDeleteUrl = "EnquiryCallAssetdelete.php";
		$this->MultiUpdateUrl = "EnquiryCallAssetupdate.php";

		// Table object (staff)
		if (!isset($GLOBALS['staff'])) $GLOBALS['staff'] = new cstaff();

		// Table object (EnquiryMaster)
		if (!isset($GLOBALS['EnquiryMaster'])) $GLOBALS['EnquiryMaster'] = new cEnquiryMaster();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'list', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'EnquiryCallAsset', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect();

		// List options
		$this->ListOptions = new cListOptions();
		$this->ListOptions->TableVar = $this->TableVar;

		// Export options
		$this->ExportOptions = new cListOptions();
		$this->ExportOptions->Tag = "span";
		$this->ExportOptions->TagClassName = "ewExportOption";
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if (!$Security->IsLoggedIn()) {
			$Security->SaveLastUrl();
			$this->Page_Terminate("login.php");
		}

		// Get export parameters
		if (@$_GET["export"] <> "") {
			$this->Export = $_GET["export"];
		} elseif (ew_IsHttpPost()) {
			if (@$_POST["exporttype"] <> "")
				$this->Export = $_POST["exporttype"];
		} else {
			$this->setExportReturnUrl(ew_CurrentUrl());
		}
		$gsExport = $this->Export; // Get export parameter, used in header
		$gsExportFile = $this->TableVar; // Get export file, used in header

		// Get grid add count
		$gridaddcnt = @$_GET[EW_TABLE_GRID_ADD_ROW_COUNT];
		if (is_numeric($gridaddcnt) && $gridaddcnt > 0)
			$this->GridAddRowCount = $gridaddcnt;

		// Set up list options
		$this->SetupListOptions();

		// Setup export options
		$this->SetupExportOptions();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"];

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $conn;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();
		$this->Page_Redirecting($url);

		 // Close connection
		$conn->Close();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}

	// Class variables
	var $ListOptions; // List options
	var $ExportOptions; // Export options
	var $DisplayRecs = 20;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $Pager;
	var $SearchWhere = ""; // Search WHERE clause
	var $RecCnt = 0; // Record count
	var $EditRowCnt;
	var $StartRowCnt = 1;
	var $RowCnt = 0;
	var $Attrs = array(); // Row attributes and cell attributes
	var $RowIndex = 0; // Row index
	var $KeyCount = 0; // Key count
	var $RowAction = ""; // Row action
	var $RowOldKey = ""; // Row old key (for copy)
	var $RecPerRow = 0;
	var $ColCnt = 0;
	var $DbMasterFilter = ""; // Master filter
	var $DbDetailFilter = ""; // Detail filter
	var $MasterRecordExists;	
	var $MultiSelectKey;
	var $Command;
	var $Recordset;
	var $OldRecordset;

	//
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError, $gsSearchError, $Security;

		// Search filters
		$sSrchAdvanced = ""; // Advanced search filter
		$sSrchBasic = ""; // Basic search filter
		$sFilter = "";

		// Get command
		$this->Command = strtolower(@$_GET["cmd"]);
		if ($this->IsPageRequest()) { // Validate request

			// Handle reset command
			$this->ResetCmd();

			// Set up master detail parameters
			$this->SetUpMasterParms();

			// Hide all options
			if ($this->Export <> "" ||
				$this->CurrentAction == "gridadd" ||
				$this->CurrentAction == "gridedit") {
				$this->ListOptions->HideAllOptions();
				$this->ExportOptions->HideAllOptions();
			}

			// Set up sorting order
			$this->SetUpSortOrder();
		}

		// Restore display records
		if ($this->getRecordsPerPage() <> "") {
			$this->DisplayRecs = $this->getRecordsPerPage(); // Restore from Session
		} else {
			$this->DisplayRecs = 20; // Load default
		}

		// Load Sorting Order
		$this->LoadSortOrder();

		// Build filter
		$sFilter = "";

		// Restore master/detail filter
		$this->DbMasterFilter = $this->GetMasterFilter(); // Restore master filter
		$this->DbDetailFilter = $this->GetDetailFilter(); // Restore detail filter
		ew_AddFilter($sFilter, $this->DbDetailFilter);
		ew_AddFilter($sFilter, $this->SearchWhere);

		// Load master record
		if ($this->CurrentMode <> "add" && $this->GetMasterFilter() <> "" && $this->getCurrentMasterTable() == "EnquiryMaster") {
			global $EnquiryMaster;
			$rsmaster = $EnquiryMaster->LoadRs($this->DbMasterFilter);
			$this->MasterRecordExists = ($rsmaster && !$rsmaster->EOF);
			if (!$this->MasterRecordExists) {
				$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record found
				$this->Page_Terminate("EnquiryMasterlist.php"); // Return to master page
			} else {
				$EnquiryMaster->LoadListRowValues($rsmaster);
				$EnquiryMaster->RowType = EW_ROWTYPE_MASTER; // Master row
				$EnquiryMaster->RenderListRow();
				$rsmaster->Close();
			}
		}

		// Set up filter in session
		$this->setSessionWhere($sFilter);
		$this->CurrentFilter = "";
	}

	// Build filter for all keys
	function BuildKeyFilter() {
		global $objForm;
		$sWrkFilter = "";

		// Update row index and get row key
		$rowindex = 1;
		$objForm->Index = $rowindex;
		$sThisKey = strval($objForm->GetValue("k_key"));
		while ($sThisKey <> "") {
			if ($this->SetupKeyValues($sThisKey)) {
				$sFilter = $this->KeyFilter();
				if ($sWrkFilter <> "") $sWrkFilter .= " OR ";
				$sWrkFilter .= $sFilter;
			} else {
				$sWrkFilter = "0=1";
				break;
			}

			// Update row index and get row key
			$rowindex++; // next row
			$objForm->Index = $rowindex;
			$sThisKey = strval($objForm->GetValue("k_key"));
		}
		return $sWrkFilter;
	}

	// Set up key values
	function SetupKeyValues($key) {
		$arrKeyFlds = explode($GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"], $key);
		if (count($arrKeyFlds) >= 0) {
		}
		return TRUE;
	}

	// Set up sort parameters
	function SetUpSortOrder() {

		// Check for "order" parameter
		if (@$_GET["order"] <> "") {
			$this->CurrentOrder = ew_StripSlashes(@$_GET["order"]);
			$this->CurrentOrderType = @$_GET["ordertype"];
			$this->UpdateSort($this->assetSerialNumber); // assetSerialNumber
			$this->UpdateSort($this->assetDescription); // assetDescription
			$this->UpdateSort($this->vendorName); // vendorName
			$this->UpdateSort($this->vendorDetails); // vendorDetails
			$this->setStartRecordNumber(1); // Reset start position
		}
	}

	// Load sort order parameters
	function LoadSortOrder() {
		$sOrderBy = $this->getSessionOrderBy(); // Get ORDER BY from Session
		if ($sOrderBy == "") {
			if ($this->SqlOrderBy() <> "") {
				$sOrderBy = $this->SqlOrderBy();
				$this->setSessionOrderBy($sOrderBy);
			}
		}
	}

	// Reset command
	// cmd=reset (Reset search parameters)
	// cmd=resetall (Reset search and master/detail parameters)
	// cmd=resetsort (Reset sort parameters)
	function ResetCmd() {

		// Check if reset command
		if (substr($this->Command,0,5) == "reset") {

			// Reset master/detail keys
			if ($this->Command == "resetall") {
				$this->setCurrentMasterTable(""); // Clear master table
				$this->DbMasterFilter = "";
				$this->DbDetailFilter = "";
				$this->callId->setSessionValue("");
			}

			// Reset sorting order
			if ($this->Command == "resetsort") {
				$sOrderBy = "";
				$this->setSessionOrderBy($sOrderBy);
				$this->assetSerialNumber->setSort("");
				$this->assetDescription->setSort("");
				$this->vendorName->setSort("");
				$this->vendorDetails->setSort("");
			}

			// Reset start position
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Set up list options
	function SetupListOptions() {
		global $Security, $Language;

		// Call ListOptions_Load event
		$this->ListOptions_Load();
	}

	// Render list options
	function RenderListOptions() {
		global $Security, $Language, $objForm;
		$this->ListOptions->LoadDefault();
		$this->RenderListOptionsExt();

		// Call ListOptions_Rendered event
		$this->ListOptions_Rendered();
	}

	function RenderListOptionsExt() {
		global $Security, $Language;
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {
		global $conn;

		// Call Recordset Selecting event
		$this->Recordset_Selecting($this->CurrentFilter);

		// Load List page SQL
		$sSql = $this->SelectSQL();
		if ($offset > -1 && $rowcnt > -1)
			$sSql .= " LIMIT $rowcnt OFFSET $offset";

		// Load recordset
		$rs = ew_LoadRecordset($sSql);

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $conn, $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		global $conn;
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->callId->setDbValue($rs->fields('callId'));
		$this->assetSerialNumber->setDbValue($rs->fields('assetSerialNumber'));
		$this->assetDescription->setDbValue($rs->fields('assetDescription'));
		$this->vendorName->setDbValue($rs->fields('vendorName'));
		$this->vendorDetails->setDbValue($rs->fields('vendorDetails'));
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$this->OldRecordset = ew_LoadRecordset($sSql);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $conn, $Security, $Language;
		global $gsLanguage;

		// Initialize URLs
		$this->ViewUrl = $this->GetViewUrl();
		$this->EditUrl = $this->GetEditUrl();
		$this->InlineEditUrl = $this->GetInlineEditUrl();
		$this->CopyUrl = $this->GetCopyUrl();
		$this->InlineCopyUrl = $this->GetInlineCopyUrl();
		$this->DeleteUrl = $this->GetDeleteUrl();

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// callId

		$this->callId->CellCssStyle = "white-space: nowrap;";

		// assetSerialNumber
		// assetDescription
		// vendorName
		// vendorDetails

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

			// assetSerialNumber
			if (strval($this->assetSerialNumber->CurrentValue) <> "") {
				$sFilterWrk = "`serialNumber`" . ew_SearchString("=", $this->assetSerialNumber->CurrentValue, EW_DATATYPE_NUMBER);
			$sSqlWrk = "SELECT `serialNumber`, `serialNumber` AS `DispFld`, `assetDescription` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `asset`";
			$sWhereWrk = "";
			if ($sFilterWrk <> "") {
				ew_AddFilter($sWhereWrk, $sFilterWrk);
			}
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
				$rswrk = $conn->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$this->assetSerialNumber->ViewValue = $rswrk->fields('DispFld');
					$this->assetSerialNumber->ViewValue .= ew_ValueSeparator(1,$this->assetSerialNumber) . $rswrk->fields('Disp2Fld');
					$rswrk->Close();
				} else {
					$this->assetSerialNumber->ViewValue = $this->assetSerialNumber->CurrentValue;
				}
			} else {
				$this->assetSerialNumber->ViewValue = NULL;
			}
			$this->assetSerialNumber->ViewCustomAttributes = "";

			// assetDescription
			$this->assetDescription->ViewValue = $this->assetDescription->CurrentValue;
			$this->assetDescription->ViewCustomAttributes = "";

			// vendorName
			$this->vendorName->ViewValue = $this->vendorName->CurrentValue;
			$this->vendorName->ViewCustomAttributes = "";

			// vendorDetails
			$this->vendorDetails->ViewValue = $this->vendorDetails->CurrentValue;
			$this->vendorDetails->ViewCustomAttributes = "";

			// assetSerialNumber
			$this->assetSerialNumber->LinkCustomAttributes = "";
			$this->assetSerialNumber->HrefValue = "";
			$this->assetSerialNumber->TooltipValue = "";

			// assetDescription
			$this->assetDescription->LinkCustomAttributes = "";
			$this->assetDescription->HrefValue = "";
			$this->assetDescription->TooltipValue = "";

			// vendorName
			$this->vendorName->LinkCustomAttributes = "";
			$this->vendorName->HrefValue = "";
			$this->vendorName->TooltipValue = "";

			// vendorDetails
			$this->vendorDetails->LinkCustomAttributes = "";
			$this->vendorDetails->HrefValue = "";
			$this->vendorDetails->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Set up export options
	function SetupExportOptions() {
		global $Language;

		// Printer friendly
		$item = &$this->ExportOptions->Add("print");
		$item->Body = "<a href=\"" . $this->ExportPrintUrl . "\">" . $Language->Phrase("PrinterFriendly") . "</a>";
		$item->Visible = FALSE;

		// Export to Excel
		$item = &$this->ExportOptions->Add("excel");
		$item->Body = "<a href=\"" . $this->ExportExcelUrl . "\">" . $Language->Phrase("ExportToExcel") . "</a>";
		$item->Visible = FALSE;

		// Export to Word
		$item = &$this->ExportOptions->Add("word");
		$item->Body = "<a href=\"" . $this->ExportWordUrl . "\">" . $Language->Phrase("ExportToWord") . "</a>";
		$item->Visible = FALSE;

		// Export to Html
		$item = &$this->ExportOptions->Add("html");
		$item->Body = "<a href=\"" . $this->ExportHtmlUrl . "\">" . $Language->Phrase("ExportToHtml") . "</a>";
		$item->Visible = FALSE;

		// Export to Xml
		$item = &$this->ExportOptions->Add("xml");
		$item->Body = "<a href=\"" . $this->ExportXmlUrl . "\">" . $Language->Phrase("ExportToXml") . "</a>";
		$item->Visible = FALSE;

		// Export to Csv
		$item = &$this->ExportOptions->Add("csv");
		$item->Body = "<a href=\"" . $this->ExportCsvUrl . "\">" . $Language->Phrase("ExportToCsv") . "</a>";
		$item->Visible = FALSE;

		// Export to Pdf
		$item = &$this->ExportOptions->Add("pdf");
		$item->Body = "<a href=\"" . $this->ExportPdfUrl . "\">" . $Language->Phrase("ExportToPDF") . "</a>";
		$item->Visible = FALSE;

		// Export to Email
		$item = &$this->ExportOptions->Add("email");
		$item->Body = "<a id=\"emf_EnquiryCallAsset\" href=\"javascript:void(0);\" onclick=\"ew_EmailDialogShow({lnk:'emf_EnquiryCallAsset',hdr:ewLanguage.Phrase('ExportToEmail'),f:document.fEnquiryCallAssetlist,sel:false});\">" . $Language->Phrase("ExportToEmail") . "</a>";
		$item->Visible = FALSE;

		// Hide options for export/action
		if ($this->Export <> "" || $this->CurrentAction <> "")
			$this->ExportOptions->HideAllOptions();
	}

	// Set up master/detail based on QueryString
	function SetUpMasterParms() {
		$bValidMaster = FALSE;

		// Get the keys for master table
		if (isset($_GET[EW_TABLE_SHOW_MASTER])) {
			$sMasterTblVar = $_GET[EW_TABLE_SHOW_MASTER];
			if ($sMasterTblVar == "") {
				$bValidMaster = TRUE;
				$this->DbMasterFilter = "";
				$this->DbDetailFilter = "";
			}
			if ($sMasterTblVar == "EnquiryMaster") {
				$bValidMaster = TRUE;
				if (@$_GET["CallId"] <> "") {
					$GLOBALS["EnquiryMaster"]->CallId->setQueryStringValue($_GET["CallId"]);
					$this->callId->setQueryStringValue($GLOBALS["EnquiryMaster"]->CallId->QueryStringValue);
					$this->callId->setSessionValue($this->callId->QueryStringValue);
					if (!is_numeric($GLOBALS["EnquiryMaster"]->CallId->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
		}
		if ($bValidMaster) {

			// Save current master table
			$this->setCurrentMasterTable($sMasterTblVar);

			// Reset start record counter (new master key)
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);

			// Clear previous master key from Session
			if ($sMasterTblVar <> "EnquiryMaster") {
				if ($this->callId->QueryStringValue == "") $this->callId->setSessionValue("");
			}
		}
		$this->DbMasterFilter = $this->GetMasterFilter(); //  Get master filter
		$this->DbDetailFilter = $this->GetDetailFilter(); // Get detail filter
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		$query = $this->DbDetailFilter ;  

		//var_dump('http://localhost/helpdesk/EnquirycallContactlist.php?showmaster=EnquiryMaster&' . $query);
	   // $footer = '<iframe scrolling="no" style="width:100%;height:500px;border-style:none;" src="http://localhost/helpdesk/EnquirycallContactlist.php?showmaster=EnquiryMaster&' . $query .'"></iframe>';  

		$footer = '<iframe scrolling="no" style="width:100%;height:500px;border-style:none;" src="http://www.helpdesk.bigfatfrog.co.uk/EnquirycallContactlist.php?showmaster=EnquiryMaster&' . $query .'"></iframe>';  
	}                                                                 

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}

	// ListOptions Load event
	function ListOptions_Load() {

		// Example:
		//$opt = &$this->ListOptions->Add("new");
		//$opt->Header = "xxx";
		//$opt->OnLeft = TRUE; // Link on left
		//$opt->MoveTo(0); // Move to first column

	}

	// ListOptions Rendered event
	function ListOptions_Rendered() {

		// Example: 
		//$this->ListOptions->Items["new"]->Body = "xxx";

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($EnquiryCallAsset_list)) $EnquiryCallAsset_list = new cEnquiryCallAsset_list();

// Page init
$EnquiryCallAsset_list->Page_Init();

// Page main
$EnquiryCallAsset_list->Page_Main();
?>
<?php include_once "header.php" ?>
<?php if ($EnquiryCallAsset->Export == "") { ?>
<script type="text/javascript">

// Page object
var EnquiryCallAsset_list = new ew_Page("EnquiryCallAsset_list");
EnquiryCallAsset_list.PageID = "list"; // Page ID
var EW_PAGE_ID = EnquiryCallAsset_list.PageID; // For backward compatibility

// Form object
var fEnquiryCallAssetlist = new ew_Form("fEnquiryCallAssetlist");

// Form_CustomValidate event
fEnquiryCallAssetlist.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fEnquiryCallAssetlist.ValidateRequired = true;
<?php } else { ?>
fEnquiryCallAssetlist.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fEnquiryCallAssetlist.Lists["x_assetSerialNumber"] = {"LinkField":"x_serialNumber","Ajax":null,"AutoFill":false,"DisplayFields":["x_serialNumber","x_assetDescription","",""],"ParentFields":[],"FilterFields":[],"Options":[]};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php } ?>
<?php if (($EnquiryCallAsset->Export == "") || (EW_EXPORT_MASTER_RECORD && $EnquiryCallAsset->Export == "print")) { ?>
<?php
$gsMasterReturnUrl = "EnquiryMasterlist.php";
if ($EnquiryCallAsset_list->DbMasterFilter <> "" && $EnquiryCallAsset->getCurrentMasterTable() == "EnquiryMaster") {
	if ($EnquiryCallAsset_list->MasterRecordExists) {
		if ($EnquiryCallAsset->getCurrentMasterTable() == $EnquiryCallAsset->TableVar) $gsMasterReturnUrl .= "?" . EW_TABLE_SHOW_MASTER . "=";
?>
<p><span class="ewTitle ewMasterTableTitle"><?php echo $Language->Phrase("MasterRecord") ?><?php echo $EnquiryMaster->TableCaption() ?>&nbsp;&nbsp;</span><?php $EnquiryCallAsset_list->ExportOptions->Render("body"); ?>
</p>
<?php if ($EnquiryCallAsset->Export == "") { ?>
<p class="phpmaker"><a href="<?php echo $gsMasterReturnUrl ?>"><?php echo $Language->Phrase("BackToMasterRecordPage") ?></a></p>
<?php } ?>
<?php include_once "EnquiryMastermaster.php" ?>
<?php
	}
}
?>
<?php } ?>
<?php
	$bSelectLimit = EW_SELECT_LIMIT;
	if ($bSelectLimit) {
		$EnquiryCallAsset_list->TotalRecs = $EnquiryCallAsset->SelectRecordCount();
	} else {
		if ($EnquiryCallAsset_list->Recordset = $EnquiryCallAsset_list->LoadRecordset())
			$EnquiryCallAsset_list->TotalRecs = $EnquiryCallAsset_list->Recordset->RecordCount();
	}
	$EnquiryCallAsset_list->StartRec = 1;
	if ($EnquiryCallAsset_list->DisplayRecs <= 0 || ($EnquiryCallAsset->Export <> "" && $EnquiryCallAsset->ExportAll)) // Display all records
		$EnquiryCallAsset_list->DisplayRecs = $EnquiryCallAsset_list->TotalRecs;
	if (!($EnquiryCallAsset->Export <> "" && $EnquiryCallAsset->ExportAll))
		$EnquiryCallAsset_list->SetUpStartRec(); // Set up start record position
	if ($bSelectLimit)
		$EnquiryCallAsset_list->Recordset = $EnquiryCallAsset_list->LoadRecordset($EnquiryCallAsset_list->StartRec-1, $EnquiryCallAsset_list->DisplayRecs);
?>
<p style="white-space: nowrap;"><span id="ewPageCaption" class="ewTitle ewTableTitle"><?php echo $Language->Phrase("TblTypeVIEW") ?><?php echo $EnquiryCallAsset->TableCaption() ?>&nbsp;&nbsp;</span>
<?php if ($EnquiryCallAsset->getCurrentMasterTable() == "") { ?>
<?php $EnquiryCallAsset_list->ExportOptions->Render("body"); ?>
<?php } ?>
</p>
<?php $EnquiryCallAsset_list->ShowPageHeader(); ?>
<?php
$EnquiryCallAsset_list->ShowMessage();
?>
<br>
<table cellspacing="0" class="ewGrid"><tr><td class="ewGridContent">
<form name="fEnquiryCallAssetlist" id="fEnquiryCallAssetlist" class="ewForm" action="" method="post">
<input type="hidden" name="t" value="EnquiryCallAsset">
<div id="gmp_EnquiryCallAsset" class="ewGridMiddlePanel">
<?php if ($EnquiryCallAsset_list->TotalRecs > 0) { ?>
<table id="tbl_EnquiryCallAssetlist" class="ewTable ewTableSeparate">
<?php echo $EnquiryCallAsset->TableCustomInnerHtml ?>
<thead><!-- Table header -->
	<tr class="ewTableHeader">
<?php

// Render list options
$EnquiryCallAsset_list->RenderListOptions();

// Render list options (header, left)
$EnquiryCallAsset_list->ListOptions->Render("header", "left");
?>
<?php if ($EnquiryCallAsset->assetSerialNumber->Visible) { // assetSerialNumber ?>
	<?php if ($EnquiryCallAsset->SortUrl($EnquiryCallAsset->assetSerialNumber) == "") { ?>
		<td><span id="elh_EnquiryCallAsset_assetSerialNumber" class="EnquiryCallAsset_assetSerialNumber"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $EnquiryCallAsset->assetSerialNumber->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div onmousedown="ew_Sort(event,'<?php echo $EnquiryCallAsset->SortUrl($EnquiryCallAsset->assetSerialNumber) ?>',1);"><span id="elh_EnquiryCallAsset_assetSerialNumber" class="EnquiryCallAsset_assetSerialNumber">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $EnquiryCallAsset->assetSerialNumber->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($EnquiryCallAsset->assetSerialNumber->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($EnquiryCallAsset->assetSerialNumber->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($EnquiryCallAsset->assetDescription->Visible) { // assetDescription ?>
	<?php if ($EnquiryCallAsset->SortUrl($EnquiryCallAsset->assetDescription) == "") { ?>
		<td><span id="elh_EnquiryCallAsset_assetDescription" class="EnquiryCallAsset_assetDescription"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $EnquiryCallAsset->assetDescription->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div onmousedown="ew_Sort(event,'<?php echo $EnquiryCallAsset->SortUrl($EnquiryCallAsset->assetDescription) ?>',1);"><span id="elh_EnquiryCallAsset_assetDescription" class="EnquiryCallAsset_assetDescription">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $EnquiryCallAsset->assetDescription->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($EnquiryCallAsset->assetDescription->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($EnquiryCallAsset->assetDescription->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($EnquiryCallAsset->vendorName->Visible) { // vendorName ?>
	<?php if ($EnquiryCallAsset->SortUrl($EnquiryCallAsset->vendorName) == "") { ?>
		<td><span id="elh_EnquiryCallAsset_vendorName" class="EnquiryCallAsset_vendorName"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $EnquiryCallAsset->vendorName->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div onmousedown="ew_Sort(event,'<?php echo $EnquiryCallAsset->SortUrl($EnquiryCallAsset->vendorName) ?>',1);"><span id="elh_EnquiryCallAsset_vendorName" class="EnquiryCallAsset_vendorName">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $EnquiryCallAsset->vendorName->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($EnquiryCallAsset->vendorName->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($EnquiryCallAsset->vendorName->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($EnquiryCallAsset->vendorDetails->Visible) { // vendorDetails ?>
	<?php if ($EnquiryCallAsset->SortUrl($EnquiryCallAsset->vendorDetails) == "") { ?>
		<td><span id="elh_EnquiryCallAsset_vendorDetails" class="EnquiryCallAsset_vendorDetails"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $EnquiryCallAsset->vendorDetails->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div onmousedown="ew_Sort(event,'<?php echo $EnquiryCallAsset->SortUrl($EnquiryCallAsset->vendorDetails) ?>',1);"><span id="elh_EnquiryCallAsset_vendorDetails" class="EnquiryCallAsset_vendorDetails">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $EnquiryCallAsset->vendorDetails->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($EnquiryCallAsset->vendorDetails->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($EnquiryCallAsset->vendorDetails->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php

// Render list options (header, right)
$EnquiryCallAsset_list->ListOptions->Render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
if ($EnquiryCallAsset->ExportAll && $EnquiryCallAsset->Export <> "") {
	$EnquiryCallAsset_list->StopRec = $EnquiryCallAsset_list->TotalRecs;
} else {

	// Set the last record to display
	if ($EnquiryCallAsset_list->TotalRecs > $EnquiryCallAsset_list->StartRec + $EnquiryCallAsset_list->DisplayRecs - 1)
		$EnquiryCallAsset_list->StopRec = $EnquiryCallAsset_list->StartRec + $EnquiryCallAsset_list->DisplayRecs - 1;
	else
		$EnquiryCallAsset_list->StopRec = $EnquiryCallAsset_list->TotalRecs;
}
$EnquiryCallAsset_list->RecCnt = $EnquiryCallAsset_list->StartRec - 1;
if ($EnquiryCallAsset_list->Recordset && !$EnquiryCallAsset_list->Recordset->EOF) {
	$EnquiryCallAsset_list->Recordset->MoveFirst();
	if (!$bSelectLimit && $EnquiryCallAsset_list->StartRec > 1)
		$EnquiryCallAsset_list->Recordset->Move($EnquiryCallAsset_list->StartRec - 1);
} elseif (!$EnquiryCallAsset->AllowAddDeleteRow && $EnquiryCallAsset_list->StopRec == 0) {
	$EnquiryCallAsset_list->StopRec = $EnquiryCallAsset->GridAddRowCount;
}

// Initialize aggregate
$EnquiryCallAsset->RowType = EW_ROWTYPE_AGGREGATEINIT;
$EnquiryCallAsset->ResetAttrs();
$EnquiryCallAsset_list->RenderRow();
while ($EnquiryCallAsset_list->RecCnt < $EnquiryCallAsset_list->StopRec) {
	$EnquiryCallAsset_list->RecCnt++;
	if (intval($EnquiryCallAsset_list->RecCnt) >= intval($EnquiryCallAsset_list->StartRec)) {
		$EnquiryCallAsset_list->RowCnt++;

		// Set up key count
		$EnquiryCallAsset_list->KeyCount = $EnquiryCallAsset_list->RowIndex;

		// Init row class and style
		$EnquiryCallAsset->ResetAttrs();
		$EnquiryCallAsset->CssClass = "";
		if ($EnquiryCallAsset->CurrentAction == "gridadd") {
		} else {
			$EnquiryCallAsset_list->LoadRowValues($EnquiryCallAsset_list->Recordset); // Load row values
		}
		$EnquiryCallAsset->RowType = EW_ROWTYPE_VIEW; // Render view

		// Set up row id / data-rowindex
		$EnquiryCallAsset->RowAttrs = array_merge($EnquiryCallAsset->RowAttrs, array('data-rowindex'=>$EnquiryCallAsset_list->RowCnt, 'id'=>'r' . $EnquiryCallAsset_list->RowCnt . '_EnquiryCallAsset', 'data-rowtype'=>$EnquiryCallAsset->RowType));

		// Render row
		$EnquiryCallAsset_list->RenderRow();

		// Render list options
		$EnquiryCallAsset_list->RenderListOptions();
?>
	<tr<?php echo $EnquiryCallAsset->RowAttributes() ?>>
<?php

// Render list options (body, left)
$EnquiryCallAsset_list->ListOptions->Render("body", "left", $EnquiryCallAsset_list->RowCnt);
?>
	<?php if ($EnquiryCallAsset->assetSerialNumber->Visible) { // assetSerialNumber ?>
		<td<?php echo $EnquiryCallAsset->assetSerialNumber->CellAttributes() ?>><span id="el<?php echo $EnquiryCallAsset_list->RowCnt ?>_EnquiryCallAsset_assetSerialNumber" class="EnquiryCallAsset_assetSerialNumber">
<span<?php echo $EnquiryCallAsset->assetSerialNumber->ViewAttributes() ?>>
<?php echo $EnquiryCallAsset->assetSerialNumber->ListViewValue() ?></span>
</span><a id="<?php echo $EnquiryCallAsset_list->PageObjName . "_row_" . $EnquiryCallAsset_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($EnquiryCallAsset->assetDescription->Visible) { // assetDescription ?>
		<td<?php echo $EnquiryCallAsset->assetDescription->CellAttributes() ?>><span id="el<?php echo $EnquiryCallAsset_list->RowCnt ?>_EnquiryCallAsset_assetDescription" class="EnquiryCallAsset_assetDescription">
<span<?php echo $EnquiryCallAsset->assetDescription->ViewAttributes() ?>>
<?php echo $EnquiryCallAsset->assetDescription->ListViewValue() ?></span>
</span><a id="<?php echo $EnquiryCallAsset_list->PageObjName . "_row_" . $EnquiryCallAsset_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($EnquiryCallAsset->vendorName->Visible) { // vendorName ?>
		<td<?php echo $EnquiryCallAsset->vendorName->CellAttributes() ?>><span id="el<?php echo $EnquiryCallAsset_list->RowCnt ?>_EnquiryCallAsset_vendorName" class="EnquiryCallAsset_vendorName">
<span<?php echo $EnquiryCallAsset->vendorName->ViewAttributes() ?>>
<?php echo $EnquiryCallAsset->vendorName->ListViewValue() ?></span>
</span><a id="<?php echo $EnquiryCallAsset_list->PageObjName . "_row_" . $EnquiryCallAsset_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($EnquiryCallAsset->vendorDetails->Visible) { // vendorDetails ?>
		<td<?php echo $EnquiryCallAsset->vendorDetails->CellAttributes() ?>><span id="el<?php echo $EnquiryCallAsset_list->RowCnt ?>_EnquiryCallAsset_vendorDetails" class="EnquiryCallAsset_vendorDetails">
<span<?php echo $EnquiryCallAsset->vendorDetails->ViewAttributes() ?>>
<?php echo $EnquiryCallAsset->vendorDetails->ListViewValue() ?></span>
</span><a id="<?php echo $EnquiryCallAsset_list->PageObjName . "_row_" . $EnquiryCallAsset_list->RowCnt ?>"></a></td>
	<?php } ?>
<?php

// Render list options (body, right)
$EnquiryCallAsset_list->ListOptions->Render("body", "right", $EnquiryCallAsset_list->RowCnt);
?>
	</tr>
<?php
	}
	if ($EnquiryCallAsset->CurrentAction <> "gridadd")
		$EnquiryCallAsset_list->Recordset->MoveNext();
}
?>
</tbody>
</table>
<?php } ?>
<?php if ($EnquiryCallAsset->CurrentAction == "") { ?>
<input type="hidden" name="a_list" id="a_list" value="">
<?php } ?>
</div>
</form>
<?php

// Close recordset
if ($EnquiryCallAsset_list->Recordset)
	$EnquiryCallAsset_list->Recordset->Close();
?>
<?php if ($EnquiryCallAsset->Export == "") { ?>
<div class="ewGridLowerPanel">
<?php if ($EnquiryCallAsset->CurrentAction <> "gridadd" && $EnquiryCallAsset->CurrentAction <> "gridedit") { ?>
<form name="ewpagerform" id="ewpagerform" class="ewForm" action="<?php echo ew_CurrentPage() ?>">
<table class="ewPager"><tr><td>
<?php if (!isset($EnquiryCallAsset_list->Pager)) $EnquiryCallAsset_list->Pager = new cPrevNextPager($EnquiryCallAsset_list->StartRec, $EnquiryCallAsset_list->DisplayRecs, $EnquiryCallAsset_list->TotalRecs) ?>
<?php if ($EnquiryCallAsset_list->Pager->RecordCount > 0) { ?>
	<table cellspacing="0" class="ewStdTable"><tbody><tr><td><span class="phpmaker"><?php echo $Language->Phrase("Page") ?>&nbsp;</span></td>
<!--first page button-->
	<?php if ($EnquiryCallAsset_list->Pager->FirstButton->Enabled) { ?>
	<td><a href="<?php echo $EnquiryCallAsset_list->PageUrl() ?>start=<?php echo $EnquiryCallAsset_list->Pager->FirstButton->Start ?>"><img src="phpimages/first.gif" alt="<?php echo $Language->Phrase("PagerFirst") ?>" width="16" height="16" style="border: 0;"></a></td>
	<?php } else { ?>
	<td><img src="phpimages/firstdisab.gif" alt="<?php echo $Language->Phrase("PagerFirst") ?>" width="16" height="16" style="border: 0;"></td>
	<?php } ?>
<!--previous page button-->
	<?php if ($EnquiryCallAsset_list->Pager->PrevButton->Enabled) { ?>
	<td><a href="<?php echo $EnquiryCallAsset_list->PageUrl() ?>start=<?php echo $EnquiryCallAsset_list->Pager->PrevButton->Start ?>"><img src="phpimages/prev.gif" alt="<?php echo $Language->Phrase("PagerPrevious") ?>" width="16" height="16" style="border: 0;"></a></td>
	<?php } else { ?>
	<td><img src="phpimages/prevdisab.gif" alt="<?php echo $Language->Phrase("PagerPrevious") ?>" width="16" height="16" style="border: 0;"></td>
	<?php } ?>
<!--current page number-->
	<td><input type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" id="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $EnquiryCallAsset_list->Pager->CurrentPage ?>" size="4"></td>
<!--next page button-->
	<?php if ($EnquiryCallAsset_list->Pager->NextButton->Enabled) { ?>
	<td><a href="<?php echo $EnquiryCallAsset_list->PageUrl() ?>start=<?php echo $EnquiryCallAsset_list->Pager->NextButton->Start ?>"><img src="phpimages/next.gif" alt="<?php echo $Language->Phrase("PagerNext") ?>" width="16" height="16" style="border: 0;"></a></td>	
	<?php } else { ?>
	<td><img src="phpimages/nextdisab.gif" alt="<?php echo $Language->Phrase("PagerNext") ?>" width="16" height="16" style="border: 0;"></td>
	<?php } ?>
<!--last page button-->
	<?php if ($EnquiryCallAsset_list->Pager->LastButton->Enabled) { ?>
	<td><a href="<?php echo $EnquiryCallAsset_list->PageUrl() ?>start=<?php echo $EnquiryCallAsset_list->Pager->LastButton->Start ?>"><img src="phpimages/last.gif" alt="<?php echo $Language->Phrase("PagerLast") ?>" width="16" height="16" style="border: 0;"></a></td>	
	<?php } else { ?>
	<td><img src="phpimages/lastdisab.gif" alt="<?php echo $Language->Phrase("PagerLast") ?>" width="16" height="16" style="border: 0;"></td>
	<?php } ?>
	<td><span class="phpmaker">&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $EnquiryCallAsset_list->Pager->PageCount ?></span></td>
	</tr></tbody></table>
	</td>	
	<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td>
	<span class="phpmaker"><?php echo $Language->Phrase("Record") ?>&nbsp;<?php echo $EnquiryCallAsset_list->Pager->FromIndex ?>&nbsp;<?php echo $Language->Phrase("To") ?>&nbsp;<?php echo $EnquiryCallAsset_list->Pager->ToIndex ?>&nbsp;<?php echo $Language->Phrase("Of") ?>&nbsp;<?php echo $EnquiryCallAsset_list->Pager->RecordCount ?></span>
<?php } else { ?>
	<?php if ($EnquiryCallAsset_list->SearchWhere == "0=101") { ?>
	<span class="phpmaker"><?php echo $Language->Phrase("EnterSearchCriteria") ?></span>
	<?php } else { ?>
	<span class="phpmaker"><?php echo $Language->Phrase("NoRecord") ?></span>
	<?php } ?>
<?php } ?>
	</td>
</tr></table>
</form>
<?php } ?>
<span class="phpmaker">
</span>
</div>
<?php } ?>
</td></tr></table>
<?php if ($EnquiryCallAsset->Export == "") { ?>
<script type="text/javascript">
fEnquiryCallAssetlist.Init();
</script>
<?php } ?>
<?php
$EnquiryCallAsset_list->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<?php if ($EnquiryCallAsset->Export == "") { ?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php } ?>
<?php include_once "footer.php" ?>
<?php
$EnquiryCallAsset_list->Page_Terminate();
?>
