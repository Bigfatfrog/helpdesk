<?php include_once "staffinfo.php" ?>
<?php

// Create page object
if (!isset($EnquiryDetails_grid)) $EnquiryDetails_grid = new cEnquiryDetails_grid();

// Page init
$EnquiryDetails_grid->Page_Init();

// Page main
$EnquiryDetails_grid->Page_Main();
?>
<?php if ($EnquiryDetails->Export == "") { ?>
<script type="text/javascript">

// Page object
var EnquiryDetails_grid = new ew_Page("EnquiryDetails_grid");
EnquiryDetails_grid.PageID = "grid"; // Page ID
var EW_PAGE_ID = EnquiryDetails_grid.PageID; // For backward compatibility

// Form object
var fEnquiryDetailsgrid = new ew_Form("fEnquiryDetailsgrid");

// Validate form
fEnquiryDetailsgrid.Validate = function(fobj) {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	fobj = fobj || this.Form;
	this.PostAutoSuggest();	
	if (fobj.a_confirm && fobj.a_confirm.value == "F")
		return true;
	var elm, aelm;
	var rowcnt = (fobj.key_count) ? Number(fobj.key_count.value) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // rowcnt == 0 => Inline-Add
	var addcnt = 0;
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = (fobj.key_count) ? String(i) : "";
		var checkrow = (fobj.a_list && fobj.a_list.value == "gridinsert") ? !this.EmptyRow(infix) : true;
		if (checkrow) {
			addcnt++;
		elm = fobj.elements["x" + infix + "_contactDescription"];
		if (elm && !ew_HasValue(elm))
			return ew_OnError(this, elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($EnquiryDetails->contactDescription->FldCaption()) ?>");
		elm = fobj.elements["x" + infix + "_contactStaffName"];
		if (elm && !ew_HasValue(elm))
			return ew_OnError(this, elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($EnquiryDetails->contactStaffName->FldCaption()) ?>");
		elm = fobj.elements["x" + infix + "_roleDescription"];
		if (elm && !ew_HasValue(elm))
			return ew_OnError(this, elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($EnquiryDetails->roleDescription->FldCaption()) ?>");
		elm = fobj.elements["x" + infix + "_assetSerialNumber"];
		if (elm && !ew_HasValue(elm))
			return ew_OnError(this, elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($EnquiryDetails->assetSerialNumber->FldCaption()) ?>");
		elm = fobj.elements["x" + infix + "_vendorName"];
		if (elm && !ew_HasValue(elm))
			return ew_OnError(this, elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($EnquiryDetails->vendorName->FldCaption()) ?>");

		// Set up row object
		ew_ElementsToRow(fobj, infix);

		// Fire Form_CustomValidate event
		if (!this.Form_CustomValidate(fobj))
			return false;
		} // End Grid Add checking
	}
	return true;
}

// Check empty row
fEnquiryDetailsgrid.EmptyRow = function(infix) {
	var fobj = this.Form;
	if (ew_ValueChanged(fobj, infix, "contactDescription", false)) return false;
	if (ew_ValueChanged(fobj, infix, "contactStaffName", false)) return false;
	if (ew_ValueChanged(fobj, infix, "roleDescription", false)) return false;
	if (ew_ValueChanged(fobj, infix, "assetSerialNumber", false)) return false;
	if (ew_ValueChanged(fobj, infix, "assetDescription", false)) return false;
	if (ew_ValueChanged(fobj, infix, "vendorName", false)) return false;
	if (ew_ValueChanged(fobj, infix, "vendorDetails", false)) return false;
	return true;
}

// Form_CustomValidate event
fEnquiryDetailsgrid.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fEnquiryDetailsgrid.ValidateRequired = true;
<?php } else { ?>
fEnquiryDetailsgrid.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fEnquiryDetailsgrid.Lists["x_assetSerialNumber"] = {"LinkField":"x_serialNumber","Ajax":null,"AutoFill":false,"DisplayFields":["x_serialNumber","x_assetDescription","",""],"ParentFields":[],"FilterFields":[],"Options":[]};

// Form object for search
</script>
<?php } ?>
<?php
if ($EnquiryDetails->CurrentAction == "gridadd") {
	if ($EnquiryDetails->CurrentMode == "copy") {
		$bSelectLimit = EW_SELECT_LIMIT;
		if ($bSelectLimit) {
			$EnquiryDetails_grid->TotalRecs = $EnquiryDetails->SelectRecordCount();
			$EnquiryDetails_grid->Recordset = $EnquiryDetails_grid->LoadRecordset($EnquiryDetails_grid->StartRec-1, $EnquiryDetails_grid->DisplayRecs);
		} else {
			if ($EnquiryDetails_grid->Recordset = $EnquiryDetails_grid->LoadRecordset())
				$EnquiryDetails_grid->TotalRecs = $EnquiryDetails_grid->Recordset->RecordCount();
		}
		$EnquiryDetails_grid->StartRec = 1;
		$EnquiryDetails_grid->DisplayRecs = $EnquiryDetails_grid->TotalRecs;
	} else {
		$EnquiryDetails->CurrentFilter = "0=1";
		$EnquiryDetails_grid->StartRec = 1;
		$EnquiryDetails_grid->DisplayRecs = $EnquiryDetails->GridAddRowCount;
	}
	$EnquiryDetails_grid->TotalRecs = $EnquiryDetails_grid->DisplayRecs;
	$EnquiryDetails_grid->StopRec = $EnquiryDetails_grid->DisplayRecs;
} else {
	$bSelectLimit = EW_SELECT_LIMIT;
	if ($bSelectLimit) {
		$EnquiryDetails_grid->TotalRecs = $EnquiryDetails->SelectRecordCount();
	} else {
		if ($EnquiryDetails_grid->Recordset = $EnquiryDetails_grid->LoadRecordset())
			$EnquiryDetails_grid->TotalRecs = $EnquiryDetails_grid->Recordset->RecordCount();
	}
	$EnquiryDetails_grid->StartRec = 1;
	$EnquiryDetails_grid->DisplayRecs = $EnquiryDetails_grid->TotalRecs; // Display all records
	if ($bSelectLimit)
		$EnquiryDetails_grid->Recordset = $EnquiryDetails_grid->LoadRecordset($EnquiryDetails_grid->StartRec-1, $EnquiryDetails_grid->DisplayRecs);
}
?>
<p style="white-space: nowrap;"><span id="ewPageCaption" class="ewTitle ewTableTitle"><?php if ($EnquiryDetails->CurrentMode == "add" || $EnquiryDetails->CurrentMode == "copy") { ?><?php echo $Language->Phrase("Add") ?><?php } elseif ($EnquiryDetails->CurrentMode == "edit") { ?><?php echo $Language->Phrase("Edit") ?><?php } ?>&nbsp;<?php echo $Language->Phrase("TblTypeVIEW") ?><?php echo $EnquiryDetails->TableCaption() ?></span></p>
</p>
<?php $EnquiryDetails_grid->ShowPageHeader(); ?>
<?php
$EnquiryDetails_grid->ShowMessage();
?>
<br>
<table cellspacing="0" class="ewGrid"><tr><td class="ewGridContent">
<div id="fEnquiryDetailsgrid" class="ewForm">
<div id="gmp_EnquiryDetails" class="ewGridMiddlePanel">
<table id="tbl_EnquiryDetailsgrid" class="ewTable ewTableSeparate">
<?php echo $EnquiryDetails->TableCustomInnerHtml ?>
<thead><!-- Table header -->
	<tr class="ewTableHeader">
<?php

// Render list options
$EnquiryDetails_grid->RenderListOptions();

// Render list options (header, left)
$EnquiryDetails_grid->ListOptions->Render("header", "left");
?>
<?php if ($EnquiryDetails->contactTime->Visible) { // contactTime ?>
	<?php if ($EnquiryDetails->SortUrl($EnquiryDetails->contactTime) == "") { ?>
		<td><span id="elh_EnquiryDetails_contactTime" class="EnquiryDetails_contactTime"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $EnquiryDetails->contactTime->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div><span id="elh_EnquiryDetails_contactTime" class="EnquiryDetails_contactTime">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $EnquiryDetails->contactTime->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($EnquiryDetails->contactTime->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($EnquiryDetails->contactTime->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($EnquiryDetails->contactDescription->Visible) { // contactDescription ?>
	<?php if ($EnquiryDetails->SortUrl($EnquiryDetails->contactDescription) == "") { ?>
		<td><span id="elh_EnquiryDetails_contactDescription" class="EnquiryDetails_contactDescription"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $EnquiryDetails->contactDescription->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div><span id="elh_EnquiryDetails_contactDescription" class="EnquiryDetails_contactDescription">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $EnquiryDetails->contactDescription->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($EnquiryDetails->contactDescription->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($EnquiryDetails->contactDescription->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($EnquiryDetails->contactStaffName->Visible) { // contactStaffName ?>
	<?php if ($EnquiryDetails->SortUrl($EnquiryDetails->contactStaffName) == "") { ?>
		<td><span id="elh_EnquiryDetails_contactStaffName" class="EnquiryDetails_contactStaffName"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $EnquiryDetails->contactStaffName->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div><span id="elh_EnquiryDetails_contactStaffName" class="EnquiryDetails_contactStaffName">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $EnquiryDetails->contactStaffName->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($EnquiryDetails->contactStaffName->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($EnquiryDetails->contactStaffName->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($EnquiryDetails->roleDescription->Visible) { // roleDescription ?>
	<?php if ($EnquiryDetails->SortUrl($EnquiryDetails->roleDescription) == "") { ?>
		<td><span id="elh_EnquiryDetails_roleDescription" class="EnquiryDetails_roleDescription"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $EnquiryDetails->roleDescription->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div><span id="elh_EnquiryDetails_roleDescription" class="EnquiryDetails_roleDescription">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $EnquiryDetails->roleDescription->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($EnquiryDetails->roleDescription->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($EnquiryDetails->roleDescription->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($EnquiryDetails->assetSerialNumber->Visible) { // assetSerialNumber ?>
	<?php if ($EnquiryDetails->SortUrl($EnquiryDetails->assetSerialNumber) == "") { ?>
		<td><span id="elh_EnquiryDetails_assetSerialNumber" class="EnquiryDetails_assetSerialNumber"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $EnquiryDetails->assetSerialNumber->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div><span id="elh_EnquiryDetails_assetSerialNumber" class="EnquiryDetails_assetSerialNumber">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $EnquiryDetails->assetSerialNumber->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($EnquiryDetails->assetSerialNumber->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($EnquiryDetails->assetSerialNumber->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($EnquiryDetails->assetDescription->Visible) { // assetDescription ?>
	<?php if ($EnquiryDetails->SortUrl($EnquiryDetails->assetDescription) == "") { ?>
		<td><span id="elh_EnquiryDetails_assetDescription" class="EnquiryDetails_assetDescription"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $EnquiryDetails->assetDescription->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div><span id="elh_EnquiryDetails_assetDescription" class="EnquiryDetails_assetDescription">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $EnquiryDetails->assetDescription->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($EnquiryDetails->assetDescription->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($EnquiryDetails->assetDescription->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($EnquiryDetails->vendorName->Visible) { // vendorName ?>
	<?php if ($EnquiryDetails->SortUrl($EnquiryDetails->vendorName) == "") { ?>
		<td><span id="elh_EnquiryDetails_vendorName" class="EnquiryDetails_vendorName"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $EnquiryDetails->vendorName->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div><span id="elh_EnquiryDetails_vendorName" class="EnquiryDetails_vendorName">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $EnquiryDetails->vendorName->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($EnquiryDetails->vendorName->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($EnquiryDetails->vendorName->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($EnquiryDetails->vendorDetails->Visible) { // vendorDetails ?>
	<?php if ($EnquiryDetails->SortUrl($EnquiryDetails->vendorDetails) == "") { ?>
		<td><span id="elh_EnquiryDetails_vendorDetails" class="EnquiryDetails_vendorDetails"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $EnquiryDetails->vendorDetails->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div><span id="elh_EnquiryDetails_vendorDetails" class="EnquiryDetails_vendorDetails">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $EnquiryDetails->vendorDetails->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($EnquiryDetails->vendorDetails->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($EnquiryDetails->vendorDetails->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php

// Render list options (header, right)
$EnquiryDetails_grid->ListOptions->Render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
$EnquiryDetails_grid->StartRec = 1;
$EnquiryDetails_grid->StopRec = $EnquiryDetails_grid->TotalRecs; // Show all records

// Restore number of post back records
if ($objForm) {
	$objForm->Index = -1;
	if ($objForm->HasValue("key_count") && ($EnquiryDetails->CurrentAction == "gridadd" || $EnquiryDetails->CurrentAction == "gridedit" || $EnquiryDetails->CurrentAction == "F")) {
		$EnquiryDetails_grid->KeyCount = $objForm->GetValue("key_count");
		$EnquiryDetails_grid->StopRec = $EnquiryDetails_grid->KeyCount;
	}
}
$EnquiryDetails_grid->RecCnt = $EnquiryDetails_grid->StartRec - 1;
if ($EnquiryDetails_grid->Recordset && !$EnquiryDetails_grid->Recordset->EOF) {
	$EnquiryDetails_grid->Recordset->MoveFirst();
	if (!$bSelectLimit && $EnquiryDetails_grid->StartRec > 1)
		$EnquiryDetails_grid->Recordset->Move($EnquiryDetails_grid->StartRec - 1);
} elseif (!$EnquiryDetails->AllowAddDeleteRow && $EnquiryDetails_grid->StopRec == 0) {
	$EnquiryDetails_grid->StopRec = $EnquiryDetails->GridAddRowCount;
}

// Initialize aggregate
$EnquiryDetails->RowType = EW_ROWTYPE_AGGREGATEINIT;
$EnquiryDetails->ResetAttrs();
$EnquiryDetails_grid->RenderRow();
if ($EnquiryDetails->CurrentAction == "gridadd")
	$EnquiryDetails_grid->RowIndex = 0;
if ($EnquiryDetails->CurrentAction == "gridedit")
	$EnquiryDetails_grid->RowIndex = 0;
while ($EnquiryDetails_grid->RecCnt < $EnquiryDetails_grid->StopRec) {
	$EnquiryDetails_grid->RecCnt++;
	if (intval($EnquiryDetails_grid->RecCnt) >= intval($EnquiryDetails_grid->StartRec)) {
		$EnquiryDetails_grid->RowCnt++;
		if ($EnquiryDetails->CurrentAction == "gridadd" || $EnquiryDetails->CurrentAction == "gridedit" || $EnquiryDetails->CurrentAction == "F") {
			$EnquiryDetails_grid->RowIndex++;
			$objForm->Index = $EnquiryDetails_grid->RowIndex;
			if ($objForm->HasValue("k_action"))
				$EnquiryDetails_grid->RowAction = strval($objForm->GetValue("k_action"));
			elseif ($EnquiryDetails->CurrentAction == "gridadd")
				$EnquiryDetails_grid->RowAction = "insert";
			else
				$EnquiryDetails_grid->RowAction = "";
		}

		// Set up key count
		$EnquiryDetails_grid->KeyCount = $EnquiryDetails_grid->RowIndex;

		// Init row class and style
		$EnquiryDetails->ResetAttrs();
		$EnquiryDetails->CssClass = "";
		if ($EnquiryDetails->CurrentAction == "gridadd") {
			if ($EnquiryDetails->CurrentMode == "copy") {
				$EnquiryDetails_grid->LoadRowValues($EnquiryDetails_grid->Recordset); // Load row values
				$EnquiryDetails_grid->SetRecordKey($EnquiryDetails_grid->RowOldKey, $EnquiryDetails_grid->Recordset); // Set old record key
			} else {
				$EnquiryDetails_grid->LoadDefaultValues(); // Load default values
				$EnquiryDetails_grid->RowOldKey = ""; // Clear old key value
			}
		} elseif ($EnquiryDetails->CurrentAction == "gridedit") {
			$EnquiryDetails_grid->LoadRowValues($EnquiryDetails_grid->Recordset); // Load row values
		}
		$EnquiryDetails->RowType = EW_ROWTYPE_VIEW; // Render view
		if ($EnquiryDetails->CurrentAction == "gridadd") // Grid add
			$EnquiryDetails->RowType = EW_ROWTYPE_ADD; // Render add
		if ($EnquiryDetails->CurrentAction == "gridadd" && $EnquiryDetails->EventCancelled && !$objForm->HasValue("k_blankrow")) // Insert failed
			$EnquiryDetails_grid->RestoreCurrentRowFormValues($EnquiryDetails_grid->RowIndex); // Restore form values
		if ($EnquiryDetails->CurrentAction == "gridedit") { // Grid edit
			if ($EnquiryDetails->EventCancelled) {
				$EnquiryDetails_grid->RestoreCurrentRowFormValues($EnquiryDetails_grid->RowIndex); // Restore form values
			}
			if ($EnquiryDetails_grid->RowAction == "insert")
				$EnquiryDetails->RowType = EW_ROWTYPE_ADD; // Render add
			else
				$EnquiryDetails->RowType = EW_ROWTYPE_EDIT; // Render edit
		}
		if ($EnquiryDetails->CurrentAction == "gridedit" && ($EnquiryDetails->RowType == EW_ROWTYPE_EDIT || $EnquiryDetails->RowType == EW_ROWTYPE_ADD) && $EnquiryDetails->EventCancelled) // Update failed
			$EnquiryDetails_grid->RestoreCurrentRowFormValues($EnquiryDetails_grid->RowIndex); // Restore form values
		if ($EnquiryDetails->RowType == EW_ROWTYPE_EDIT) // Edit row
			$EnquiryDetails_grid->EditRowCnt++;
		if ($EnquiryDetails->CurrentAction == "F") // Confirm row
			$EnquiryDetails_grid->RestoreCurrentRowFormValues($EnquiryDetails_grid->RowIndex); // Restore form values

		// Set up row id / data-rowindex
		$EnquiryDetails->RowAttrs = array_merge($EnquiryDetails->RowAttrs, array('data-rowindex'=>$EnquiryDetails_grid->RowCnt, 'id'=>'r' . $EnquiryDetails_grid->RowCnt . '_EnquiryDetails', 'data-rowtype'=>$EnquiryDetails->RowType));

		// Render row
		$EnquiryDetails_grid->RenderRow();

		// Render list options
		$EnquiryDetails_grid->RenderListOptions();

		// Skip delete row / empty row for confirm page
		if ($EnquiryDetails_grid->RowAction <> "delete" && $EnquiryDetails_grid->RowAction <> "insertdelete" && !($EnquiryDetails_grid->RowAction == "insert" && $EnquiryDetails->CurrentAction == "F" && $EnquiryDetails_grid->EmptyRow())) {
?>
	<tr<?php echo $EnquiryDetails->RowAttributes() ?>>
<?php

// Render list options (body, left)
$EnquiryDetails_grid->ListOptions->Render("body", "left", $EnquiryDetails_grid->RowCnt);
?>
	<?php if ($EnquiryDetails->contactTime->Visible) { // contactTime ?>
		<td<?php echo $EnquiryDetails->contactTime->CellAttributes() ?>><span id="el<?php echo $EnquiryDetails_grid->RowCnt ?>_EnquiryDetails_contactTime" class="EnquiryDetails_contactTime">
<?php if ($EnquiryDetails->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="hidden" name="o<?php echo $EnquiryDetails_grid->RowIndex ?>_contactTime" id="o<?php echo $EnquiryDetails_grid->RowIndex ?>_contactTime" value="<?php echo ew_HtmlEncode($EnquiryDetails->contactTime->OldValue) ?>">
<?php } ?>
<?php if ($EnquiryDetails->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<?php } ?>
<?php if ($EnquiryDetails->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span<?php echo $EnquiryDetails->contactTime->ViewAttributes() ?>>
<?php echo $EnquiryDetails->contactTime->ListViewValue() ?></span>
<input type="hidden" name="x<?php echo $EnquiryDetails_grid->RowIndex ?>_contactTime" id="x<?php echo $EnquiryDetails_grid->RowIndex ?>_contactTime" value="<?php echo ew_HtmlEncode($EnquiryDetails->contactTime->FormValue) ?>">
<input type="hidden" name="o<?php echo $EnquiryDetails_grid->RowIndex ?>_contactTime" id="o<?php echo $EnquiryDetails_grid->RowIndex ?>_contactTime" value="<?php echo ew_HtmlEncode($EnquiryDetails->contactTime->OldValue) ?>">
<?php } ?>
</span><a id="<?php echo $EnquiryDetails_grid->PageObjName . "_row_" . $EnquiryDetails_grid->RowCnt ?>"></a></td>
	<?php } ?>
<?php if ($EnquiryDetails->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="hidden" name="x<?php echo $EnquiryDetails_grid->RowIndex ?>_Call_Id" id="x<?php echo $EnquiryDetails_grid->RowIndex ?>_Call_Id" value="<?php echo ew_HtmlEncode($EnquiryDetails->Call_Id->CurrentValue) ?>">
<input type="hidden" name="o<?php echo $EnquiryDetails_grid->RowIndex ?>_Call_Id" id="o<?php echo $EnquiryDetails_grid->RowIndex ?>_Call_Id" value="<?php echo ew_HtmlEncode($EnquiryDetails->Call_Id->OldValue) ?>">
<?php } ?>
<?php if ($EnquiryDetails->RowType == EW_ROWTYPE_EDIT || $EnquiryDetails->CurrentMode == "edit") { ?>
<input type="hidden" name="x<?php echo $EnquiryDetails_grid->RowIndex ?>_Call_Id" id="x<?php echo $EnquiryDetails_grid->RowIndex ?>_Call_Id" value="<?php echo ew_HtmlEncode($EnquiryDetails->Call_Id->CurrentValue) ?>">
<?php } ?>
	<?php if ($EnquiryDetails->contactDescription->Visible) { // contactDescription ?>
		<td<?php echo $EnquiryDetails->contactDescription->CellAttributes() ?>><span id="el<?php echo $EnquiryDetails_grid->RowCnt ?>_EnquiryDetails_contactDescription" class="EnquiryDetails_contactDescription">
<?php if ($EnquiryDetails->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<textarea name="x<?php echo $EnquiryDetails_grid->RowIndex ?>_contactDescription" id="x<?php echo $EnquiryDetails_grid->RowIndex ?>_contactDescription" cols="35" rows="4"<?php echo $EnquiryDetails->contactDescription->EditAttributes() ?>><?php echo $EnquiryDetails->contactDescription->EditValue ?></textarea>
<input type="hidden" name="o<?php echo $EnquiryDetails_grid->RowIndex ?>_contactDescription" id="o<?php echo $EnquiryDetails_grid->RowIndex ?>_contactDescription" value="<?php echo ew_HtmlEncode($EnquiryDetails->contactDescription->OldValue) ?>">
<?php } ?>
<?php if ($EnquiryDetails->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<textarea name="x<?php echo $EnquiryDetails_grid->RowIndex ?>_contactDescription" id="x<?php echo $EnquiryDetails_grid->RowIndex ?>_contactDescription" cols="35" rows="4"<?php echo $EnquiryDetails->contactDescription->EditAttributes() ?>><?php echo $EnquiryDetails->contactDescription->EditValue ?></textarea>
<?php } ?>
<?php if ($EnquiryDetails->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span<?php echo $EnquiryDetails->contactDescription->ViewAttributes() ?>>
<?php echo $EnquiryDetails->contactDescription->ListViewValue() ?></span>
<input type="hidden" name="x<?php echo $EnquiryDetails_grid->RowIndex ?>_contactDescription" id="x<?php echo $EnquiryDetails_grid->RowIndex ?>_contactDescription" value="<?php echo ew_HtmlEncode($EnquiryDetails->contactDescription->FormValue) ?>">
<input type="hidden" name="o<?php echo $EnquiryDetails_grid->RowIndex ?>_contactDescription" id="o<?php echo $EnquiryDetails_grid->RowIndex ?>_contactDescription" value="<?php echo ew_HtmlEncode($EnquiryDetails->contactDescription->OldValue) ?>">
<?php } ?>
</span><a id="<?php echo $EnquiryDetails_grid->PageObjName . "_row_" . $EnquiryDetails_grid->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($EnquiryDetails->contactStaffName->Visible) { // contactStaffName ?>
		<td<?php echo $EnquiryDetails->contactStaffName->CellAttributes() ?>><span id="el<?php echo $EnquiryDetails_grid->RowCnt ?>_EnquiryDetails_contactStaffName" class="EnquiryDetails_contactStaffName">
<?php if ($EnquiryDetails->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="text" name="x<?php echo $EnquiryDetails_grid->RowIndex ?>_contactStaffName" id="x<?php echo $EnquiryDetails_grid->RowIndex ?>_contactStaffName" size="30" maxlength="50" value="<?php echo $EnquiryDetails->contactStaffName->EditValue ?>"<?php echo $EnquiryDetails->contactStaffName->EditAttributes() ?>>
<input type="hidden" name="o<?php echo $EnquiryDetails_grid->RowIndex ?>_contactStaffName" id="o<?php echo $EnquiryDetails_grid->RowIndex ?>_contactStaffName" value="<?php echo ew_HtmlEncode($EnquiryDetails->contactStaffName->OldValue) ?>">
<?php } ?>
<?php if ($EnquiryDetails->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<input type="text" name="x<?php echo $EnquiryDetails_grid->RowIndex ?>_contactStaffName" id="x<?php echo $EnquiryDetails_grid->RowIndex ?>_contactStaffName" size="30" maxlength="50" value="<?php echo $EnquiryDetails->contactStaffName->EditValue ?>"<?php echo $EnquiryDetails->contactStaffName->EditAttributes() ?>>
<?php } ?>
<?php if ($EnquiryDetails->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span<?php echo $EnquiryDetails->contactStaffName->ViewAttributes() ?>>
<?php echo $EnquiryDetails->contactStaffName->ListViewValue() ?></span>
<input type="hidden" name="x<?php echo $EnquiryDetails_grid->RowIndex ?>_contactStaffName" id="x<?php echo $EnquiryDetails_grid->RowIndex ?>_contactStaffName" value="<?php echo ew_HtmlEncode($EnquiryDetails->contactStaffName->FormValue) ?>">
<input type="hidden" name="o<?php echo $EnquiryDetails_grid->RowIndex ?>_contactStaffName" id="o<?php echo $EnquiryDetails_grid->RowIndex ?>_contactStaffName" value="<?php echo ew_HtmlEncode($EnquiryDetails->contactStaffName->OldValue) ?>">
<?php } ?>
</span><a id="<?php echo $EnquiryDetails_grid->PageObjName . "_row_" . $EnquiryDetails_grid->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($EnquiryDetails->roleDescription->Visible) { // roleDescription ?>
		<td<?php echo $EnquiryDetails->roleDescription->CellAttributes() ?>><span id="el<?php echo $EnquiryDetails_grid->RowCnt ?>_EnquiryDetails_roleDescription" class="EnquiryDetails_roleDescription">
<?php if ($EnquiryDetails->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="text" name="x<?php echo $EnquiryDetails_grid->RowIndex ?>_roleDescription" id="x<?php echo $EnquiryDetails_grid->RowIndex ?>_roleDescription" size="30" maxlength="50" value="<?php echo $EnquiryDetails->roleDescription->EditValue ?>"<?php echo $EnquiryDetails->roleDescription->EditAttributes() ?>>
<input type="hidden" name="o<?php echo $EnquiryDetails_grid->RowIndex ?>_roleDescription" id="o<?php echo $EnquiryDetails_grid->RowIndex ?>_roleDescription" value="<?php echo ew_HtmlEncode($EnquiryDetails->roleDescription->OldValue) ?>">
<?php } ?>
<?php if ($EnquiryDetails->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<input type="text" name="x<?php echo $EnquiryDetails_grid->RowIndex ?>_roleDescription" id="x<?php echo $EnquiryDetails_grid->RowIndex ?>_roleDescription" size="30" maxlength="50" value="<?php echo $EnquiryDetails->roleDescription->EditValue ?>"<?php echo $EnquiryDetails->roleDescription->EditAttributes() ?>>
<?php } ?>
<?php if ($EnquiryDetails->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span<?php echo $EnquiryDetails->roleDescription->ViewAttributes() ?>>
<?php echo $EnquiryDetails->roleDescription->ListViewValue() ?></span>
<input type="hidden" name="x<?php echo $EnquiryDetails_grid->RowIndex ?>_roleDescription" id="x<?php echo $EnquiryDetails_grid->RowIndex ?>_roleDescription" value="<?php echo ew_HtmlEncode($EnquiryDetails->roleDescription->FormValue) ?>">
<input type="hidden" name="o<?php echo $EnquiryDetails_grid->RowIndex ?>_roleDescription" id="o<?php echo $EnquiryDetails_grid->RowIndex ?>_roleDescription" value="<?php echo ew_HtmlEncode($EnquiryDetails->roleDescription->OldValue) ?>">
<?php } ?>
</span><a id="<?php echo $EnquiryDetails_grid->PageObjName . "_row_" . $EnquiryDetails_grid->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($EnquiryDetails->assetSerialNumber->Visible) { // assetSerialNumber ?>
		<td<?php echo $EnquiryDetails->assetSerialNumber->CellAttributes() ?>><span id="el<?php echo $EnquiryDetails_grid->RowCnt ?>_EnquiryDetails_assetSerialNumber" class="EnquiryDetails_assetSerialNumber">
<?php if ($EnquiryDetails->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<select id="x<?php echo $EnquiryDetails_grid->RowIndex ?>_assetSerialNumber" name="x<?php echo $EnquiryDetails_grid->RowIndex ?>_assetSerialNumber"<?php echo $EnquiryDetails->assetSerialNumber->EditAttributes() ?>>
<?php
if (is_array($EnquiryDetails->assetSerialNumber->EditValue)) {
	$arwrk = $EnquiryDetails->assetSerialNumber->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($EnquiryDetails->assetSerialNumber->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " selected=\"selected\"" : "";
		if ($selwrk <> "") $emptywrk = FALSE;
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $arwrk[$rowcntwrk][1] ?>
<?php if ($arwrk[$rowcntwrk][2] <> "") { ?>
<?php echo ew_ValueSeparator(1,$EnquiryDetails->assetSerialNumber) ?><?php echo $arwrk[$rowcntwrk][2] ?>
<?php } ?>
</option>
<?php
	}
}
if (@$emptywrk) $EnquiryDetails->assetSerialNumber->OldValue = "";
?>
</select>
<script type="text/javascript">
fEnquiryDetailsgrid.Lists["x_assetSerialNumber"].Options = <?php echo (is_array($EnquiryDetails->assetSerialNumber->EditValue)) ? ew_ArrayToJson($EnquiryDetails->assetSerialNumber->EditValue, 1) : "[]" ?>;
</script>
<input type="hidden" name="o<?php echo $EnquiryDetails_grid->RowIndex ?>_assetSerialNumber" id="o<?php echo $EnquiryDetails_grid->RowIndex ?>_assetSerialNumber" value="<?php echo ew_HtmlEncode($EnquiryDetails->assetSerialNumber->OldValue) ?>">
<?php } ?>
<?php if ($EnquiryDetails->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<select id="x<?php echo $EnquiryDetails_grid->RowIndex ?>_assetSerialNumber" name="x<?php echo $EnquiryDetails_grid->RowIndex ?>_assetSerialNumber"<?php echo $EnquiryDetails->assetSerialNumber->EditAttributes() ?>>
<?php
if (is_array($EnquiryDetails->assetSerialNumber->EditValue)) {
	$arwrk = $EnquiryDetails->assetSerialNumber->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($EnquiryDetails->assetSerialNumber->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " selected=\"selected\"" : "";
		if ($selwrk <> "") $emptywrk = FALSE;
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $arwrk[$rowcntwrk][1] ?>
<?php if ($arwrk[$rowcntwrk][2] <> "") { ?>
<?php echo ew_ValueSeparator(1,$EnquiryDetails->assetSerialNumber) ?><?php echo $arwrk[$rowcntwrk][2] ?>
<?php } ?>
</option>
<?php
	}
}
if (@$emptywrk) $EnquiryDetails->assetSerialNumber->OldValue = "";
?>
</select>
<script type="text/javascript">
fEnquiryDetailsgrid.Lists["x_assetSerialNumber"].Options = <?php echo (is_array($EnquiryDetails->assetSerialNumber->EditValue)) ? ew_ArrayToJson($EnquiryDetails->assetSerialNumber->EditValue, 1) : "[]" ?>;
</script>
<?php } ?>
<?php if ($EnquiryDetails->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span<?php echo $EnquiryDetails->assetSerialNumber->ViewAttributes() ?>>
<?php echo $EnquiryDetails->assetSerialNumber->ListViewValue() ?></span>
<input type="hidden" name="x<?php echo $EnquiryDetails_grid->RowIndex ?>_assetSerialNumber" id="x<?php echo $EnquiryDetails_grid->RowIndex ?>_assetSerialNumber" value="<?php echo ew_HtmlEncode($EnquiryDetails->assetSerialNumber->FormValue) ?>">
<input type="hidden" name="o<?php echo $EnquiryDetails_grid->RowIndex ?>_assetSerialNumber" id="o<?php echo $EnquiryDetails_grid->RowIndex ?>_assetSerialNumber" value="<?php echo ew_HtmlEncode($EnquiryDetails->assetSerialNumber->OldValue) ?>">
<?php } ?>
</span><a id="<?php echo $EnquiryDetails_grid->PageObjName . "_row_" . $EnquiryDetails_grid->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($EnquiryDetails->assetDescription->Visible) { // assetDescription ?>
		<td<?php echo $EnquiryDetails->assetDescription->CellAttributes() ?>><span id="el<?php echo $EnquiryDetails_grid->RowCnt ?>_EnquiryDetails_assetDescription" class="EnquiryDetails_assetDescription">
<?php if ($EnquiryDetails->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<textarea name="x<?php echo $EnquiryDetails_grid->RowIndex ?>_assetDescription" id="x<?php echo $EnquiryDetails_grid->RowIndex ?>_assetDescription" cols="35" rows="4"<?php echo $EnquiryDetails->assetDescription->EditAttributes() ?>><?php echo $EnquiryDetails->assetDescription->EditValue ?></textarea>
<input type="hidden" name="o<?php echo $EnquiryDetails_grid->RowIndex ?>_assetDescription" id="o<?php echo $EnquiryDetails_grid->RowIndex ?>_assetDescription" value="<?php echo ew_HtmlEncode($EnquiryDetails->assetDescription->OldValue) ?>">
<?php } ?>
<?php if ($EnquiryDetails->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<textarea name="x<?php echo $EnquiryDetails_grid->RowIndex ?>_assetDescription" id="x<?php echo $EnquiryDetails_grid->RowIndex ?>_assetDescription" cols="35" rows="4"<?php echo $EnquiryDetails->assetDescription->EditAttributes() ?>><?php echo $EnquiryDetails->assetDescription->EditValue ?></textarea>
<?php } ?>
<?php if ($EnquiryDetails->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span<?php echo $EnquiryDetails->assetDescription->ViewAttributes() ?>>
<?php echo $EnquiryDetails->assetDescription->ListViewValue() ?></span>
<input type="hidden" name="x<?php echo $EnquiryDetails_grid->RowIndex ?>_assetDescription" id="x<?php echo $EnquiryDetails_grid->RowIndex ?>_assetDescription" value="<?php echo ew_HtmlEncode($EnquiryDetails->assetDescription->FormValue) ?>">
<input type="hidden" name="o<?php echo $EnquiryDetails_grid->RowIndex ?>_assetDescription" id="o<?php echo $EnquiryDetails_grid->RowIndex ?>_assetDescription" value="<?php echo ew_HtmlEncode($EnquiryDetails->assetDescription->OldValue) ?>">
<?php } ?>
</span><a id="<?php echo $EnquiryDetails_grid->PageObjName . "_row_" . $EnquiryDetails_grid->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($EnquiryDetails->vendorName->Visible) { // vendorName ?>
		<td<?php echo $EnquiryDetails->vendorName->CellAttributes() ?>><span id="el<?php echo $EnquiryDetails_grid->RowCnt ?>_EnquiryDetails_vendorName" class="EnquiryDetails_vendorName">
<?php if ($EnquiryDetails->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="text" name="x<?php echo $EnquiryDetails_grid->RowIndex ?>_vendorName" id="x<?php echo $EnquiryDetails_grid->RowIndex ?>_vendorName" size="30" maxlength="50" value="<?php echo $EnquiryDetails->vendorName->EditValue ?>"<?php echo $EnquiryDetails->vendorName->EditAttributes() ?>>
<input type="hidden" name="o<?php echo $EnquiryDetails_grid->RowIndex ?>_vendorName" id="o<?php echo $EnquiryDetails_grid->RowIndex ?>_vendorName" value="<?php echo ew_HtmlEncode($EnquiryDetails->vendorName->OldValue) ?>">
<?php } ?>
<?php if ($EnquiryDetails->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<input type="text" name="x<?php echo $EnquiryDetails_grid->RowIndex ?>_vendorName" id="x<?php echo $EnquiryDetails_grid->RowIndex ?>_vendorName" size="30" maxlength="50" value="<?php echo $EnquiryDetails->vendorName->EditValue ?>"<?php echo $EnquiryDetails->vendorName->EditAttributes() ?>>
<?php } ?>
<?php if ($EnquiryDetails->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span<?php echo $EnquiryDetails->vendorName->ViewAttributes() ?>>
<?php echo $EnquiryDetails->vendorName->ListViewValue() ?></span>
<input type="hidden" name="x<?php echo $EnquiryDetails_grid->RowIndex ?>_vendorName" id="x<?php echo $EnquiryDetails_grid->RowIndex ?>_vendorName" value="<?php echo ew_HtmlEncode($EnquiryDetails->vendorName->FormValue) ?>">
<input type="hidden" name="o<?php echo $EnquiryDetails_grid->RowIndex ?>_vendorName" id="o<?php echo $EnquiryDetails_grid->RowIndex ?>_vendorName" value="<?php echo ew_HtmlEncode($EnquiryDetails->vendorName->OldValue) ?>">
<?php } ?>
</span><a id="<?php echo $EnquiryDetails_grid->PageObjName . "_row_" . $EnquiryDetails_grid->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($EnquiryDetails->vendorDetails->Visible) { // vendorDetails ?>
		<td<?php echo $EnquiryDetails->vendorDetails->CellAttributes() ?>><span id="el<?php echo $EnquiryDetails_grid->RowCnt ?>_EnquiryDetails_vendorDetails" class="EnquiryDetails_vendorDetails">
<?php if ($EnquiryDetails->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="text" name="x<?php echo $EnquiryDetails_grid->RowIndex ?>_vendorDetails" id="x<?php echo $EnquiryDetails_grid->RowIndex ?>_vendorDetails" size="30" maxlength="250" value="<?php echo $EnquiryDetails->vendorDetails->EditValue ?>"<?php echo $EnquiryDetails->vendorDetails->EditAttributes() ?>>
<input type="hidden" name="o<?php echo $EnquiryDetails_grid->RowIndex ?>_vendorDetails" id="o<?php echo $EnquiryDetails_grid->RowIndex ?>_vendorDetails" value="<?php echo ew_HtmlEncode($EnquiryDetails->vendorDetails->OldValue) ?>">
<?php } ?>
<?php if ($EnquiryDetails->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<input type="text" name="x<?php echo $EnquiryDetails_grid->RowIndex ?>_vendorDetails" id="x<?php echo $EnquiryDetails_grid->RowIndex ?>_vendorDetails" size="30" maxlength="250" value="<?php echo $EnquiryDetails->vendorDetails->EditValue ?>"<?php echo $EnquiryDetails->vendorDetails->EditAttributes() ?>>
<?php } ?>
<?php if ($EnquiryDetails->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span<?php echo $EnquiryDetails->vendorDetails->ViewAttributes() ?>>
<?php echo $EnquiryDetails->vendorDetails->ListViewValue() ?></span>
<input type="hidden" name="x<?php echo $EnquiryDetails_grid->RowIndex ?>_vendorDetails" id="x<?php echo $EnquiryDetails_grid->RowIndex ?>_vendorDetails" value="<?php echo ew_HtmlEncode($EnquiryDetails->vendorDetails->FormValue) ?>">
<input type="hidden" name="o<?php echo $EnquiryDetails_grid->RowIndex ?>_vendorDetails" id="o<?php echo $EnquiryDetails_grid->RowIndex ?>_vendorDetails" value="<?php echo ew_HtmlEncode($EnquiryDetails->vendorDetails->OldValue) ?>">
<?php } ?>
</span><a id="<?php echo $EnquiryDetails_grid->PageObjName . "_row_" . $EnquiryDetails_grid->RowCnt ?>"></a></td>
	<?php } ?>
<?php

// Render list options (body, right)
$EnquiryDetails_grid->ListOptions->Render("body", "right", $EnquiryDetails_grid->RowCnt);
?>
	</tr>
<?php if ($EnquiryDetails->RowType == EW_ROWTYPE_ADD || $EnquiryDetails->RowType == EW_ROWTYPE_EDIT) { ?>
<script type="text/javascript">
fEnquiryDetailsgrid.UpdateOpts(<?php echo $EnquiryDetails_grid->RowIndex ?>);
</script>
<?php } ?>
<?php
	}
	} // End delete row checking
	if ($EnquiryDetails->CurrentAction <> "gridadd" || $EnquiryDetails->CurrentMode == "copy")
		if (!$EnquiryDetails_grid->Recordset->EOF) $EnquiryDetails_grid->Recordset->MoveNext();
}
?>
<?php
	if ($EnquiryDetails->CurrentMode == "add" || $EnquiryDetails->CurrentMode == "copy" || $EnquiryDetails->CurrentMode == "edit") {
		$EnquiryDetails_grid->RowIndex = '$rowindex$';
		$EnquiryDetails_grid->LoadDefaultValues();

		// Set row properties
		$EnquiryDetails->ResetAttrs();
		$EnquiryDetails->RowAttrs = array_merge($EnquiryDetails->RowAttrs, array('data-rowindex'=>$EnquiryDetails_grid->RowIndex, 'id'=>'r0_EnquiryDetails', 'data-rowtype'=>EW_ROWTYPE_ADD));
		ew_AppendClass($EnquiryDetails->RowAttrs["class"], "ewTemplate");
		$EnquiryDetails->RowType = EW_ROWTYPE_ADD;

		// Render row
		$EnquiryDetails_grid->RenderRow();

		// Render list options
		$EnquiryDetails_grid->RenderListOptions();
		$EnquiryDetails_grid->StartRowCnt = 0;
?>
	<tr<?php echo $EnquiryDetails->RowAttributes() ?>>
<?php

// Render list options (body, left)
$EnquiryDetails_grid->ListOptions->Render("body", "left", $EnquiryDetails_grid->RowIndex);
?>
	<?php if ($EnquiryDetails->contactTime->Visible) { // contactTime ?>
		<td><span id="el$rowindex$_EnquiryDetails_contactTime" class="EnquiryDetails_contactTime">
<?php if ($EnquiryDetails->CurrentAction <> "F") { ?>
<?php } else { ?>
<span<?php echo $EnquiryDetails->contactTime->ViewAttributes() ?>>
<?php echo $EnquiryDetails->contactTime->ViewValue ?></span>
<input type="hidden" name="x<?php echo $EnquiryDetails_grid->RowIndex ?>_contactTime" id="x<?php echo $EnquiryDetails_grid->RowIndex ?>_contactTime" value="<?php echo ew_HtmlEncode($EnquiryDetails->contactTime->FormValue) ?>">
<?php } ?>
<input type="hidden" name="o<?php echo $EnquiryDetails_grid->RowIndex ?>_contactTime" id="o<?php echo $EnquiryDetails_grid->RowIndex ?>_contactTime" value="<?php echo ew_HtmlEncode($EnquiryDetails->contactTime->OldValue) ?>">
</span></td>
	<?php } ?>
	<?php if ($EnquiryDetails->contactDescription->Visible) { // contactDescription ?>
		<td><span id="el$rowindex$_EnquiryDetails_contactDescription" class="EnquiryDetails_contactDescription">
<?php if ($EnquiryDetails->CurrentAction <> "F") { ?>
<textarea name="x<?php echo $EnquiryDetails_grid->RowIndex ?>_contactDescription" id="x<?php echo $EnquiryDetails_grid->RowIndex ?>_contactDescription" cols="35" rows="4"<?php echo $EnquiryDetails->contactDescription->EditAttributes() ?>><?php echo $EnquiryDetails->contactDescription->EditValue ?></textarea>
<?php } else { ?>
<span<?php echo $EnquiryDetails->contactDescription->ViewAttributes() ?>>
<?php echo $EnquiryDetails->contactDescription->ViewValue ?></span>
<input type="hidden" name="x<?php echo $EnquiryDetails_grid->RowIndex ?>_contactDescription" id="x<?php echo $EnquiryDetails_grid->RowIndex ?>_contactDescription" value="<?php echo ew_HtmlEncode($EnquiryDetails->contactDescription->FormValue) ?>">
<?php } ?>
<input type="hidden" name="o<?php echo $EnquiryDetails_grid->RowIndex ?>_contactDescription" id="o<?php echo $EnquiryDetails_grid->RowIndex ?>_contactDescription" value="<?php echo ew_HtmlEncode($EnquiryDetails->contactDescription->OldValue) ?>">
</span></td>
	<?php } ?>
	<?php if ($EnquiryDetails->contactStaffName->Visible) { // contactStaffName ?>
		<td><span id="el$rowindex$_EnquiryDetails_contactStaffName" class="EnquiryDetails_contactStaffName">
<?php if ($EnquiryDetails->CurrentAction <> "F") { ?>
<input type="text" name="x<?php echo $EnquiryDetails_grid->RowIndex ?>_contactStaffName" id="x<?php echo $EnquiryDetails_grid->RowIndex ?>_contactStaffName" size="30" maxlength="50" value="<?php echo $EnquiryDetails->contactStaffName->EditValue ?>"<?php echo $EnquiryDetails->contactStaffName->EditAttributes() ?>>
<?php } else { ?>
<span<?php echo $EnquiryDetails->contactStaffName->ViewAttributes() ?>>
<?php echo $EnquiryDetails->contactStaffName->ViewValue ?></span>
<input type="hidden" name="x<?php echo $EnquiryDetails_grid->RowIndex ?>_contactStaffName" id="x<?php echo $EnquiryDetails_grid->RowIndex ?>_contactStaffName" value="<?php echo ew_HtmlEncode($EnquiryDetails->contactStaffName->FormValue) ?>">
<?php } ?>
<input type="hidden" name="o<?php echo $EnquiryDetails_grid->RowIndex ?>_contactStaffName" id="o<?php echo $EnquiryDetails_grid->RowIndex ?>_contactStaffName" value="<?php echo ew_HtmlEncode($EnquiryDetails->contactStaffName->OldValue) ?>">
</span></td>
	<?php } ?>
	<?php if ($EnquiryDetails->roleDescription->Visible) { // roleDescription ?>
		<td><span id="el$rowindex$_EnquiryDetails_roleDescription" class="EnquiryDetails_roleDescription">
<?php if ($EnquiryDetails->CurrentAction <> "F") { ?>
<input type="text" name="x<?php echo $EnquiryDetails_grid->RowIndex ?>_roleDescription" id="x<?php echo $EnquiryDetails_grid->RowIndex ?>_roleDescription" size="30" maxlength="50" value="<?php echo $EnquiryDetails->roleDescription->EditValue ?>"<?php echo $EnquiryDetails->roleDescription->EditAttributes() ?>>
<?php } else { ?>
<span<?php echo $EnquiryDetails->roleDescription->ViewAttributes() ?>>
<?php echo $EnquiryDetails->roleDescription->ViewValue ?></span>
<input type="hidden" name="x<?php echo $EnquiryDetails_grid->RowIndex ?>_roleDescription" id="x<?php echo $EnquiryDetails_grid->RowIndex ?>_roleDescription" value="<?php echo ew_HtmlEncode($EnquiryDetails->roleDescription->FormValue) ?>">
<?php } ?>
<input type="hidden" name="o<?php echo $EnquiryDetails_grid->RowIndex ?>_roleDescription" id="o<?php echo $EnquiryDetails_grid->RowIndex ?>_roleDescription" value="<?php echo ew_HtmlEncode($EnquiryDetails->roleDescription->OldValue) ?>">
</span></td>
	<?php } ?>
	<?php if ($EnquiryDetails->assetSerialNumber->Visible) { // assetSerialNumber ?>
		<td><span id="el$rowindex$_EnquiryDetails_assetSerialNumber" class="EnquiryDetails_assetSerialNumber">
<?php if ($EnquiryDetails->CurrentAction <> "F") { ?>
<select id="x<?php echo $EnquiryDetails_grid->RowIndex ?>_assetSerialNumber" name="x<?php echo $EnquiryDetails_grid->RowIndex ?>_assetSerialNumber"<?php echo $EnquiryDetails->assetSerialNumber->EditAttributes() ?>>
<?php
if (is_array($EnquiryDetails->assetSerialNumber->EditValue)) {
	$arwrk = $EnquiryDetails->assetSerialNumber->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($EnquiryDetails->assetSerialNumber->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " selected=\"selected\"" : "";
		if ($selwrk <> "") $emptywrk = FALSE;
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $arwrk[$rowcntwrk][1] ?>
<?php if ($arwrk[$rowcntwrk][2] <> "") { ?>
<?php echo ew_ValueSeparator(1,$EnquiryDetails->assetSerialNumber) ?><?php echo $arwrk[$rowcntwrk][2] ?>
<?php } ?>
</option>
<?php
	}
}
if (@$emptywrk) $EnquiryDetails->assetSerialNumber->OldValue = "";
?>
</select>
<script type="text/javascript">
fEnquiryDetailsgrid.Lists["x_assetSerialNumber"].Options = <?php echo (is_array($EnquiryDetails->assetSerialNumber->EditValue)) ? ew_ArrayToJson($EnquiryDetails->assetSerialNumber->EditValue, 1) : "[]" ?>;
</script>
<?php } else { ?>
<span<?php echo $EnquiryDetails->assetSerialNumber->ViewAttributes() ?>>
<?php echo $EnquiryDetails->assetSerialNumber->ViewValue ?></span>
<input type="hidden" name="x<?php echo $EnquiryDetails_grid->RowIndex ?>_assetSerialNumber" id="x<?php echo $EnquiryDetails_grid->RowIndex ?>_assetSerialNumber" value="<?php echo ew_HtmlEncode($EnquiryDetails->assetSerialNumber->FormValue) ?>">
<?php } ?>
<input type="hidden" name="o<?php echo $EnquiryDetails_grid->RowIndex ?>_assetSerialNumber" id="o<?php echo $EnquiryDetails_grid->RowIndex ?>_assetSerialNumber" value="<?php echo ew_HtmlEncode($EnquiryDetails->assetSerialNumber->OldValue) ?>">
</span></td>
	<?php } ?>
	<?php if ($EnquiryDetails->assetDescription->Visible) { // assetDescription ?>
		<td><span id="el$rowindex$_EnquiryDetails_assetDescription" class="EnquiryDetails_assetDescription">
<?php if ($EnquiryDetails->CurrentAction <> "F") { ?>
<textarea name="x<?php echo $EnquiryDetails_grid->RowIndex ?>_assetDescription" id="x<?php echo $EnquiryDetails_grid->RowIndex ?>_assetDescription" cols="35" rows="4"<?php echo $EnquiryDetails->assetDescription->EditAttributes() ?>><?php echo $EnquiryDetails->assetDescription->EditValue ?></textarea>
<?php } else { ?>
<span<?php echo $EnquiryDetails->assetDescription->ViewAttributes() ?>>
<?php echo $EnquiryDetails->assetDescription->ViewValue ?></span>
<input type="hidden" name="x<?php echo $EnquiryDetails_grid->RowIndex ?>_assetDescription" id="x<?php echo $EnquiryDetails_grid->RowIndex ?>_assetDescription" value="<?php echo ew_HtmlEncode($EnquiryDetails->assetDescription->FormValue) ?>">
<?php } ?>
<input type="hidden" name="o<?php echo $EnquiryDetails_grid->RowIndex ?>_assetDescription" id="o<?php echo $EnquiryDetails_grid->RowIndex ?>_assetDescription" value="<?php echo ew_HtmlEncode($EnquiryDetails->assetDescription->OldValue) ?>">
</span></td>
	<?php } ?>
	<?php if ($EnquiryDetails->vendorName->Visible) { // vendorName ?>
		<td><span id="el$rowindex$_EnquiryDetails_vendorName" class="EnquiryDetails_vendorName">
<?php if ($EnquiryDetails->CurrentAction <> "F") { ?>
<input type="text" name="x<?php echo $EnquiryDetails_grid->RowIndex ?>_vendorName" id="x<?php echo $EnquiryDetails_grid->RowIndex ?>_vendorName" size="30" maxlength="50" value="<?php echo $EnquiryDetails->vendorName->EditValue ?>"<?php echo $EnquiryDetails->vendorName->EditAttributes() ?>>
<?php } else { ?>
<span<?php echo $EnquiryDetails->vendorName->ViewAttributes() ?>>
<?php echo $EnquiryDetails->vendorName->ViewValue ?></span>
<input type="hidden" name="x<?php echo $EnquiryDetails_grid->RowIndex ?>_vendorName" id="x<?php echo $EnquiryDetails_grid->RowIndex ?>_vendorName" value="<?php echo ew_HtmlEncode($EnquiryDetails->vendorName->FormValue) ?>">
<?php } ?>
<input type="hidden" name="o<?php echo $EnquiryDetails_grid->RowIndex ?>_vendorName" id="o<?php echo $EnquiryDetails_grid->RowIndex ?>_vendorName" value="<?php echo ew_HtmlEncode($EnquiryDetails->vendorName->OldValue) ?>">
</span></td>
	<?php } ?>
	<?php if ($EnquiryDetails->vendorDetails->Visible) { // vendorDetails ?>
		<td><span id="el$rowindex$_EnquiryDetails_vendorDetails" class="EnquiryDetails_vendorDetails">
<?php if ($EnquiryDetails->CurrentAction <> "F") { ?>
<input type="text" name="x<?php echo $EnquiryDetails_grid->RowIndex ?>_vendorDetails" id="x<?php echo $EnquiryDetails_grid->RowIndex ?>_vendorDetails" size="30" maxlength="250" value="<?php echo $EnquiryDetails->vendorDetails->EditValue ?>"<?php echo $EnquiryDetails->vendorDetails->EditAttributes() ?>>
<?php } else { ?>
<span<?php echo $EnquiryDetails->vendorDetails->ViewAttributes() ?>>
<?php echo $EnquiryDetails->vendorDetails->ViewValue ?></span>
<input type="hidden" name="x<?php echo $EnquiryDetails_grid->RowIndex ?>_vendorDetails" id="x<?php echo $EnquiryDetails_grid->RowIndex ?>_vendorDetails" value="<?php echo ew_HtmlEncode($EnquiryDetails->vendorDetails->FormValue) ?>">
<?php } ?>
<input type="hidden" name="o<?php echo $EnquiryDetails_grid->RowIndex ?>_vendorDetails" id="o<?php echo $EnquiryDetails_grid->RowIndex ?>_vendorDetails" value="<?php echo ew_HtmlEncode($EnquiryDetails->vendorDetails->OldValue) ?>">
</span></td>
	<?php } ?>
<?php

// Render list options (body, right)
$EnquiryDetails_grid->ListOptions->Render("body", "right", $EnquiryDetails_grid->RowCnt);
?>
<script type="text/javascript">
fEnquiryDetailsgrid.UpdateOpts(<?php echo $EnquiryDetails_grid->RowIndex ?>);
</script>
	</tr>
<?php
}
?>
</tbody>
</table>
<?php if ($EnquiryDetails->CurrentMode == "add" || $EnquiryDetails->CurrentMode == "copy") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridinsert">
<input type="hidden" name="key_count" id="key_count" value="<?php echo $EnquiryDetails_grid->KeyCount ?>">
<?php echo $EnquiryDetails_grid->MultiSelectKey ?>
<?php } ?>
<?php if ($EnquiryDetails->CurrentMode == "edit") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridupdate">
<input type="hidden" name="key_count" id="key_count" value="<?php echo $EnquiryDetails_grid->KeyCount ?>">
<?php echo $EnquiryDetails_grid->MultiSelectKey ?>
<?php } ?>
<?php if ($EnquiryDetails->CurrentMode == "") { ?>
<input type="hidden" name="a_list" id="a_list" value="">
<?php } ?>
<input type="hidden" name="detailpage" id="detailpage" value="fEnquiryDetailsgrid">
</div>
<?php

// Close recordset
if ($EnquiryDetails_grid->Recordset)
	$EnquiryDetails_grid->Recordset->Close();
?>
<?php if (($EnquiryDetails->CurrentMode == "add" || $EnquiryDetails->CurrentMode == "copy" || $EnquiryDetails->CurrentMode == "edit") && $EnquiryDetails->CurrentAction != "F") { // add/copy/edit mode ?>
<div class="ewGridLowerPanel">
</div>
<?php } ?>
</div>
</td></tr></table>
<?php if ($EnquiryDetails->Export == "") { ?>
<script type="text/javascript">
fEnquiryDetailsgrid.Init();
</script>
<?php } ?>
<?php
$EnquiryDetails_grid->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<?php
$EnquiryDetails_grid->Page_Terminate();
$Page = &$MasterPage;
?>
