<?php

// Global variable for table object
$EnquiryDetails = NULL;

//
// Table class for EnquiryDetails
//
class cEnquiryDetails extends cTable {
	var $Call_Id;
	var $contactTime;
	var $contactDescription;
	var $contactStaffName;
	var $roleDescription;
	var $assetSerialNumber;
	var $assetDescription;
	var $vendorName;
	var $vendorDetails;

	//
	// Table class constructor
	//
	function __construct() {
		global $Language;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();
		$this->TableVar = 'EnquiryDetails';
		$this->TableName = 'EnquiryDetails';
		$this->TableType = 'VIEW';
		$this->ExportAll = TRUE;
		$this->ExportPageBreakCount = 0; // Page break per every n record (PDF only)
		$this->ExportPageOrientation = "portrait"; // Page orientation (PDF only)
		$this->ExportPageSize = "a4"; // Page size (PDF only)
		$this->DetailAdd = FALSE; // Allow detail add
		$this->DetailEdit = FALSE; // Allow detail edit
		$this->GridAddRowCount = 5;
		$this->AllowAddDeleteRow = ew_AllowAddDeleteRow(); // Allow add/delete row
		$this->UserIDAllowSecurity = 0; // User ID Allow
		$this->BasicSearch = new cBasicSearch($this->TableVar);

		// Call Id
		$this->Call_Id = new cField('EnquiryDetails', 'EnquiryDetails', 'x_Call_Id', 'Call Id', '`Call Id`', '`Call Id`', 3, -1, FALSE, '`Call Id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->Call_Id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['Call Id'] = &$this->Call_Id;

		// contactTime
		$this->contactTime = new cField('EnquiryDetails', 'EnquiryDetails', 'x_contactTime', 'contactTime', '`contactTime`', 'DATE_FORMAT(`contactTime`, \'%d/%m/%Y %H:%i:%s\')', 135, 9, FALSE, '`contactTime`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->contactTime->FldDefaultErrMsg = str_replace("%s", "/", $Language->Phrase("IncorrectDateDMY"));
		$this->fields['contactTime'] = &$this->contactTime;

		// contactDescription
		$this->contactDescription = new cField('EnquiryDetails', 'EnquiryDetails', 'x_contactDescription', 'contactDescription', '`contactDescription`', '`contactDescription`', 201, -1, FALSE, '`contactDescription`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->fields['contactDescription'] = &$this->contactDescription;

		// contactStaffName
		$this->contactStaffName = new cField('EnquiryDetails', 'EnquiryDetails', 'x_contactStaffName', 'contactStaffName', '`contactStaffName`', '`contactStaffName`', 200, -1, FALSE, '`contactStaffName`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->fields['contactStaffName'] = &$this->contactStaffName;

		// roleDescription
		$this->roleDescription = new cField('EnquiryDetails', 'EnquiryDetails', 'x_roleDescription', 'roleDescription', '`roleDescription`', '`roleDescription`', 200, -1, FALSE, '`roleDescription`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->fields['roleDescription'] = &$this->roleDescription;

		// assetSerialNumber
		$this->assetSerialNumber = new cField('EnquiryDetails', 'EnquiryDetails', 'x_assetSerialNumber', 'assetSerialNumber', '`assetSerialNumber`', '`assetSerialNumber`', 3, -1, FALSE, '`assetSerialNumber`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->assetSerialNumber->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['assetSerialNumber'] = &$this->assetSerialNumber;

		// assetDescription
		$this->assetDescription = new cField('EnquiryDetails', 'EnquiryDetails', 'x_assetDescription', 'assetDescription', '`assetDescription`', '`assetDescription`', 201, -1, FALSE, '`assetDescription`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->fields['assetDescription'] = &$this->assetDescription;

		// vendorName
		$this->vendorName = new cField('EnquiryDetails', 'EnquiryDetails', 'x_vendorName', 'vendorName', '`vendorName`', '`vendorName`', 200, -1, FALSE, '`vendorName`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->fields['vendorName'] = &$this->vendorName;

		// vendorDetails
		$this->vendorDetails = new cField('EnquiryDetails', 'EnquiryDetails', 'x_vendorDetails', 'vendorDetails', '`vendorDetails`', '`vendorDetails`', 200, -1, FALSE, '`vendorDetails`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->fields['vendorDetails'] = &$this->vendorDetails;
	}

	// Single column sort
	function UpdateSort(&$ofld) {
		if ($this->CurrentOrder == $ofld->FldName) {
			$sSortField = $ofld->FldExpression;
			$sLastSort = $ofld->getSort();
			if ($this->CurrentOrderType == "ASC" || $this->CurrentOrderType == "DESC") {
				$sThisSort = $this->CurrentOrderType;
			} else {
				$sThisSort = ($sLastSort == "ASC") ? "DESC" : "ASC";
			}
			$ofld->setSort($sThisSort);
			$this->setSessionOrderBy($sSortField . " " . $sThisSort); // Save to Session
		} else {
			$ofld->setSort("");
		}
	}

	// Current master table name
	function getCurrentMasterTable() {
		return @$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_MASTER_TABLE];
	}

	function setCurrentMasterTable($v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_MASTER_TABLE] = $v;
	}

	// Session master WHERE clause
	function GetMasterFilter() {

		// Master filter
		$sMasterFilter = "";
		if ($this->getCurrentMasterTable() == "EnquiryMaster") {
			if ($this->Call_Id->getSessionValue() <> "")
				$sMasterFilter .= "`CallId`=" . ew_QuotedValue($this->Call_Id->getSessionValue(), EW_DATATYPE_NUMBER);
			else
				return "";
		}
		return $sMasterFilter;
	}

	// Session detail WHERE clause
	function GetDetailFilter() {

		// Detail filter
		$sDetailFilter = "";
		if ($this->getCurrentMasterTable() == "EnquiryMaster") {
			if ($this->Call_Id->getSessionValue() <> "")
				$sDetailFilter .= "`Call Id`=" . ew_QuotedValue($this->Call_Id->getSessionValue(), EW_DATATYPE_NUMBER);
			else
				return "";
		}
		return $sDetailFilter;
	}

	// Master filter
	function SqlMasterFilter_EnquiryMaster() {
		return "`CallId`=@CallId@";
	}

	// Detail filter
	function SqlDetailFilter_EnquiryMaster() {
		return "`Call Id`=@Call_Id@";
	}

	// Table level SQL
	function SqlFrom() { // From
		return "`EnquiryDetails`";
	}

	function SqlSelect() { // Select
		return "SELECT * FROM " . $this->SqlFrom();
	}

	function SqlWhere() { // Where
		$sWhere = "";
		$this->TableFilter = "";
		ew_AddFilter($sWhere, $this->TableFilter);
		return $sWhere;
	}

	function SqlGroupBy() { // Group By
		return "";
	}

	function SqlHaving() { // Having
		return "";
	}

	function SqlOrderBy() { // Order By
		return "";
	}

	// Check if Anonymous User is allowed
	function AllowAnonymousUser() {
		switch (@$this->PageID) {
			case "add":
			case "register":
			case "addopt":
				return FALSE;
			case "edit":
			case "update":
			case "changepwd":
			case "forgotpwd":
				return FALSE;
			case "delete":
				return FALSE;
			case "view":
				return FALSE;
			case "search":
				return FALSE;
			default:
				return FALSE;
		}
	}

	// Apply User ID filters
	function ApplyUserIDFilters($sFilter) {
		return $sFilter;
	}

	// Check if User ID security allows view all
	function UserIDAllow($id = "") {
		return TRUE;
	}

	// Get SQL
	function GetSQL($where, $orderby) {
		return ew_BuildSelectSql($this->SqlSelect(), $this->SqlWhere(),
			$this->SqlGroupBy(), $this->SqlHaving(), $this->SqlOrderBy(),
			$where, $orderby);
	}

	// Table SQL
	function SQL() {
		$sFilter = $this->CurrentFilter;
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->SqlSelect(), $this->SqlWhere(),
			$this->SqlGroupBy(), $this->SqlHaving(), $this->SqlOrderBy(),
			$sFilter, $sSort);
	}

	// Table SQL with List page filter
	function SelectSQL() {
		$sFilter = $this->getSessionWhere();
		ew_AddFilter($sFilter, $this->CurrentFilter);
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->SqlSelect(), $this->SqlWhere(), $this->SqlGroupBy(),
			$this->SqlHaving(), $this->SqlOrderBy(), $sFilter, $sSort);
	}

	// Get ORDER BY clause
	function GetOrderBy() {
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql("", "", "", "", $this->SqlOrderBy(), "", $sSort);
	}

	// Try to get record count
	function TryGetRecordCount($sSql) {
		global $conn;
		$cnt = -1;
		if ($this->TableType == 'TABLE' || $this->TableType == 'VIEW') {
			$sSql = "SELECT COUNT(*) FROM" . substr($sSql, 13);
			$sOrderBy = $this->GetOrderBy();
			if (substr($sSql, strlen($sOrderBy) * -1) == $sOrderBy)
				$sSql = substr($sSql, 0, strlen($sSql) - strlen($sOrderBy)); // Remove ORDER BY clause
		} else {
			$sSql = "SELECT COUNT(*) FROM (" . $sSql . ") EW_COUNT_TABLE";
		}
		if ($rs = $conn->Execute($sSql)) {
			if (!$rs->EOF && $rs->FieldCount() > 0) {
				$cnt = $rs->fields[0];
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// Get record count based on filter (for detail record count in master table pages)
	function LoadRecordCount($sFilter) {
		$origFilter = $this->CurrentFilter;
		$this->CurrentFilter = $sFilter;
		$this->Recordset_Selecting($this->CurrentFilter);

		//$sSql = $this->SQL();
		$sSql = $this->GetSQL($this->CurrentFilter, "");
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			if ($rs = $this->LoadRs($this->CurrentFilter)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		$this->CurrentFilter = $origFilter;
		return intval($cnt);
	}

	// Get record count (for current List page)
	function SelectRecordCount() {
		global $conn;
		$origFilter = $this->CurrentFilter;
		$this->Recordset_Selecting($this->CurrentFilter);
		$sSql = $this->SelectSQL();
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			if ($rs = $conn->Execute($sSql)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		$this->CurrentFilter = $origFilter;
		return intval($cnt);
	}

	// Update Table
	var $UpdateTable = "`EnquiryDetails`";

	// INSERT statement
	function InsertSQL(&$rs) {
		global $conn;
		$names = "";
		$values = "";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]))
				continue;
			$names .= $this->fields[$name]->FldExpression . ",";
			$values .= ew_QuotedValue($value, $this->fields[$name]->FldDataType) . ",";
		}
		while (substr($names, -1) == ",")
			$names = substr($names, 0, -1);
		while (substr($values, -1) == ",")
			$values = substr($values, 0, -1);
		return "INSERT INTO " . $this->UpdateTable . " ($names) VALUES ($values)";
	}

	// Insert
	function Insert(&$rs) {
		global $conn;
		return $conn->Execute($this->InsertSQL($rs));
	}

	// UPDATE statement
	function UpdateSQL(&$rs, $where = "") {
		$sql = "UPDATE " . $this->UpdateTable . " SET ";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]))
				continue;
			$sql .= $this->fields[$name]->FldExpression . "=";
			$sql .= ew_QuotedValue($value, $this->fields[$name]->FldDataType) . ",";
		}
		while (substr($sql, -1) == ",")
			$sql = substr($sql, 0, -1);
		$filter = $this->CurrentFilter;
		ew_AddFilter($filter, $where);
		if ($filter <> "")	$sql .= " WHERE " . $filter;
		return $sql;
	}

	// Update
	function Update(&$rs, $where = "") {
		global $conn;
		return $conn->Execute($this->UpdateSQL($rs, $where));
	}

	// DELETE statement
	function DeleteSQL(&$rs, $where = "") {
		$sql = "DELETE FROM " . $this->UpdateTable . " WHERE ";
		if ($rs) {
			$sql .= ew_QuotedName('Call Id') . '=' . ew_QuotedValue($rs['Call Id'], $this->Call_Id->FldDataType) . ' AND ';
		}
		if (substr($sql, -5) == " AND ") $sql = substr($sql, 0, -5);
		$filter = $this->CurrentFilter;
		ew_AddFilter($filter, $where);
		if ($filter <> "")	$sql .= " AND " . $filter;
		return $sql;
	}

	// Delete
	function Delete(&$rs, $where = "") {
		global $conn;
		return $conn->Execute($this->DeleteSQL($rs, $where));
	}

	// Key filter WHERE clause
	function SqlKeyFilter() {
		return "`Call Id` = @Call_Id@";
	}

	// Key filter
	function KeyFilter() {
		$sKeyFilter = $this->SqlKeyFilter();
		if (!is_numeric($this->Call_Id->CurrentValue))
			$sKeyFilter = "0=1"; // Invalid key
		$sKeyFilter = str_replace("@Call_Id@", ew_AdjustSql($this->Call_Id->CurrentValue), $sKeyFilter); // Replace key value
		return $sKeyFilter;
	}

	// Return page URL
	function getReturnUrl() {
		$name = EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL;

		// Get referer URL automatically
		if (ew_ServerVar("HTTP_REFERER") <> "" && ew_ReferPage() <> ew_CurrentPage() && ew_ReferPage() <> "login.php") // Referer not same page or login page
			$_SESSION[$name] = ew_ServerVar("HTTP_REFERER"); // Save to Session
		if (@$_SESSION[$name] <> "") {
			return $_SESSION[$name];
		} else {
			return "EnquiryDetailslist.php";
		}
	}

	function setReturnUrl($v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL] = $v;
	}

	// List URL
	function GetListUrl() {
		return "EnquiryDetailslist.php";
	}

	// View URL
	function GetViewUrl() {
		return $this->KeyUrl("EnquiryDetailsview.php", $this->UrlParm());
	}

	// Add URL
	function GetAddUrl() {
		return "EnquiryDetailsadd.php";
	}

	// Edit URL
	function GetEditUrl($parm = "") {
		return $this->KeyUrl("EnquiryDetailsedit.php", $this->UrlParm($parm));
	}

	// Inline edit URL
	function GetInlineEditUrl() {
		return $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=edit"));
	}

	// Copy URL
	function GetCopyUrl($parm = "") {
		return $this->KeyUrl("EnquiryDetailsadd.php", $this->UrlParm($parm));
	}

	// Inline copy URL
	function GetInlineCopyUrl() {
		return $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=copy"));
	}

	// Delete URL
	function GetDeleteUrl() {
		return $this->KeyUrl("EnquiryDetailsdelete.php", $this->UrlParm());
	}

	// Add key value to URL
	function KeyUrl($url, $parm = "") {
		$sUrl = $url . "?";
		if ($parm <> "") $sUrl .= $parm . "&";
		if (!is_null($this->Call_Id->CurrentValue)) {
			$sUrl .= "Call_Id=" . urlencode($this->Call_Id->CurrentValue);
		} else {
			return "javascript:alert(ewLanguage.Phrase('InvalidRecord'));";
		}
		return $sUrl;
	}

	// Sort URL
	function SortUrl(&$fld) {
		if ($this->CurrentAction <> "" || $this->Export <> "" ||
			in_array($fld->FldType, array(128, 204, 205))) { // Unsortable data type
				return "";
		} elseif ($fld->Sortable) {
			$sUrlParm = $this->UrlParm("order=" . urlencode($fld->FldName) . "&ordertype=" . $fld->ReverseSort());
			return ew_CurrentPage() . "?" . $sUrlParm;
		} else {
			return "";
		}
	}

	// Get record keys from $_POST/$_GET/$_SESSION
	function GetRecordKeys() {
		global $EW_COMPOSITE_KEY_SEPARATOR;
		$arKeys = array();
		$arKey = array();
		if (isset($_POST["key_m"])) {
			$arKeys = ew_StripSlashes($_POST["key_m"]);
			$cnt = count($arKeys);
		} elseif (isset($_GET["key_m"])) {
			$arKeys = ew_StripSlashes($_GET["key_m"]);
			$cnt = count($arKeys);
		} elseif (isset($_GET)) {
			$arKeys[] = @$_GET["Call_Id"]; // Call Id

			//return $arKeys; // do not return yet, so the values will also be checked by the following code
		}

		// check keys
		$ar = array();
		foreach ($arKeys as $key) {
			if (!is_numeric($key))
				continue;
			$ar[] = $key;
		}
		return $ar;
	}

	// Get key filter
	function GetKeyFilter() {
		$arKeys = $this->GetRecordKeys();
		$sKeyFilter = "";
		foreach ($arKeys as $key) {
			if ($sKeyFilter <> "") $sKeyFilter .= " OR ";
			$this->Call_Id->CurrentValue = $key;
			$sKeyFilter .= "(" . $this->KeyFilter() . ")";
		}
		return $sKeyFilter;
	}

	// Load rows based on filter
	function &LoadRs($sFilter) {
		global $conn;

		// Set up filter (SQL WHERE clause) and get return SQL
		//$this->CurrentFilter = $sFilter;
		//$sSql = $this->SQL();

		$sSql = $this->GetSQL($sFilter, "");
		$rs = $conn->Execute($sSql);
		return $rs;
	}

	// Load row values from recordset
	function LoadListRowValues(&$rs) {
		$this->Call_Id->setDbValue($rs->fields('Call Id'));
		$this->contactTime->setDbValue($rs->fields('contactTime'));
		$this->contactDescription->setDbValue($rs->fields('contactDescription'));
		$this->contactStaffName->setDbValue($rs->fields('contactStaffName'));
		$this->roleDescription->setDbValue($rs->fields('roleDescription'));
		$this->assetSerialNumber->setDbValue($rs->fields('assetSerialNumber'));
		$this->assetDescription->setDbValue($rs->fields('assetDescription'));
		$this->vendorName->setDbValue($rs->fields('vendorName'));
		$this->vendorDetails->setDbValue($rs->fields('vendorDetails'));
	}

	// Render list row values
	function RenderListRow() {
		global $conn, $Security;

		// Call Row Rendering event
		$this->Row_Rendering();

   // Common render codes
		// Call Id

		$this->Call_Id->CellCssStyle = "white-space: nowrap;";

		// contactTime
		// contactDescription
		// contactStaffName
		// roleDescription
		// assetSerialNumber
		// assetDescription
		// vendorName
		// vendorDetails
		// Call Id

		$this->Call_Id->ViewValue = $this->Call_Id->CurrentValue;
		$this->Call_Id->ViewCustomAttributes = "";

		// contactTime
		$this->contactTime->ViewValue = $this->contactTime->CurrentValue;
		$this->contactTime->ViewValue = ew_FormatDateTime($this->contactTime->ViewValue, 9);
		$this->contactTime->ViewCustomAttributes = "";

		// contactDescription
		$this->contactDescription->ViewValue = $this->contactDescription->CurrentValue;
		$this->contactDescription->ViewCustomAttributes = "";

		// contactStaffName
		$this->contactStaffName->ViewValue = $this->contactStaffName->CurrentValue;
		$this->contactStaffName->ViewCustomAttributes = "";

		// roleDescription
		$this->roleDescription->ViewValue = $this->roleDescription->CurrentValue;
		$this->roleDescription->ViewCustomAttributes = "";

		// assetSerialNumber
		if (strval($this->assetSerialNumber->CurrentValue) <> "") {
			$sFilterWrk = "`serialNumber`" . ew_SearchString("=", $this->assetSerialNumber->CurrentValue, EW_DATATYPE_NUMBER);
		$sSqlWrk = "SELECT `serialNumber`, `serialNumber` AS `DispFld`, `assetDescription` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `asset`";
		$sWhereWrk = "";
		if ($sFilterWrk <> "") {
			ew_AddFilter($sWhereWrk, $sFilterWrk);
		}
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = $conn->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$this->assetSerialNumber->ViewValue = $rswrk->fields('DispFld');
				$this->assetSerialNumber->ViewValue .= ew_ValueSeparator(1,$this->assetSerialNumber) . $rswrk->fields('Disp2Fld');
				$rswrk->Close();
			} else {
				$this->assetSerialNumber->ViewValue = $this->assetSerialNumber->CurrentValue;
			}
		} else {
			$this->assetSerialNumber->ViewValue = NULL;
		}
		$this->assetSerialNumber->ViewCustomAttributes = "";

		// assetDescription
		$this->assetDescription->ViewValue = $this->assetDescription->CurrentValue;
		$this->assetDescription->ViewCustomAttributes = "";

		// vendorName
		$this->vendorName->ViewValue = $this->vendorName->CurrentValue;
		$this->vendorName->ViewCustomAttributes = "";

		// vendorDetails
		$this->vendorDetails->ViewValue = $this->vendorDetails->CurrentValue;
		$this->vendorDetails->ViewCustomAttributes = "";

		// Call Id
		$this->Call_Id->LinkCustomAttributes = "";
		$this->Call_Id->HrefValue = "";
		$this->Call_Id->TooltipValue = "";

		// contactTime
		$this->contactTime->LinkCustomAttributes = "";
		$this->contactTime->HrefValue = "";
		$this->contactTime->TooltipValue = "";

		// contactDescription
		$this->contactDescription->LinkCustomAttributes = "";
		$this->contactDescription->HrefValue = "";
		$this->contactDescription->TooltipValue = "";

		// contactStaffName
		$this->contactStaffName->LinkCustomAttributes = "";
		$this->contactStaffName->HrefValue = "";
		$this->contactStaffName->TooltipValue = "";

		// roleDescription
		$this->roleDescription->LinkCustomAttributes = "";
		$this->roleDescription->HrefValue = "";
		$this->roleDescription->TooltipValue = "";

		// assetSerialNumber
		$this->assetSerialNumber->LinkCustomAttributes = "";
		$this->assetSerialNumber->HrefValue = "";
		$this->assetSerialNumber->TooltipValue = "";

		// assetDescription
		$this->assetDescription->LinkCustomAttributes = "";
		$this->assetDescription->HrefValue = "";
		$this->assetDescription->TooltipValue = "";

		// vendorName
		$this->vendorName->LinkCustomAttributes = "";
		$this->vendorName->HrefValue = "";
		$this->vendorName->TooltipValue = "";

		// vendorDetails
		$this->vendorDetails->LinkCustomAttributes = "";
		$this->vendorDetails->HrefValue = "";
		$this->vendorDetails->TooltipValue = "";

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Aggregate list row values
	function AggregateListRowValues() {
	}

	// Aggregate list row (for rendering)
	function AggregateListRow() {
	}

	// Export data in HTML/CSV/Word/Excel/Email/PDF format
	function ExportDocument(&$Doc, &$Recordset, $StartRec, $StopRec, $ExportPageType = "") {
		if (!$Recordset || !$Doc)
			return;

		// Write header
		$Doc->ExportTableHeader();
		if ($Doc->Horizontal) { // Horizontal format, write header
			$Doc->BeginExportRow();
			if ($ExportPageType == "view") {
				if ($this->Call_Id->Exportable) $Doc->ExportCaption($this->Call_Id);
				if ($this->contactTime->Exportable) $Doc->ExportCaption($this->contactTime);
				if ($this->contactDescription->Exportable) $Doc->ExportCaption($this->contactDescription);
				if ($this->contactStaffName->Exportable) $Doc->ExportCaption($this->contactStaffName);
				if ($this->roleDescription->Exportable) $Doc->ExportCaption($this->roleDescription);
				if ($this->assetSerialNumber->Exportable) $Doc->ExportCaption($this->assetSerialNumber);
				if ($this->assetDescription->Exportable) $Doc->ExportCaption($this->assetDescription);
				if ($this->vendorName->Exportable) $Doc->ExportCaption($this->vendorName);
				if ($this->vendorDetails->Exportable) $Doc->ExportCaption($this->vendorDetails);
			} else {
				if ($this->contactTime->Exportable) $Doc->ExportCaption($this->contactTime);
				if ($this->contactDescription->Exportable) $Doc->ExportCaption($this->contactDescription);
				if ($this->contactStaffName->Exportable) $Doc->ExportCaption($this->contactStaffName);
				if ($this->roleDescription->Exportable) $Doc->ExportCaption($this->roleDescription);
				if ($this->assetSerialNumber->Exportable) $Doc->ExportCaption($this->assetSerialNumber);
				if ($this->vendorName->Exportable) $Doc->ExportCaption($this->vendorName);
				if ($this->vendorDetails->Exportable) $Doc->ExportCaption($this->vendorDetails);
			}
			$Doc->EndExportRow();
		}

		// Move to first record
		$RecCnt = $StartRec - 1;
		if (!$Recordset->EOF) {
			$Recordset->MoveFirst();
			if ($StartRec > 1)
				$Recordset->Move($StartRec - 1);
		}
		while (!$Recordset->EOF && $RecCnt < $StopRec) {
			$RecCnt++;
			if (intval($RecCnt) >= intval($StartRec)) {
				$RowCnt = intval($RecCnt) - intval($StartRec) + 1;

				// Page break
				if ($this->ExportPageBreakCount > 0) {
					if ($RowCnt > 1 && ($RowCnt - 1) % $this->ExportPageBreakCount == 0)
						$Doc->ExportPageBreak();
				}
				$this->LoadListRowValues($Recordset);

				// Render row
				$this->RowType = EW_ROWTYPE_VIEW; // Render view
				$this->ResetAttrs();
				$this->RenderListRow();
				$Doc->BeginExportRow($RowCnt); // Allow CSS styles if enabled
				if ($ExportPageType == "view") {
					if ($this->Call_Id->Exportable) $Doc->ExportField($this->Call_Id);
					if ($this->contactTime->Exportable) $Doc->ExportField($this->contactTime);
					if ($this->contactDescription->Exportable) $Doc->ExportField($this->contactDescription);
					if ($this->contactStaffName->Exportable) $Doc->ExportField($this->contactStaffName);
					if ($this->roleDescription->Exportable) $Doc->ExportField($this->roleDescription);
					if ($this->assetSerialNumber->Exportable) $Doc->ExportField($this->assetSerialNumber);
					if ($this->assetDescription->Exportable) $Doc->ExportField($this->assetDescription);
					if ($this->vendorName->Exportable) $Doc->ExportField($this->vendorName);
					if ($this->vendorDetails->Exportable) $Doc->ExportField($this->vendorDetails);
				} else {
					if ($this->contactTime->Exportable) $Doc->ExportField($this->contactTime);
					if ($this->contactDescription->Exportable) $Doc->ExportField($this->contactDescription);
					if ($this->contactStaffName->Exportable) $Doc->ExportField($this->contactStaffName);
					if ($this->roleDescription->Exportable) $Doc->ExportField($this->roleDescription);
					if ($this->assetSerialNumber->Exportable) $Doc->ExportField($this->assetSerialNumber);
					if ($this->vendorName->Exportable) $Doc->ExportField($this->vendorName);
					if ($this->vendorDetails->Exportable) $Doc->ExportField($this->vendorDetails);
				}
				$Doc->EndExportRow();
			}
			$Recordset->MoveNext();
		}
		$Doc->ExportTableFooter();
	}

	// Table level events
	// Recordset Selecting event
	function Recordset_Selecting(&$filter) {

		// Enter your code here	
	}

	// Recordset Selected event
	function Recordset_Selected(&$rs) {

		//echo "Recordset Selected";
	}

	// Recordset Search Validated event
	function Recordset_SearchValidated() {

		// Example:
		//$this->MyField1->AdvancedSearch->SearchValue = "your search criteria"; // Search value

	}

	// Recordset Searching event
	function Recordset_Searching(&$filter) {

		// Enter your code here	
	}

	// Row_Selecting event
	function Row_Selecting(&$filter) {

		// Enter your code here	
	}

	// Row Selected event
	function Row_Selected(&$rs) {

		//echo "Row Selected";
	}

	// Row Inserting event
	function Row_Inserting($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Inserted event
	function Row_Inserted($rsold, &$rsnew) {

		//echo "Row Inserted"
	}

	// Row Updating event
	function Row_Updating($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Updated event
	function Row_Updated($rsold, &$rsnew) {

		//echo "Row Updated";
	}

	// Row Update Conflict event
	function Row_UpdateConflict($rsold, &$rsnew) {

		// Enter your code here
		// To ignore conflict, set return value to FALSE

		return TRUE;
	}

	// Row Deleting event
	function Row_Deleting(&$rs) {

		// Enter your code here
		// To cancel, set return value to False

		return TRUE;
	}

	// Row Deleted event
	function Row_Deleted(&$rs) {

		//echo "Row Deleted";
	}

	// Email Sending event
	function Email_Sending(&$Email, &$Args) {

		//var_dump($Email); var_dump($Args); exit();
		return TRUE;
	}

	// Row Rendering event
	function Row_Rendering() {

		// Enter your code here	
	}

	// Row Rendered event
	function Row_Rendered() {

		// To view properties of field class, use:
		//var_dump($this-><FieldName>); 

	}

	// User ID Filtering event
	function UserID_Filtering(&$filter) {

		// Enter your code here
	}
}
?>
