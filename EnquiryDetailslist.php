<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg9.php" ?>
<?php include_once "ewmysql9.php" ?>
<?php include_once "phpfn9.php" ?>
<?php include_once "EnquiryDetailsinfo.php" ?>
<?php include_once "staffinfo.php" ?>
<?php include_once "EnquiryMasterinfo.php" ?>
<?php include_once "userfn9.php" ?>
<?php

//
// Page class
//

$EnquiryDetails_list = NULL; // Initialize page object first

class cEnquiryDetails_list extends cEnquiryDetails {

	// Page ID
	var $PageID = 'list';

	// Project ID
	var $ProjectID = "{4F01D370-BA3A-4C91-9445-0AA347261657}";

	// Table name
	var $TableName = 'EnquiryDetails';

	// Page object name
	var $PageObjName = 'EnquiryDetails_list';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Page URLs
	var $AddUrl;
	var $EditUrl;
	var $CopyUrl;
	var $DeleteUrl;
	var $ViewUrl;
	var $ListUrl;

	// Export URLs
	var $ExportPrintUrl;
	var $ExportHtmlUrl;
	var $ExportExcelUrl;
	var $ExportWordUrl;
	var $ExportXmlUrl;
	var $ExportCsvUrl;
	var $ExportPdfUrl;

	// Update URLs
	var $InlineAddUrl;
	var $InlineCopyUrl;
	var $InlineEditUrl;
	var $GridAddUrl;
	var $GridEditUrl;
	var $MultiDeleteUrl;
	var $MultiUpdateUrl;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			$html .= "<p class=\"ewMessage\">" . $sMessage . "</p>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			$html .= "<table class=\"ewMessageTable\"><tr><td class=\"ewWarningIcon\"></td><td class=\"ewWarningMessage\">" . $sWarningMessage . "</td></tr></table>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			$html .= "<table class=\"ewMessageTable\"><tr><td class=\"ewSuccessIcon\"></td><td class=\"ewSuccessMessage\">" . $sSuccessMessage . "</td></tr></table>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			$html .= "<table class=\"ewMessageTable\"><tr><td class=\"ewErrorIcon\"></td><td class=\"ewErrorMessage\">" . $sErrorMessage . "</td></tr></table>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p class=\"phpmaker\">" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Fotoer exists, display
			echo "<p class=\"phpmaker\">" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language, $UserAgent;

		// User agent
		$UserAgent = ew_UserAgent();
		$GLOBALS["Page"] = &$this;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (EnquiryDetails)
		if (!isset($GLOBALS["EnquiryDetails"])) {
			$GLOBALS["EnquiryDetails"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["EnquiryDetails"];
		}

		// Initialize URLs
		$this->ExportPrintUrl = $this->PageUrl() . "export=print";
		$this->ExportExcelUrl = $this->PageUrl() . "export=excel";
		$this->ExportWordUrl = $this->PageUrl() . "export=word";
		$this->ExportHtmlUrl = $this->PageUrl() . "export=html";
		$this->ExportXmlUrl = $this->PageUrl() . "export=xml";
		$this->ExportCsvUrl = $this->PageUrl() . "export=csv";
		$this->ExportPdfUrl = $this->PageUrl() . "export=pdf";
		$this->AddUrl = "EnquiryDetailsadd.php";
		$this->InlineAddUrl = $this->PageUrl() . "a=add";
		$this->GridAddUrl = $this->PageUrl() . "a=gridadd";
		$this->GridEditUrl = $this->PageUrl() . "a=gridedit";
		$this->MultiDeleteUrl = "EnquiryDetailsdelete.php";
		$this->MultiUpdateUrl = "EnquiryDetailsupdate.php";

		// Table object (staff)
		if (!isset($GLOBALS['staff'])) $GLOBALS['staff'] = new cstaff();

		// Table object (EnquiryMaster)
		if (!isset($GLOBALS['EnquiryMaster'])) $GLOBALS['EnquiryMaster'] = new cEnquiryMaster();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'list', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'EnquiryDetails', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect();

		// List options
		$this->ListOptions = new cListOptions();
		$this->ListOptions->TableVar = $this->TableVar;

		// Export options
		$this->ExportOptions = new cListOptions();
		$this->ExportOptions->Tag = "span";
		$this->ExportOptions->TagClassName = "ewExportOption";
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if (!$Security->IsLoggedIn()) {
			$Security->SaveLastUrl();
			$this->Page_Terminate("login.php");
		}

		// Get export parameters
		if (@$_GET["export"] <> "") {
			$this->Export = $_GET["export"];
		} elseif (ew_IsHttpPost()) {
			if (@$_POST["exporttype"] <> "")
				$this->Export = $_POST["exporttype"];
		} else {
			$this->setExportReturnUrl(ew_CurrentUrl());
		}
		$gsExport = $this->Export; // Get export parameter, used in header
		$gsExportFile = $this->TableVar; // Get export file, used in header

		// Get grid add count
		$gridaddcnt = @$_GET[EW_TABLE_GRID_ADD_ROW_COUNT];
		if (is_numeric($gridaddcnt) && $gridaddcnt > 0)
			$this->GridAddRowCount = $gridaddcnt;

		// Set up list options
		$this->SetupListOptions();

		// Setup export options
		$this->SetupExportOptions();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"];
		$this->contactTime->Visible = !$this->IsAddOrEdit();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $conn;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();
		$this->Page_Redirecting($url);

		 // Close connection
		$conn->Close();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}

	// Class variables
	var $ListOptions; // List options
	var $ExportOptions; // Export options
	var $DisplayRecs = 20;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $Pager;
	var $SearchWhere = ""; // Search WHERE clause
	var $RecCnt = 0; // Record count
	var $EditRowCnt;
	var $StartRowCnt = 1;
	var $RowCnt = 0;
	var $Attrs = array(); // Row attributes and cell attributes
	var $RowIndex = 0; // Row index
	var $KeyCount = 0; // Key count
	var $RowAction = ""; // Row action
	var $RowOldKey = ""; // Row old key (for copy)
	var $RecPerRow = 0;
	var $ColCnt = 0;
	var $DbMasterFilter = ""; // Master filter
	var $DbDetailFilter = ""; // Detail filter
	var $MasterRecordExists;	
	var $MultiSelectKey;
	var $Command;
	var $Recordset;
	var $OldRecordset;

	//
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError, $gsSearchError, $Security;

		// Search filters
		$sSrchAdvanced = ""; // Advanced search filter
		$sSrchBasic = ""; // Basic search filter
		$sFilter = "";

		// Get command
		$this->Command = strtolower(@$_GET["cmd"]);
		if ($this->IsPageRequest()) { // Validate request

			// Handle reset command
			$this->ResetCmd();

			// Set up master detail parameters
			$this->SetUpMasterParms();

			// Hide all options
			if ($this->Export <> "" ||
				$this->CurrentAction == "gridadd" ||
				$this->CurrentAction == "gridedit") {
				$this->ListOptions->HideAllOptions();
				$this->ExportOptions->HideAllOptions();
			}

			// Set up sorting order
			$this->SetUpSortOrder();
		}

		// Restore display records
		if ($this->getRecordsPerPage() <> "") {
			$this->DisplayRecs = $this->getRecordsPerPage(); // Restore from Session
		} else {
			$this->DisplayRecs = 20; // Load default
		}

		// Load Sorting Order
		$this->LoadSortOrder();

		// Build filter
		$sFilter = "";

		// Restore master/detail filter
		$this->DbMasterFilter = $this->GetMasterFilter(); // Restore master filter
		$this->DbDetailFilter = $this->GetDetailFilter(); // Restore detail filter
		ew_AddFilter($sFilter, $this->DbDetailFilter);
		ew_AddFilter($sFilter, $this->SearchWhere);

		// Load master record
		if ($this->CurrentMode <> "add" && $this->GetMasterFilter() <> "" && $this->getCurrentMasterTable() == "EnquiryMaster") {
			global $EnquiryMaster;
			$rsmaster = $EnquiryMaster->LoadRs($this->DbMasterFilter);
			$this->MasterRecordExists = ($rsmaster && !$rsmaster->EOF);
			if (!$this->MasterRecordExists) {
				$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record found
				$this->Page_Terminate("EnquiryMasterlist.php"); // Return to master page
			} else {
				$EnquiryMaster->LoadListRowValues($rsmaster);
				$EnquiryMaster->RowType = EW_ROWTYPE_MASTER; // Master row
				$EnquiryMaster->RenderListRow();
				$rsmaster->Close();
			}
		}

		// Set up filter in session
		$this->setSessionWhere($sFilter);
		$this->CurrentFilter = "";

		// Export data only
		if (in_array($this->Export, array("html","word","excel","xml","csv","email","pdf"))) {
			$this->ExportData();
			if ($this->Export == "email")
				$this->Page_Terminate($this->ExportReturnUrl());
			else
				$this->Page_Terminate(); // Terminate response
			exit();
		}
	}

	// Build filter for all keys
	function BuildKeyFilter() {
		global $objForm;
		$sWrkFilter = "";

		// Update row index and get row key
		$rowindex = 1;
		$objForm->Index = $rowindex;
		$sThisKey = strval($objForm->GetValue("k_key"));
		while ($sThisKey <> "") {
			if ($this->SetupKeyValues($sThisKey)) {
				$sFilter = $this->KeyFilter();
				if ($sWrkFilter <> "") $sWrkFilter .= " OR ";
				$sWrkFilter .= $sFilter;
			} else {
				$sWrkFilter = "0=1";
				break;
			}

			// Update row index and get row key
			$rowindex++; // next row
			$objForm->Index = $rowindex;
			$sThisKey = strval($objForm->GetValue("k_key"));
		}
		return $sWrkFilter;
	}

	// Set up key values
	function SetupKeyValues($key) {
		$arrKeyFlds = explode($GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"], $key);
		if (count($arrKeyFlds) >= 1) {
			$this->Call_Id->setFormValue($arrKeyFlds[0]);
			if (!is_numeric($this->Call_Id->FormValue))
				return FALSE;
		}
		return TRUE;
	}

	// Set up sort parameters
	function SetUpSortOrder() {

		// Check for "order" parameter
		if (@$_GET["order"] <> "") {
			$this->CurrentOrder = ew_StripSlashes(@$_GET["order"]);
			$this->CurrentOrderType = @$_GET["ordertype"];
			$this->UpdateSort($this->contactTime); // contactTime
			$this->UpdateSort($this->contactDescription); // contactDescription
			$this->UpdateSort($this->contactStaffName); // contactStaffName
			$this->UpdateSort($this->roleDescription); // roleDescription
			$this->UpdateSort($this->assetSerialNumber); // assetSerialNumber
			$this->UpdateSort($this->assetDescription); // assetDescription
			$this->UpdateSort($this->vendorName); // vendorName
			$this->UpdateSort($this->vendorDetails); // vendorDetails
			$this->setStartRecordNumber(1); // Reset start position
		}
	}

	// Load sort order parameters
	function LoadSortOrder() {
		$sOrderBy = $this->getSessionOrderBy(); // Get ORDER BY from Session
		if ($sOrderBy == "") {
			if ($this->SqlOrderBy() <> "") {
				$sOrderBy = $this->SqlOrderBy();
				$this->setSessionOrderBy($sOrderBy);
			}
		}
	}

	// Reset command
	// cmd=reset (Reset search parameters)
	// cmd=resetall (Reset search and master/detail parameters)
	// cmd=resetsort (Reset sort parameters)
	function ResetCmd() {

		// Check if reset command
		if (substr($this->Command,0,5) == "reset") {

			// Reset master/detail keys
			if ($this->Command == "resetall") {
				$this->setCurrentMasterTable(""); // Clear master table
				$this->DbMasterFilter = "";
				$this->DbDetailFilter = "";
				$this->Call_Id->setSessionValue("");
			}

			// Reset sorting order
			if ($this->Command == "resetsort") {
				$sOrderBy = "";
				$this->setSessionOrderBy($sOrderBy);
				$this->contactTime->setSort("");
				$this->contactDescription->setSort("");
				$this->contactStaffName->setSort("");
				$this->roleDescription->setSort("");
				$this->assetSerialNumber->setSort("");
				$this->assetDescription->setSort("");
				$this->vendorName->setSort("");
				$this->vendorDetails->setSort("");
			}

			// Reset start position
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Set up list options
	function SetupListOptions() {
		global $Security, $Language;

		// Call ListOptions_Load event
		$this->ListOptions_Load();
	}

	// Render list options
	function RenderListOptions() {
		global $Security, $Language, $objForm;
		$this->ListOptions->LoadDefault();
		$this->RenderListOptionsExt();

		// Call ListOptions_Rendered event
		$this->ListOptions_Rendered();
	}

	function RenderListOptionsExt() {
		global $Security, $Language;
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {
		global $conn;

		// Call Recordset Selecting event
		$this->Recordset_Selecting($this->CurrentFilter);

		// Load List page SQL
		$sSql = $this->SelectSQL();
		if ($offset > -1 && $rowcnt > -1)
			$sSql .= " LIMIT $rowcnt OFFSET $offset";

		// Load recordset
		$rs = ew_LoadRecordset($sSql);

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $conn, $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		global $conn;
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->Call_Id->setDbValue($rs->fields('Call Id'));
		$this->contactTime->setDbValue($rs->fields('contactTime'));
		$this->contactDescription->setDbValue($rs->fields('contactDescription'));
		$this->contactStaffName->setDbValue($rs->fields('contactStaffName'));
		$this->roleDescription->setDbValue($rs->fields('roleDescription'));
		$this->assetSerialNumber->setDbValue($rs->fields('assetSerialNumber'));
		$this->assetDescription->setDbValue($rs->fields('assetDescription'));
		$this->vendorName->setDbValue($rs->fields('vendorName'));
		$this->vendorDetails->setDbValue($rs->fields('vendorDetails'));
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("Call_Id")) <> "")
			$this->Call_Id->CurrentValue = $this->getKey("Call_Id"); // Call Id
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$this->OldRecordset = ew_LoadRecordset($sSql);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $conn, $Security, $Language;
		global $gsLanguage;

		// Initialize URLs
		$this->ViewUrl = $this->GetViewUrl();
		$this->EditUrl = $this->GetEditUrl();
		$this->InlineEditUrl = $this->GetInlineEditUrl();
		$this->CopyUrl = $this->GetCopyUrl();
		$this->InlineCopyUrl = $this->GetInlineCopyUrl();
		$this->DeleteUrl = $this->GetDeleteUrl();

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// Call Id

		$this->Call_Id->CellCssStyle = "white-space: nowrap;";

		// contactTime
		// contactDescription
		// contactStaffName
		// roleDescription
		// assetSerialNumber
		// assetDescription
		// vendorName
		// vendorDetails

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

			// contactTime
			$this->contactTime->ViewValue = $this->contactTime->CurrentValue;
			$this->contactTime->ViewValue = ew_FormatDateTime($this->contactTime->ViewValue, 9);
			$this->contactTime->ViewCustomAttributes = "";

			// contactDescription
			$this->contactDescription->ViewValue = $this->contactDescription->CurrentValue;
			$this->contactDescription->ViewCustomAttributes = "";

			// contactStaffName
			$this->contactStaffName->ViewValue = $this->contactStaffName->CurrentValue;
			$this->contactStaffName->ViewCustomAttributes = "";

			// roleDescription
			$this->roleDescription->ViewValue = $this->roleDescription->CurrentValue;
			$this->roleDescription->ViewCustomAttributes = "";

			// assetSerialNumber
			if (strval($this->assetSerialNumber->CurrentValue) <> "") {
				$sFilterWrk = "`serialNumber`" . ew_SearchString("=", $this->assetSerialNumber->CurrentValue, EW_DATATYPE_NUMBER);
			$sSqlWrk = "SELECT `serialNumber`, `serialNumber` AS `DispFld`, `assetDescription` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `asset`";
			$sWhereWrk = "";
			if ($sFilterWrk <> "") {
				ew_AddFilter($sWhereWrk, $sFilterWrk);
			}
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
				$rswrk = $conn->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$this->assetSerialNumber->ViewValue = $rswrk->fields('DispFld');
					$this->assetSerialNumber->ViewValue .= ew_ValueSeparator(1,$this->assetSerialNumber) . $rswrk->fields('Disp2Fld');
					$rswrk->Close();
				} else {
					$this->assetSerialNumber->ViewValue = $this->assetSerialNumber->CurrentValue;
				}
			} else {
				$this->assetSerialNumber->ViewValue = NULL;
			}
			$this->assetSerialNumber->ViewCustomAttributes = "";

			// assetDescription
			$this->assetDescription->ViewValue = $this->assetDescription->CurrentValue;
			$this->assetDescription->ViewCustomAttributes = "";

			// vendorName
			$this->vendorName->ViewValue = $this->vendorName->CurrentValue;
			$this->vendorName->ViewCustomAttributes = "";

			// vendorDetails
			$this->vendorDetails->ViewValue = $this->vendorDetails->CurrentValue;
			$this->vendorDetails->ViewCustomAttributes = "";

			// contactTime
			$this->contactTime->LinkCustomAttributes = "";
			$this->contactTime->HrefValue = "";
			$this->contactTime->TooltipValue = "";

			// contactDescription
			$this->contactDescription->LinkCustomAttributes = "";
			$this->contactDescription->HrefValue = "";
			$this->contactDescription->TooltipValue = "";

			// contactStaffName
			$this->contactStaffName->LinkCustomAttributes = "";
			$this->contactStaffName->HrefValue = "";
			$this->contactStaffName->TooltipValue = "";

			// roleDescription
			$this->roleDescription->LinkCustomAttributes = "";
			$this->roleDescription->HrefValue = "";
			$this->roleDescription->TooltipValue = "";

			// assetSerialNumber
			$this->assetSerialNumber->LinkCustomAttributes = "";
			$this->assetSerialNumber->HrefValue = "";
			$this->assetSerialNumber->TooltipValue = "";

			// assetDescription
			$this->assetDescription->LinkCustomAttributes = "";
			$this->assetDescription->HrefValue = "";
			$this->assetDescription->TooltipValue = "";

			// vendorName
			$this->vendorName->LinkCustomAttributes = "";
			$this->vendorName->HrefValue = "";
			$this->vendorName->TooltipValue = "";

			// vendorDetails
			$this->vendorDetails->LinkCustomAttributes = "";
			$this->vendorDetails->HrefValue = "";
			$this->vendorDetails->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Set up export options
	function SetupExportOptions() {
		global $Language;

		// Printer friendly
		$item = &$this->ExportOptions->Add("print");
		$item->Body = "<a href=\"" . $this->ExportPrintUrl . "\">" . $Language->Phrase("PrinterFriendly") . "</a>";
		$item->Visible = FALSE;

		// Export to Excel
		$item = &$this->ExportOptions->Add("excel");
		$item->Body = "<a href=\"" . $this->ExportExcelUrl . "\">" . $Language->Phrase("ExportToExcel") . "</a>";
		$item->Visible = FALSE;

		// Export to Word
		$item = &$this->ExportOptions->Add("word");
		$item->Body = "<a href=\"" . $this->ExportWordUrl . "\">" . $Language->Phrase("ExportToWord") . "</a>";
		$item->Visible = FALSE;

		// Export to Html
		$item = &$this->ExportOptions->Add("html");
		$item->Body = "<a href=\"" . $this->ExportHtmlUrl . "\">" . $Language->Phrase("ExportToHtml") . "</a>";
		$item->Visible = FALSE;

		// Export to Xml
		$item = &$this->ExportOptions->Add("xml");
		$item->Body = "<a href=\"" . $this->ExportXmlUrl . "\">" . $Language->Phrase("ExportToXml") . "</a>";
		$item->Visible = FALSE;

		// Export to Csv
		$item = &$this->ExportOptions->Add("csv");
		$item->Body = "<a href=\"" . $this->ExportCsvUrl . "\">" . $Language->Phrase("ExportToCsv") . "</a>";
		$item->Visible = TRUE;

		// Export to Pdf
		$item = &$this->ExportOptions->Add("pdf");
		$item->Body = "<a href=\"" . $this->ExportPdfUrl . "\">" . $Language->Phrase("ExportToPDF") . "</a>";
		$item->Visible = FALSE;

		// Export to Email
		$item = &$this->ExportOptions->Add("email");
		$item->Body = "<a id=\"emf_EnquiryDetails\" href=\"javascript:void(0);\" onclick=\"ew_EmailDialogShow({lnk:'emf_EnquiryDetails',hdr:ewLanguage.Phrase('ExportToEmail'),f:document.fEnquiryDetailslist,sel:false});\">" . $Language->Phrase("ExportToEmail") . "</a>";
		$item->Visible = FALSE;

		// Hide options for export/action
		if ($this->Export <> "" || $this->CurrentAction <> "")
			$this->ExportOptions->HideAllOptions();
	}

	// Export data in HTML/CSV/Word/Excel/XML/Email/PDF format
	function ExportData() {
		$utf8 = (strtolower(EW_CHARSET) == "utf-8");
		$bSelectLimit = EW_SELECT_LIMIT;

		// Load recordset
		if ($bSelectLimit) {
			$this->TotalRecs = $this->SelectRecordCount();
		} else {
			if ($rs = $this->LoadRecordset())
				$this->TotalRecs = $rs->RecordCount();
		}
		$this->StartRec = 1;

		// Export all
		if ($this->ExportAll) {
			set_time_limit(EW_EXPORT_ALL_TIME_LIMIT);
			$this->DisplayRecs = $this->TotalRecs;
			$this->StopRec = $this->TotalRecs;
		} else { // Export one page only
			$this->SetUpStartRec(); // Set up start record position

			// Set the last record to display
			if ($this->DisplayRecs <= 0) {
				$this->StopRec = $this->TotalRecs;
			} else {
				$this->StopRec = $this->StartRec + $this->DisplayRecs - 1;
			}
		}
		if ($bSelectLimit)
			$rs = $this->LoadRecordset($this->StartRec-1, $this->DisplayRecs <= 0 ? $this->TotalRecs : $this->DisplayRecs);
		if (!$rs) {
			header("Content-Type:"); // Remove header
			header("Content-Disposition:");
			$this->ShowMessage();
			return;
		}
		$ExportDoc = ew_ExportDocument($this, "h");
		$ParentTable = "";

		// Export master record
		if (EW_EXPORT_MASTER_RECORD && $this->GetMasterFilter() <> "" && $this->getCurrentMasterTable() == "EnquiryMaster") {
			global $EnquiryMaster;
			$rsmaster = $EnquiryMaster->LoadRs($this->DbMasterFilter); // Load master record
			if ($rsmaster && !$rsmaster->EOF) {
				$ExportStyle = $ExportDoc->Style;
				$ExportDoc->SetStyle("v"); // Change to vertical
				if ($this->Export <> "csv" || EW_EXPORT_MASTER_RECORD_FOR_CSV) {
					$EnquiryMaster->ExportDocument($ExportDoc, $rsmaster, 1, 1);
					$ExportDoc->ExportEmptyRow();
				}
				$ExportDoc->SetStyle($ExportStyle); // Restore
				$rsmaster->Close();
			}
		}
		if ($bSelectLimit) {
			$StartRec = 1;
			$StopRec = $this->DisplayRecs <= 0 ? $this->TotalRecs : $this->DisplayRecs;
		} else {
			$StartRec = $this->StartRec;
			$StopRec = $this->StopRec;
		}
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		$ExportDoc->Text .= $sHeader;
		$this->ExportDocument($ExportDoc, $rs, $StartRec, $StopRec, "");
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		$ExportDoc->Text .= $sFooter;

		// Close recordset
		$rs->Close();

		// Export header and footer
		$ExportDoc->ExportHeaderAndFooter();

		// Clean output buffer
		if (!EW_DEBUG_ENABLED && ob_get_length())
			ob_end_clean();

		// Write debug message if enabled
		if (EW_DEBUG_ENABLED)
			echo ew_DebugMsg();

		// Output data
		$ExportDoc->Export();
	}

	// Set up master/detail based on QueryString
	function SetUpMasterParms() {
		$bValidMaster = FALSE;

		// Get the keys for master table
		if (isset($_GET[EW_TABLE_SHOW_MASTER])) {
			$sMasterTblVar = $_GET[EW_TABLE_SHOW_MASTER];
			if ($sMasterTblVar == "") {
				$bValidMaster = TRUE;
				$this->DbMasterFilter = "";
				$this->DbDetailFilter = "";
			}
			if ($sMasterTblVar == "EnquiryMaster") {
				$bValidMaster = TRUE;
				if (@$_GET["CallId"] <> "") {
					$GLOBALS["EnquiryMaster"]->CallId->setQueryStringValue($_GET["CallId"]);
					$this->Call_Id->setQueryStringValue($GLOBALS["EnquiryMaster"]->CallId->QueryStringValue);
					$this->Call_Id->setSessionValue($this->Call_Id->QueryStringValue);
					if (!is_numeric($GLOBALS["EnquiryMaster"]->CallId->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
		}
		if ($bValidMaster) {

			// Save current master table
			$this->setCurrentMasterTable($sMasterTblVar);

			// Reset start record counter (new master key)
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);

			// Clear previous master key from Session
			if ($sMasterTblVar <> "EnquiryMaster") {
				if ($this->Call_Id->QueryStringValue == "") $this->Call_Id->setSessionValue("");
			}
		}
		$this->DbMasterFilter = $this->GetMasterFilter(); //  Get master filter
		$this->DbDetailFilter = $this->GetDetailFilter(); // Get detail filter
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}

	// ListOptions Load event
	function ListOptions_Load() {

		// Example:
		//$opt = &$this->ListOptions->Add("new");
		//$opt->Header = "xxx";
		//$opt->OnLeft = TRUE; // Link on left
		//$opt->MoveTo(0); // Move to first column

	}

	// ListOptions Rendered event
	function ListOptions_Rendered() {

		// Example: 
		//$this->ListOptions->Items["new"]->Body = "xxx";

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($EnquiryDetails_list)) $EnquiryDetails_list = new cEnquiryDetails_list();

// Page init
$EnquiryDetails_list->Page_Init();

// Page main
$EnquiryDetails_list->Page_Main();
?>
<?php include_once "header.php" ?>
<?php if ($EnquiryDetails->Export == "") { ?>
<script type="text/javascript">

// Page object
var EnquiryDetails_list = new ew_Page("EnquiryDetails_list");
EnquiryDetails_list.PageID = "list"; // Page ID
var EW_PAGE_ID = EnquiryDetails_list.PageID; // For backward compatibility

// Form object
var fEnquiryDetailslist = new ew_Form("fEnquiryDetailslist");

// Form_CustomValidate event
fEnquiryDetailslist.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fEnquiryDetailslist.ValidateRequired = true;
<?php } else { ?>
fEnquiryDetailslist.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fEnquiryDetailslist.Lists["x_assetSerialNumber"] = {"LinkField":"x_serialNumber","Ajax":null,"AutoFill":false,"DisplayFields":["x_serialNumber","x_assetDescription","",""],"ParentFields":[],"FilterFields":[],"Options":[]};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php } ?>
<?php if (($EnquiryDetails->Export == "") || (EW_EXPORT_MASTER_RECORD && $EnquiryDetails->Export == "print")) { ?>
<?php
$gsMasterReturnUrl = "EnquiryMasterlist.php";
if ($EnquiryDetails_list->DbMasterFilter <> "" && $EnquiryDetails->getCurrentMasterTable() == "EnquiryMaster") {
	if ($EnquiryDetails_list->MasterRecordExists) {
		if ($EnquiryDetails->getCurrentMasterTable() == $EnquiryDetails->TableVar) $gsMasterReturnUrl .= "?" . EW_TABLE_SHOW_MASTER . "=";
?>
<p><span class="ewTitle ewMasterTableTitle"><?php echo $Language->Phrase("MasterRecord") ?><?php echo $EnquiryMaster->TableCaption() ?>&nbsp;&nbsp;</span><?php $EnquiryDetails_list->ExportOptions->Render("body"); ?>
</p>
<?php if ($EnquiryDetails->Export == "") { ?>
<p class="phpmaker"><a href="<?php echo $gsMasterReturnUrl ?>"><?php echo $Language->Phrase("BackToMasterRecordPage") ?></a></p>
<?php } ?>
<?php include_once "EnquiryMastermaster.php" ?>
<?php
	}
}
?>
<?php } ?>
<?php
	$bSelectLimit = EW_SELECT_LIMIT;
	if ($bSelectLimit) {
		$EnquiryDetails_list->TotalRecs = $EnquiryDetails->SelectRecordCount();
	} else {
		if ($EnquiryDetails_list->Recordset = $EnquiryDetails_list->LoadRecordset())
			$EnquiryDetails_list->TotalRecs = $EnquiryDetails_list->Recordset->RecordCount();
	}
	$EnquiryDetails_list->StartRec = 1;
	if ($EnquiryDetails_list->DisplayRecs <= 0 || ($EnquiryDetails->Export <> "" && $EnquiryDetails->ExportAll)) // Display all records
		$EnquiryDetails_list->DisplayRecs = $EnquiryDetails_list->TotalRecs;
	if (!($EnquiryDetails->Export <> "" && $EnquiryDetails->ExportAll))
		$EnquiryDetails_list->SetUpStartRec(); // Set up start record position
	if ($bSelectLimit)
		$EnquiryDetails_list->Recordset = $EnquiryDetails_list->LoadRecordset($EnquiryDetails_list->StartRec-1, $EnquiryDetails_list->DisplayRecs);
?>
<p style="white-space: nowrap;"><span id="ewPageCaption" class="ewTitle ewTableTitle"><?php echo $Language->Phrase("TblTypeVIEW") ?><?php echo $EnquiryDetails->TableCaption() ?>&nbsp;&nbsp;</span>
<?php if ($EnquiryDetails->getCurrentMasterTable() == "") { ?>
<?php $EnquiryDetails_list->ExportOptions->Render("body"); ?>
<?php } ?>
</p>
<?php $EnquiryDetails_list->ShowPageHeader(); ?>
<?php
$EnquiryDetails_list->ShowMessage();
?>
<br>
<table cellspacing="0" class="ewGrid"><tr><td class="ewGridContent">
<form name="fEnquiryDetailslist" id="fEnquiryDetailslist" class="ewForm" action="" method="post">
<input type="hidden" name="t" value="EnquiryDetails">
<div id="gmp_EnquiryDetails" class="ewGridMiddlePanel">
<?php if ($EnquiryDetails_list->TotalRecs > 0) { ?>
<table id="tbl_EnquiryDetailslist" class="ewTable ewTableSeparate">
<?php echo $EnquiryDetails->TableCustomInnerHtml ?>
<thead><!-- Table header -->
	<tr class="ewTableHeader">
<?php

// Render list options
$EnquiryDetails_list->RenderListOptions();

// Render list options (header, left)
$EnquiryDetails_list->ListOptions->Render("header", "left");
?>
<?php if ($EnquiryDetails->contactTime->Visible) { // contactTime ?>
	<?php if ($EnquiryDetails->SortUrl($EnquiryDetails->contactTime) == "") { ?>
		<td><span id="elh_EnquiryDetails_contactTime" class="EnquiryDetails_contactTime"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $EnquiryDetails->contactTime->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div onmousedown="ew_Sort(event,'<?php echo $EnquiryDetails->SortUrl($EnquiryDetails->contactTime) ?>',1);"><span id="elh_EnquiryDetails_contactTime" class="EnquiryDetails_contactTime">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $EnquiryDetails->contactTime->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($EnquiryDetails->contactTime->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($EnquiryDetails->contactTime->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($EnquiryDetails->contactDescription->Visible) { // contactDescription ?>
	<?php if ($EnquiryDetails->SortUrl($EnquiryDetails->contactDescription) == "") { ?>
		<td><span id="elh_EnquiryDetails_contactDescription" class="EnquiryDetails_contactDescription"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $EnquiryDetails->contactDescription->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div onmousedown="ew_Sort(event,'<?php echo $EnquiryDetails->SortUrl($EnquiryDetails->contactDescription) ?>',1);"><span id="elh_EnquiryDetails_contactDescription" class="EnquiryDetails_contactDescription">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $EnquiryDetails->contactDescription->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($EnquiryDetails->contactDescription->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($EnquiryDetails->contactDescription->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($EnquiryDetails->contactStaffName->Visible) { // contactStaffName ?>
	<?php if ($EnquiryDetails->SortUrl($EnquiryDetails->contactStaffName) == "") { ?>
		<td><span id="elh_EnquiryDetails_contactStaffName" class="EnquiryDetails_contactStaffName"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $EnquiryDetails->contactStaffName->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div onmousedown="ew_Sort(event,'<?php echo $EnquiryDetails->SortUrl($EnquiryDetails->contactStaffName) ?>',1);"><span id="elh_EnquiryDetails_contactStaffName" class="EnquiryDetails_contactStaffName">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $EnquiryDetails->contactStaffName->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($EnquiryDetails->contactStaffName->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($EnquiryDetails->contactStaffName->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($EnquiryDetails->roleDescription->Visible) { // roleDescription ?>
	<?php if ($EnquiryDetails->SortUrl($EnquiryDetails->roleDescription) == "") { ?>
		<td><span id="elh_EnquiryDetails_roleDescription" class="EnquiryDetails_roleDescription"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $EnquiryDetails->roleDescription->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div onmousedown="ew_Sort(event,'<?php echo $EnquiryDetails->SortUrl($EnquiryDetails->roleDescription) ?>',1);"><span id="elh_EnquiryDetails_roleDescription" class="EnquiryDetails_roleDescription">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $EnquiryDetails->roleDescription->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($EnquiryDetails->roleDescription->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($EnquiryDetails->roleDescription->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($EnquiryDetails->assetSerialNumber->Visible) { // assetSerialNumber ?>
	<?php if ($EnquiryDetails->SortUrl($EnquiryDetails->assetSerialNumber) == "") { ?>
		<td><span id="elh_EnquiryDetails_assetSerialNumber" class="EnquiryDetails_assetSerialNumber"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $EnquiryDetails->assetSerialNumber->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div onmousedown="ew_Sort(event,'<?php echo $EnquiryDetails->SortUrl($EnquiryDetails->assetSerialNumber) ?>',1);"><span id="elh_EnquiryDetails_assetSerialNumber" class="EnquiryDetails_assetSerialNumber">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $EnquiryDetails->assetSerialNumber->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($EnquiryDetails->assetSerialNumber->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($EnquiryDetails->assetSerialNumber->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($EnquiryDetails->assetDescription->Visible) { // assetDescription ?>
	<?php if ($EnquiryDetails->SortUrl($EnquiryDetails->assetDescription) == "") { ?>
		<td><span id="elh_EnquiryDetails_assetDescription" class="EnquiryDetails_assetDescription"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $EnquiryDetails->assetDescription->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div onmousedown="ew_Sort(event,'<?php echo $EnquiryDetails->SortUrl($EnquiryDetails->assetDescription) ?>',1);"><span id="elh_EnquiryDetails_assetDescription" class="EnquiryDetails_assetDescription">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $EnquiryDetails->assetDescription->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($EnquiryDetails->assetDescription->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($EnquiryDetails->assetDescription->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($EnquiryDetails->vendorName->Visible) { // vendorName ?>
	<?php if ($EnquiryDetails->SortUrl($EnquiryDetails->vendorName) == "") { ?>
		<td><span id="elh_EnquiryDetails_vendorName" class="EnquiryDetails_vendorName"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $EnquiryDetails->vendorName->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div onmousedown="ew_Sort(event,'<?php echo $EnquiryDetails->SortUrl($EnquiryDetails->vendorName) ?>',1);"><span id="elh_EnquiryDetails_vendorName" class="EnquiryDetails_vendorName">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $EnquiryDetails->vendorName->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($EnquiryDetails->vendorName->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($EnquiryDetails->vendorName->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($EnquiryDetails->vendorDetails->Visible) { // vendorDetails ?>
	<?php if ($EnquiryDetails->SortUrl($EnquiryDetails->vendorDetails) == "") { ?>
		<td><span id="elh_EnquiryDetails_vendorDetails" class="EnquiryDetails_vendorDetails"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $EnquiryDetails->vendorDetails->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div onmousedown="ew_Sort(event,'<?php echo $EnquiryDetails->SortUrl($EnquiryDetails->vendorDetails) ?>',1);"><span id="elh_EnquiryDetails_vendorDetails" class="EnquiryDetails_vendorDetails">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $EnquiryDetails->vendorDetails->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($EnquiryDetails->vendorDetails->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($EnquiryDetails->vendorDetails->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php

// Render list options (header, right)
$EnquiryDetails_list->ListOptions->Render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
if ($EnquiryDetails->ExportAll && $EnquiryDetails->Export <> "") {
	$EnquiryDetails_list->StopRec = $EnquiryDetails_list->TotalRecs;
} else {

	// Set the last record to display
	if ($EnquiryDetails_list->TotalRecs > $EnquiryDetails_list->StartRec + $EnquiryDetails_list->DisplayRecs - 1)
		$EnquiryDetails_list->StopRec = $EnquiryDetails_list->StartRec + $EnquiryDetails_list->DisplayRecs - 1;
	else
		$EnquiryDetails_list->StopRec = $EnquiryDetails_list->TotalRecs;
}
$EnquiryDetails_list->RecCnt = $EnquiryDetails_list->StartRec - 1;
if ($EnquiryDetails_list->Recordset && !$EnquiryDetails_list->Recordset->EOF) {
	$EnquiryDetails_list->Recordset->MoveFirst();
	if (!$bSelectLimit && $EnquiryDetails_list->StartRec > 1)
		$EnquiryDetails_list->Recordset->Move($EnquiryDetails_list->StartRec - 1);
} elseif (!$EnquiryDetails->AllowAddDeleteRow && $EnquiryDetails_list->StopRec == 0) {
	$EnquiryDetails_list->StopRec = $EnquiryDetails->GridAddRowCount;
}

// Initialize aggregate
$EnquiryDetails->RowType = EW_ROWTYPE_AGGREGATEINIT;
$EnquiryDetails->ResetAttrs();
$EnquiryDetails_list->RenderRow();
while ($EnquiryDetails_list->RecCnt < $EnquiryDetails_list->StopRec) {
	$EnquiryDetails_list->RecCnt++;
	if (intval($EnquiryDetails_list->RecCnt) >= intval($EnquiryDetails_list->StartRec)) {
		$EnquiryDetails_list->RowCnt++;

		// Set up key count
		$EnquiryDetails_list->KeyCount = $EnquiryDetails_list->RowIndex;

		// Init row class and style
		$EnquiryDetails->ResetAttrs();
		$EnquiryDetails->CssClass = "";
		if ($EnquiryDetails->CurrentAction == "gridadd") {
		} else {
			$EnquiryDetails_list->LoadRowValues($EnquiryDetails_list->Recordset); // Load row values
		}
		$EnquiryDetails->RowType = EW_ROWTYPE_VIEW; // Render view

		// Set up row id / data-rowindex
		$EnquiryDetails->RowAttrs = array_merge($EnquiryDetails->RowAttrs, array('data-rowindex'=>$EnquiryDetails_list->RowCnt, 'id'=>'r' . $EnquiryDetails_list->RowCnt . '_EnquiryDetails', 'data-rowtype'=>$EnquiryDetails->RowType));

		// Render row
		$EnquiryDetails_list->RenderRow();

		// Render list options
		$EnquiryDetails_list->RenderListOptions();
?>
	<tr<?php echo $EnquiryDetails->RowAttributes() ?>>
<?php

// Render list options (body, left)
$EnquiryDetails_list->ListOptions->Render("body", "left", $EnquiryDetails_list->RowCnt);
?>
	<?php if ($EnquiryDetails->contactTime->Visible) { // contactTime ?>
		<td<?php echo $EnquiryDetails->contactTime->CellAttributes() ?>><span id="el<?php echo $EnquiryDetails_list->RowCnt ?>_EnquiryDetails_contactTime" class="EnquiryDetails_contactTime">
<span<?php echo $EnquiryDetails->contactTime->ViewAttributes() ?>>
<?php echo $EnquiryDetails->contactTime->ListViewValue() ?></span>
</span><a id="<?php echo $EnquiryDetails_list->PageObjName . "_row_" . $EnquiryDetails_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($EnquiryDetails->contactDescription->Visible) { // contactDescription ?>
		<td<?php echo $EnquiryDetails->contactDescription->CellAttributes() ?>><span id="el<?php echo $EnquiryDetails_list->RowCnt ?>_EnquiryDetails_contactDescription" class="EnquiryDetails_contactDescription">
<span<?php echo $EnquiryDetails->contactDescription->ViewAttributes() ?>>
<?php echo $EnquiryDetails->contactDescription->ListViewValue() ?></span>
</span><a id="<?php echo $EnquiryDetails_list->PageObjName . "_row_" . $EnquiryDetails_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($EnquiryDetails->contactStaffName->Visible) { // contactStaffName ?>
		<td<?php echo $EnquiryDetails->contactStaffName->CellAttributes() ?>><span id="el<?php echo $EnquiryDetails_list->RowCnt ?>_EnquiryDetails_contactStaffName" class="EnquiryDetails_contactStaffName">
<span<?php echo $EnquiryDetails->contactStaffName->ViewAttributes() ?>>
<?php echo $EnquiryDetails->contactStaffName->ListViewValue() ?></span>
</span><a id="<?php echo $EnquiryDetails_list->PageObjName . "_row_" . $EnquiryDetails_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($EnquiryDetails->roleDescription->Visible) { // roleDescription ?>
		<td<?php echo $EnquiryDetails->roleDescription->CellAttributes() ?>><span id="el<?php echo $EnquiryDetails_list->RowCnt ?>_EnquiryDetails_roleDescription" class="EnquiryDetails_roleDescription">
<span<?php echo $EnquiryDetails->roleDescription->ViewAttributes() ?>>
<?php echo $EnquiryDetails->roleDescription->ListViewValue() ?></span>
</span><a id="<?php echo $EnquiryDetails_list->PageObjName . "_row_" . $EnquiryDetails_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($EnquiryDetails->assetSerialNumber->Visible) { // assetSerialNumber ?>
		<td<?php echo $EnquiryDetails->assetSerialNumber->CellAttributes() ?>><span id="el<?php echo $EnquiryDetails_list->RowCnt ?>_EnquiryDetails_assetSerialNumber" class="EnquiryDetails_assetSerialNumber">
<span<?php echo $EnquiryDetails->assetSerialNumber->ViewAttributes() ?>>
<?php echo $EnquiryDetails->assetSerialNumber->ListViewValue() ?></span>
</span><a id="<?php echo $EnquiryDetails_list->PageObjName . "_row_" . $EnquiryDetails_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($EnquiryDetails->assetDescription->Visible) { // assetDescription ?>
		<td<?php echo $EnquiryDetails->assetDescription->CellAttributes() ?>><span id="el<?php echo $EnquiryDetails_list->RowCnt ?>_EnquiryDetails_assetDescription" class="EnquiryDetails_assetDescription">
<span<?php echo $EnquiryDetails->assetDescription->ViewAttributes() ?>>
<?php echo $EnquiryDetails->assetDescription->ListViewValue() ?></span>
</span><a id="<?php echo $EnquiryDetails_list->PageObjName . "_row_" . $EnquiryDetails_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($EnquiryDetails->vendorName->Visible) { // vendorName ?>
		<td<?php echo $EnquiryDetails->vendorName->CellAttributes() ?>><span id="el<?php echo $EnquiryDetails_list->RowCnt ?>_EnquiryDetails_vendorName" class="EnquiryDetails_vendorName">
<span<?php echo $EnquiryDetails->vendorName->ViewAttributes() ?>>
<?php echo $EnquiryDetails->vendorName->ListViewValue() ?></span>
</span><a id="<?php echo $EnquiryDetails_list->PageObjName . "_row_" . $EnquiryDetails_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($EnquiryDetails->vendorDetails->Visible) { // vendorDetails ?>
		<td<?php echo $EnquiryDetails->vendorDetails->CellAttributes() ?>><span id="el<?php echo $EnquiryDetails_list->RowCnt ?>_EnquiryDetails_vendorDetails" class="EnquiryDetails_vendorDetails">
<span<?php echo $EnquiryDetails->vendorDetails->ViewAttributes() ?>>
<?php echo $EnquiryDetails->vendorDetails->ListViewValue() ?></span>
</span><a id="<?php echo $EnquiryDetails_list->PageObjName . "_row_" . $EnquiryDetails_list->RowCnt ?>"></a></td>
	<?php } ?>
<?php

// Render list options (body, right)
$EnquiryDetails_list->ListOptions->Render("body", "right", $EnquiryDetails_list->RowCnt);
?>
	</tr>
<?php
	}
	if ($EnquiryDetails->CurrentAction <> "gridadd")
		$EnquiryDetails_list->Recordset->MoveNext();
}
?>
</tbody>
</table>
<?php } ?>
<?php if ($EnquiryDetails->CurrentAction == "") { ?>
<input type="hidden" name="a_list" id="a_list" value="">
<?php } ?>
</div>
</form>
<?php

// Close recordset
if ($EnquiryDetails_list->Recordset)
	$EnquiryDetails_list->Recordset->Close();
?>
<?php if ($EnquiryDetails->Export == "") { ?>
<div class="ewGridLowerPanel">
<?php if ($EnquiryDetails->CurrentAction <> "gridadd" && $EnquiryDetails->CurrentAction <> "gridedit") { ?>
<form name="ewpagerform" id="ewpagerform" class="ewForm" action="<?php echo ew_CurrentPage() ?>">
<table class="ewPager"><tr><td>
<?php if (!isset($EnquiryDetails_list->Pager)) $EnquiryDetails_list->Pager = new cPrevNextPager($EnquiryDetails_list->StartRec, $EnquiryDetails_list->DisplayRecs, $EnquiryDetails_list->TotalRecs) ?>
<?php if ($EnquiryDetails_list->Pager->RecordCount > 0) { ?>
	<table cellspacing="0" class="ewStdTable"><tbody><tr><td><span class="phpmaker"><?php echo $Language->Phrase("Page") ?>&nbsp;</span></td>
<!--first page button-->
	<?php if ($EnquiryDetails_list->Pager->FirstButton->Enabled) { ?>
	<td><a href="<?php echo $EnquiryDetails_list->PageUrl() ?>start=<?php echo $EnquiryDetails_list->Pager->FirstButton->Start ?>"><img src="phpimages/first.gif" alt="<?php echo $Language->Phrase("PagerFirst") ?>" width="16" height="16" style="border: 0;"></a></td>
	<?php } else { ?>
	<td><img src="phpimages/firstdisab.gif" alt="<?php echo $Language->Phrase("PagerFirst") ?>" width="16" height="16" style="border: 0;"></td>
	<?php } ?>
<!--previous page button-->
	<?php if ($EnquiryDetails_list->Pager->PrevButton->Enabled) { ?>
	<td><a href="<?php echo $EnquiryDetails_list->PageUrl() ?>start=<?php echo $EnquiryDetails_list->Pager->PrevButton->Start ?>"><img src="phpimages/prev.gif" alt="<?php echo $Language->Phrase("PagerPrevious") ?>" width="16" height="16" style="border: 0;"></a></td>
	<?php } else { ?>
	<td><img src="phpimages/prevdisab.gif" alt="<?php echo $Language->Phrase("PagerPrevious") ?>" width="16" height="16" style="border: 0;"></td>
	<?php } ?>
<!--current page number-->
	<td><input type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" id="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $EnquiryDetails_list->Pager->CurrentPage ?>" size="4"></td>
<!--next page button-->
	<?php if ($EnquiryDetails_list->Pager->NextButton->Enabled) { ?>
	<td><a href="<?php echo $EnquiryDetails_list->PageUrl() ?>start=<?php echo $EnquiryDetails_list->Pager->NextButton->Start ?>"><img src="phpimages/next.gif" alt="<?php echo $Language->Phrase("PagerNext") ?>" width="16" height="16" style="border: 0;"></a></td>	
	<?php } else { ?>
	<td><img src="phpimages/nextdisab.gif" alt="<?php echo $Language->Phrase("PagerNext") ?>" width="16" height="16" style="border: 0;"></td>
	<?php } ?>
<!--last page button-->
	<?php if ($EnquiryDetails_list->Pager->LastButton->Enabled) { ?>
	<td><a href="<?php echo $EnquiryDetails_list->PageUrl() ?>start=<?php echo $EnquiryDetails_list->Pager->LastButton->Start ?>"><img src="phpimages/last.gif" alt="<?php echo $Language->Phrase("PagerLast") ?>" width="16" height="16" style="border: 0;"></a></td>	
	<?php } else { ?>
	<td><img src="phpimages/lastdisab.gif" alt="<?php echo $Language->Phrase("PagerLast") ?>" width="16" height="16" style="border: 0;"></td>
	<?php } ?>
	<td><span class="phpmaker">&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $EnquiryDetails_list->Pager->PageCount ?></span></td>
	</tr></tbody></table>
	</td>	
	<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td>
	<span class="phpmaker"><?php echo $Language->Phrase("Record") ?>&nbsp;<?php echo $EnquiryDetails_list->Pager->FromIndex ?>&nbsp;<?php echo $Language->Phrase("To") ?>&nbsp;<?php echo $EnquiryDetails_list->Pager->ToIndex ?>&nbsp;<?php echo $Language->Phrase("Of") ?>&nbsp;<?php echo $EnquiryDetails_list->Pager->RecordCount ?></span>
<?php } else { ?>
	<?php if ($EnquiryDetails_list->SearchWhere == "0=101") { ?>
	<span class="phpmaker"><?php echo $Language->Phrase("EnterSearchCriteria") ?></span>
	<?php } else { ?>
	<span class="phpmaker"><?php echo $Language->Phrase("NoRecord") ?></span>
	<?php } ?>
<?php } ?>
	</td>
</tr></table>
</form>
<?php } ?>
<span class="phpmaker">
</span>
</div>
<?php } ?>
</td></tr></table>
<?php if ($EnquiryDetails->Export == "") { ?>
<script type="text/javascript">
fEnquiryDetailslist.Init();
</script>
<?php } ?>
<?php
$EnquiryDetails_list->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<?php if ($EnquiryDetails->Export == "") { ?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php } ?>
<?php include_once "footer.php" ?>
<?php
$EnquiryDetails_list->Page_Terminate();
?>
