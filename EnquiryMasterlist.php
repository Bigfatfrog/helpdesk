<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg9.php" ?>
<?php include_once "ewmysql9.php" ?>
<?php include_once "phpfn9.php" ?>
<?php include_once "EnquiryMasterinfo.php" ?>
<?php include_once "staffinfo.php" ?>
<?php include_once "EnquiryCallAssetgridcls.php" ?>
<?php include_once "EnquirycallContactgridcls.php" ?>
<?php include_once "userfn9.php" ?>
<?php

//
// Page class
//

$EnquiryMaster_list = NULL; // Initialize page object first

class cEnquiryMaster_list extends cEnquiryMaster {

	// Page ID
	var $PageID = 'list';

	// Project ID
	var $ProjectID = "{4F01D370-BA3A-4C91-9445-0AA347261657}";

	// Table name
	var $TableName = 'EnquiryMaster';

	// Page object name
	var $PageObjName = 'EnquiryMaster_list';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Page URLs
	var $AddUrl;
	var $EditUrl;
	var $CopyUrl;
	var $DeleteUrl;
	var $ViewUrl;
	var $ListUrl;

	// Export URLs
	var $ExportPrintUrl;
	var $ExportHtmlUrl;
	var $ExportExcelUrl;
	var $ExportWordUrl;
	var $ExportXmlUrl;
	var $ExportCsvUrl;
	var $ExportPdfUrl;

	// Update URLs
	var $InlineAddUrl;
	var $InlineCopyUrl;
	var $InlineEditUrl;
	var $GridAddUrl;
	var $GridEditUrl;
	var $MultiDeleteUrl;
	var $MultiUpdateUrl;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			$html .= "<p class=\"ewMessage\">" . $sMessage . "</p>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			$html .= "<table class=\"ewMessageTable\"><tr><td class=\"ewWarningIcon\"></td><td class=\"ewWarningMessage\">" . $sWarningMessage . "</td></tr></table>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			$html .= "<table class=\"ewMessageTable\"><tr><td class=\"ewSuccessIcon\"></td><td class=\"ewSuccessMessage\">" . $sSuccessMessage . "</td></tr></table>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			$html .= "<table class=\"ewMessageTable\"><tr><td class=\"ewErrorIcon\"></td><td class=\"ewErrorMessage\">" . $sErrorMessage . "</td></tr></table>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p class=\"phpmaker\">" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Fotoer exists, display
			echo "<p class=\"phpmaker\">" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language, $UserAgent;

		// User agent
		$UserAgent = ew_UserAgent();
		$GLOBALS["Page"] = &$this;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (EnquiryMaster)
		if (!isset($GLOBALS["EnquiryMaster"])) {
			$GLOBALS["EnquiryMaster"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["EnquiryMaster"];
		}

		// Initialize URLs
		$this->ExportPrintUrl = $this->PageUrl() . "export=print";
		$this->ExportExcelUrl = $this->PageUrl() . "export=excel";
		$this->ExportWordUrl = $this->PageUrl() . "export=word";
		$this->ExportHtmlUrl = $this->PageUrl() . "export=html";
		$this->ExportXmlUrl = $this->PageUrl() . "export=xml";
		$this->ExportCsvUrl = $this->PageUrl() . "export=csv";
		$this->ExportPdfUrl = $this->PageUrl() . "export=pdf";
		$this->AddUrl = "EnquiryMasteradd.php?" . EW_TABLE_SHOW_DETAIL . "=";
		$this->InlineAddUrl = $this->PageUrl() . "a=add";
		$this->GridAddUrl = $this->PageUrl() . "a=gridadd";
		$this->GridEditUrl = $this->PageUrl() . "a=gridedit";
		$this->MultiDeleteUrl = "EnquiryMasterdelete.php";
		$this->MultiUpdateUrl = "EnquiryMasterupdate.php";

		// Table object (staff)
		if (!isset($GLOBALS['staff'])) $GLOBALS['staff'] = new cstaff();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'list', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'EnquiryMaster', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect();

		// List options
		$this->ListOptions = new cListOptions();
		$this->ListOptions->TableVar = $this->TableVar;

		// Export options
		$this->ExportOptions = new cListOptions();
		$this->ExportOptions->Tag = "span";
		$this->ExportOptions->TagClassName = "ewExportOption";
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if (!$Security->IsLoggedIn()) {
			$Security->SaveLastUrl();
			$this->Page_Terminate("login.php");
		}

		// Get export parameters
		if (@$_GET["export"] <> "") {
			$this->Export = $_GET["export"];
		} elseif (ew_IsHttpPost()) {
			if (@$_POST["exporttype"] <> "")
				$this->Export = $_POST["exporttype"];
		} else {
			$this->setExportReturnUrl(ew_CurrentUrl());
		}
		$gsExport = $this->Export; // Get export parameter, used in header
		$gsExportFile = $this->TableVar; // Get export file, used in header

		// Get grid add count
		$gridaddcnt = @$_GET[EW_TABLE_GRID_ADD_ROW_COUNT];
		if (is_numeric($gridaddcnt) && $gridaddcnt > 0)
			$this->GridAddRowCount = $gridaddcnt;

		// Set up list options
		$this->SetupListOptions();

		// Setup export options
		$this->SetupExportOptions();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"];
		$this->CallId->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();
		$this->timeOfCall->Visible = !$this->IsAddOrEdit();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $conn;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();
		$this->Page_Redirecting($url);

		 // Close connection
		$conn->Close();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}

	// Class variables
	var $ListOptions; // List options
	var $ExportOptions; // Export options
	var $DisplayRecs = 20;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $Pager;
	var $SearchWhere = ""; // Search WHERE clause
	var $RecCnt = 0; // Record count
	var $EditRowCnt;
	var $StartRowCnt = 1;
	var $RowCnt = 0;
	var $Attrs = array(); // Row attributes and cell attributes
	var $RowIndex = 0; // Row index
	var $KeyCount = 0; // Key count
	var $RowAction = ""; // Row action
	var $RowOldKey = ""; // Row old key (for copy)
	var $RecPerRow = 0;
	var $ColCnt = 0;
	var $DbMasterFilter = ""; // Master filter
	var $DbDetailFilter = ""; // Detail filter
	var $MasterRecordExists;	
	var $MultiSelectKey;
	var $Command;
	var $Recordset;
	var $OldRecordset;

	//
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError, $gsSearchError, $Security;

		// Search filters
		$sSrchAdvanced = ""; // Advanced search filter
		$sSrchBasic = ""; // Basic search filter
		$sFilter = "";

		// Get command
		$this->Command = strtolower(@$_GET["cmd"]);
		if ($this->IsPageRequest()) { // Validate request

			// Handle reset command
			$this->ResetCmd();

			// Hide all options
			if ($this->Export <> "" ||
				$this->CurrentAction == "gridadd" ||
				$this->CurrentAction == "gridedit") {
				$this->ListOptions->HideAllOptions();
				$this->ExportOptions->HideAllOptions();
			}

			// Get and validate search values for advanced search
			$this->LoadSearchValues(); // Get search values
			if (!$this->ValidateSearch())
				$this->setFailureMessage($gsSearchError);

			// Restore search parms from Session if not searching / reset
			if ($this->Command <> "search" && $this->Command <> "reset" && $this->Command <> "resetall")
				$this->RestoreSearchParms();

			// Call Recordset SearchValidated event
			$this->Recordset_SearchValidated();

			// Set up sorting order
			$this->SetUpSortOrder();

			// Get search criteria for advanced search
			if ($gsSearchError == "")
				$sSrchAdvanced = $this->AdvancedSearchWhere();
		}

		// Restore display records
		if ($this->getRecordsPerPage() <> "") {
			$this->DisplayRecs = $this->getRecordsPerPage(); // Restore from Session
		} else {
			$this->DisplayRecs = 20; // Load default
		}

		// Load Sorting Order
		$this->LoadSortOrder();

		// Load search default if no existing search criteria
		if (!$this->CheckSearchParms()) {

			// Load advanced search from default
			if ($this->LoadAdvancedSearchDefault()) {
				$sSrchAdvanced = $this->AdvancedSearchWhere();
			}
		}

		// Build search criteria
		ew_AddFilter($this->SearchWhere, $sSrchAdvanced);
		ew_AddFilter($this->SearchWhere, $sSrchBasic);

		// Call Recordset_Searching event
		$this->Recordset_Searching($this->SearchWhere);

		// Save search criteria
		if ($this->Command == "search") {
			$this->setSearchWhere($this->SearchWhere); // Save to Session
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} else {
			$this->SearchWhere = $this->getSearchWhere();
		}

		// Build filter
		$sFilter = "";
		ew_AddFilter($sFilter, $this->DbDetailFilter);
		ew_AddFilter($sFilter, $this->SearchWhere);

		// Set up filter in session
		$this->setSessionWhere($sFilter);
		$this->CurrentFilter = "";

		// Export data only
		if (in_array($this->Export, array("html","word","excel","xml","csv","email","pdf"))) {
			$this->ExportData();
			if ($this->Export == "email")
				$this->Page_Terminate($this->ExportReturnUrl());
			else
				$this->Page_Terminate(); // Terminate response
			exit();
		}
	}

	// Build filter for all keys
	function BuildKeyFilter() {
		global $objForm;
		$sWrkFilter = "";

		// Update row index and get row key
		$rowindex = 1;
		$objForm->Index = $rowindex;
		$sThisKey = strval($objForm->GetValue("k_key"));
		while ($sThisKey <> "") {
			if ($this->SetupKeyValues($sThisKey)) {
				$sFilter = $this->KeyFilter();
				if ($sWrkFilter <> "") $sWrkFilter .= " OR ";
				$sWrkFilter .= $sFilter;
			} else {
				$sWrkFilter = "0=1";
				break;
			}

			// Update row index and get row key
			$rowindex++; // next row
			$objForm->Index = $rowindex;
			$sThisKey = strval($objForm->GetValue("k_key"));
		}
		return $sWrkFilter;
	}

	// Set up key values
	function SetupKeyValues($key) {
		$arrKeyFlds = explode($GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"], $key);
		if (count($arrKeyFlds) >= 1) {
			$this->CallId->setFormValue($arrKeyFlds[0]);
			if (!is_numeric($this->CallId->FormValue))
				return FALSE;
		}
		return TRUE;
	}

	// Advanced search WHERE clause based on QueryString
	function AdvancedSearchWhere() {
		global $Security;
		$sWhere = "";
		$this->BuildSearchSql($sWhere, $this->CallId, FALSE); // CallId

		// Set up search parm
		if ($sWhere <> "") {
			$this->Command = "search";
		}
		if ($this->Command == "search") {
			$this->CallId->AdvancedSearch->Save(); // CallId
		}
		return $sWhere;
	}

	// Build search SQL
	function BuildSearchSql(&$Where, &$Fld, $MultiValue) {
		$FldParm = substr($Fld->FldVar, 2);
		$FldVal = $Fld->AdvancedSearch->SearchValue; // @$_GET["x_$FldParm"]
		$FldOpr = $Fld->AdvancedSearch->SearchOperator; // @$_GET["z_$FldParm"]
		$FldCond = $Fld->AdvancedSearch->SearchCondition; // @$_GET["v_$FldParm"]
		$FldVal2 = $Fld->AdvancedSearch->SearchValue2; // @$_GET["y_$FldParm"]
		$FldOpr2 = $Fld->AdvancedSearch->SearchOperator2; // @$_GET["w_$FldParm"]
		$sWrk = "";

		//$FldVal = ew_StripSlashes($FldVal);
		if (is_array($FldVal)) $FldVal = implode(",", $FldVal);

		//$FldVal2 = ew_StripSlashes($FldVal2);
		if (is_array($FldVal2)) $FldVal2 = implode(",", $FldVal2);
		$FldOpr = strtoupper(trim($FldOpr));
		if ($FldOpr == "") $FldOpr = "=";
		$FldOpr2 = strtoupper(trim($FldOpr2));
		if ($FldOpr2 == "") $FldOpr2 = "=";
		if (EW_SEARCH_MULTI_VALUE_OPTION == 1 || $FldOpr <> "LIKE" ||
			($FldOpr2 <> "LIKE" && $FldVal2 <> ""))
			$MultiValue = FALSE;
		if ($MultiValue) {
			$sWrk1 = ($FldVal <> "") ? ew_GetMultiSearchSql($Fld, $FldOpr, $FldVal) : ""; // Field value 1
			$sWrk2 = ($FldVal2 <> "") ? ew_GetMultiSearchSql($Fld, $FldOpr2, $FldVal2) : ""; // Field value 2
			$sWrk = $sWrk1; // Build final SQL
			if ($sWrk2 <> "")
				$sWrk = ($sWrk <> "") ? "($sWrk) $FldCond ($sWrk2)" : $sWrk2;
		} else {
			$FldVal = $this->ConvertSearchValue($Fld, $FldVal);
			$FldVal2 = $this->ConvertSearchValue($Fld, $FldVal2);
			$sWrk = ew_GetSearchSql($Fld, $FldVal, $FldOpr, $FldCond, $FldVal2, $FldOpr2);
		}
		ew_AddFilter($Where, $sWrk);
	}

	// Convert search value
	function ConvertSearchValue(&$Fld, $FldVal) {
		if ($FldVal == EW_NULL_VALUE || $FldVal == EW_NOT_NULL_VALUE)
			return $FldVal;
		$Value = $FldVal;
		if ($Fld->FldDataType == EW_DATATYPE_BOOLEAN) {
			if ($FldVal <> "") $Value = ($FldVal == "1" || strtolower(strval($FldVal)) == "y" || strtolower(strval($FldVal)) == "t") ? $Fld->TrueValue : $Fld->FalseValue;
		} elseif ($Fld->FldDataType == EW_DATATYPE_DATE) {
			if ($FldVal <> "") $Value = ew_UnFormatDateTime($FldVal, $Fld->FldDateTimeFormat);
		}
		return $Value;
	}

	// Check if search parm exists
	function CheckSearchParms() {
		if ($this->CallId->AdvancedSearch->IssetSession())
			return TRUE;
		return FALSE;
	}

	// Clear all search parameters
	function ResetSearchParms() {

		// Clear search WHERE clause
		$this->SearchWhere = "";
		$this->setSearchWhere($this->SearchWhere);

		// Clear advanced search parameters
		$this->ResetAdvancedSearchParms();
	}

	// Load advanced search default values
	function LoadAdvancedSearchDefault() {
		return FALSE;
	}

	// Clear all advanced search parameters
	function ResetAdvancedSearchParms() {
		$this->CallId->AdvancedSearch->UnsetSession();
	}

	// Restore all search parameters
	function RestoreSearchParms() {

		// Restore advanced search values
		$this->CallId->AdvancedSearch->Load();
	}

	// Set up sort parameters
	function SetUpSortOrder() {

		// Check for "order" parameter
		if (@$_GET["order"] <> "") {
			$this->CurrentOrder = ew_StripSlashes(@$_GET["order"]);
			$this->CurrentOrderType = @$_GET["ordertype"];
			$this->UpdateSort($this->CallId); // CallId
			$this->UpdateSort($this->CallerName); // CallerName
			$this->UpdateSort($this->OperatorName); // OperatorName
			$this->UpdateSort($this->AllocatedTo); // AllocatedTo
			$this->UpdateSort($this->timeOfCall); // timeOfCall
			$this->UpdateSort($this->callDescription); // callDescription
			$this->UpdateSort($this->status); // status
			$this->UpdateSort($this->solution); // solution
			$this->setStartRecordNumber(1); // Reset start position
		}
	}

	// Load sort order parameters
	function LoadSortOrder() {
		$sOrderBy = $this->getSessionOrderBy(); // Get ORDER BY from Session
		if ($sOrderBy == "") {
			if ($this->SqlOrderBy() <> "") {
				$sOrderBy = $this->SqlOrderBy();
				$this->setSessionOrderBy($sOrderBy);
			}
		}
	}

	// Reset command
	// cmd=reset (Reset search parameters)
	// cmd=resetall (Reset search and master/detail parameters)
	// cmd=resetsort (Reset sort parameters)
	function ResetCmd() {

		// Check if reset command
		if (substr($this->Command,0,5) == "reset") {

			// Reset search criteria
			if ($this->Command == "reset" || $this->Command == "resetall")
				$this->ResetSearchParms();

			// Reset sorting order
			if ($this->Command == "resetsort") {
				$sOrderBy = "";
				$this->setSessionOrderBy($sOrderBy);
				$this->CallId->setSort("");
				$this->CallerName->setSort("");
				$this->OperatorName->setSort("");
				$this->AllocatedTo->setSort("");
				$this->timeOfCall->setSort("");
				$this->callDescription->setSort("");
				$this->status->setSort("");
				$this->solution->setSort("");
			}

			// Reset start position
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Set up list options
	function SetupListOptions() {
		global $Security, $Language;

		// "detail_EnquiryCallAsset"
		$item = &$this->ListOptions->Add("detail_EnquiryCallAsset");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->IsLoggedIn();
		$item->OnLeft = FALSE;
		if (!isset($GLOBALS["EnquiryCallAsset_grid"])) $GLOBALS["EnquiryCallAsset_grid"] = new cEnquiryCallAsset_grid;

		// "detail_EnquirycallContact"
		$item = &$this->ListOptions->Add("detail_EnquirycallContact");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->IsLoggedIn();
		$item->OnLeft = FALSE;
		if (!isset($GLOBALS["EnquirycallContact_grid"])) $GLOBALS["EnquirycallContact_grid"] = new cEnquirycallContact_grid;

		// Call ListOptions_Load event
		$this->ListOptions_Load();
	}

	// Render list options
	function RenderListOptions() {
		global $Security, $Language, $objForm;
		$this->ListOptions->LoadDefault();

		// "detail_EnquiryCallAsset"
		$oListOpt = &$this->ListOptions->Items["detail_EnquiryCallAsset"];
		if ($Security->IsLoggedIn()) {
			$oListOpt->Body = $Language->Phrase("DetailLink") . $Language->TablePhrase("EnquiryCallAsset", "TblCaption");
			$oListOpt->Body = "<a class=\"ewRowLink\" href=\"EnquiryCallAssetlist.php?" . EW_TABLE_SHOW_MASTER . "=EnquiryMaster&CallId=" . urlencode(strval($this->CallId->CurrentValue)) . "\">" . $oListOpt->Body . "</a>";
			$links = "";
			if ($links <> "") $oListOpt->Body .= "<br>" . $links;
		}

		// "detail_EnquirycallContact"
		$oListOpt = &$this->ListOptions->Items["detail_EnquirycallContact"];
		if ($Security->IsLoggedIn()) {
			$oListOpt->Body = $Language->Phrase("DetailLink") . $Language->TablePhrase("EnquirycallContact", "TblCaption");
			$oListOpt->Body = "<a class=\"ewRowLink\" href=\"EnquirycallContactlist.php?" . EW_TABLE_SHOW_MASTER . "=EnquiryMaster&CallId=" . urlencode(strval($this->CallId->CurrentValue)) . "\">" . $oListOpt->Body . "</a>";
			$links = "";
			if ($links <> "") $oListOpt->Body .= "<br>" . $links;
		}
		$this->RenderListOptionsExt();

		// Call ListOptions_Rendered event
		$this->ListOptions_Rendered();
	}

	function RenderListOptionsExt() {
		global $Security, $Language;
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	//  Load search values for validation
	function LoadSearchValues() {
		global $objForm;

		// Load search values
		// CallId

		$this->CallId->AdvancedSearch->SearchValue = ew_StripSlashes(@$_GET["x_CallId"]);
		if ($this->CallId->AdvancedSearch->SearchValue <> "") $this->Command = "search";
		$this->CallId->AdvancedSearch->SearchOperator = @$_GET["z_CallId"];
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {
		global $conn;

		// Call Recordset Selecting event
		$this->Recordset_Selecting($this->CurrentFilter);

		// Load List page SQL
		$sSql = $this->SelectSQL();
		if ($offset > -1 && $rowcnt > -1)
			$sSql .= " LIMIT $rowcnt OFFSET $offset";

		// Load recordset
		$rs = ew_LoadRecordset($sSql);

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $conn, $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		global $conn;
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->CallId->setDbValue($rs->fields('CallId'));
		$this->CallerName->setDbValue($rs->fields('CallerName'));
		$this->OperatorName->setDbValue($rs->fields('OperatorName'));
		$this->AllocatedTo->setDbValue($rs->fields('AllocatedTo'));
		$this->timeOfCall->setDbValue($rs->fields('timeOfCall'));
		$this->callDescription->setDbValue($rs->fields('callDescription'));
		$this->status->setDbValue($rs->fields('status'));
		$this->solution->setDbValue($rs->fields('solution'));
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("CallId")) <> "")
			$this->CallId->CurrentValue = $this->getKey("CallId"); // CallId
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$this->OldRecordset = ew_LoadRecordset($sSql);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $conn, $Security, $Language;
		global $gsLanguage;

		// Initialize URLs
		$this->ViewUrl = $this->GetViewUrl();
		$this->EditUrl = $this->GetEditUrl();
		$this->InlineEditUrl = $this->GetInlineEditUrl();
		$this->CopyUrl = $this->GetCopyUrl();
		$this->InlineCopyUrl = $this->GetInlineCopyUrl();
		$this->DeleteUrl = $this->GetDeleteUrl();

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// CallId
		// CallerName
		// OperatorName
		// AllocatedTo
		// timeOfCall
		// callDescription
		// status
		// solution

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

			// CallId
			$this->CallId->ViewValue = $this->CallId->CurrentValue;
			$this->CallId->ViewCustomAttributes = "";

			// CallerName
			$this->CallerName->ViewValue = $this->CallerName->CurrentValue;
			$this->CallerName->ViewCustomAttributes = "";

			// OperatorName
			$this->OperatorName->ViewValue = $this->OperatorName->CurrentValue;
			$this->OperatorName->ViewCustomAttributes = "";

			// AllocatedTo
			$this->AllocatedTo->ViewValue = $this->AllocatedTo->CurrentValue;
			$this->AllocatedTo->ViewCustomAttributes = "";

			// timeOfCall
			$this->timeOfCall->ViewValue = $this->timeOfCall->CurrentValue;
			$this->timeOfCall->ViewValue = ew_FormatDateTime($this->timeOfCall->ViewValue, 7);
			$this->timeOfCall->ViewCustomAttributes = "";

			// callDescription
			$this->callDescription->ViewValue = $this->callDescription->CurrentValue;
			$this->callDescription->ViewCustomAttributes = "";

			// status
			if (strval($this->status->CurrentValue) <> "") {
				switch ($this->status->CurrentValue) {
					case $this->status->FldTagValue(1):
						$this->status->ViewValue = $this->status->FldTagCaption(1) <> "" ? $this->status->FldTagCaption(1) : $this->status->CurrentValue;
						break;
					case $this->status->FldTagValue(2):
						$this->status->ViewValue = $this->status->FldTagCaption(2) <> "" ? $this->status->FldTagCaption(2) : $this->status->CurrentValue;
						break;
					case $this->status->FldTagValue(3):
						$this->status->ViewValue = $this->status->FldTagCaption(3) <> "" ? $this->status->FldTagCaption(3) : $this->status->CurrentValue;
						break;
					default:
						$this->status->ViewValue = $this->status->CurrentValue;
				}
			} else {
				$this->status->ViewValue = NULL;
			}
			$this->status->ViewValue = ew_FormatDateTime($this->status->ViewValue, 7);
			$this->status->ViewCustomAttributes = "";

			// solution
			$this->solution->ViewValue = $this->solution->CurrentValue;
			$this->solution->ViewCustomAttributes = "";

			// CallId
			$this->CallId->LinkCustomAttributes = "";
			$this->CallId->HrefValue = "";
			$this->CallId->TooltipValue = "";

			// CallerName
			$this->CallerName->LinkCustomAttributes = "";
			$this->CallerName->HrefValue = "";
			$this->CallerName->TooltipValue = "";

			// OperatorName
			$this->OperatorName->LinkCustomAttributes = "";
			$this->OperatorName->HrefValue = "";
			$this->OperatorName->TooltipValue = "";

			// AllocatedTo
			$this->AllocatedTo->LinkCustomAttributes = "";
			$this->AllocatedTo->HrefValue = "";
			$this->AllocatedTo->TooltipValue = "";

			// timeOfCall
			$this->timeOfCall->LinkCustomAttributes = "";
			$this->timeOfCall->HrefValue = "";
			$this->timeOfCall->TooltipValue = "";

			// callDescription
			$this->callDescription->LinkCustomAttributes = "";
			$this->callDescription->HrefValue = "";
			$this->callDescription->TooltipValue = "";

			// status
			$this->status->LinkCustomAttributes = "";
			$this->status->HrefValue = "";
			$this->status->TooltipValue = "";

			// solution
			$this->solution->LinkCustomAttributes = "";
			$this->solution->HrefValue = "";
			$this->solution->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate search
	function ValidateSearch() {
		global $gsSearchError;

		// Initialize
		$gsSearchError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return TRUE;

		// Return validate result
		$ValidateSearch = ($gsSearchError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateSearch = $ValidateSearch && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsSearchError, $sFormCustomError);
		}
		return $ValidateSearch;
	}

	// Load advanced search
	function LoadAdvancedSearch() {
		$this->CallId->AdvancedSearch->Load();
	}

	// Set up export options
	function SetupExportOptions() {
		global $Language;

		// Printer friendly
		$item = &$this->ExportOptions->Add("print");
		$item->Body = "<a href=\"" . $this->ExportPrintUrl . "\">" . $Language->Phrase("PrinterFriendly") . "</a>";
		$item->Visible = FALSE;

		// Export to Excel
		$item = &$this->ExportOptions->Add("excel");
		$item->Body = "<a href=\"" . $this->ExportExcelUrl . "\">" . $Language->Phrase("ExportToExcel") . "</a>";
		$item->Visible = FALSE;

		// Export to Word
		$item = &$this->ExportOptions->Add("word");
		$item->Body = "<a href=\"" . $this->ExportWordUrl . "\">" . $Language->Phrase("ExportToWord") . "</a>";
		$item->Visible = FALSE;

		// Export to Html
		$item = &$this->ExportOptions->Add("html");
		$item->Body = "<a href=\"" . $this->ExportHtmlUrl . "\">" . $Language->Phrase("ExportToHtml") . "</a>";
		$item->Visible = FALSE;

		// Export to Xml
		$item = &$this->ExportOptions->Add("xml");
		$item->Body = "<a href=\"" . $this->ExportXmlUrl . "\">" . $Language->Phrase("ExportToXml") . "</a>";
		$item->Visible = FALSE;

		// Export to Csv
		$item = &$this->ExportOptions->Add("csv");
		$item->Body = "<a href=\"" . $this->ExportCsvUrl . "\">" . $Language->Phrase("ExportToCsv") . "</a>";
		$item->Visible = TRUE;

		// Export to Pdf
		$item = &$this->ExportOptions->Add("pdf");
		$item->Body = "<a href=\"" . $this->ExportPdfUrl . "\">" . $Language->Phrase("ExportToPDF") . "</a>";
		$item->Visible = FALSE;

		// Export to Email
		$item = &$this->ExportOptions->Add("email");
		$item->Body = "<a id=\"emf_EnquiryMaster\" href=\"javascript:void(0);\" onclick=\"ew_EmailDialogShow({lnk:'emf_EnquiryMaster',hdr:ewLanguage.Phrase('ExportToEmail'),f:document.fEnquiryMasterlist,sel:false});\">" . $Language->Phrase("ExportToEmail") . "</a>";
		$item->Visible = FALSE;

		// Hide options for export/action
		if ($this->Export <> "" || $this->CurrentAction <> "")
			$this->ExportOptions->HideAllOptions();
	}

	// Export data in HTML/CSV/Word/Excel/XML/Email/PDF format
	function ExportData() {
		$utf8 = (strtolower(EW_CHARSET) == "utf-8");
		$bSelectLimit = EW_SELECT_LIMIT;

		// Load recordset
		if ($bSelectLimit) {
			$this->TotalRecs = $this->SelectRecordCount();
		} else {
			if ($rs = $this->LoadRecordset())
				$this->TotalRecs = $rs->RecordCount();
		}
		$this->StartRec = 1;

		// Export all
		if ($this->ExportAll) {
			set_time_limit(EW_EXPORT_ALL_TIME_LIMIT);
			$this->DisplayRecs = $this->TotalRecs;
			$this->StopRec = $this->TotalRecs;
		} else { // Export one page only
			$this->SetUpStartRec(); // Set up start record position

			// Set the last record to display
			if ($this->DisplayRecs <= 0) {
				$this->StopRec = $this->TotalRecs;
			} else {
				$this->StopRec = $this->StartRec + $this->DisplayRecs - 1;
			}
		}
		if ($bSelectLimit)
			$rs = $this->LoadRecordset($this->StartRec-1, $this->DisplayRecs <= 0 ? $this->TotalRecs : $this->DisplayRecs);
		if (!$rs) {
			header("Content-Type:"); // Remove header
			header("Content-Disposition:");
			$this->ShowMessage();
			return;
		}
		$ExportDoc = ew_ExportDocument($this, "h");
		$ParentTable = "";
		if ($bSelectLimit) {
			$StartRec = 1;
			$StopRec = $this->DisplayRecs <= 0 ? $this->TotalRecs : $this->DisplayRecs;
		} else {
			$StartRec = $this->StartRec;
			$StopRec = $this->StopRec;
		}
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		$ExportDoc->Text .= $sHeader;
		$this->ExportDocument($ExportDoc, $rs, $StartRec, $StopRec, "");
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		$ExportDoc->Text .= $sFooter;

		// Close recordset
		$rs->Close();

		// Export header and footer
		$ExportDoc->ExportHeaderAndFooter();

		// Clean output buffer
		if (!EW_DEBUG_ENABLED && ob_get_length())
			ob_end_clean();

		// Write debug message if enabled
		if (EW_DEBUG_ENABLED)
			echo ew_DebugMsg();

		// Output data
		$ExportDoc->Export();
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}

	// ListOptions Load event
	function ListOptions_Load() {

		// Example:
		//$opt = &$this->ListOptions->Add("new");
		//$opt->Header = "xxx";
		//$opt->OnLeft = TRUE; // Link on left
		//$opt->MoveTo(0); // Move to first column

	}

	// ListOptions Rendered event
	function ListOptions_Rendered() {

		// Example: 
		//$this->ListOptions->Items["new"]->Body = "xxx";

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($EnquiryMaster_list)) $EnquiryMaster_list = new cEnquiryMaster_list();

// Page init
$EnquiryMaster_list->Page_Init();

// Page main
$EnquiryMaster_list->Page_Main();
?>
<?php include_once "header.php" ?>
<?php if ($EnquiryMaster->Export == "") { ?>
<script type="text/javascript">

// Page object
var EnquiryMaster_list = new ew_Page("EnquiryMaster_list");
EnquiryMaster_list.PageID = "list"; // Page ID
var EW_PAGE_ID = EnquiryMaster_list.PageID; // For backward compatibility

// Form object
var fEnquiryMasterlist = new ew_Form("fEnquiryMasterlist");

// Form_CustomValidate event
fEnquiryMasterlist.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fEnquiryMasterlist.ValidateRequired = true;
<?php } else { ?>
fEnquiryMasterlist.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
// Form object for search

var fEnquiryMasterlistsrch = new ew_Form("fEnquiryMasterlistsrch");
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php } ?>
<?php
	$bSelectLimit = EW_SELECT_LIMIT;
	if ($bSelectLimit) {
		$EnquiryMaster_list->TotalRecs = $EnquiryMaster->SelectRecordCount();
	} else {
		if ($EnquiryMaster_list->Recordset = $EnquiryMaster_list->LoadRecordset())
			$EnquiryMaster_list->TotalRecs = $EnquiryMaster_list->Recordset->RecordCount();
	}
	$EnquiryMaster_list->StartRec = 1;
	if ($EnquiryMaster_list->DisplayRecs <= 0 || ($EnquiryMaster->Export <> "" && $EnquiryMaster->ExportAll)) // Display all records
		$EnquiryMaster_list->DisplayRecs = $EnquiryMaster_list->TotalRecs;
	if (!($EnquiryMaster->Export <> "" && $EnquiryMaster->ExportAll))
		$EnquiryMaster_list->SetUpStartRec(); // Set up start record position
	if ($bSelectLimit)
		$EnquiryMaster_list->Recordset = $EnquiryMaster_list->LoadRecordset($EnquiryMaster_list->StartRec-1, $EnquiryMaster_list->DisplayRecs);
?>
<p style="white-space: nowrap;"><span id="ewPageCaption" class="ewTitle ewTableTitle"><?php echo $Language->Phrase("TblTypeVIEW") ?><?php echo $EnquiryMaster->TableCaption() ?>&nbsp;&nbsp;</span>
<?php $EnquiryMaster_list->ExportOptions->Render("body"); ?>
</p>
<?php if ($Security->IsLoggedIn()) { ?>
<?php if ($EnquiryMaster->Export == "" && $EnquiryMaster->CurrentAction == "") { ?>
<form name="fEnquiryMasterlistsrch" id="fEnquiryMasterlistsrch" class="ewForm" action="<?php echo ew_CurrentPage() ?>">
<a href="javascript:fEnquiryMasterlistsrch.ToggleSearchPanel();" style="text-decoration: none;"><img id="fEnquiryMasterlistsrch_SearchImage" src="phpimages/collapse.gif" alt="" width="9" height="9" style="border: 0;"></a><span class="phpmaker">&nbsp;<?php echo $Language->Phrase("Search") ?></span><br>
<div id="fEnquiryMasterlistsrch_SearchPanel">
<input type="hidden" name="cmd" value="search">
<input type="hidden" name="t" value="EnquiryMaster">
<div class="ewBasicSearch">
<div id="xsr_1" class="ewRow">
	<a href="<?php echo $EnquiryMaster_list->PageUrl() ?>cmd=reset" id="a_ShowAll" class="ewLink"><?php echo $Language->Phrase("ShowAll") ?></a>&nbsp;
	<a href="EnquiryMastersrch.php" id="a_AdvancedSearch" class="ewLink"><?php echo $Language->Phrase("AdvancedSearch") ?></a>&nbsp;
</div>
</div>
</div>
</form>
<?php } ?>
<?php } ?>
<?php $EnquiryMaster_list->ShowPageHeader(); ?>
<?php
$EnquiryMaster_list->ShowMessage();
?>
<br>
<table cellspacing="0" class="ewGrid"><tr><td class="ewGridContent">
<form name="fEnquiryMasterlist" id="fEnquiryMasterlist" class="ewForm" action="" method="post">
<input type="hidden" name="t" value="EnquiryMaster">
<div id="gmp_EnquiryMaster" class="ewGridMiddlePanel">
<?php if ($EnquiryMaster_list->TotalRecs > 0) { ?>
<table id="tbl_EnquiryMasterlist" class="ewTable ewTableSeparate">
<?php echo $EnquiryMaster->TableCustomInnerHtml ?>
<thead><!-- Table header -->
	<tr class="ewTableHeader">
<?php

// Render list options
$EnquiryMaster_list->RenderListOptions();

// Render list options (header, left)
$EnquiryMaster_list->ListOptions->Render("header", "left");
?>
<?php if ($EnquiryMaster->CallId->Visible) { // CallId ?>
	<?php if ($EnquiryMaster->SortUrl($EnquiryMaster->CallId) == "") { ?>
		<td><span id="elh_EnquiryMaster_CallId" class="EnquiryMaster_CallId"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $EnquiryMaster->CallId->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div onmousedown="ew_Sort(event,'<?php echo $EnquiryMaster->SortUrl($EnquiryMaster->CallId) ?>',1);"><span id="elh_EnquiryMaster_CallId" class="EnquiryMaster_CallId">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $EnquiryMaster->CallId->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($EnquiryMaster->CallId->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($EnquiryMaster->CallId->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($EnquiryMaster->CallerName->Visible) { // CallerName ?>
	<?php if ($EnquiryMaster->SortUrl($EnquiryMaster->CallerName) == "") { ?>
		<td><span id="elh_EnquiryMaster_CallerName" class="EnquiryMaster_CallerName"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $EnquiryMaster->CallerName->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div onmousedown="ew_Sort(event,'<?php echo $EnquiryMaster->SortUrl($EnquiryMaster->CallerName) ?>',1);"><span id="elh_EnquiryMaster_CallerName" class="EnquiryMaster_CallerName">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $EnquiryMaster->CallerName->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($EnquiryMaster->CallerName->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($EnquiryMaster->CallerName->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($EnquiryMaster->OperatorName->Visible) { // OperatorName ?>
	<?php if ($EnquiryMaster->SortUrl($EnquiryMaster->OperatorName) == "") { ?>
		<td><span id="elh_EnquiryMaster_OperatorName" class="EnquiryMaster_OperatorName"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $EnquiryMaster->OperatorName->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div onmousedown="ew_Sort(event,'<?php echo $EnquiryMaster->SortUrl($EnquiryMaster->OperatorName) ?>',1);"><span id="elh_EnquiryMaster_OperatorName" class="EnquiryMaster_OperatorName">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $EnquiryMaster->OperatorName->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($EnquiryMaster->OperatorName->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($EnquiryMaster->OperatorName->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($EnquiryMaster->AllocatedTo->Visible) { // AllocatedTo ?>
	<?php if ($EnquiryMaster->SortUrl($EnquiryMaster->AllocatedTo) == "") { ?>
		<td><span id="elh_EnquiryMaster_AllocatedTo" class="EnquiryMaster_AllocatedTo"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $EnquiryMaster->AllocatedTo->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div onmousedown="ew_Sort(event,'<?php echo $EnquiryMaster->SortUrl($EnquiryMaster->AllocatedTo) ?>',1);"><span id="elh_EnquiryMaster_AllocatedTo" class="EnquiryMaster_AllocatedTo">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $EnquiryMaster->AllocatedTo->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($EnquiryMaster->AllocatedTo->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($EnquiryMaster->AllocatedTo->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($EnquiryMaster->timeOfCall->Visible) { // timeOfCall ?>
	<?php if ($EnquiryMaster->SortUrl($EnquiryMaster->timeOfCall) == "") { ?>
		<td><span id="elh_EnquiryMaster_timeOfCall" class="EnquiryMaster_timeOfCall"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $EnquiryMaster->timeOfCall->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div onmousedown="ew_Sort(event,'<?php echo $EnquiryMaster->SortUrl($EnquiryMaster->timeOfCall) ?>',1);"><span id="elh_EnquiryMaster_timeOfCall" class="EnquiryMaster_timeOfCall">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $EnquiryMaster->timeOfCall->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($EnquiryMaster->timeOfCall->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($EnquiryMaster->timeOfCall->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($EnquiryMaster->callDescription->Visible) { // callDescription ?>
	<?php if ($EnquiryMaster->SortUrl($EnquiryMaster->callDescription) == "") { ?>
		<td><span id="elh_EnquiryMaster_callDescription" class="EnquiryMaster_callDescription"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $EnquiryMaster->callDescription->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div onmousedown="ew_Sort(event,'<?php echo $EnquiryMaster->SortUrl($EnquiryMaster->callDescription) ?>',1);"><span id="elh_EnquiryMaster_callDescription" class="EnquiryMaster_callDescription">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $EnquiryMaster->callDescription->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($EnquiryMaster->callDescription->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($EnquiryMaster->callDescription->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($EnquiryMaster->status->Visible) { // status ?>
	<?php if ($EnquiryMaster->SortUrl($EnquiryMaster->status) == "") { ?>
		<td><span id="elh_EnquiryMaster_status" class="EnquiryMaster_status"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $EnquiryMaster->status->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div onmousedown="ew_Sort(event,'<?php echo $EnquiryMaster->SortUrl($EnquiryMaster->status) ?>',1);"><span id="elh_EnquiryMaster_status" class="EnquiryMaster_status">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $EnquiryMaster->status->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($EnquiryMaster->status->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($EnquiryMaster->status->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($EnquiryMaster->solution->Visible) { // solution ?>
	<?php if ($EnquiryMaster->SortUrl($EnquiryMaster->solution) == "") { ?>
		<td><span id="elh_EnquiryMaster_solution" class="EnquiryMaster_solution"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $EnquiryMaster->solution->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div onmousedown="ew_Sort(event,'<?php echo $EnquiryMaster->SortUrl($EnquiryMaster->solution) ?>',1);"><span id="elh_EnquiryMaster_solution" class="EnquiryMaster_solution">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $EnquiryMaster->solution->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($EnquiryMaster->solution->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($EnquiryMaster->solution->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php

// Render list options (header, right)
$EnquiryMaster_list->ListOptions->Render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
if ($EnquiryMaster->ExportAll && $EnquiryMaster->Export <> "") {
	$EnquiryMaster_list->StopRec = $EnquiryMaster_list->TotalRecs;
} else {

	// Set the last record to display
	if ($EnquiryMaster_list->TotalRecs > $EnquiryMaster_list->StartRec + $EnquiryMaster_list->DisplayRecs - 1)
		$EnquiryMaster_list->StopRec = $EnquiryMaster_list->StartRec + $EnquiryMaster_list->DisplayRecs - 1;
	else
		$EnquiryMaster_list->StopRec = $EnquiryMaster_list->TotalRecs;
}
$EnquiryMaster_list->RecCnt = $EnquiryMaster_list->StartRec - 1;
if ($EnquiryMaster_list->Recordset && !$EnquiryMaster_list->Recordset->EOF) {
	$EnquiryMaster_list->Recordset->MoveFirst();
	if (!$bSelectLimit && $EnquiryMaster_list->StartRec > 1)
		$EnquiryMaster_list->Recordset->Move($EnquiryMaster_list->StartRec - 1);
} elseif (!$EnquiryMaster->AllowAddDeleteRow && $EnquiryMaster_list->StopRec == 0) {
	$EnquiryMaster_list->StopRec = $EnquiryMaster->GridAddRowCount;
}

// Initialize aggregate
$EnquiryMaster->RowType = EW_ROWTYPE_AGGREGATEINIT;
$EnquiryMaster->ResetAttrs();
$EnquiryMaster_list->RenderRow();
while ($EnquiryMaster_list->RecCnt < $EnquiryMaster_list->StopRec) {
	$EnquiryMaster_list->RecCnt++;
	if (intval($EnquiryMaster_list->RecCnt) >= intval($EnquiryMaster_list->StartRec)) {
		$EnquiryMaster_list->RowCnt++;

		// Set up key count
		$EnquiryMaster_list->KeyCount = $EnquiryMaster_list->RowIndex;

		// Init row class and style
		$EnquiryMaster->ResetAttrs();
		$EnquiryMaster->CssClass = "";
		if ($EnquiryMaster->CurrentAction == "gridadd") {
		} else {
			$EnquiryMaster_list->LoadRowValues($EnquiryMaster_list->Recordset); // Load row values
		}
		$EnquiryMaster->RowType = EW_ROWTYPE_VIEW; // Render view

		// Set up row id / data-rowindex
		$EnquiryMaster->RowAttrs = array_merge($EnquiryMaster->RowAttrs, array('data-rowindex'=>$EnquiryMaster_list->RowCnt, 'id'=>'r' . $EnquiryMaster_list->RowCnt . '_EnquiryMaster', 'data-rowtype'=>$EnquiryMaster->RowType));

		// Render row
		$EnquiryMaster_list->RenderRow();

		// Render list options
		$EnquiryMaster_list->RenderListOptions();
?>
	<tr<?php echo $EnquiryMaster->RowAttributes() ?>>
<?php

// Render list options (body, left)
$EnquiryMaster_list->ListOptions->Render("body", "left", $EnquiryMaster_list->RowCnt);
?>
	<?php if ($EnquiryMaster->CallId->Visible) { // CallId ?>
		<td<?php echo $EnquiryMaster->CallId->CellAttributes() ?>><span id="el<?php echo $EnquiryMaster_list->RowCnt ?>_EnquiryMaster_CallId" class="EnquiryMaster_CallId">
<span<?php echo $EnquiryMaster->CallId->ViewAttributes() ?>>
<?php echo $EnquiryMaster->CallId->ListViewValue() ?></span>
</span><a id="<?php echo $EnquiryMaster_list->PageObjName . "_row_" . $EnquiryMaster_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($EnquiryMaster->CallerName->Visible) { // CallerName ?>
		<td<?php echo $EnquiryMaster->CallerName->CellAttributes() ?>><span id="el<?php echo $EnquiryMaster_list->RowCnt ?>_EnquiryMaster_CallerName" class="EnquiryMaster_CallerName">
<span<?php echo $EnquiryMaster->CallerName->ViewAttributes() ?>>
<?php echo $EnquiryMaster->CallerName->ListViewValue() ?></span>
</span><a id="<?php echo $EnquiryMaster_list->PageObjName . "_row_" . $EnquiryMaster_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($EnquiryMaster->OperatorName->Visible) { // OperatorName ?>
		<td<?php echo $EnquiryMaster->OperatorName->CellAttributes() ?>><span id="el<?php echo $EnquiryMaster_list->RowCnt ?>_EnquiryMaster_OperatorName" class="EnquiryMaster_OperatorName">
<span<?php echo $EnquiryMaster->OperatorName->ViewAttributes() ?>>
<?php echo $EnquiryMaster->OperatorName->ListViewValue() ?></span>
</span><a id="<?php echo $EnquiryMaster_list->PageObjName . "_row_" . $EnquiryMaster_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($EnquiryMaster->AllocatedTo->Visible) { // AllocatedTo ?>
		<td<?php echo $EnquiryMaster->AllocatedTo->CellAttributes() ?>><span id="el<?php echo $EnquiryMaster_list->RowCnt ?>_EnquiryMaster_AllocatedTo" class="EnquiryMaster_AllocatedTo">
<span<?php echo $EnquiryMaster->AllocatedTo->ViewAttributes() ?>>
<?php echo $EnquiryMaster->AllocatedTo->ListViewValue() ?></span>
</span><a id="<?php echo $EnquiryMaster_list->PageObjName . "_row_" . $EnquiryMaster_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($EnquiryMaster->timeOfCall->Visible) { // timeOfCall ?>
		<td<?php echo $EnquiryMaster->timeOfCall->CellAttributes() ?>><span id="el<?php echo $EnquiryMaster_list->RowCnt ?>_EnquiryMaster_timeOfCall" class="EnquiryMaster_timeOfCall">
<span<?php echo $EnquiryMaster->timeOfCall->ViewAttributes() ?>>
<?php echo $EnquiryMaster->timeOfCall->ListViewValue() ?></span>
</span><a id="<?php echo $EnquiryMaster_list->PageObjName . "_row_" . $EnquiryMaster_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($EnquiryMaster->callDescription->Visible) { // callDescription ?>
		<td<?php echo $EnquiryMaster->callDescription->CellAttributes() ?>><span id="el<?php echo $EnquiryMaster_list->RowCnt ?>_EnquiryMaster_callDescription" class="EnquiryMaster_callDescription">
<span<?php echo $EnquiryMaster->callDescription->ViewAttributes() ?>>
<?php echo $EnquiryMaster->callDescription->ListViewValue() ?></span>
</span><a id="<?php echo $EnquiryMaster_list->PageObjName . "_row_" . $EnquiryMaster_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($EnquiryMaster->status->Visible) { // status ?>
		<td<?php echo $EnquiryMaster->status->CellAttributes() ?>><span id="el<?php echo $EnquiryMaster_list->RowCnt ?>_EnquiryMaster_status" class="EnquiryMaster_status">
<span<?php echo $EnquiryMaster->status->ViewAttributes() ?>>
<?php echo $EnquiryMaster->status->ListViewValue() ?></span>
</span><a id="<?php echo $EnquiryMaster_list->PageObjName . "_row_" . $EnquiryMaster_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($EnquiryMaster->solution->Visible) { // solution ?>
		<td<?php echo $EnquiryMaster->solution->CellAttributes() ?>><span id="el<?php echo $EnquiryMaster_list->RowCnt ?>_EnquiryMaster_solution" class="EnquiryMaster_solution">
<span<?php echo $EnquiryMaster->solution->ViewAttributes() ?>>
<?php echo $EnquiryMaster->solution->ListViewValue() ?></span>
</span><a id="<?php echo $EnquiryMaster_list->PageObjName . "_row_" . $EnquiryMaster_list->RowCnt ?>"></a></td>
	<?php } ?>
<?php

// Render list options (body, right)
$EnquiryMaster_list->ListOptions->Render("body", "right", $EnquiryMaster_list->RowCnt);
?>
	</tr>
<?php
	}
	if ($EnquiryMaster->CurrentAction <> "gridadd")
		$EnquiryMaster_list->Recordset->MoveNext();
}
?>
</tbody>
</table>
<?php } ?>
<?php if ($EnquiryMaster->CurrentAction == "") { ?>
<input type="hidden" name="a_list" id="a_list" value="">
<?php } ?>
</div>
</form>
<?php

// Close recordset
if ($EnquiryMaster_list->Recordset)
	$EnquiryMaster_list->Recordset->Close();
?>
<?php if ($EnquiryMaster->Export == "") { ?>
<div class="ewGridLowerPanel">
<?php if ($EnquiryMaster->CurrentAction <> "gridadd" && $EnquiryMaster->CurrentAction <> "gridedit") { ?>
<form name="ewpagerform" id="ewpagerform" class="ewForm" action="<?php echo ew_CurrentPage() ?>">
<table class="ewPager"><tr><td>
<?php if (!isset($EnquiryMaster_list->Pager)) $EnquiryMaster_list->Pager = new cPrevNextPager($EnquiryMaster_list->StartRec, $EnquiryMaster_list->DisplayRecs, $EnquiryMaster_list->TotalRecs) ?>
<?php if ($EnquiryMaster_list->Pager->RecordCount > 0) { ?>
	<table cellspacing="0" class="ewStdTable"><tbody><tr><td><span class="phpmaker"><?php echo $Language->Phrase("Page") ?>&nbsp;</span></td>
<!--first page button-->
	<?php if ($EnquiryMaster_list->Pager->FirstButton->Enabled) { ?>
	<td><a href="<?php echo $EnquiryMaster_list->PageUrl() ?>start=<?php echo $EnquiryMaster_list->Pager->FirstButton->Start ?>"><img src="phpimages/first.gif" alt="<?php echo $Language->Phrase("PagerFirst") ?>" width="16" height="16" style="border: 0;"></a></td>
	<?php } else { ?>
	<td><img src="phpimages/firstdisab.gif" alt="<?php echo $Language->Phrase("PagerFirst") ?>" width="16" height="16" style="border: 0;"></td>
	<?php } ?>
<!--previous page button-->
	<?php if ($EnquiryMaster_list->Pager->PrevButton->Enabled) { ?>
	<td><a href="<?php echo $EnquiryMaster_list->PageUrl() ?>start=<?php echo $EnquiryMaster_list->Pager->PrevButton->Start ?>"><img src="phpimages/prev.gif" alt="<?php echo $Language->Phrase("PagerPrevious") ?>" width="16" height="16" style="border: 0;"></a></td>
	<?php } else { ?>
	<td><img src="phpimages/prevdisab.gif" alt="<?php echo $Language->Phrase("PagerPrevious") ?>" width="16" height="16" style="border: 0;"></td>
	<?php } ?>
<!--current page number-->
	<td><input type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" id="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $EnquiryMaster_list->Pager->CurrentPage ?>" size="4"></td>
<!--next page button-->
	<?php if ($EnquiryMaster_list->Pager->NextButton->Enabled) { ?>
	<td><a href="<?php echo $EnquiryMaster_list->PageUrl() ?>start=<?php echo $EnquiryMaster_list->Pager->NextButton->Start ?>"><img src="phpimages/next.gif" alt="<?php echo $Language->Phrase("PagerNext") ?>" width="16" height="16" style="border: 0;"></a></td>	
	<?php } else { ?>
	<td><img src="phpimages/nextdisab.gif" alt="<?php echo $Language->Phrase("PagerNext") ?>" width="16" height="16" style="border: 0;"></td>
	<?php } ?>
<!--last page button-->
	<?php if ($EnquiryMaster_list->Pager->LastButton->Enabled) { ?>
	<td><a href="<?php echo $EnquiryMaster_list->PageUrl() ?>start=<?php echo $EnquiryMaster_list->Pager->LastButton->Start ?>"><img src="phpimages/last.gif" alt="<?php echo $Language->Phrase("PagerLast") ?>" width="16" height="16" style="border: 0;"></a></td>	
	<?php } else { ?>
	<td><img src="phpimages/lastdisab.gif" alt="<?php echo $Language->Phrase("PagerLast") ?>" width="16" height="16" style="border: 0;"></td>
	<?php } ?>
	<td><span class="phpmaker">&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $EnquiryMaster_list->Pager->PageCount ?></span></td>
	</tr></tbody></table>
	</td>	
	<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td>
	<span class="phpmaker"><?php echo $Language->Phrase("Record") ?>&nbsp;<?php echo $EnquiryMaster_list->Pager->FromIndex ?>&nbsp;<?php echo $Language->Phrase("To") ?>&nbsp;<?php echo $EnquiryMaster_list->Pager->ToIndex ?>&nbsp;<?php echo $Language->Phrase("Of") ?>&nbsp;<?php echo $EnquiryMaster_list->Pager->RecordCount ?></span>
<?php } else { ?>
	<?php if ($EnquiryMaster_list->SearchWhere == "0=101") { ?>
	<span class="phpmaker"><?php echo $Language->Phrase("EnterSearchCriteria") ?></span>
	<?php } else { ?>
	<span class="phpmaker"><?php echo $Language->Phrase("NoRecord") ?></span>
	<?php } ?>
<?php } ?>
	</td>
</tr></table>
</form>
<?php } ?>
<span class="phpmaker">
</span>
</div>
<?php } ?>
</td></tr></table>
<?php if ($EnquiryMaster->Export == "") { ?>
<script type="text/javascript">
fEnquiryMasterlistsrch.Init();
fEnquiryMasterlist.Init();
</script>
<?php } ?>
<?php
$EnquiryMaster_list->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<?php if ($EnquiryMaster->Export == "") { ?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php } ?>
<?php include_once "footer.php" ?>
<?php
$EnquiryMaster_list->Page_Terminate();
?>
