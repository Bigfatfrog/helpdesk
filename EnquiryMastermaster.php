<?php

// CallId
// CallerName
// OperatorName
// AllocatedTo
// timeOfCall
// callDescription
// status
// solution

?>
<?php if ($EnquiryMaster->Visible) { ?>
<table cellspacing="0" id="t_EnquiryMaster" class="ewGrid"><tr><td class="ewGridContent">
<div class="ewGridMiddlePanel">
<table id="tbl_EnquiryMastermaster" class="ewTable ewTableSeparate">
	<tbody>
<?php if ($EnquiryMaster->CallId->Visible) { // CallId ?>
		<tr id="r_CallId">
			<td class="ewTableHeader"><table class="ewTableHeaderBtn"><tr><td><?php echo $EnquiryMaster->CallId->FldCaption() ?></td></tr></table></td>
			<td<?php echo $EnquiryMaster->CallId->CellAttributes() ?>><span id="el_EnquiryMaster_CallId">
<span<?php echo $EnquiryMaster->CallId->ViewAttributes() ?>>
<?php echo $EnquiryMaster->CallId->ListViewValue() ?></span>
</span></td>
		</tr>
<?php } ?>
<?php if ($EnquiryMaster->CallerName->Visible) { // CallerName ?>
		<tr id="r_CallerName">
			<td class="ewTableHeader"><table class="ewTableHeaderBtn"><tr><td><?php echo $EnquiryMaster->CallerName->FldCaption() ?></td></tr></table></td>
			<td<?php echo $EnquiryMaster->CallerName->CellAttributes() ?>><span id="el_EnquiryMaster_CallerName">
<span<?php echo $EnquiryMaster->CallerName->ViewAttributes() ?>>
<?php echo $EnquiryMaster->CallerName->ListViewValue() ?></span>
</span></td>
		</tr>
<?php } ?>
<?php if ($EnquiryMaster->OperatorName->Visible) { // OperatorName ?>
		<tr id="r_OperatorName">
			<td class="ewTableHeader"><table class="ewTableHeaderBtn"><tr><td><?php echo $EnquiryMaster->OperatorName->FldCaption() ?></td></tr></table></td>
			<td<?php echo $EnquiryMaster->OperatorName->CellAttributes() ?>><span id="el_EnquiryMaster_OperatorName">
<span<?php echo $EnquiryMaster->OperatorName->ViewAttributes() ?>>
<?php echo $EnquiryMaster->OperatorName->ListViewValue() ?></span>
</span></td>
		</tr>
<?php } ?>
<?php if ($EnquiryMaster->AllocatedTo->Visible) { // AllocatedTo ?>
		<tr id="r_AllocatedTo">
			<td class="ewTableHeader"><table class="ewTableHeaderBtn"><tr><td><?php echo $EnquiryMaster->AllocatedTo->FldCaption() ?></td></tr></table></td>
			<td<?php echo $EnquiryMaster->AllocatedTo->CellAttributes() ?>><span id="el_EnquiryMaster_AllocatedTo">
<span<?php echo $EnquiryMaster->AllocatedTo->ViewAttributes() ?>>
<?php echo $EnquiryMaster->AllocatedTo->ListViewValue() ?></span>
</span></td>
		</tr>
<?php } ?>
<?php if ($EnquiryMaster->timeOfCall->Visible) { // timeOfCall ?>
		<tr id="r_timeOfCall">
			<td class="ewTableHeader"><table class="ewTableHeaderBtn"><tr><td><?php echo $EnquiryMaster->timeOfCall->FldCaption() ?></td></tr></table></td>
			<td<?php echo $EnquiryMaster->timeOfCall->CellAttributes() ?>><span id="el_EnquiryMaster_timeOfCall">
<span<?php echo $EnquiryMaster->timeOfCall->ViewAttributes() ?>>
<?php echo $EnquiryMaster->timeOfCall->ListViewValue() ?></span>
</span></td>
		</tr>
<?php } ?>
<?php if ($EnquiryMaster->callDescription->Visible) { // callDescription ?>
		<tr id="r_callDescription">
			<td class="ewTableHeader"><table class="ewTableHeaderBtn"><tr><td><?php echo $EnquiryMaster->callDescription->FldCaption() ?></td></tr></table></td>
			<td<?php echo $EnquiryMaster->callDescription->CellAttributes() ?>><span id="el_EnquiryMaster_callDescription">
<span<?php echo $EnquiryMaster->callDescription->ViewAttributes() ?>>
<?php echo $EnquiryMaster->callDescription->ListViewValue() ?></span>
</span></td>
		</tr>
<?php } ?>
<?php if ($EnquiryMaster->status->Visible) { // status ?>
		<tr id="r_status">
			<td class="ewTableHeader"><table class="ewTableHeaderBtn"><tr><td><?php echo $EnquiryMaster->status->FldCaption() ?></td></tr></table></td>
			<td<?php echo $EnquiryMaster->status->CellAttributes() ?>><span id="el_EnquiryMaster_status">
<span<?php echo $EnquiryMaster->status->ViewAttributes() ?>>
<?php echo $EnquiryMaster->status->ListViewValue() ?></span>
</span></td>
		</tr>
<?php } ?>
<?php if ($EnquiryMaster->solution->Visible) { // solution ?>
		<tr id="r_solution">
			<td class="ewTableHeader"><table class="ewTableHeaderBtn"><tr><td><?php echo $EnquiryMaster->solution->FldCaption() ?></td></tr></table></td>
			<td<?php echo $EnquiryMaster->solution->CellAttributes() ?>><span id="el_EnquiryMaster_solution">
<span<?php echo $EnquiryMaster->solution->ViewAttributes() ?>>
<?php echo $EnquiryMaster->solution->ListViewValue() ?></span>
</span></td>
		</tr>
<?php } ?>
	</tbody>
</table>
</div>
</td></tr></table>
<br>
<?php } ?>
