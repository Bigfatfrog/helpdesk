<?php include_once "staffinfo.php" ?>
<?php

// Create page object
if (!isset($EnquirycallContact_grid)) $EnquirycallContact_grid = new cEnquirycallContact_grid();

// Page init
$EnquirycallContact_grid->Page_Init();

// Page main
$EnquirycallContact_grid->Page_Main();
?>
<?php if ($EnquirycallContact->Export == "") { ?>
<script type="text/javascript">

// Page object
var EnquirycallContact_grid = new ew_Page("EnquirycallContact_grid");
EnquirycallContact_grid.PageID = "grid"; // Page ID
var EW_PAGE_ID = EnquirycallContact_grid.PageID; // For backward compatibility

// Form object
var fEnquirycallContactgrid = new ew_Form("fEnquirycallContactgrid");

// Validate form
fEnquirycallContactgrid.Validate = function(fobj) {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	fobj = fobj || this.Form;
	this.PostAutoSuggest();	
	if (fobj.a_confirm && fobj.a_confirm.value == "F")
		return true;
	var elm, aelm;
	var rowcnt = (fobj.key_count) ? Number(fobj.key_count.value) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // rowcnt == 0 => Inline-Add
	var addcnt = 0;
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = (fobj.key_count) ? String(i) : "";
		var checkrow = (fobj.a_list && fobj.a_list.value == "gridinsert") ? !this.EmptyRow(infix) : true;
		if (checkrow) {
			addcnt++;
		elm = fobj.elements["x" + infix + "_name"];
		if (elm && !ew_HasValue(elm))
			return ew_OnError(this, elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($EnquirycallContact->name->FldCaption()) ?>");
		elm = fobj.elements["x" + infix + "_roleDescription"];
		if (elm && !ew_HasValue(elm))
			return ew_OnError(this, elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($EnquirycallContact->roleDescription->FldCaption()) ?>");
		elm = fobj.elements["x" + infix + "_contactDescription"];
		if (elm && !ew_HasValue(elm))
			return ew_OnError(this, elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($EnquirycallContact->contactDescription->FldCaption()) ?>");

		// Set up row object
		ew_ElementsToRow(fobj, infix);

		// Fire Form_CustomValidate event
		if (!this.Form_CustomValidate(fobj))
			return false;
		} // End Grid Add checking
	}
	return true;
}

// Check empty row
fEnquirycallContactgrid.EmptyRow = function(infix) {
	var fobj = this.Form;
	if (ew_ValueChanged(fobj, infix, "name", false)) return false;
	if (ew_ValueChanged(fobj, infix, "roleDescription", false)) return false;
	if (ew_ValueChanged(fobj, infix, "contactDescription", false)) return false;
	return true;
}

// Form_CustomValidate event
fEnquirycallContactgrid.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fEnquirycallContactgrid.ValidateRequired = true;
<?php } else { ?>
fEnquirycallContactgrid.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
// Form object for search

</script>
<?php } ?>
<?php
if ($EnquirycallContact->CurrentAction == "gridadd") {
	if ($EnquirycallContact->CurrentMode == "copy") {
		$bSelectLimit = EW_SELECT_LIMIT;
		if ($bSelectLimit) {
			$EnquirycallContact_grid->TotalRecs = $EnquirycallContact->SelectRecordCount();
			$EnquirycallContact_grid->Recordset = $EnquirycallContact_grid->LoadRecordset($EnquirycallContact_grid->StartRec-1, $EnquirycallContact_grid->DisplayRecs);
		} else {
			if ($EnquirycallContact_grid->Recordset = $EnquirycallContact_grid->LoadRecordset())
				$EnquirycallContact_grid->TotalRecs = $EnquirycallContact_grid->Recordset->RecordCount();
		}
		$EnquirycallContact_grid->StartRec = 1;
		$EnquirycallContact_grid->DisplayRecs = $EnquirycallContact_grid->TotalRecs;
	} else {
		$EnquirycallContact->CurrentFilter = "0=1";
		$EnquirycallContact_grid->StartRec = 1;
		$EnquirycallContact_grid->DisplayRecs = $EnquirycallContact->GridAddRowCount;
	}
	$EnquirycallContact_grid->TotalRecs = $EnquirycallContact_grid->DisplayRecs;
	$EnquirycallContact_grid->StopRec = $EnquirycallContact_grid->DisplayRecs;
} else {
	$bSelectLimit = EW_SELECT_LIMIT;
	if ($bSelectLimit) {
		$EnquirycallContact_grid->TotalRecs = $EnquirycallContact->SelectRecordCount();
	} else {
		if ($EnquirycallContact_grid->Recordset = $EnquirycallContact_grid->LoadRecordset())
			$EnquirycallContact_grid->TotalRecs = $EnquirycallContact_grid->Recordset->RecordCount();
	}
	$EnquirycallContact_grid->StartRec = 1;
	$EnquirycallContact_grid->DisplayRecs = $EnquirycallContact_grid->TotalRecs; // Display all records
	if ($bSelectLimit)
		$EnquirycallContact_grid->Recordset = $EnquirycallContact_grid->LoadRecordset($EnquirycallContact_grid->StartRec-1, $EnquirycallContact_grid->DisplayRecs);
}
?>
<p style="white-space: nowrap;"><span id="ewPageCaption" class="ewTitle ewTableTitle"><?php if ($EnquirycallContact->CurrentMode == "add" || $EnquirycallContact->CurrentMode == "copy") { ?><?php echo $Language->Phrase("Add") ?><?php } elseif ($EnquirycallContact->CurrentMode == "edit") { ?><?php echo $Language->Phrase("Edit") ?><?php } ?>&nbsp;<?php echo $Language->Phrase("TblTypeVIEW") ?><?php echo $EnquirycallContact->TableCaption() ?></span></p>
</p>
<?php $EnquirycallContact_grid->ShowPageHeader(); ?>
<?php
$EnquirycallContact_grid->ShowMessage();
?>
<br>
<table cellspacing="0" class="ewGrid"><tr><td class="ewGridContent">
<div id="fEnquirycallContactgrid" class="ewForm">
<div id="gmp_EnquirycallContact" class="ewGridMiddlePanel">
<table id="tbl_EnquirycallContactgrid" class="ewTable ewTableSeparate">
<?php echo $EnquirycallContact->TableCustomInnerHtml ?>
<thead><!-- Table header -->
	<tr class="ewTableHeader">
<?php

// Render list options
$EnquirycallContact_grid->RenderListOptions();

// Render list options (header, left)
$EnquirycallContact_grid->ListOptions->Render("header", "left");
?>
<?php if ($EnquirycallContact->name->Visible) { // name ?>
	<?php if ($EnquirycallContact->SortUrl($EnquirycallContact->name) == "") { ?>
		<td><span id="elh_EnquirycallContact_name" class="EnquirycallContact_name"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $EnquirycallContact->name->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div><span id="elh_EnquirycallContact_name" class="EnquirycallContact_name">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $EnquirycallContact->name->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($EnquirycallContact->name->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($EnquirycallContact->name->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($EnquirycallContact->roleDescription->Visible) { // roleDescription ?>
	<?php if ($EnquirycallContact->SortUrl($EnquirycallContact->roleDescription) == "") { ?>
		<td><span id="elh_EnquirycallContact_roleDescription" class="EnquirycallContact_roleDescription"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $EnquirycallContact->roleDescription->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div><span id="elh_EnquirycallContact_roleDescription" class="EnquirycallContact_roleDescription">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $EnquirycallContact->roleDescription->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($EnquirycallContact->roleDescription->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($EnquirycallContact->roleDescription->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($EnquirycallContact->contactDescription->Visible) { // contactDescription ?>
	<?php if ($EnquirycallContact->SortUrl($EnquirycallContact->contactDescription) == "") { ?>
		<td><span id="elh_EnquirycallContact_contactDescription" class="EnquirycallContact_contactDescription"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $EnquirycallContact->contactDescription->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div><span id="elh_EnquirycallContact_contactDescription" class="EnquirycallContact_contactDescription">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $EnquirycallContact->contactDescription->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($EnquirycallContact->contactDescription->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($EnquirycallContact->contactDescription->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($EnquirycallContact->contactTime->Visible) { // contactTime ?>
	<?php if ($EnquirycallContact->SortUrl($EnquirycallContact->contactTime) == "") { ?>
		<td><span id="elh_EnquirycallContact_contactTime" class="EnquirycallContact_contactTime"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $EnquirycallContact->contactTime->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div><span id="elh_EnquirycallContact_contactTime" class="EnquirycallContact_contactTime">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $EnquirycallContact->contactTime->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($EnquirycallContact->contactTime->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($EnquirycallContact->contactTime->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php

// Render list options (header, right)
$EnquirycallContact_grid->ListOptions->Render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
$EnquirycallContact_grid->StartRec = 1;
$EnquirycallContact_grid->StopRec = $EnquirycallContact_grid->TotalRecs; // Show all records

// Restore number of post back records
if ($objForm) {
	$objForm->Index = -1;
	if ($objForm->HasValue("key_count") && ($EnquirycallContact->CurrentAction == "gridadd" || $EnquirycallContact->CurrentAction == "gridedit" || $EnquirycallContact->CurrentAction == "F")) {
		$EnquirycallContact_grid->KeyCount = $objForm->GetValue("key_count");
		$EnquirycallContact_grid->StopRec = $EnquirycallContact_grid->KeyCount;
	}
}
$EnquirycallContact_grid->RecCnt = $EnquirycallContact_grid->StartRec - 1;
if ($EnquirycallContact_grid->Recordset && !$EnquirycallContact_grid->Recordset->EOF) {
	$EnquirycallContact_grid->Recordset->MoveFirst();
	if (!$bSelectLimit && $EnquirycallContact_grid->StartRec > 1)
		$EnquirycallContact_grid->Recordset->Move($EnquirycallContact_grid->StartRec - 1);
} elseif (!$EnquirycallContact->AllowAddDeleteRow && $EnquirycallContact_grid->StopRec == 0) {
	$EnquirycallContact_grid->StopRec = $EnquirycallContact->GridAddRowCount;
}

// Initialize aggregate
$EnquirycallContact->RowType = EW_ROWTYPE_AGGREGATEINIT;
$EnquirycallContact->ResetAttrs();
$EnquirycallContact_grid->RenderRow();
if ($EnquirycallContact->CurrentAction == "gridadd")
	$EnquirycallContact_grid->RowIndex = 0;
if ($EnquirycallContact->CurrentAction == "gridedit")
	$EnquirycallContact_grid->RowIndex = 0;
while ($EnquirycallContact_grid->RecCnt < $EnquirycallContact_grid->StopRec) {
	$EnquirycallContact_grid->RecCnt++;
	if (intval($EnquirycallContact_grid->RecCnt) >= intval($EnquirycallContact_grid->StartRec)) {
		$EnquirycallContact_grid->RowCnt++;
		if ($EnquirycallContact->CurrentAction == "gridadd" || $EnquirycallContact->CurrentAction == "gridedit" || $EnquirycallContact->CurrentAction == "F") {
			$EnquirycallContact_grid->RowIndex++;
			$objForm->Index = $EnquirycallContact_grid->RowIndex;
			if ($objForm->HasValue("k_action"))
				$EnquirycallContact_grid->RowAction = strval($objForm->GetValue("k_action"));
			elseif ($EnquirycallContact->CurrentAction == "gridadd")
				$EnquirycallContact_grid->RowAction = "insert";
			else
				$EnquirycallContact_grid->RowAction = "";
		}

		// Set up key count
		$EnquirycallContact_grid->KeyCount = $EnquirycallContact_grid->RowIndex;

		// Init row class and style
		$EnquirycallContact->ResetAttrs();
		$EnquirycallContact->CssClass = "";
		if ($EnquirycallContact->CurrentAction == "gridadd") {
			if ($EnquirycallContact->CurrentMode == "copy") {
				$EnquirycallContact_grid->LoadRowValues($EnquirycallContact_grid->Recordset); // Load row values
				$EnquirycallContact_grid->SetRecordKey($EnquirycallContact_grid->RowOldKey, $EnquirycallContact_grid->Recordset); // Set old record key
			} else {
				$EnquirycallContact_grid->LoadDefaultValues(); // Load default values
				$EnquirycallContact_grid->RowOldKey = ""; // Clear old key value
			}
		} elseif ($EnquirycallContact->CurrentAction == "gridedit") {
			$EnquirycallContact_grid->LoadRowValues($EnquirycallContact_grid->Recordset); // Load row values
		}
		$EnquirycallContact->RowType = EW_ROWTYPE_VIEW; // Render view
		if ($EnquirycallContact->CurrentAction == "gridadd") // Grid add
			$EnquirycallContact->RowType = EW_ROWTYPE_ADD; // Render add
		if ($EnquirycallContact->CurrentAction == "gridadd" && $EnquirycallContact->EventCancelled && !$objForm->HasValue("k_blankrow")) // Insert failed
			$EnquirycallContact_grid->RestoreCurrentRowFormValues($EnquirycallContact_grid->RowIndex); // Restore form values
		if ($EnquirycallContact->CurrentAction == "gridedit") { // Grid edit
			if ($EnquirycallContact->EventCancelled) {
				$EnquirycallContact_grid->RestoreCurrentRowFormValues($EnquirycallContact_grid->RowIndex); // Restore form values
			}
			if ($EnquirycallContact_grid->RowAction == "insert")
				$EnquirycallContact->RowType = EW_ROWTYPE_ADD; // Render add
			else
				$EnquirycallContact->RowType = EW_ROWTYPE_EDIT; // Render edit
		}
		if ($EnquirycallContact->CurrentAction == "gridedit" && ($EnquirycallContact->RowType == EW_ROWTYPE_EDIT || $EnquirycallContact->RowType == EW_ROWTYPE_ADD) && $EnquirycallContact->EventCancelled) // Update failed
			$EnquirycallContact_grid->RestoreCurrentRowFormValues($EnquirycallContact_grid->RowIndex); // Restore form values
		if ($EnquirycallContact->RowType == EW_ROWTYPE_EDIT) // Edit row
			$EnquirycallContact_grid->EditRowCnt++;
		if ($EnquirycallContact->CurrentAction == "F") // Confirm row
			$EnquirycallContact_grid->RestoreCurrentRowFormValues($EnquirycallContact_grid->RowIndex); // Restore form values

		// Set up row id / data-rowindex
		$EnquirycallContact->RowAttrs = array_merge($EnquirycallContact->RowAttrs, array('data-rowindex'=>$EnquirycallContact_grid->RowCnt, 'id'=>'r' . $EnquirycallContact_grid->RowCnt . '_EnquirycallContact', 'data-rowtype'=>$EnquirycallContact->RowType));

		// Render row
		$EnquirycallContact_grid->RenderRow();

		// Render list options
		$EnquirycallContact_grid->RenderListOptions();

		// Skip delete row / empty row for confirm page
		if ($EnquirycallContact_grid->RowAction <> "delete" && $EnquirycallContact_grid->RowAction <> "insertdelete" && !($EnquirycallContact_grid->RowAction == "insert" && $EnquirycallContact->CurrentAction == "F" && $EnquirycallContact_grid->EmptyRow())) {
?>
	<tr<?php echo $EnquirycallContact->RowAttributes() ?>>
<?php

// Render list options (body, left)
$EnquirycallContact_grid->ListOptions->Render("body", "left", $EnquirycallContact_grid->RowCnt);
?>
	<?php if ($EnquirycallContact->name->Visible) { // name ?>
		<td<?php echo $EnquirycallContact->name->CellAttributes() ?>><span id="el<?php echo $EnquirycallContact_grid->RowCnt ?>_EnquirycallContact_name" class="EnquirycallContact_name">
<?php if ($EnquirycallContact->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="text" name="x<?php echo $EnquirycallContact_grid->RowIndex ?>_name" id="x<?php echo $EnquirycallContact_grid->RowIndex ?>_name" size="30" maxlength="50" value="<?php echo $EnquirycallContact->name->EditValue ?>"<?php echo $EnquirycallContact->name->EditAttributes() ?>>
<input type="hidden" name="o<?php echo $EnquirycallContact_grid->RowIndex ?>_name" id="o<?php echo $EnquirycallContact_grid->RowIndex ?>_name" value="<?php echo ew_HtmlEncode($EnquirycallContact->name->OldValue) ?>">
<?php } ?>
<?php if ($EnquirycallContact->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<input type="text" name="x<?php echo $EnquirycallContact_grid->RowIndex ?>_name" id="x<?php echo $EnquirycallContact_grid->RowIndex ?>_name" size="30" maxlength="50" value="<?php echo $EnquirycallContact->name->EditValue ?>"<?php echo $EnquirycallContact->name->EditAttributes() ?>>
<?php } ?>
<?php if ($EnquirycallContact->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span<?php echo $EnquirycallContact->name->ViewAttributes() ?>>
<?php echo $EnquirycallContact->name->ListViewValue() ?></span>
<input type="hidden" name="x<?php echo $EnquirycallContact_grid->RowIndex ?>_name" id="x<?php echo $EnquirycallContact_grid->RowIndex ?>_name" value="<?php echo ew_HtmlEncode($EnquirycallContact->name->FormValue) ?>">
<input type="hidden" name="o<?php echo $EnquirycallContact_grid->RowIndex ?>_name" id="o<?php echo $EnquirycallContact_grid->RowIndex ?>_name" value="<?php echo ew_HtmlEncode($EnquirycallContact->name->OldValue) ?>">
<?php } ?>
</span><a id="<?php echo $EnquirycallContact_grid->PageObjName . "_row_" . $EnquirycallContact_grid->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($EnquirycallContact->roleDescription->Visible) { // roleDescription ?>
		<td<?php echo $EnquirycallContact->roleDescription->CellAttributes() ?>><span id="el<?php echo $EnquirycallContact_grid->RowCnt ?>_EnquirycallContact_roleDescription" class="EnquirycallContact_roleDescription">
<?php if ($EnquirycallContact->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="text" name="x<?php echo $EnquirycallContact_grid->RowIndex ?>_roleDescription" id="x<?php echo $EnquirycallContact_grid->RowIndex ?>_roleDescription" size="30" maxlength="50" value="<?php echo $EnquirycallContact->roleDescription->EditValue ?>"<?php echo $EnquirycallContact->roleDescription->EditAttributes() ?>>
<input type="hidden" name="o<?php echo $EnquirycallContact_grid->RowIndex ?>_roleDescription" id="o<?php echo $EnquirycallContact_grid->RowIndex ?>_roleDescription" value="<?php echo ew_HtmlEncode($EnquirycallContact->roleDescription->OldValue) ?>">
<?php } ?>
<?php if ($EnquirycallContact->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<input type="text" name="x<?php echo $EnquirycallContact_grid->RowIndex ?>_roleDescription" id="x<?php echo $EnquirycallContact_grid->RowIndex ?>_roleDescription" size="30" maxlength="50" value="<?php echo $EnquirycallContact->roleDescription->EditValue ?>"<?php echo $EnquirycallContact->roleDescription->EditAttributes() ?>>
<?php } ?>
<?php if ($EnquirycallContact->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span<?php echo $EnquirycallContact->roleDescription->ViewAttributes() ?>>
<?php echo $EnquirycallContact->roleDescription->ListViewValue() ?></span>
<input type="hidden" name="x<?php echo $EnquirycallContact_grid->RowIndex ?>_roleDescription" id="x<?php echo $EnquirycallContact_grid->RowIndex ?>_roleDescription" value="<?php echo ew_HtmlEncode($EnquirycallContact->roleDescription->FormValue) ?>">
<input type="hidden" name="o<?php echo $EnquirycallContact_grid->RowIndex ?>_roleDescription" id="o<?php echo $EnquirycallContact_grid->RowIndex ?>_roleDescription" value="<?php echo ew_HtmlEncode($EnquirycallContact->roleDescription->OldValue) ?>">
<?php } ?>
</span><a id="<?php echo $EnquirycallContact_grid->PageObjName . "_row_" . $EnquirycallContact_grid->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($EnquirycallContact->contactDescription->Visible) { // contactDescription ?>
		<td<?php echo $EnquirycallContact->contactDescription->CellAttributes() ?>><span id="el<?php echo $EnquirycallContact_grid->RowCnt ?>_EnquirycallContact_contactDescription" class="EnquirycallContact_contactDescription">
<?php if ($EnquirycallContact->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<textarea name="x<?php echo $EnquirycallContact_grid->RowIndex ?>_contactDescription" id="x<?php echo $EnquirycallContact_grid->RowIndex ?>_contactDescription" cols="35" rows="4"<?php echo $EnquirycallContact->contactDescription->EditAttributes() ?>><?php echo $EnquirycallContact->contactDescription->EditValue ?></textarea>
<input type="hidden" name="o<?php echo $EnquirycallContact_grid->RowIndex ?>_contactDescription" id="o<?php echo $EnquirycallContact_grid->RowIndex ?>_contactDescription" value="<?php echo ew_HtmlEncode($EnquirycallContact->contactDescription->OldValue) ?>">
<?php } ?>
<?php if ($EnquirycallContact->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<textarea name="x<?php echo $EnquirycallContact_grid->RowIndex ?>_contactDescription" id="x<?php echo $EnquirycallContact_grid->RowIndex ?>_contactDescription" cols="35" rows="4"<?php echo $EnquirycallContact->contactDescription->EditAttributes() ?>><?php echo $EnquirycallContact->contactDescription->EditValue ?></textarea>
<?php } ?>
<?php if ($EnquirycallContact->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span<?php echo $EnquirycallContact->contactDescription->ViewAttributes() ?>>
<?php echo $EnquirycallContact->contactDescription->ListViewValue() ?></span>
<input type="hidden" name="x<?php echo $EnquirycallContact_grid->RowIndex ?>_contactDescription" id="x<?php echo $EnquirycallContact_grid->RowIndex ?>_contactDescription" value="<?php echo ew_HtmlEncode($EnquirycallContact->contactDescription->FormValue) ?>">
<input type="hidden" name="o<?php echo $EnquirycallContact_grid->RowIndex ?>_contactDescription" id="o<?php echo $EnquirycallContact_grid->RowIndex ?>_contactDescription" value="<?php echo ew_HtmlEncode($EnquirycallContact->contactDescription->OldValue) ?>">
<?php } ?>
</span><a id="<?php echo $EnquirycallContact_grid->PageObjName . "_row_" . $EnquirycallContact_grid->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($EnquirycallContact->contactTime->Visible) { // contactTime ?>
		<td<?php echo $EnquirycallContact->contactTime->CellAttributes() ?>><span id="el<?php echo $EnquirycallContact_grid->RowCnt ?>_EnquirycallContact_contactTime" class="EnquirycallContact_contactTime">
<?php if ($EnquirycallContact->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="hidden" name="o<?php echo $EnquirycallContact_grid->RowIndex ?>_contactTime" id="o<?php echo $EnquirycallContact_grid->RowIndex ?>_contactTime" value="<?php echo ew_HtmlEncode($EnquirycallContact->contactTime->OldValue) ?>">
<?php } ?>
<?php if ($EnquirycallContact->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<?php } ?>
<?php if ($EnquirycallContact->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span<?php echo $EnquirycallContact->contactTime->ViewAttributes() ?>>
<?php echo $EnquirycallContact->contactTime->ListViewValue() ?></span>
<input type="hidden" name="x<?php echo $EnquirycallContact_grid->RowIndex ?>_contactTime" id="x<?php echo $EnquirycallContact_grid->RowIndex ?>_contactTime" value="<?php echo ew_HtmlEncode($EnquirycallContact->contactTime->FormValue) ?>">
<input type="hidden" name="o<?php echo $EnquirycallContact_grid->RowIndex ?>_contactTime" id="o<?php echo $EnquirycallContact_grid->RowIndex ?>_contactTime" value="<?php echo ew_HtmlEncode($EnquirycallContact->contactTime->OldValue) ?>">
<?php } ?>
</span><a id="<?php echo $EnquirycallContact_grid->PageObjName . "_row_" . $EnquirycallContact_grid->RowCnt ?>"></a></td>
	<?php } ?>
<?php

// Render list options (body, right)
$EnquirycallContact_grid->ListOptions->Render("body", "right", $EnquirycallContact_grid->RowCnt);
?>
	</tr>
<?php if ($EnquirycallContact->RowType == EW_ROWTYPE_ADD || $EnquirycallContact->RowType == EW_ROWTYPE_EDIT) { ?>
<script type="text/javascript">
fEnquirycallContactgrid.UpdateOpts(<?php echo $EnquirycallContact_grid->RowIndex ?>);
</script>
<?php } ?>
<?php
	}
	} // End delete row checking
	if ($EnquirycallContact->CurrentAction <> "gridadd" || $EnquirycallContact->CurrentMode == "copy")
		if (!$EnquirycallContact_grid->Recordset->EOF) $EnquirycallContact_grid->Recordset->MoveNext();
}
?>
<?php
	if ($EnquirycallContact->CurrentMode == "add" || $EnquirycallContact->CurrentMode == "copy" || $EnquirycallContact->CurrentMode == "edit") {
		$EnquirycallContact_grid->RowIndex = '$rowindex$';
		$EnquirycallContact_grid->LoadDefaultValues();

		// Set row properties
		$EnquirycallContact->ResetAttrs();
		$EnquirycallContact->RowAttrs = array_merge($EnquirycallContact->RowAttrs, array('data-rowindex'=>$EnquirycallContact_grid->RowIndex, 'id'=>'r0_EnquirycallContact', 'data-rowtype'=>EW_ROWTYPE_ADD));
		ew_AppendClass($EnquirycallContact->RowAttrs["class"], "ewTemplate");
		$EnquirycallContact->RowType = EW_ROWTYPE_ADD;

		// Render row
		$EnquirycallContact_grid->RenderRow();

		// Render list options
		$EnquirycallContact_grid->RenderListOptions();
		$EnquirycallContact_grid->StartRowCnt = 0;
?>
	<tr<?php echo $EnquirycallContact->RowAttributes() ?>>
<?php

// Render list options (body, left)
$EnquirycallContact_grid->ListOptions->Render("body", "left", $EnquirycallContact_grid->RowIndex);
?>
	<?php if ($EnquirycallContact->name->Visible) { // name ?>
		<td><span id="el$rowindex$_EnquirycallContact_name" class="EnquirycallContact_name">
<?php if ($EnquirycallContact->CurrentAction <> "F") { ?>
<input type="text" name="x<?php echo $EnquirycallContact_grid->RowIndex ?>_name" id="x<?php echo $EnquirycallContact_grid->RowIndex ?>_name" size="30" maxlength="50" value="<?php echo $EnquirycallContact->name->EditValue ?>"<?php echo $EnquirycallContact->name->EditAttributes() ?>>
<?php } else { ?>
<span<?php echo $EnquirycallContact->name->ViewAttributes() ?>>
<?php echo $EnquirycallContact->name->ViewValue ?></span>
<input type="hidden" name="x<?php echo $EnquirycallContact_grid->RowIndex ?>_name" id="x<?php echo $EnquirycallContact_grid->RowIndex ?>_name" value="<?php echo ew_HtmlEncode($EnquirycallContact->name->FormValue) ?>">
<?php } ?>
<input type="hidden" name="o<?php echo $EnquirycallContact_grid->RowIndex ?>_name" id="o<?php echo $EnquirycallContact_grid->RowIndex ?>_name" value="<?php echo ew_HtmlEncode($EnquirycallContact->name->OldValue) ?>">
</span></td>
	<?php } ?>
	<?php if ($EnquirycallContact->roleDescription->Visible) { // roleDescription ?>
		<td><span id="el$rowindex$_EnquirycallContact_roleDescription" class="EnquirycallContact_roleDescription">
<?php if ($EnquirycallContact->CurrentAction <> "F") { ?>
<input type="text" name="x<?php echo $EnquirycallContact_grid->RowIndex ?>_roleDescription" id="x<?php echo $EnquirycallContact_grid->RowIndex ?>_roleDescription" size="30" maxlength="50" value="<?php echo $EnquirycallContact->roleDescription->EditValue ?>"<?php echo $EnquirycallContact->roleDescription->EditAttributes() ?>>
<?php } else { ?>
<span<?php echo $EnquirycallContact->roleDescription->ViewAttributes() ?>>
<?php echo $EnquirycallContact->roleDescription->ViewValue ?></span>
<input type="hidden" name="x<?php echo $EnquirycallContact_grid->RowIndex ?>_roleDescription" id="x<?php echo $EnquirycallContact_grid->RowIndex ?>_roleDescription" value="<?php echo ew_HtmlEncode($EnquirycallContact->roleDescription->FormValue) ?>">
<?php } ?>
<input type="hidden" name="o<?php echo $EnquirycallContact_grid->RowIndex ?>_roleDescription" id="o<?php echo $EnquirycallContact_grid->RowIndex ?>_roleDescription" value="<?php echo ew_HtmlEncode($EnquirycallContact->roleDescription->OldValue) ?>">
</span></td>
	<?php } ?>
	<?php if ($EnquirycallContact->contactDescription->Visible) { // contactDescription ?>
		<td><span id="el$rowindex$_EnquirycallContact_contactDescription" class="EnquirycallContact_contactDescription">
<?php if ($EnquirycallContact->CurrentAction <> "F") { ?>
<textarea name="x<?php echo $EnquirycallContact_grid->RowIndex ?>_contactDescription" id="x<?php echo $EnquirycallContact_grid->RowIndex ?>_contactDescription" cols="35" rows="4"<?php echo $EnquirycallContact->contactDescription->EditAttributes() ?>><?php echo $EnquirycallContact->contactDescription->EditValue ?></textarea>
<?php } else { ?>
<span<?php echo $EnquirycallContact->contactDescription->ViewAttributes() ?>>
<?php echo $EnquirycallContact->contactDescription->ViewValue ?></span>
<input type="hidden" name="x<?php echo $EnquirycallContact_grid->RowIndex ?>_contactDescription" id="x<?php echo $EnquirycallContact_grid->RowIndex ?>_contactDescription" value="<?php echo ew_HtmlEncode($EnquirycallContact->contactDescription->FormValue) ?>">
<?php } ?>
<input type="hidden" name="o<?php echo $EnquirycallContact_grid->RowIndex ?>_contactDescription" id="o<?php echo $EnquirycallContact_grid->RowIndex ?>_contactDescription" value="<?php echo ew_HtmlEncode($EnquirycallContact->contactDescription->OldValue) ?>">
</span></td>
	<?php } ?>
	<?php if ($EnquirycallContact->contactTime->Visible) { // contactTime ?>
		<td><span id="el$rowindex$_EnquirycallContact_contactTime" class="EnquirycallContact_contactTime">
<?php if ($EnquirycallContact->CurrentAction <> "F") { ?>
<?php } else { ?>
<span<?php echo $EnquirycallContact->contactTime->ViewAttributes() ?>>
<?php echo $EnquirycallContact->contactTime->ViewValue ?></span>
<input type="hidden" name="x<?php echo $EnquirycallContact_grid->RowIndex ?>_contactTime" id="x<?php echo $EnquirycallContact_grid->RowIndex ?>_contactTime" value="<?php echo ew_HtmlEncode($EnquirycallContact->contactTime->FormValue) ?>">
<?php } ?>
<input type="hidden" name="o<?php echo $EnquirycallContact_grid->RowIndex ?>_contactTime" id="o<?php echo $EnquirycallContact_grid->RowIndex ?>_contactTime" value="<?php echo ew_HtmlEncode($EnquirycallContact->contactTime->OldValue) ?>">
</span></td>
	<?php } ?>
<?php

// Render list options (body, right)
$EnquirycallContact_grid->ListOptions->Render("body", "right", $EnquirycallContact_grid->RowCnt);
?>
<script type="text/javascript">
fEnquirycallContactgrid.UpdateOpts(<?php echo $EnquirycallContact_grid->RowIndex ?>);
</script>
	</tr>
<?php
}
?>
</tbody>
</table>
<?php if ($EnquirycallContact->CurrentMode == "add" || $EnquirycallContact->CurrentMode == "copy") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridinsert">
<input type="hidden" name="key_count" id="key_count" value="<?php echo $EnquirycallContact_grid->KeyCount ?>">
<?php echo $EnquirycallContact_grid->MultiSelectKey ?>
<?php } ?>
<?php if ($EnquirycallContact->CurrentMode == "edit") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridupdate">
<input type="hidden" name="key_count" id="key_count" value="<?php echo $EnquirycallContact_grid->KeyCount ?>">
<?php echo $EnquirycallContact_grid->MultiSelectKey ?>
<?php } ?>
<?php if ($EnquirycallContact->CurrentMode == "") { ?>
<input type="hidden" name="a_list" id="a_list" value="">
<?php } ?>
<input type="hidden" name="detailpage" id="detailpage" value="fEnquirycallContactgrid">
</div>
<?php

// Close recordset
if ($EnquirycallContact_grid->Recordset)
	$EnquirycallContact_grid->Recordset->Close();
?>
<?php if (($EnquirycallContact->CurrentMode == "add" || $EnquirycallContact->CurrentMode == "copy" || $EnquirycallContact->CurrentMode == "edit") && $EnquirycallContact->CurrentAction != "F") { // add/copy/edit mode ?>
<div class="ewGridLowerPanel">
</div>
<?php } ?>
</div>
</td></tr></table>
<?php if ($EnquirycallContact->Export == "") { ?>
<script type="text/javascript">
fEnquirycallContactgrid.Init();
</script>
<?php } ?>
<?php
$EnquirycallContact_grid->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<?php
$EnquirycallContact_grid->Page_Terminate();
$Page = &$MasterPage;
?>
