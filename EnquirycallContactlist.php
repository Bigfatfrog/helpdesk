<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg9.php" ?>
<?php include_once "ewmysql9.php" ?>
<?php include_once "phpfn9.php" ?>
<?php include_once "EnquirycallContactinfo.php" ?>
<?php include_once "staffinfo.php" ?>
<?php include_once "EnquiryMasterinfo.php" ?>
<?php include_once "userfn9.php" ?>
<?php

//
// Page class
//

$EnquirycallContact_list = NULL; // Initialize page object first

class cEnquirycallContact_list extends cEnquirycallContact {

	// Page ID
	var $PageID = 'list';

	// Project ID
	var $ProjectID = "{4F01D370-BA3A-4C91-9445-0AA347261657}";

	// Table name
	var $TableName = 'EnquirycallContact';

	// Page object name
	var $PageObjName = 'EnquirycallContact_list';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Page URLs
	var $AddUrl;
	var $EditUrl;
	var $CopyUrl;
	var $DeleteUrl;
	var $ViewUrl;
	var $ListUrl;

	// Export URLs
	var $ExportPrintUrl;
	var $ExportHtmlUrl;
	var $ExportExcelUrl;
	var $ExportWordUrl;
	var $ExportXmlUrl;
	var $ExportCsvUrl;
	var $ExportPdfUrl;

	// Update URLs
	var $InlineAddUrl;
	var $InlineCopyUrl;
	var $InlineEditUrl;
	var $GridAddUrl;
	var $GridEditUrl;
	var $MultiDeleteUrl;
	var $MultiUpdateUrl;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			$html .= "<p class=\"ewMessage\">" . $sMessage . "</p>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			$html .= "<table class=\"ewMessageTable\"><tr><td class=\"ewWarningIcon\"></td><td class=\"ewWarningMessage\">" . $sWarningMessage . "</td></tr></table>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			$html .= "<table class=\"ewMessageTable\"><tr><td class=\"ewSuccessIcon\"></td><td class=\"ewSuccessMessage\">" . $sSuccessMessage . "</td></tr></table>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			$html .= "<table class=\"ewMessageTable\"><tr><td class=\"ewErrorIcon\"></td><td class=\"ewErrorMessage\">" . $sErrorMessage . "</td></tr></table>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p class=\"phpmaker\">" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Fotoer exists, display
			echo "<p class=\"phpmaker\">" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language, $UserAgent;

		// User agent
		$UserAgent = ew_UserAgent();
		$GLOBALS["Page"] = &$this;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (EnquirycallContact)
		if (!isset($GLOBALS["EnquirycallContact"])) {
			$GLOBALS["EnquirycallContact"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["EnquirycallContact"];
		}

		// Initialize URLs
		$this->ExportPrintUrl = $this->PageUrl() . "export=print";
		$this->ExportExcelUrl = $this->PageUrl() . "export=excel";
		$this->ExportWordUrl = $this->PageUrl() . "export=word";
		$this->ExportHtmlUrl = $this->PageUrl() . "export=html";
		$this->ExportXmlUrl = $this->PageUrl() . "export=xml";
		$this->ExportCsvUrl = $this->PageUrl() . "export=csv";
		$this->ExportPdfUrl = $this->PageUrl() . "export=pdf";
		$this->AddUrl = "EnquirycallContactadd.php";
		$this->InlineAddUrl = $this->PageUrl() . "a=add";
		$this->GridAddUrl = $this->PageUrl() . "a=gridadd";
		$this->GridEditUrl = $this->PageUrl() . "a=gridedit";
		$this->MultiDeleteUrl = "EnquirycallContactdelete.php";
		$this->MultiUpdateUrl = "EnquirycallContactupdate.php";

		// Table object (staff)
		if (!isset($GLOBALS['staff'])) $GLOBALS['staff'] = new cstaff();

		// Table object (EnquiryMaster)
		if (!isset($GLOBALS['EnquiryMaster'])) $GLOBALS['EnquiryMaster'] = new cEnquiryMaster();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'list', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'EnquirycallContact', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect();

		// List options
		$this->ListOptions = new cListOptions();
		$this->ListOptions->TableVar = $this->TableVar;

		// Export options
		$this->ExportOptions = new cListOptions();
		$this->ExportOptions->Tag = "span";
		$this->ExportOptions->TagClassName = "ewExportOption";
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if (!$Security->IsLoggedIn()) {
			$Security->SaveLastUrl();
			$this->Page_Terminate("login.php");
		}

		// Get export parameters
		if (@$_GET["export"] <> "") {
			$this->Export = $_GET["export"];
		} elseif (ew_IsHttpPost()) {
			if (@$_POST["exporttype"] <> "")
				$this->Export = $_POST["exporttype"];
		} else {
			$this->setExportReturnUrl(ew_CurrentUrl());
		}
		$gsExport = $this->Export; // Get export parameter, used in header
		$gsExportFile = $this->TableVar; // Get export file, used in header

		// Get grid add count
		$gridaddcnt = @$_GET[EW_TABLE_GRID_ADD_ROW_COUNT];
		if (is_numeric($gridaddcnt) && $gridaddcnt > 0)
			$this->GridAddRowCount = $gridaddcnt;

		// Set up list options
		$this->SetupListOptions();

		// Setup export options
		$this->SetupExportOptions();
		global $gbOldSkipHeaderFooter, $gbSkipHeaderFooter;
		$gbOldSkipHeaderFooter = $gbSkipHeaderFooter;
		$gbSkipHeaderFooter = TRUE;
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"];
		$this->contactTime->Visible = !$this->IsAddOrEdit();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $conn;
		global $gbOldSkipHeaderFooter, $gbSkipHeaderFooter;
		$gbSkipHeaderFooter = $gbOldSkipHeaderFooter;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();
		$this->Page_Redirecting($url);

		 // Close connection
		$conn->Close();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
	}

	// Class variables
	var $ListOptions; // List options
	var $ExportOptions; // Export options
	var $DisplayRecs = 20;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $Pager;
	var $SearchWhere = ""; // Search WHERE clause
	var $RecCnt = 0; // Record count
	var $EditRowCnt;
	var $StartRowCnt = 1;
	var $RowCnt = 0;
	var $Attrs = array(); // Row attributes and cell attributes
	var $RowIndex = 0; // Row index
	var $KeyCount = 0; // Key count
	var $RowAction = ""; // Row action
	var $RowOldKey = ""; // Row old key (for copy)
	var $RecPerRow = 0;
	var $ColCnt = 0;
	var $DbMasterFilter = ""; // Master filter
	var $DbDetailFilter = ""; // Detail filter
	var $MasterRecordExists;	
	var $MultiSelectKey;
	var $Command;
	var $Recordset;
	var $OldRecordset;

	//
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError, $gsSearchError, $Security;

		// Search filters
		$sSrchAdvanced = ""; // Advanced search filter
		$sSrchBasic = ""; // Basic search filter
		$sFilter = "";

		// Get command
		$this->Command = strtolower(@$_GET["cmd"]);
		if ($this->IsPageRequest()) { // Validate request

			// Handle reset command
			$this->ResetCmd();

			// Set up master detail parameters
			$this->SetUpMasterParms();

			// Hide all options
			if ($this->Export <> "" ||
				$this->CurrentAction == "gridadd" ||
				$this->CurrentAction == "gridedit") {
				$this->ListOptions->HideAllOptions();
				$this->ExportOptions->HideAllOptions();
			}

			// Set up sorting order
			$this->SetUpSortOrder();
		}

		// Restore display records
		if ($this->getRecordsPerPage() <> "") {
			$this->DisplayRecs = $this->getRecordsPerPage(); // Restore from Session
		} else {
			$this->DisplayRecs = 20; // Load default
		}

		// Load Sorting Order
		$this->LoadSortOrder();

		// Build filter
		$sFilter = "";

		// Restore master/detail filter
		$this->DbMasterFilter = $this->GetMasterFilter(); // Restore master filter
		$this->DbDetailFilter = $this->GetDetailFilter(); // Restore detail filter
		ew_AddFilter($sFilter, $this->DbDetailFilter);
		ew_AddFilter($sFilter, $this->SearchWhere);

		// Load master record
		if ($this->CurrentMode <> "add" && $this->GetMasterFilter() <> "" && $this->getCurrentMasterTable() == "EnquiryMaster") {
			global $EnquiryMaster;
			$rsmaster = $EnquiryMaster->LoadRs($this->DbMasterFilter);
			$this->MasterRecordExists = ($rsmaster && !$rsmaster->EOF);
			if (!$this->MasterRecordExists) {
				$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record found
				$this->Page_Terminate("EnquiryMasterlist.php"); // Return to master page
			} else {
				$EnquiryMaster->LoadListRowValues($rsmaster);
				$EnquiryMaster->RowType = EW_ROWTYPE_MASTER; // Master row
				$EnquiryMaster->RenderListRow();
				$rsmaster->Close();
			}
		}

		// Set up filter in session
		$this->setSessionWhere($sFilter);
		$this->CurrentFilter = "";
	}

	// Build filter for all keys
	function BuildKeyFilter() {
		global $objForm;
		$sWrkFilter = "";

		// Update row index and get row key
		$rowindex = 1;
		$objForm->Index = $rowindex;
		$sThisKey = strval($objForm->GetValue("k_key"));
		while ($sThisKey <> "") {
			if ($this->SetupKeyValues($sThisKey)) {
				$sFilter = $this->KeyFilter();
				if ($sWrkFilter <> "") $sWrkFilter .= " OR ";
				$sWrkFilter .= $sFilter;
			} else {
				$sWrkFilter = "0=1";
				break;
			}

			// Update row index and get row key
			$rowindex++; // next row
			$objForm->Index = $rowindex;
			$sThisKey = strval($objForm->GetValue("k_key"));
		}
		return $sWrkFilter;
	}

	// Set up key values
	function SetupKeyValues($key) {
		$arrKeyFlds = explode($GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"], $key);
		if (count($arrKeyFlds) >= 0) {
		}
		return TRUE;
	}

	// Set up sort parameters
	function SetUpSortOrder() {

		// Check for "order" parameter
		if (@$_GET["order"] <> "") {
			$this->CurrentOrder = ew_StripSlashes(@$_GET["order"]);
			$this->CurrentOrderType = @$_GET["ordertype"];
			$this->UpdateSort($this->name); // name
			$this->UpdateSort($this->roleDescription); // roleDescription
			$this->UpdateSort($this->contactDescription); // contactDescription
			$this->UpdateSort($this->contactTime); // contactTime
			$this->setStartRecordNumber(1); // Reset start position
		}
	}

	// Load sort order parameters
	function LoadSortOrder() {
		$sOrderBy = $this->getSessionOrderBy(); // Get ORDER BY from Session
		if ($sOrderBy == "") {
			if ($this->SqlOrderBy() <> "") {
				$sOrderBy = $this->SqlOrderBy();
				$this->setSessionOrderBy($sOrderBy);
			}
		}
	}

	// Reset command
	// cmd=reset (Reset search parameters)
	// cmd=resetall (Reset search and master/detail parameters)
	// cmd=resetsort (Reset sort parameters)
	function ResetCmd() {

		// Check if reset command
		if (substr($this->Command,0,5) == "reset") {

			// Reset master/detail keys
			if ($this->Command == "resetall") {
				$this->setCurrentMasterTable(""); // Clear master table
				$this->DbMasterFilter = "";
				$this->DbDetailFilter = "";
				$this->callId->setSessionValue("");
			}

			// Reset sorting order
			if ($this->Command == "resetsort") {
				$sOrderBy = "";
				$this->setSessionOrderBy($sOrderBy);
				$this->name->setSort("");
				$this->roleDescription->setSort("");
				$this->contactDescription->setSort("");
				$this->contactTime->setSort("");
			}

			// Reset start position
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Set up list options
	function SetupListOptions() {
		global $Security, $Language;

		// Call ListOptions_Load event
		$this->ListOptions_Load();
	}

	// Render list options
	function RenderListOptions() {
		global $Security, $Language, $objForm;
		$this->ListOptions->LoadDefault();
		$this->RenderListOptionsExt();

		// Call ListOptions_Rendered event
		$this->ListOptions_Rendered();
	}

	function RenderListOptionsExt() {
		global $Security, $Language;
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {
		global $conn;

		// Call Recordset Selecting event
		$this->Recordset_Selecting($this->CurrentFilter);

		// Load List page SQL
		$sSql = $this->SelectSQL();
		if ($offset > -1 && $rowcnt > -1)
			$sSql .= " LIMIT $rowcnt OFFSET $offset";

		// Load recordset
		$rs = ew_LoadRecordset($sSql);

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $conn, $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		global $conn;
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->callId->setDbValue($rs->fields('callId'));
		$this->name->setDbValue($rs->fields('name'));
		$this->roleDescription->setDbValue($rs->fields('roleDescription'));
		$this->contactDescription->setDbValue($rs->fields('contactDescription'));
		$this->contactTime->setDbValue($rs->fields('contactTime'));
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$this->OldRecordset = ew_LoadRecordset($sSql);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $conn, $Security, $Language;
		global $gsLanguage;

		// Initialize URLs
		$this->ViewUrl = $this->GetViewUrl();
		$this->EditUrl = $this->GetEditUrl();
		$this->InlineEditUrl = $this->GetInlineEditUrl();
		$this->CopyUrl = $this->GetCopyUrl();
		$this->InlineCopyUrl = $this->GetInlineCopyUrl();
		$this->DeleteUrl = $this->GetDeleteUrl();

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// callId

		$this->callId->CellCssStyle = "white-space: nowrap;";

		// name
		// roleDescription
		// contactDescription
		// contactTime

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

			// name
			$this->name->ViewValue = $this->name->CurrentValue;
			$this->name->ViewCustomAttributes = "";

			// roleDescription
			$this->roleDescription->ViewValue = $this->roleDescription->CurrentValue;
			$this->roleDescription->ViewCustomAttributes = "";

			// contactDescription
			$this->contactDescription->ViewValue = $this->contactDescription->CurrentValue;
			$this->contactDescription->ViewCustomAttributes = "";

			// contactTime
			$this->contactTime->ViewValue = $this->contactTime->CurrentValue;
			$this->contactTime->ViewValue = ew_FormatDateTime($this->contactTime->ViewValue, 9);
			$this->contactTime->ViewCustomAttributes = "";

			// name
			$this->name->LinkCustomAttributes = "";
			$this->name->HrefValue = "";
			$this->name->TooltipValue = "";

			// roleDescription
			$this->roleDescription->LinkCustomAttributes = "";
			$this->roleDescription->HrefValue = "";
			$this->roleDescription->TooltipValue = "";

			// contactDescription
			$this->contactDescription->LinkCustomAttributes = "";
			$this->contactDescription->HrefValue = "";
			$this->contactDescription->TooltipValue = "";

			// contactTime
			$this->contactTime->LinkCustomAttributes = "";
			$this->contactTime->HrefValue = "";
			$this->contactTime->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Set up export options
	function SetupExportOptions() {
		global $Language;

		// Printer friendly
		$item = &$this->ExportOptions->Add("print");
		$item->Body = "<a href=\"" . $this->ExportPrintUrl . "\">" . $Language->Phrase("PrinterFriendly") . "</a>";
		$item->Visible = FALSE;

		// Export to Excel
		$item = &$this->ExportOptions->Add("excel");
		$item->Body = "<a href=\"" . $this->ExportExcelUrl . "\">" . $Language->Phrase("ExportToExcel") . "</a>";
		$item->Visible = FALSE;

		// Export to Word
		$item = &$this->ExportOptions->Add("word");
		$item->Body = "<a href=\"" . $this->ExportWordUrl . "\">" . $Language->Phrase("ExportToWord") . "</a>";
		$item->Visible = FALSE;

		// Export to Html
		$item = &$this->ExportOptions->Add("html");
		$item->Body = "<a href=\"" . $this->ExportHtmlUrl . "\">" . $Language->Phrase("ExportToHtml") . "</a>";
		$item->Visible = FALSE;

		// Export to Xml
		$item = &$this->ExportOptions->Add("xml");
		$item->Body = "<a href=\"" . $this->ExportXmlUrl . "\">" . $Language->Phrase("ExportToXml") . "</a>";
		$item->Visible = FALSE;

		// Export to Csv
		$item = &$this->ExportOptions->Add("csv");
		$item->Body = "<a href=\"" . $this->ExportCsvUrl . "\">" . $Language->Phrase("ExportToCsv") . "</a>";
		$item->Visible = FALSE;

		// Export to Pdf
		$item = &$this->ExportOptions->Add("pdf");
		$item->Body = "<a href=\"" . $this->ExportPdfUrl . "\">" . $Language->Phrase("ExportToPDF") . "</a>";
		$item->Visible = FALSE;

		// Export to Email
		$item = &$this->ExportOptions->Add("email");
		$item->Body = "<a id=\"emf_EnquirycallContact\" href=\"javascript:void(0);\" onclick=\"ew_EmailDialogShow({lnk:'emf_EnquirycallContact',hdr:ewLanguage.Phrase('ExportToEmail'),f:document.fEnquirycallContactlist,sel:false});\">" . $Language->Phrase("ExportToEmail") . "</a>";
		$item->Visible = FALSE;

		// Hide options for export/action
		if ($this->Export <> "" || $this->CurrentAction <> "")
			$this->ExportOptions->HideAllOptions();
	}

	// Set up master/detail based on QueryString
	function SetUpMasterParms() {
		$bValidMaster = FALSE;

		// Get the keys for master table
		if (isset($_GET[EW_TABLE_SHOW_MASTER])) {
			$sMasterTblVar = $_GET[EW_TABLE_SHOW_MASTER];
			if ($sMasterTblVar == "") {
				$bValidMaster = TRUE;
				$this->DbMasterFilter = "";
				$this->DbDetailFilter = "";
			}
			if ($sMasterTblVar == "EnquiryMaster") {
				$bValidMaster = TRUE;
				if (@$_GET["CallId"] <> "") {
					$GLOBALS["EnquiryMaster"]->CallId->setQueryStringValue($_GET["CallId"]);
					$this->callId->setQueryStringValue($GLOBALS["EnquiryMaster"]->CallId->QueryStringValue);
					$this->callId->setSessionValue($this->callId->QueryStringValue);
					if (!is_numeric($GLOBALS["EnquiryMaster"]->CallId->QueryStringValue)) $bValidMaster = FALSE;
				} else {
					$bValidMaster = FALSE;
				}
			}
		}
		if ($bValidMaster) {

			// Save current master table
			$this->setCurrentMasterTable($sMasterTblVar);

			// Reset start record counter (new master key)
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);

			// Clear previous master key from Session
			if ($sMasterTblVar <> "EnquiryMaster") {
				if ($this->callId->QueryStringValue == "") $this->callId->setSessionValue("");
			}
		}
		$this->DbMasterFilter = $this->GetMasterFilter(); //  Get master filter
		$this->DbDetailFilter = $this->GetDetailFilter(); // Get detail filter
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}

	// ListOptions Load event
	function ListOptions_Load() {

		// Example:
		//$opt = &$this->ListOptions->Add("new");
		//$opt->Header = "xxx";
		//$opt->OnLeft = TRUE; // Link on left
		//$opt->MoveTo(0); // Move to first column

	}

	// ListOptions Rendered event
	function ListOptions_Rendered() {

		// Example: 
		//$this->ListOptions->Items["new"]->Body = "xxx";

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($EnquirycallContact_list)) $EnquirycallContact_list = new cEnquirycallContact_list();

// Page init
$EnquirycallContact_list->Page_Init();

// Page main
$EnquirycallContact_list->Page_Main();
?>
<?php include_once "header.php" ?>
<?php if ($EnquirycallContact->Export == "") { ?>
<script type="text/javascript">

// Page object
var EnquirycallContact_list = new ew_Page("EnquirycallContact_list");
EnquirycallContact_list.PageID = "list"; // Page ID
var EW_PAGE_ID = EnquirycallContact_list.PageID; // For backward compatibility

// Form object
var fEnquirycallContactlist = new ew_Form("fEnquirycallContactlist");

// Form_CustomValidate event
fEnquirycallContactlist.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fEnquirycallContactlist.ValidateRequired = true;
<?php } else { ?>
fEnquirycallContactlist.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
// Form object for search

</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php } ?>
<?php if (($EnquirycallContact->Export == "") || (EW_EXPORT_MASTER_RECORD && $EnquirycallContact->Export == "print")) { ?>
<?php
$gsMasterReturnUrl = "EnquiryMasterlist.php";
if ($EnquirycallContact_list->DbMasterFilter <> "" && $EnquirycallContact->getCurrentMasterTable() == "EnquiryMaster") {
	if ($EnquirycallContact_list->MasterRecordExists) {
		if ($EnquirycallContact->getCurrentMasterTable() == $EnquirycallContact->TableVar) $gsMasterReturnUrl .= "?" . EW_TABLE_SHOW_MASTER . "=";
?>
<p><span class="ewTitle ewMasterTableTitle"><?php echo $Language->Phrase("MasterRecord") ?><?php echo $EnquiryMaster->TableCaption() ?>&nbsp;&nbsp;</span><?php $EnquirycallContact_list->ExportOptions->Render("body"); ?>
</p>
<?php if ($EnquirycallContact->Export == "") { ?>
<p class="phpmaker"><a href="<?php echo $gsMasterReturnUrl ?>"><?php echo $Language->Phrase("BackToMasterRecordPage") ?></a></p>
<?php } ?>
<?php include_once "EnquiryMastermaster.php" ?>
<?php
	}
}
?>
<?php } ?>
<?php
	$bSelectLimit = EW_SELECT_LIMIT;
	if ($bSelectLimit) {
		$EnquirycallContact_list->TotalRecs = $EnquirycallContact->SelectRecordCount();
	} else {
		if ($EnquirycallContact_list->Recordset = $EnquirycallContact_list->LoadRecordset())
			$EnquirycallContact_list->TotalRecs = $EnquirycallContact_list->Recordset->RecordCount();
	}
	$EnquirycallContact_list->StartRec = 1;
	if ($EnquirycallContact_list->DisplayRecs <= 0 || ($EnquirycallContact->Export <> "" && $EnquirycallContact->ExportAll)) // Display all records
		$EnquirycallContact_list->DisplayRecs = $EnquirycallContact_list->TotalRecs;
	if (!($EnquirycallContact->Export <> "" && $EnquirycallContact->ExportAll))
		$EnquirycallContact_list->SetUpStartRec(); // Set up start record position
	if ($bSelectLimit)
		$EnquirycallContact_list->Recordset = $EnquirycallContact_list->LoadRecordset($EnquirycallContact_list->StartRec-1, $EnquirycallContact_list->DisplayRecs);
?>
<p style="white-space: nowrap;"><span id="ewPageCaption" class="ewTitle ewTableTitle"><?php echo $Language->Phrase("TblTypeVIEW") ?><?php echo $EnquirycallContact->TableCaption() ?>&nbsp;&nbsp;</span>
<?php if ($EnquirycallContact->getCurrentMasterTable() == "") { ?>
<?php $EnquirycallContact_list->ExportOptions->Render("body"); ?>
<?php } ?>
</p>
<?php $EnquirycallContact_list->ShowPageHeader(); ?>
<?php
$EnquirycallContact_list->ShowMessage();
?>
<br>
<table cellspacing="0" class="ewGrid"><tr><td class="ewGridContent">
<form name="fEnquirycallContactlist" id="fEnquirycallContactlist" class="ewForm" action="" method="post">
<input type="hidden" name="t" value="EnquirycallContact">
<div id="gmp_EnquirycallContact" class="ewGridMiddlePanel">
<?php if ($EnquirycallContact_list->TotalRecs > 0) { ?>
<table id="tbl_EnquirycallContactlist" class="ewTable ewTableSeparate">
<?php echo $EnquirycallContact->TableCustomInnerHtml ?>
<thead><!-- Table header -->
	<tr class="ewTableHeader">
<?php

// Render list options
$EnquirycallContact_list->RenderListOptions();

// Render list options (header, left)
$EnquirycallContact_list->ListOptions->Render("header", "left");
?>
<?php if ($EnquirycallContact->name->Visible) { // name ?>
	<?php if ($EnquirycallContact->SortUrl($EnquirycallContact->name) == "") { ?>
		<td><span id="elh_EnquirycallContact_name" class="EnquirycallContact_name"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $EnquirycallContact->name->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div onmousedown="ew_Sort(event,'<?php echo $EnquirycallContact->SortUrl($EnquirycallContact->name) ?>',1);"><span id="elh_EnquirycallContact_name" class="EnquirycallContact_name">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $EnquirycallContact->name->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($EnquirycallContact->name->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($EnquirycallContact->name->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($EnquirycallContact->roleDescription->Visible) { // roleDescription ?>
	<?php if ($EnquirycallContact->SortUrl($EnquirycallContact->roleDescription) == "") { ?>
		<td><span id="elh_EnquirycallContact_roleDescription" class="EnquirycallContact_roleDescription"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $EnquirycallContact->roleDescription->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div onmousedown="ew_Sort(event,'<?php echo $EnquirycallContact->SortUrl($EnquirycallContact->roleDescription) ?>',1);"><span id="elh_EnquirycallContact_roleDescription" class="EnquirycallContact_roleDescription">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $EnquirycallContact->roleDescription->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($EnquirycallContact->roleDescription->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($EnquirycallContact->roleDescription->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($EnquirycallContact->contactDescription->Visible) { // contactDescription ?>
	<?php if ($EnquirycallContact->SortUrl($EnquirycallContact->contactDescription) == "") { ?>
		<td><span id="elh_EnquirycallContact_contactDescription" class="EnquirycallContact_contactDescription"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $EnquirycallContact->contactDescription->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div onmousedown="ew_Sort(event,'<?php echo $EnquirycallContact->SortUrl($EnquirycallContact->contactDescription) ?>',1);"><span id="elh_EnquirycallContact_contactDescription" class="EnquirycallContact_contactDescription">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $EnquirycallContact->contactDescription->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($EnquirycallContact->contactDescription->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($EnquirycallContact->contactDescription->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($EnquirycallContact->contactTime->Visible) { // contactTime ?>
	<?php if ($EnquirycallContact->SortUrl($EnquirycallContact->contactTime) == "") { ?>
		<td><span id="elh_EnquirycallContact_contactTime" class="EnquirycallContact_contactTime"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $EnquirycallContact->contactTime->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div onmousedown="ew_Sort(event,'<?php echo $EnquirycallContact->SortUrl($EnquirycallContact->contactTime) ?>',1);"><span id="elh_EnquirycallContact_contactTime" class="EnquirycallContact_contactTime">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $EnquirycallContact->contactTime->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($EnquirycallContact->contactTime->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($EnquirycallContact->contactTime->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php

// Render list options (header, right)
$EnquirycallContact_list->ListOptions->Render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
if ($EnquirycallContact->ExportAll && $EnquirycallContact->Export <> "") {
	$EnquirycallContact_list->StopRec = $EnquirycallContact_list->TotalRecs;
} else {

	// Set the last record to display
	if ($EnquirycallContact_list->TotalRecs > $EnquirycallContact_list->StartRec + $EnquirycallContact_list->DisplayRecs - 1)
		$EnquirycallContact_list->StopRec = $EnquirycallContact_list->StartRec + $EnquirycallContact_list->DisplayRecs - 1;
	else
		$EnquirycallContact_list->StopRec = $EnquirycallContact_list->TotalRecs;
}
$EnquirycallContact_list->RecCnt = $EnquirycallContact_list->StartRec - 1;
if ($EnquirycallContact_list->Recordset && !$EnquirycallContact_list->Recordset->EOF) {
	$EnquirycallContact_list->Recordset->MoveFirst();
	if (!$bSelectLimit && $EnquirycallContact_list->StartRec > 1)
		$EnquirycallContact_list->Recordset->Move($EnquirycallContact_list->StartRec - 1);
} elseif (!$EnquirycallContact->AllowAddDeleteRow && $EnquirycallContact_list->StopRec == 0) {
	$EnquirycallContact_list->StopRec = $EnquirycallContact->GridAddRowCount;
}

// Initialize aggregate
$EnquirycallContact->RowType = EW_ROWTYPE_AGGREGATEINIT;
$EnquirycallContact->ResetAttrs();
$EnquirycallContact_list->RenderRow();
while ($EnquirycallContact_list->RecCnt < $EnquirycallContact_list->StopRec) {
	$EnquirycallContact_list->RecCnt++;
	if (intval($EnquirycallContact_list->RecCnt) >= intval($EnquirycallContact_list->StartRec)) {
		$EnquirycallContact_list->RowCnt++;

		// Set up key count
		$EnquirycallContact_list->KeyCount = $EnquirycallContact_list->RowIndex;

		// Init row class and style
		$EnquirycallContact->ResetAttrs();
		$EnquirycallContact->CssClass = "";
		if ($EnquirycallContact->CurrentAction == "gridadd") {
		} else {
			$EnquirycallContact_list->LoadRowValues($EnquirycallContact_list->Recordset); // Load row values
		}
		$EnquirycallContact->RowType = EW_ROWTYPE_VIEW; // Render view

		// Set up row id / data-rowindex
		$EnquirycallContact->RowAttrs = array_merge($EnquirycallContact->RowAttrs, array('data-rowindex'=>$EnquirycallContact_list->RowCnt, 'id'=>'r' . $EnquirycallContact_list->RowCnt . '_EnquirycallContact', 'data-rowtype'=>$EnquirycallContact->RowType));

		// Render row
		$EnquirycallContact_list->RenderRow();

		// Render list options
		$EnquirycallContact_list->RenderListOptions();
?>
	<tr<?php echo $EnquirycallContact->RowAttributes() ?>>
<?php

// Render list options (body, left)
$EnquirycallContact_list->ListOptions->Render("body", "left", $EnquirycallContact_list->RowCnt);
?>
	<?php if ($EnquirycallContact->name->Visible) { // name ?>
		<td<?php echo $EnquirycallContact->name->CellAttributes() ?>><span id="el<?php echo $EnquirycallContact_list->RowCnt ?>_EnquirycallContact_name" class="EnquirycallContact_name">
<span<?php echo $EnquirycallContact->name->ViewAttributes() ?>>
<?php echo $EnquirycallContact->name->ListViewValue() ?></span>
</span><a id="<?php echo $EnquirycallContact_list->PageObjName . "_row_" . $EnquirycallContact_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($EnquirycallContact->roleDescription->Visible) { // roleDescription ?>
		<td<?php echo $EnquirycallContact->roleDescription->CellAttributes() ?>><span id="el<?php echo $EnquirycallContact_list->RowCnt ?>_EnquirycallContact_roleDescription" class="EnquirycallContact_roleDescription">
<span<?php echo $EnquirycallContact->roleDescription->ViewAttributes() ?>>
<?php echo $EnquirycallContact->roleDescription->ListViewValue() ?></span>
</span><a id="<?php echo $EnquirycallContact_list->PageObjName . "_row_" . $EnquirycallContact_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($EnquirycallContact->contactDescription->Visible) { // contactDescription ?>
		<td<?php echo $EnquirycallContact->contactDescription->CellAttributes() ?>><span id="el<?php echo $EnquirycallContact_list->RowCnt ?>_EnquirycallContact_contactDescription" class="EnquirycallContact_contactDescription">
<span<?php echo $EnquirycallContact->contactDescription->ViewAttributes() ?>>
<?php echo $EnquirycallContact->contactDescription->ListViewValue() ?></span>
</span><a id="<?php echo $EnquirycallContact_list->PageObjName . "_row_" . $EnquirycallContact_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($EnquirycallContact->contactTime->Visible) { // contactTime ?>
		<td<?php echo $EnquirycallContact->contactTime->CellAttributes() ?>><span id="el<?php echo $EnquirycallContact_list->RowCnt ?>_EnquirycallContact_contactTime" class="EnquirycallContact_contactTime">
<span<?php echo $EnquirycallContact->contactTime->ViewAttributes() ?>>
<?php echo $EnquirycallContact->contactTime->ListViewValue() ?></span>
</span><a id="<?php echo $EnquirycallContact_list->PageObjName . "_row_" . $EnquirycallContact_list->RowCnt ?>"></a></td>
	<?php } ?>
<?php

// Render list options (body, right)
$EnquirycallContact_list->ListOptions->Render("body", "right", $EnquirycallContact_list->RowCnt);
?>
	</tr>
<?php
	}
	if ($EnquirycallContact->CurrentAction <> "gridadd")
		$EnquirycallContact_list->Recordset->MoveNext();
}
?>
</tbody>
</table>
<?php } ?>
<?php if ($EnquirycallContact->CurrentAction == "") { ?>
<input type="hidden" name="a_list" id="a_list" value="">
<?php } ?>
</div>
</form>
<?php

// Close recordset
if ($EnquirycallContact_list->Recordset)
	$EnquirycallContact_list->Recordset->Close();
?>
<?php if ($EnquirycallContact->Export == "") { ?>
<div class="ewGridLowerPanel">
<?php if ($EnquirycallContact->CurrentAction <> "gridadd" && $EnquirycallContact->CurrentAction <> "gridedit") { ?>
<form name="ewpagerform" id="ewpagerform" class="ewForm" action="<?php echo ew_CurrentPage() ?>">
<table class="ewPager"><tr><td>
<?php if (!isset($EnquirycallContact_list->Pager)) $EnquirycallContact_list->Pager = new cPrevNextPager($EnquirycallContact_list->StartRec, $EnquirycallContact_list->DisplayRecs, $EnquirycallContact_list->TotalRecs) ?>
<?php if ($EnquirycallContact_list->Pager->RecordCount > 0) { ?>
	<table cellspacing="0" class="ewStdTable"><tbody><tr><td><span class="phpmaker"><?php echo $Language->Phrase("Page") ?>&nbsp;</span></td>
<!--first page button-->
	<?php if ($EnquirycallContact_list->Pager->FirstButton->Enabled) { ?>
	<td><a href="<?php echo $EnquirycallContact_list->PageUrl() ?>start=<?php echo $EnquirycallContact_list->Pager->FirstButton->Start ?>"><img src="phpimages/first.gif" alt="<?php echo $Language->Phrase("PagerFirst") ?>" width="16" height="16" style="border: 0;"></a></td>
	<?php } else { ?>
	<td><img src="phpimages/firstdisab.gif" alt="<?php echo $Language->Phrase("PagerFirst") ?>" width="16" height="16" style="border: 0;"></td>
	<?php } ?>
<!--previous page button-->
	<?php if ($EnquirycallContact_list->Pager->PrevButton->Enabled) { ?>
	<td><a href="<?php echo $EnquirycallContact_list->PageUrl() ?>start=<?php echo $EnquirycallContact_list->Pager->PrevButton->Start ?>"><img src="phpimages/prev.gif" alt="<?php echo $Language->Phrase("PagerPrevious") ?>" width="16" height="16" style="border: 0;"></a></td>
	<?php } else { ?>
	<td><img src="phpimages/prevdisab.gif" alt="<?php echo $Language->Phrase("PagerPrevious") ?>" width="16" height="16" style="border: 0;"></td>
	<?php } ?>
<!--current page number-->
	<td><input type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" id="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $EnquirycallContact_list->Pager->CurrentPage ?>" size="4"></td>
<!--next page button-->
	<?php if ($EnquirycallContact_list->Pager->NextButton->Enabled) { ?>
	<td><a href="<?php echo $EnquirycallContact_list->PageUrl() ?>start=<?php echo $EnquirycallContact_list->Pager->NextButton->Start ?>"><img src="phpimages/next.gif" alt="<?php echo $Language->Phrase("PagerNext") ?>" width="16" height="16" style="border: 0;"></a></td>	
	<?php } else { ?>
	<td><img src="phpimages/nextdisab.gif" alt="<?php echo $Language->Phrase("PagerNext") ?>" width="16" height="16" style="border: 0;"></td>
	<?php } ?>
<!--last page button-->
	<?php if ($EnquirycallContact_list->Pager->LastButton->Enabled) { ?>
	<td><a href="<?php echo $EnquirycallContact_list->PageUrl() ?>start=<?php echo $EnquirycallContact_list->Pager->LastButton->Start ?>"><img src="phpimages/last.gif" alt="<?php echo $Language->Phrase("PagerLast") ?>" width="16" height="16" style="border: 0;"></a></td>	
	<?php } else { ?>
	<td><img src="phpimages/lastdisab.gif" alt="<?php echo $Language->Phrase("PagerLast") ?>" width="16" height="16" style="border: 0;"></td>
	<?php } ?>
	<td><span class="phpmaker">&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $EnquirycallContact_list->Pager->PageCount ?></span></td>
	</tr></tbody></table>
	</td>	
	<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td>
	<span class="phpmaker"><?php echo $Language->Phrase("Record") ?>&nbsp;<?php echo $EnquirycallContact_list->Pager->FromIndex ?>&nbsp;<?php echo $Language->Phrase("To") ?>&nbsp;<?php echo $EnquirycallContact_list->Pager->ToIndex ?>&nbsp;<?php echo $Language->Phrase("Of") ?>&nbsp;<?php echo $EnquirycallContact_list->Pager->RecordCount ?></span>
<?php } else { ?>
	<?php if ($EnquirycallContact_list->SearchWhere == "0=101") { ?>
	<span class="phpmaker"><?php echo $Language->Phrase("EnterSearchCriteria") ?></span>
	<?php } else { ?>
	<span class="phpmaker"><?php echo $Language->Phrase("NoRecord") ?></span>
	<?php } ?>
<?php } ?>
	</td>
</tr></table>
</form>
<?php } ?>
<span class="phpmaker">
</span>
</div>
<?php } ?>
</td></tr></table>
<?php if ($EnquirycallContact->Export == "") { ?>
<script type="text/javascript">
fEnquirycallContactlist.Init();
</script>
<?php } ?>
<?php
$EnquirycallContact_list->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<?php if ($EnquirycallContact->Export == "") { ?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php } ?>
<?php include_once "footer.php" ?>
<?php
$EnquirycallContact_list->Page_Terminate();
?>
