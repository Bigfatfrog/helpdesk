<?php

// Global variable for table object
$View1 = NULL;

//
// Table class for View1
//
class cView1 extends cTable {
	var $Call_Id;
	var $allocatedToId;
	var $timeOfCall;
	var $status;
	var $callDescription;
	var $solution;
	var $priority;
	var $contactTime;
	var $contactDescription;
	var $contactStaffName;
	var $roleDescription;
	var $callerName;
	var $operatorName;
	var $allocatedToName;
	var $assetSerialNumber;
	var $assetDescription;
	var $vendorName;
	var $vendorDetails;

	//
	// Table class constructor
	//
	function __construct() {
		global $Language;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();
		$this->TableVar = 'View1';
		$this->TableName = 'View1';
		$this->TableType = 'VIEW';
		$this->ExportAll = TRUE;
		$this->ExportPageBreakCount = 0; // Page break per every n record (PDF only)
		$this->ExportPageOrientation = "portrait"; // Page orientation (PDF only)
		$this->ExportPageSize = "a4"; // Page size (PDF only)
		$this->DetailAdd = FALSE; // Allow detail add
		$this->DetailEdit = FALSE; // Allow detail edit
		$this->GridAddRowCount = 5;
		$this->AllowAddDeleteRow = ew_AllowAddDeleteRow(); // Allow add/delete row
		$this->UserIDAllowSecurity = 0; // User ID Allow
		$this->BasicSearch = new cBasicSearch($this->TableVar);

		// Call Id
		$this->Call_Id = new cField('View1', 'View1', 'x_Call_Id', 'Call Id', '`Call Id`', '`Call Id`', 3, -1, FALSE, '`Call Id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->Call_Id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['Call Id'] = &$this->Call_Id;

		// allocatedToId
		$this->allocatedToId = new cField('View1', 'View1', 'x_allocatedToId', 'allocatedToId', '`allocatedToId`', '`allocatedToId`', 3, -1, FALSE, '`allocatedToId`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->allocatedToId->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['allocatedToId'] = &$this->allocatedToId;

		// timeOfCall
		$this->timeOfCall = new cField('View1', 'View1', 'x_timeOfCall', 'timeOfCall', '`timeOfCall`', 'DATE_FORMAT(`timeOfCall`, \'%d/%m/%Y %H:%i:%s\')', 135, 7, FALSE, '`timeOfCall`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->timeOfCall->FldDefaultErrMsg = str_replace("%s", "/", $Language->Phrase("IncorrectDateDMY"));
		$this->fields['timeOfCall'] = &$this->timeOfCall;

		// status
		$this->status = new cField('View1', 'View1', 'x_status', 'status', '`status`', '`status`', 3, -1, FALSE, '`status`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->status->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['status'] = &$this->status;

		// callDescription
		$this->callDescription = new cField('View1', 'View1', 'x_callDescription', 'callDescription', '`callDescription`', '`callDescription`', 201, -1, FALSE, '`callDescription`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->fields['callDescription'] = &$this->callDescription;

		// solution
		$this->solution = new cField('View1', 'View1', 'x_solution', 'solution', '`solution`', '`solution`', 201, -1, FALSE, '`solution`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->fields['solution'] = &$this->solution;

		// priority
		$this->priority = new cField('View1', 'View1', 'x_priority', 'priority', '`priority`', '`priority`', 3, -1, FALSE, '`priority`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->priority->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['priority'] = &$this->priority;

		// contactTime
		$this->contactTime = new cField('View1', 'View1', 'x_contactTime', 'contactTime', '`contactTime`', 'DATE_FORMAT(`contactTime`, \'%d/%m/%Y %H:%i:%s\')', 135, 9, FALSE, '`contactTime`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->contactTime->FldDefaultErrMsg = str_replace("%s", "/", $Language->Phrase("IncorrectDateDMY"));
		$this->fields['contactTime'] = &$this->contactTime;

		// contactDescription
		$this->contactDescription = new cField('View1', 'View1', 'x_contactDescription', 'contactDescription', '`contactDescription`', '`contactDescription`', 201, -1, FALSE, '`contactDescription`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->fields['contactDescription'] = &$this->contactDescription;

		// contactStaffName
		$this->contactStaffName = new cField('View1', 'View1', 'x_contactStaffName', 'contactStaffName', '`contactStaffName`', '`contactStaffName`', 200, -1, FALSE, '`contactStaffName`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->fields['contactStaffName'] = &$this->contactStaffName;

		// roleDescription
		$this->roleDescription = new cField('View1', 'View1', 'x_roleDescription', 'roleDescription', '`roleDescription`', '`roleDescription`', 200, -1, FALSE, '`roleDescription`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->fields['roleDescription'] = &$this->roleDescription;

		// callerName
		$this->callerName = new cField('View1', 'View1', 'x_callerName', 'callerName', '`callerName`', '`callerName`', 200, -1, FALSE, '`callerName`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->fields['callerName'] = &$this->callerName;

		// operatorName
		$this->operatorName = new cField('View1', 'View1', 'x_operatorName', 'operatorName', '`operatorName`', '`operatorName`', 200, -1, FALSE, '`operatorName`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->fields['operatorName'] = &$this->operatorName;

		// allocatedToName
		$this->allocatedToName = new cField('View1', 'View1', 'x_allocatedToName', 'allocatedToName', '`allocatedToName`', '`allocatedToName`', 200, -1, FALSE, '`allocatedToName`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->fields['allocatedToName'] = &$this->allocatedToName;

		// assetSerialNumber
		$this->assetSerialNumber = new cField('View1', 'View1', 'x_assetSerialNumber', 'assetSerialNumber', '`assetSerialNumber`', '`assetSerialNumber`', 3, -1, FALSE, '`assetSerialNumber`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->assetSerialNumber->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['assetSerialNumber'] = &$this->assetSerialNumber;

		// assetDescription
		$this->assetDescription = new cField('View1', 'View1', 'x_assetDescription', 'assetDescription', '`assetDescription`', '`assetDescription`', 201, -1, FALSE, '`assetDescription`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->fields['assetDescription'] = &$this->assetDescription;

		// vendorName
		$this->vendorName = new cField('View1', 'View1', 'x_vendorName', 'vendorName', '`vendorName`', '`vendorName`', 200, -1, FALSE, '`vendorName`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->fields['vendorName'] = &$this->vendorName;

		// vendorDetails
		$this->vendorDetails = new cField('View1', 'View1', 'x_vendorDetails', 'vendorDetails', '`vendorDetails`', '`vendorDetails`', 200, -1, FALSE, '`vendorDetails`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->fields['vendorDetails'] = &$this->vendorDetails;
	}

	// Single column sort
	function UpdateSort(&$ofld) {
		if ($this->CurrentOrder == $ofld->FldName) {
			$sSortField = $ofld->FldExpression;
			$sLastSort = $ofld->getSort();
			if ($this->CurrentOrderType == "ASC" || $this->CurrentOrderType == "DESC") {
				$sThisSort = $this->CurrentOrderType;
			} else {
				$sThisSort = ($sLastSort == "ASC") ? "DESC" : "ASC";
			}
			$ofld->setSort($sThisSort);
			$this->setSessionOrderBy($sSortField . " " . $sThisSort); // Save to Session
		} else {
			$ofld->setSort("");
		}
	}

	// Table level SQL
	function SqlFrom() { // From
		return "`View1`";
	}

	function SqlSelect() { // Select
		return "SELECT * FROM " . $this->SqlFrom();
	}

	function SqlWhere() { // Where
		$sWhere = "";
		$this->TableFilter = "";
		ew_AddFilter($sWhere, $this->TableFilter);
		return $sWhere;
	}

	function SqlGroupBy() { // Group By
		return "";
	}

	function SqlHaving() { // Having
		return "";
	}

	function SqlOrderBy() { // Order By
		return "";
	}

	// Check if Anonymous User is allowed
	function AllowAnonymousUser() {
		switch (@$this->PageID) {
			case "add":
			case "register":
			case "addopt":
				return FALSE;
			case "edit":
			case "update":
			case "changepwd":
			case "forgotpwd":
				return FALSE;
			case "delete":
				return FALSE;
			case "view":
				return FALSE;
			case "search":
				return FALSE;
			default:
				return FALSE;
		}
	}

	// Apply User ID filters
	function ApplyUserIDFilters($sFilter) {
		return $sFilter;
	}

	// Check if User ID security allows view all
	function UserIDAllow($id = "") {
		return TRUE;
	}

	// Get SQL
	function GetSQL($where, $orderby) {
		return ew_BuildSelectSql($this->SqlSelect(), $this->SqlWhere(),
			$this->SqlGroupBy(), $this->SqlHaving(), $this->SqlOrderBy(),
			$where, $orderby);
	}

	// Table SQL
	function SQL() {
		$sFilter = $this->CurrentFilter;
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->SqlSelect(), $this->SqlWhere(),
			$this->SqlGroupBy(), $this->SqlHaving(), $this->SqlOrderBy(),
			$sFilter, $sSort);
	}

	// Table SQL with List page filter
	function SelectSQL() {
		$sFilter = $this->getSessionWhere();
		ew_AddFilter($sFilter, $this->CurrentFilter);
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->SqlSelect(), $this->SqlWhere(), $this->SqlGroupBy(),
			$this->SqlHaving(), $this->SqlOrderBy(), $sFilter, $sSort);
	}

	// Get ORDER BY clause
	function GetOrderBy() {
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql("", "", "", "", $this->SqlOrderBy(), "", $sSort);
	}

	// Try to get record count
	function TryGetRecordCount($sSql) {
		global $conn;
		$cnt = -1;
		if ($this->TableType == 'TABLE' || $this->TableType == 'VIEW') {
			$sSql = "SELECT COUNT(*) FROM" . substr($sSql, 13);
			$sOrderBy = $this->GetOrderBy();
			if (substr($sSql, strlen($sOrderBy) * -1) == $sOrderBy)
				$sSql = substr($sSql, 0, strlen($sSql) - strlen($sOrderBy)); // Remove ORDER BY clause
		} else {
			$sSql = "SELECT COUNT(*) FROM (" . $sSql . ") EW_COUNT_TABLE";
		}
		if ($rs = $conn->Execute($sSql)) {
			if (!$rs->EOF && $rs->FieldCount() > 0) {
				$cnt = $rs->fields[0];
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// Get record count based on filter (for detail record count in master table pages)
	function LoadRecordCount($sFilter) {
		$origFilter = $this->CurrentFilter;
		$this->CurrentFilter = $sFilter;
		$this->Recordset_Selecting($this->CurrentFilter);

		//$sSql = $this->SQL();
		$sSql = $this->GetSQL($this->CurrentFilter, "");
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			if ($rs = $this->LoadRs($this->CurrentFilter)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		$this->CurrentFilter = $origFilter;
		return intval($cnt);
	}

	// Get record count (for current List page)
	function SelectRecordCount() {
		global $conn;
		$origFilter = $this->CurrentFilter;
		$this->Recordset_Selecting($this->CurrentFilter);
		$sSql = $this->SelectSQL();
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			if ($rs = $conn->Execute($sSql)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		$this->CurrentFilter = $origFilter;
		return intval($cnt);
	}

	// Update Table
	var $UpdateTable = "`View1`";

	// INSERT statement
	function InsertSQL(&$rs) {
		global $conn;
		$names = "";
		$values = "";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]))
				continue;
			$names .= $this->fields[$name]->FldExpression . ",";
			$values .= ew_QuotedValue($value, $this->fields[$name]->FldDataType) . ",";
		}
		while (substr($names, -1) == ",")
			$names = substr($names, 0, -1);
		while (substr($values, -1) == ",")
			$values = substr($values, 0, -1);
		return "INSERT INTO " . $this->UpdateTable . " ($names) VALUES ($values)";
	}

	// Insert
	function Insert(&$rs) {
		global $conn;
		return $conn->Execute($this->InsertSQL($rs));
	}

	// UPDATE statement
	function UpdateSQL(&$rs, $where = "") {
		$sql = "UPDATE " . $this->UpdateTable . " SET ";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]))
				continue;
			$sql .= $this->fields[$name]->FldExpression . "=";
			$sql .= ew_QuotedValue($value, $this->fields[$name]->FldDataType) . ",";
		}
		while (substr($sql, -1) == ",")
			$sql = substr($sql, 0, -1);
		$filter = $this->CurrentFilter;
		ew_AddFilter($filter, $where);
		if ($filter <> "")	$sql .= " WHERE " . $filter;
		return $sql;
	}

	// Update
	function Update(&$rs, $where = "") {
		global $conn;
		return $conn->Execute($this->UpdateSQL($rs, $where));
	}

	// DELETE statement
	function DeleteSQL(&$rs, $where = "") {
		$sql = "DELETE FROM " . $this->UpdateTable . " WHERE ";
		if ($rs) {
			$sql .= ew_QuotedName('Call Id') . '=' . ew_QuotedValue($rs['Call Id'], $this->Call_Id->FldDataType) . ' AND ';
		}
		if (substr($sql, -5) == " AND ") $sql = substr($sql, 0, -5);
		$filter = $this->CurrentFilter;
		ew_AddFilter($filter, $where);
		if ($filter <> "")	$sql .= " AND " . $filter;
		return $sql;
	}

	// Delete
	function Delete(&$rs, $where = "") {
		global $conn;
		return $conn->Execute($this->DeleteSQL($rs, $where));
	}

	// Key filter WHERE clause
	function SqlKeyFilter() {
		return "`Call Id` = @Call_Id@";
	}

	// Key filter
	function KeyFilter() {
		$sKeyFilter = $this->SqlKeyFilter();
		if (!is_numeric($this->Call_Id->CurrentValue))
			$sKeyFilter = "0=1"; // Invalid key
		$sKeyFilter = str_replace("@Call_Id@", ew_AdjustSql($this->Call_Id->CurrentValue), $sKeyFilter); // Replace key value
		return $sKeyFilter;
	}

	// Return page URL
	function getReturnUrl() {
		$name = EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL;

		// Get referer URL automatically
		if (ew_ServerVar("HTTP_REFERER") <> "" && ew_ReferPage() <> ew_CurrentPage() && ew_ReferPage() <> "login.php") // Referer not same page or login page
			$_SESSION[$name] = ew_ServerVar("HTTP_REFERER"); // Save to Session
		if (@$_SESSION[$name] <> "") {
			return $_SESSION[$name];
		} else {
			return "View1list.php";
		}
	}

	function setReturnUrl($v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL] = $v;
	}

	// List URL
	function GetListUrl() {
		return "View1list.php";
	}

	// View URL
	function GetViewUrl() {
		return $this->KeyUrl("View1view.php", $this->UrlParm());
	}

	// Add URL
	function GetAddUrl() {
		return "View1add.php";
	}

	// Edit URL
	function GetEditUrl($parm = "") {
		return $this->KeyUrl("View1edit.php", $this->UrlParm($parm));
	}

	// Inline edit URL
	function GetInlineEditUrl() {
		return $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=edit"));
	}

	// Copy URL
	function GetCopyUrl($parm = "") {
		return $this->KeyUrl("View1add.php", $this->UrlParm($parm));
	}

	// Inline copy URL
	function GetInlineCopyUrl() {
		return $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=copy"));
	}

	// Delete URL
	function GetDeleteUrl() {
		return $this->KeyUrl("View1delete.php", $this->UrlParm());
	}

	// Add key value to URL
	function KeyUrl($url, $parm = "") {
		$sUrl = $url . "?";
		if ($parm <> "") $sUrl .= $parm . "&";
		if (!is_null($this->Call_Id->CurrentValue)) {
			$sUrl .= "Call_Id=" . urlencode($this->Call_Id->CurrentValue);
		} else {
			return "javascript:alert(ewLanguage.Phrase('InvalidRecord'));";
		}
		return $sUrl;
	}

	// Sort URL
	function SortUrl(&$fld) {
		if ($this->CurrentAction <> "" || $this->Export <> "" ||
			in_array($fld->FldType, array(128, 204, 205))) { // Unsortable data type
				return "";
		} elseif ($fld->Sortable) {
			$sUrlParm = $this->UrlParm("order=" . urlencode($fld->FldName) . "&ordertype=" . $fld->ReverseSort());
			return ew_CurrentPage() . "?" . $sUrlParm;
		} else {
			return "";
		}
	}

	// Get record keys from $_POST/$_GET/$_SESSION
	function GetRecordKeys() {
		global $EW_COMPOSITE_KEY_SEPARATOR;
		$arKeys = array();
		$arKey = array();
		if (isset($_POST["key_m"])) {
			$arKeys = ew_StripSlashes($_POST["key_m"]);
			$cnt = count($arKeys);
		} elseif (isset($_GET["key_m"])) {
			$arKeys = ew_StripSlashes($_GET["key_m"]);
			$cnt = count($arKeys);
		} elseif (isset($_GET)) {
			$arKeys[] = @$_GET["Call_Id"]; // Call Id

			//return $arKeys; // do not return yet, so the values will also be checked by the following code
		}

		// check keys
		$ar = array();
		foreach ($arKeys as $key) {
			if (!is_numeric($key))
				continue;
			$ar[] = $key;
		}
		return $ar;
	}

	// Get key filter
	function GetKeyFilter() {
		$arKeys = $this->GetRecordKeys();
		$sKeyFilter = "";
		foreach ($arKeys as $key) {
			if ($sKeyFilter <> "") $sKeyFilter .= " OR ";
			$this->Call_Id->CurrentValue = $key;
			$sKeyFilter .= "(" . $this->KeyFilter() . ")";
		}
		return $sKeyFilter;
	}

	// Load rows based on filter
	function &LoadRs($sFilter) {
		global $conn;

		// Set up filter (SQL WHERE clause) and get return SQL
		//$this->CurrentFilter = $sFilter;
		//$sSql = $this->SQL();

		$sSql = $this->GetSQL($sFilter, "");
		$rs = $conn->Execute($sSql);
		return $rs;
	}

	// Load row values from recordset
	function LoadListRowValues(&$rs) {
		$this->Call_Id->setDbValue($rs->fields('Call Id'));
		$this->allocatedToId->setDbValue($rs->fields('allocatedToId'));
		$this->timeOfCall->setDbValue($rs->fields('timeOfCall'));
		$this->status->setDbValue($rs->fields('status'));
		$this->callDescription->setDbValue($rs->fields('callDescription'));
		$this->solution->setDbValue($rs->fields('solution'));
		$this->priority->setDbValue($rs->fields('priority'));
		$this->contactTime->setDbValue($rs->fields('contactTime'));
		$this->contactDescription->setDbValue($rs->fields('contactDescription'));
		$this->contactStaffName->setDbValue($rs->fields('contactStaffName'));
		$this->roleDescription->setDbValue($rs->fields('roleDescription'));
		$this->callerName->setDbValue($rs->fields('callerName'));
		$this->operatorName->setDbValue($rs->fields('operatorName'));
		$this->allocatedToName->setDbValue($rs->fields('allocatedToName'));
		$this->assetSerialNumber->setDbValue($rs->fields('assetSerialNumber'));
		$this->assetDescription->setDbValue($rs->fields('assetDescription'));
		$this->vendorName->setDbValue($rs->fields('vendorName'));
		$this->vendorDetails->setDbValue($rs->fields('vendorDetails'));
	}

	// Render list row values
	function RenderListRow() {
		global $conn, $Security;

		// Call Row Rendering event
		$this->Row_Rendering();

   // Common render codes
		// Call Id
		// allocatedToId
		// timeOfCall
		// status
		// callDescription
		// solution
		// priority
		// contactTime
		// contactDescription
		// contactStaffName
		// roleDescription
		// callerName
		// operatorName
		// allocatedToName
		// assetSerialNumber
		// assetDescription
		// vendorName
		// vendorDetails
		// Call Id

		$this->Call_Id->ViewValue = $this->Call_Id->CurrentValue;
		$this->Call_Id->ViewCustomAttributes = "";

		// allocatedToId
		$this->allocatedToId->ViewValue = $this->allocatedToId->CurrentValue;
		$this->allocatedToId->ViewCustomAttributes = "";

		// timeOfCall
		$this->timeOfCall->ViewValue = $this->timeOfCall->CurrentValue;
		$this->timeOfCall->ViewValue = ew_FormatDateTime($this->timeOfCall->ViewValue, 7);
		$this->timeOfCall->ViewCustomAttributes = "";

		// status
		$this->status->ViewValue = $this->status->CurrentValue;
		$this->status->ViewCustomAttributes = "";

		// callDescription
		$this->callDescription->ViewValue = $this->callDescription->CurrentValue;
		$this->callDescription->ViewCustomAttributes = "";

		// solution
		$this->solution->ViewValue = $this->solution->CurrentValue;
		$this->solution->ViewCustomAttributes = "";

		// priority
		$this->priority->ViewValue = $this->priority->CurrentValue;
		$this->priority->ViewCustomAttributes = "";

		// contactTime
		$this->contactTime->ViewValue = $this->contactTime->CurrentValue;
		$this->contactTime->ViewValue = ew_FormatDateTime($this->contactTime->ViewValue, 9);
		$this->contactTime->ViewCustomAttributes = "";

		// contactDescription
		$this->contactDescription->ViewValue = $this->contactDescription->CurrentValue;
		$this->contactDescription->ViewCustomAttributes = "";

		// contactStaffName
		$this->contactStaffName->ViewValue = $this->contactStaffName->CurrentValue;
		$this->contactStaffName->ViewCustomAttributes = "";

		// roleDescription
		$this->roleDescription->ViewValue = $this->roleDescription->CurrentValue;
		$this->roleDescription->ViewCustomAttributes = "";

		// callerName
		$this->callerName->ViewValue = $this->callerName->CurrentValue;
		$this->callerName->ViewCustomAttributes = "";

		// operatorName
		$this->operatorName->ViewValue = $this->operatorName->CurrentValue;
		$this->operatorName->ViewCustomAttributes = "";

		// allocatedToName
		$this->allocatedToName->ViewValue = $this->allocatedToName->CurrentValue;
		$this->allocatedToName->ViewCustomAttributes = "";

		// assetSerialNumber
		$this->assetSerialNumber->ViewValue = $this->assetSerialNumber->CurrentValue;
		$this->assetSerialNumber->ViewCustomAttributes = "";

		// assetDescription
		$this->assetDescription->ViewValue = $this->assetDescription->CurrentValue;
		$this->assetDescription->ViewCustomAttributes = "";

		// vendorName
		$this->vendorName->ViewValue = $this->vendorName->CurrentValue;
		$this->vendorName->ViewCustomAttributes = "";

		// vendorDetails
		$this->vendorDetails->ViewValue = $this->vendorDetails->CurrentValue;
		$this->vendorDetails->ViewCustomAttributes = "";

		// Call Id
		$this->Call_Id->LinkCustomAttributes = "";
		$this->Call_Id->HrefValue = "";
		$this->Call_Id->TooltipValue = "";

		// allocatedToId
		$this->allocatedToId->LinkCustomAttributes = "";
		$this->allocatedToId->HrefValue = "";
		$this->allocatedToId->TooltipValue = "";

		// timeOfCall
		$this->timeOfCall->LinkCustomAttributes = "";
		$this->timeOfCall->HrefValue = "";
		$this->timeOfCall->TooltipValue = "";

		// status
		$this->status->LinkCustomAttributes = "";
		$this->status->HrefValue = "";
		$this->status->TooltipValue = "";

		// callDescription
		$this->callDescription->LinkCustomAttributes = "";
		$this->callDescription->HrefValue = "";
		$this->callDescription->TooltipValue = "";

		// solution
		$this->solution->LinkCustomAttributes = "";
		$this->solution->HrefValue = "";
		$this->solution->TooltipValue = "";

		// priority
		$this->priority->LinkCustomAttributes = "";
		$this->priority->HrefValue = "";
		$this->priority->TooltipValue = "";

		// contactTime
		$this->contactTime->LinkCustomAttributes = "";
		$this->contactTime->HrefValue = "";
		$this->contactTime->TooltipValue = "";

		// contactDescription
		$this->contactDescription->LinkCustomAttributes = "";
		$this->contactDescription->HrefValue = "";
		$this->contactDescription->TooltipValue = "";

		// contactStaffName
		$this->contactStaffName->LinkCustomAttributes = "";
		$this->contactStaffName->HrefValue = "";
		$this->contactStaffName->TooltipValue = "";

		// roleDescription
		$this->roleDescription->LinkCustomAttributes = "";
		$this->roleDescription->HrefValue = "";
		$this->roleDescription->TooltipValue = "";

		// callerName
		$this->callerName->LinkCustomAttributes = "";
		$this->callerName->HrefValue = "";
		$this->callerName->TooltipValue = "";

		// operatorName
		$this->operatorName->LinkCustomAttributes = "";
		$this->operatorName->HrefValue = "";
		$this->operatorName->TooltipValue = "";

		// allocatedToName
		$this->allocatedToName->LinkCustomAttributes = "";
		$this->allocatedToName->HrefValue = "";
		$this->allocatedToName->TooltipValue = "";

		// assetSerialNumber
		$this->assetSerialNumber->LinkCustomAttributes = "";
		$this->assetSerialNumber->HrefValue = "";
		$this->assetSerialNumber->TooltipValue = "";

		// assetDescription
		$this->assetDescription->LinkCustomAttributes = "";
		$this->assetDescription->HrefValue = "";
		$this->assetDescription->TooltipValue = "";

		// vendorName
		$this->vendorName->LinkCustomAttributes = "";
		$this->vendorName->HrefValue = "";
		$this->vendorName->TooltipValue = "";

		// vendorDetails
		$this->vendorDetails->LinkCustomAttributes = "";
		$this->vendorDetails->HrefValue = "";
		$this->vendorDetails->TooltipValue = "";

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Aggregate list row values
	function AggregateListRowValues() {
	}

	// Aggregate list row (for rendering)
	function AggregateListRow() {
	}

	// Export data in HTML/CSV/Word/Excel/Email/PDF format
	function ExportDocument(&$Doc, &$Recordset, $StartRec, $StopRec, $ExportPageType = "") {
		if (!$Recordset || !$Doc)
			return;

		// Write header
		$Doc->ExportTableHeader();
		if ($Doc->Horizontal) { // Horizontal format, write header
			$Doc->BeginExportRow();
			if ($ExportPageType == "view") {
				if ($this->Call_Id->Exportable) $Doc->ExportCaption($this->Call_Id);
				if ($this->allocatedToId->Exportable) $Doc->ExportCaption($this->allocatedToId);
				if ($this->timeOfCall->Exportable) $Doc->ExportCaption($this->timeOfCall);
				if ($this->status->Exportable) $Doc->ExportCaption($this->status);
				if ($this->callDescription->Exportable) $Doc->ExportCaption($this->callDescription);
				if ($this->solution->Exportable) $Doc->ExportCaption($this->solution);
				if ($this->priority->Exportable) $Doc->ExportCaption($this->priority);
				if ($this->contactTime->Exportable) $Doc->ExportCaption($this->contactTime);
				if ($this->contactDescription->Exportable) $Doc->ExportCaption($this->contactDescription);
				if ($this->contactStaffName->Exportable) $Doc->ExportCaption($this->contactStaffName);
				if ($this->roleDescription->Exportable) $Doc->ExportCaption($this->roleDescription);
				if ($this->callerName->Exportable) $Doc->ExportCaption($this->callerName);
				if ($this->operatorName->Exportable) $Doc->ExportCaption($this->operatorName);
				if ($this->allocatedToName->Exportable) $Doc->ExportCaption($this->allocatedToName);
				if ($this->assetSerialNumber->Exportable) $Doc->ExportCaption($this->assetSerialNumber);
				if ($this->assetDescription->Exportable) $Doc->ExportCaption($this->assetDescription);
				if ($this->vendorName->Exportable) $Doc->ExportCaption($this->vendorName);
				if ($this->vendorDetails->Exportable) $Doc->ExportCaption($this->vendorDetails);
			} else {
				if ($this->Call_Id->Exportable) $Doc->ExportCaption($this->Call_Id);
				if ($this->allocatedToId->Exportable) $Doc->ExportCaption($this->allocatedToId);
				if ($this->timeOfCall->Exportable) $Doc->ExportCaption($this->timeOfCall);
				if ($this->status->Exportable) $Doc->ExportCaption($this->status);
				if ($this->callDescription->Exportable) $Doc->ExportCaption($this->callDescription);
				if ($this->solution->Exportable) $Doc->ExportCaption($this->solution);
				if ($this->priority->Exportable) $Doc->ExportCaption($this->priority);
				if ($this->contactTime->Exportable) $Doc->ExportCaption($this->contactTime);
				if ($this->contactDescription->Exportable) $Doc->ExportCaption($this->contactDescription);
				if ($this->contactStaffName->Exportable) $Doc->ExportCaption($this->contactStaffName);
				if ($this->roleDescription->Exportable) $Doc->ExportCaption($this->roleDescription);
				if ($this->callerName->Exportable) $Doc->ExportCaption($this->callerName);
				if ($this->operatorName->Exportable) $Doc->ExportCaption($this->operatorName);
				if ($this->allocatedToName->Exportable) $Doc->ExportCaption($this->allocatedToName);
				if ($this->assetSerialNumber->Exportable) $Doc->ExportCaption($this->assetSerialNumber);
				if ($this->assetDescription->Exportable) $Doc->ExportCaption($this->assetDescription);
				if ($this->vendorName->Exportable) $Doc->ExportCaption($this->vendorName);
				if ($this->vendorDetails->Exportable) $Doc->ExportCaption($this->vendorDetails);
			}
			$Doc->EndExportRow();
		}

		// Move to first record
		$RecCnt = $StartRec - 1;
		if (!$Recordset->EOF) {
			$Recordset->MoveFirst();
			if ($StartRec > 1)
				$Recordset->Move($StartRec - 1);
		}
		while (!$Recordset->EOF && $RecCnt < $StopRec) {
			$RecCnt++;
			if (intval($RecCnt) >= intval($StartRec)) {
				$RowCnt = intval($RecCnt) - intval($StartRec) + 1;

				// Page break
				if ($this->ExportPageBreakCount > 0) {
					if ($RowCnt > 1 && ($RowCnt - 1) % $this->ExportPageBreakCount == 0)
						$Doc->ExportPageBreak();
				}
				$this->LoadListRowValues($Recordset);

				// Render row
				$this->RowType = EW_ROWTYPE_VIEW; // Render view
				$this->ResetAttrs();
				$this->RenderListRow();
				$Doc->BeginExportRow($RowCnt); // Allow CSS styles if enabled
				if ($ExportPageType == "view") {
					if ($this->Call_Id->Exportable) $Doc->ExportField($this->Call_Id);
					if ($this->allocatedToId->Exportable) $Doc->ExportField($this->allocatedToId);
					if ($this->timeOfCall->Exportable) $Doc->ExportField($this->timeOfCall);
					if ($this->status->Exportable) $Doc->ExportField($this->status);
					if ($this->callDescription->Exportable) $Doc->ExportField($this->callDescription);
					if ($this->solution->Exportable) $Doc->ExportField($this->solution);
					if ($this->priority->Exportable) $Doc->ExportField($this->priority);
					if ($this->contactTime->Exportable) $Doc->ExportField($this->contactTime);
					if ($this->contactDescription->Exportable) $Doc->ExportField($this->contactDescription);
					if ($this->contactStaffName->Exportable) $Doc->ExportField($this->contactStaffName);
					if ($this->roleDescription->Exportable) $Doc->ExportField($this->roleDescription);
					if ($this->callerName->Exportable) $Doc->ExportField($this->callerName);
					if ($this->operatorName->Exportable) $Doc->ExportField($this->operatorName);
					if ($this->allocatedToName->Exportable) $Doc->ExportField($this->allocatedToName);
					if ($this->assetSerialNumber->Exportable) $Doc->ExportField($this->assetSerialNumber);
					if ($this->assetDescription->Exportable) $Doc->ExportField($this->assetDescription);
					if ($this->vendorName->Exportable) $Doc->ExportField($this->vendorName);
					if ($this->vendorDetails->Exportable) $Doc->ExportField($this->vendorDetails);
				} else {
					if ($this->Call_Id->Exportable) $Doc->ExportField($this->Call_Id);
					if ($this->allocatedToId->Exportable) $Doc->ExportField($this->allocatedToId);
					if ($this->timeOfCall->Exportable) $Doc->ExportField($this->timeOfCall);
					if ($this->status->Exportable) $Doc->ExportField($this->status);
					if ($this->callDescription->Exportable) $Doc->ExportField($this->callDescription);
					if ($this->solution->Exportable) $Doc->ExportField($this->solution);
					if ($this->priority->Exportable) $Doc->ExportField($this->priority);
					if ($this->contactTime->Exportable) $Doc->ExportField($this->contactTime);
					if ($this->contactDescription->Exportable) $Doc->ExportField($this->contactDescription);
					if ($this->contactStaffName->Exportable) $Doc->ExportField($this->contactStaffName);
					if ($this->roleDescription->Exportable) $Doc->ExportField($this->roleDescription);
					if ($this->callerName->Exportable) $Doc->ExportField($this->callerName);
					if ($this->operatorName->Exportable) $Doc->ExportField($this->operatorName);
					if ($this->allocatedToName->Exportable) $Doc->ExportField($this->allocatedToName);
					if ($this->assetSerialNumber->Exportable) $Doc->ExportField($this->assetSerialNumber);
					if ($this->assetDescription->Exportable) $Doc->ExportField($this->assetDescription);
					if ($this->vendorName->Exportable) $Doc->ExportField($this->vendorName);
					if ($this->vendorDetails->Exportable) $Doc->ExportField($this->vendorDetails);
				}
				$Doc->EndExportRow();
			}
			$Recordset->MoveNext();
		}
		$Doc->ExportTableFooter();
	}

	// Table level events
	// Recordset Selecting event
	function Recordset_Selecting(&$filter) {

		// Enter your code here	
	}

	// Recordset Selected event
	function Recordset_Selected(&$rs) {

		//echo "Recordset Selected";
	}

	// Recordset Search Validated event
	function Recordset_SearchValidated() {

		// Example:
		//$this->MyField1->AdvancedSearch->SearchValue = "your search criteria"; // Search value

	}

	// Recordset Searching event
	function Recordset_Searching(&$filter) {

		// Enter your code here	
	}

	// Row_Selecting event
	function Row_Selecting(&$filter) {

		// Enter your code here	
	}

	// Row Selected event
	function Row_Selected(&$rs) {

		//echo "Row Selected";
	}

	// Row Inserting event
	function Row_Inserting($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Inserted event
	function Row_Inserted($rsold, &$rsnew) {

		//echo "Row Inserted"
	}

	// Row Updating event
	function Row_Updating($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Updated event
	function Row_Updated($rsold, &$rsnew) {

		//echo "Row Updated";
	}

	// Row Update Conflict event
	function Row_UpdateConflict($rsold, &$rsnew) {

		// Enter your code here
		// To ignore conflict, set return value to FALSE

		return TRUE;
	}

	// Row Deleting event
	function Row_Deleting(&$rs) {

		// Enter your code here
		// To cancel, set return value to False

		return TRUE;
	}

	// Row Deleted event
	function Row_Deleted(&$rs) {

		//echo "Row Deleted";
	}

	// Email Sending event
	function Email_Sending(&$Email, &$Args) {

		//var_dump($Email); var_dump($Args); exit();
		return TRUE;
	}

	// Row Rendering event
	function Row_Rendering() {

		// Enter your code here	
	}

	// Row Rendered event
	function Row_Rendered() {

		// To view properties of field class, use:
		//var_dump($this-><FieldName>); 

	}

	// User ID Filtering event
	function UserID_Filtering(&$filter) {

		// Enter your code here
	}
}
?>
