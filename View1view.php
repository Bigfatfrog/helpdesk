<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg9.php" ?>
<?php include_once "ewmysql9.php" ?>
<?php include_once "phpfn9.php" ?>
<?php include_once "View1info.php" ?>
<?php include_once "staffinfo.php" ?>
<?php include_once "userfn9.php" ?>
<?php

//
// Page class
//

$View1_view = NULL; // Initialize page object first

class cView1_view extends cView1 {

	// Page ID
	var $PageID = 'view';

	// Project ID
	var $ProjectID = "{4F01D370-BA3A-4C91-9445-0AA347261657}";

	// Table name
	var $TableName = 'View1';

	// Page object name
	var $PageObjName = 'View1_view';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Page URLs
	var $AddUrl;
	var $EditUrl;
	var $CopyUrl;
	var $DeleteUrl;
	var $ViewUrl;
	var $ListUrl;

	// Export URLs
	var $ExportPrintUrl;
	var $ExportHtmlUrl;
	var $ExportExcelUrl;
	var $ExportWordUrl;
	var $ExportXmlUrl;
	var $ExportCsvUrl;
	var $ExportPdfUrl;

	// Update URLs
	var $InlineAddUrl;
	var $InlineCopyUrl;
	var $InlineEditUrl;
	var $GridAddUrl;
	var $GridEditUrl;
	var $MultiDeleteUrl;
	var $MultiUpdateUrl;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			$html .= "<p class=\"ewMessage\">" . $sMessage . "</p>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			$html .= "<table class=\"ewMessageTable\"><tr><td class=\"ewWarningIcon\"></td><td class=\"ewWarningMessage\">" . $sWarningMessage . "</td></tr></table>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			$html .= "<table class=\"ewMessageTable\"><tr><td class=\"ewSuccessIcon\"></td><td class=\"ewSuccessMessage\">" . $sSuccessMessage . "</td></tr></table>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			$html .= "<table class=\"ewMessageTable\"><tr><td class=\"ewErrorIcon\"></td><td class=\"ewErrorMessage\">" . $sErrorMessage . "</td></tr></table>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p class=\"phpmaker\">" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Fotoer exists, display
			echo "<p class=\"phpmaker\">" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language, $UserAgent;

		// User agent
		$UserAgent = ew_UserAgent();
		$GLOBALS["Page"] = &$this;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (View1)
		if (!isset($GLOBALS["View1"])) {
			$GLOBALS["View1"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["View1"];
		}
		$KeyUrl = "";
		if (@$_GET["Call_Id"] <> "") {
			$this->RecKey["Call_Id"] = $_GET["Call_Id"];
			$KeyUrl .= "&Call_Id=" . urlencode($this->RecKey["Call_Id"]);
		}
		$this->ExportPrintUrl = $this->PageUrl() . "export=print" . $KeyUrl;
		$this->ExportHtmlUrl = $this->PageUrl() . "export=html" . $KeyUrl;
		$this->ExportExcelUrl = $this->PageUrl() . "export=excel" . $KeyUrl;
		$this->ExportWordUrl = $this->PageUrl() . "export=word" . $KeyUrl;
		$this->ExportXmlUrl = $this->PageUrl() . "export=xml" . $KeyUrl;
		$this->ExportCsvUrl = $this->PageUrl() . "export=csv" . $KeyUrl;
		$this->ExportPdfUrl = $this->PageUrl() . "export=pdf" . $KeyUrl;

		// Table object (staff)
		if (!isset($GLOBALS['staff'])) $GLOBALS['staff'] = new cstaff();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'view', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'View1', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect();

		// Export options
		$this->ExportOptions = new cListOptions();
		$this->ExportOptions->Tag = "span";
		$this->ExportOptions->TagClassName = "ewExportOption";
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if (!$Security->IsLoggedIn()) {
			$Security->SaveLastUrl();
			$this->Page_Terminate("login.php");
		}
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"];
		$this->Call_Id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $conn;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();
		$this->Page_Redirecting($url);

		 // Close connection
		$conn->Close();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $ExportOptions; // Export options
	var $DisplayRecs = 1;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $RecCnt;
	var $RecKey = array();
	var $Recordset;

	//
	// Page main
	//
	function Page_Main() {
		global $Language;

		// Load current record
		$bLoadCurrentRecord = FALSE;
		$sReturnUrl = "";
		$bMatchRecord = FALSE;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET["Call_Id"] <> "") {
				$this->Call_Id->setQueryStringValue($_GET["Call_Id"]);
				$this->RecKey["Call_Id"] = $this->Call_Id->QueryStringValue;
			} else {
				$sReturnUrl = "View1list.php"; // Return to list
			}

			// Get action
			$this->CurrentAction = "I"; // Display form
			switch ($this->CurrentAction) {
				case "I": // Get a record to display
					if (!$this->LoadRow()) { // Load record based on key
						if ($this->getSuccessMessage() == "" && $this->getFailureMessage() == "")
							$this->setFailureMessage($Language->Phrase("NoRecord")); // Set no record message
						$sReturnUrl = "View1list.php"; // No matching record, return to list
					}
			}
		} else {
			$sReturnUrl = "View1list.php"; // Not page request, return to list
		}
		if ($sReturnUrl <> "")
			$this->Page_Terminate($sReturnUrl);

		// Render row
		$this->RowType = EW_ROWTYPE_VIEW;
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Load row based on key values
	function LoadRow() {
		global $conn, $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		global $conn;
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->Call_Id->setDbValue($rs->fields('Call Id'));
		$this->allocatedToId->setDbValue($rs->fields('allocatedToId'));
		$this->timeOfCall->setDbValue($rs->fields('timeOfCall'));
		$this->status->setDbValue($rs->fields('status'));
		$this->callDescription->setDbValue($rs->fields('callDescription'));
		$this->solution->setDbValue($rs->fields('solution'));
		$this->priority->setDbValue($rs->fields('priority'));
		$this->contactTime->setDbValue($rs->fields('contactTime'));
		$this->contactDescription->setDbValue($rs->fields('contactDescription'));
		$this->contactStaffName->setDbValue($rs->fields('contactStaffName'));
		$this->roleDescription->setDbValue($rs->fields('roleDescription'));
		$this->callerName->setDbValue($rs->fields('callerName'));
		$this->operatorName->setDbValue($rs->fields('operatorName'));
		$this->allocatedToName->setDbValue($rs->fields('allocatedToName'));
		$this->assetSerialNumber->setDbValue($rs->fields('assetSerialNumber'));
		$this->assetDescription->setDbValue($rs->fields('assetDescription'));
		$this->vendorName->setDbValue($rs->fields('vendorName'));
		$this->vendorDetails->setDbValue($rs->fields('vendorDetails'));
	}

	// Render row values based on field settings
	function RenderRow() {
		global $conn, $Security, $Language;
		global $gsLanguage;

		// Initialize URLs
		$this->AddUrl = $this->GetAddUrl();
		$this->EditUrl = $this->GetEditUrl();
		$this->CopyUrl = $this->GetCopyUrl();
		$this->DeleteUrl = $this->GetDeleteUrl();
		$this->ListUrl = $this->GetListUrl();

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// Call Id
		// allocatedToId
		// timeOfCall
		// status
		// callDescription
		// solution
		// priority
		// contactTime
		// contactDescription
		// contactStaffName
		// roleDescription
		// callerName
		// operatorName
		// allocatedToName
		// assetSerialNumber
		// assetDescription
		// vendorName
		// vendorDetails

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

			// Call Id
			$this->Call_Id->ViewValue = $this->Call_Id->CurrentValue;
			$this->Call_Id->ViewCustomAttributes = "";

			// allocatedToId
			$this->allocatedToId->ViewValue = $this->allocatedToId->CurrentValue;
			$this->allocatedToId->ViewCustomAttributes = "";

			// timeOfCall
			$this->timeOfCall->ViewValue = $this->timeOfCall->CurrentValue;
			$this->timeOfCall->ViewValue = ew_FormatDateTime($this->timeOfCall->ViewValue, 7);
			$this->timeOfCall->ViewCustomAttributes = "";

			// status
			$this->status->ViewValue = $this->status->CurrentValue;
			$this->status->ViewCustomAttributes = "";

			// callDescription
			$this->callDescription->ViewValue = $this->callDescription->CurrentValue;
			$this->callDescription->ViewCustomAttributes = "";

			// solution
			$this->solution->ViewValue = $this->solution->CurrentValue;
			$this->solution->ViewCustomAttributes = "";

			// priority
			$this->priority->ViewValue = $this->priority->CurrentValue;
			$this->priority->ViewCustomAttributes = "";

			// contactTime
			$this->contactTime->ViewValue = $this->contactTime->CurrentValue;
			$this->contactTime->ViewValue = ew_FormatDateTime($this->contactTime->ViewValue, 9);
			$this->contactTime->ViewCustomAttributes = "";

			// contactDescription
			$this->contactDescription->ViewValue = $this->contactDescription->CurrentValue;
			$this->contactDescription->ViewCustomAttributes = "";

			// contactStaffName
			$this->contactStaffName->ViewValue = $this->contactStaffName->CurrentValue;
			$this->contactStaffName->ViewCustomAttributes = "";

			// roleDescription
			$this->roleDescription->ViewValue = $this->roleDescription->CurrentValue;
			$this->roleDescription->ViewCustomAttributes = "";

			// callerName
			$this->callerName->ViewValue = $this->callerName->CurrentValue;
			$this->callerName->ViewCustomAttributes = "";

			// operatorName
			$this->operatorName->ViewValue = $this->operatorName->CurrentValue;
			$this->operatorName->ViewCustomAttributes = "";

			// allocatedToName
			$this->allocatedToName->ViewValue = $this->allocatedToName->CurrentValue;
			$this->allocatedToName->ViewCustomAttributes = "";

			// assetSerialNumber
			$this->assetSerialNumber->ViewValue = $this->assetSerialNumber->CurrentValue;
			$this->assetSerialNumber->ViewCustomAttributes = "";

			// assetDescription
			$this->assetDescription->ViewValue = $this->assetDescription->CurrentValue;
			$this->assetDescription->ViewCustomAttributes = "";

			// vendorName
			$this->vendorName->ViewValue = $this->vendorName->CurrentValue;
			$this->vendorName->ViewCustomAttributes = "";

			// vendorDetails
			$this->vendorDetails->ViewValue = $this->vendorDetails->CurrentValue;
			$this->vendorDetails->ViewCustomAttributes = "";

			// Call Id
			$this->Call_Id->LinkCustomAttributes = "";
			$this->Call_Id->HrefValue = "";
			$this->Call_Id->TooltipValue = "";

			// allocatedToId
			$this->allocatedToId->LinkCustomAttributes = "";
			$this->allocatedToId->HrefValue = "";
			$this->allocatedToId->TooltipValue = "";

			// timeOfCall
			$this->timeOfCall->LinkCustomAttributes = "";
			$this->timeOfCall->HrefValue = "";
			$this->timeOfCall->TooltipValue = "";

			// status
			$this->status->LinkCustomAttributes = "";
			$this->status->HrefValue = "";
			$this->status->TooltipValue = "";

			// callDescription
			$this->callDescription->LinkCustomAttributes = "";
			$this->callDescription->HrefValue = "";
			$this->callDescription->TooltipValue = "";

			// solution
			$this->solution->LinkCustomAttributes = "";
			$this->solution->HrefValue = "";
			$this->solution->TooltipValue = "";

			// priority
			$this->priority->LinkCustomAttributes = "";
			$this->priority->HrefValue = "";
			$this->priority->TooltipValue = "";

			// contactTime
			$this->contactTime->LinkCustomAttributes = "";
			$this->contactTime->HrefValue = "";
			$this->contactTime->TooltipValue = "";

			// contactDescription
			$this->contactDescription->LinkCustomAttributes = "";
			$this->contactDescription->HrefValue = "";
			$this->contactDescription->TooltipValue = "";

			// contactStaffName
			$this->contactStaffName->LinkCustomAttributes = "";
			$this->contactStaffName->HrefValue = "";
			$this->contactStaffName->TooltipValue = "";

			// roleDescription
			$this->roleDescription->LinkCustomAttributes = "";
			$this->roleDescription->HrefValue = "";
			$this->roleDescription->TooltipValue = "";

			// callerName
			$this->callerName->LinkCustomAttributes = "";
			$this->callerName->HrefValue = "";
			$this->callerName->TooltipValue = "";

			// operatorName
			$this->operatorName->LinkCustomAttributes = "";
			$this->operatorName->HrefValue = "";
			$this->operatorName->TooltipValue = "";

			// allocatedToName
			$this->allocatedToName->LinkCustomAttributes = "";
			$this->allocatedToName->HrefValue = "";
			$this->allocatedToName->TooltipValue = "";

			// assetSerialNumber
			$this->assetSerialNumber->LinkCustomAttributes = "";
			$this->assetSerialNumber->HrefValue = "";
			$this->assetSerialNumber->TooltipValue = "";

			// assetDescription
			$this->assetDescription->LinkCustomAttributes = "";
			$this->assetDescription->HrefValue = "";
			$this->assetDescription->TooltipValue = "";

			// vendorName
			$this->vendorName->LinkCustomAttributes = "";
			$this->vendorName->HrefValue = "";
			$this->vendorName->TooltipValue = "";

			// vendorDetails
			$this->vendorDetails->LinkCustomAttributes = "";
			$this->vendorDetails->HrefValue = "";
			$this->vendorDetails->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($View1_view)) $View1_view = new cView1_view();

// Page init
$View1_view->Page_Init();

// Page main
$View1_view->Page_Main();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Page object
var View1_view = new ew_Page("View1_view");
View1_view.PageID = "view"; // Page ID
var EW_PAGE_ID = View1_view.PageID; // For backward compatibility

// Form object
var fView1view = new ew_Form("fView1view");

// Form_CustomValidate event
fView1view.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fView1view.ValidateRequired = true;
<?php } else { ?>
fView1view.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
// Form object for search

</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<p><span id="ewPageCaption" class="ewTitle ewTableTitle"><?php echo $Language->Phrase("View") ?>&nbsp;<?php echo $Language->Phrase("TblTypeVIEW") ?><?php echo $View1->TableCaption() ?>&nbsp;&nbsp;</span><?php $View1_view->ExportOptions->Render("body"); ?>
</p>
<p class="phpmaker">
<a href="<?php echo $View1_view->ListUrl ?>" id="a_BackToList" class="ewLink"><?php echo $Language->Phrase("BackToList") ?></a>&nbsp;
</p>
<?php $View1_view->ShowPageHeader(); ?>
<?php
$View1_view->ShowMessage();
?>
<form name="fView1view" id="fView1view" class="ewForm" action="" method="post">
<input type="hidden" name="t" value="View1">
<table cellspacing="0" class="ewGrid"><tr><td class="ewGridContent">
<div class="ewGridMiddlePanel">
<table id="tbl_View1view" class="ewTable">
<?php if ($View1->Call_Id->Visible) { // Call Id ?>
	<tr id="r_Call_Id"<?php echo $View1->RowAttributes() ?>>
		<td class="ewTableHeader"><span id="elh_View1_Call_Id"><table class="ewTableHeaderBtn"><tr><td><?php echo $View1->Call_Id->FldCaption() ?></td></tr></table></span></td>
		<td<?php echo $View1->Call_Id->CellAttributes() ?>><span id="el_View1_Call_Id">
<span<?php echo $View1->Call_Id->ViewAttributes() ?>>
<?php echo $View1->Call_Id->ViewValue ?></span>
</span></td>
	</tr>
<?php } ?>
<?php if ($View1->allocatedToId->Visible) { // allocatedToId ?>
	<tr id="r_allocatedToId"<?php echo $View1->RowAttributes() ?>>
		<td class="ewTableHeader"><span id="elh_View1_allocatedToId"><table class="ewTableHeaderBtn"><tr><td><?php echo $View1->allocatedToId->FldCaption() ?></td></tr></table></span></td>
		<td<?php echo $View1->allocatedToId->CellAttributes() ?>><span id="el_View1_allocatedToId">
<span<?php echo $View1->allocatedToId->ViewAttributes() ?>>
<?php echo $View1->allocatedToId->ViewValue ?></span>
</span></td>
	</tr>
<?php } ?>
<?php if ($View1->timeOfCall->Visible) { // timeOfCall ?>
	<tr id="r_timeOfCall"<?php echo $View1->RowAttributes() ?>>
		<td class="ewTableHeader"><span id="elh_View1_timeOfCall"><table class="ewTableHeaderBtn"><tr><td><?php echo $View1->timeOfCall->FldCaption() ?></td></tr></table></span></td>
		<td<?php echo $View1->timeOfCall->CellAttributes() ?>><span id="el_View1_timeOfCall">
<span<?php echo $View1->timeOfCall->ViewAttributes() ?>>
<?php echo $View1->timeOfCall->ViewValue ?></span>
</span></td>
	</tr>
<?php } ?>
<?php if ($View1->status->Visible) { // status ?>
	<tr id="r_status"<?php echo $View1->RowAttributes() ?>>
		<td class="ewTableHeader"><span id="elh_View1_status"><table class="ewTableHeaderBtn"><tr><td><?php echo $View1->status->FldCaption() ?></td></tr></table></span></td>
		<td<?php echo $View1->status->CellAttributes() ?>><span id="el_View1_status">
<span<?php echo $View1->status->ViewAttributes() ?>>
<?php echo $View1->status->ViewValue ?></span>
</span></td>
	</tr>
<?php } ?>
<?php if ($View1->callDescription->Visible) { // callDescription ?>
	<tr id="r_callDescription"<?php echo $View1->RowAttributes() ?>>
		<td class="ewTableHeader"><span id="elh_View1_callDescription"><table class="ewTableHeaderBtn"><tr><td><?php echo $View1->callDescription->FldCaption() ?></td></tr></table></span></td>
		<td<?php echo $View1->callDescription->CellAttributes() ?>><span id="el_View1_callDescription">
<span<?php echo $View1->callDescription->ViewAttributes() ?>>
<?php echo $View1->callDescription->ViewValue ?></span>
</span></td>
	</tr>
<?php } ?>
<?php if ($View1->solution->Visible) { // solution ?>
	<tr id="r_solution"<?php echo $View1->RowAttributes() ?>>
		<td class="ewTableHeader"><span id="elh_View1_solution"><table class="ewTableHeaderBtn"><tr><td><?php echo $View1->solution->FldCaption() ?></td></tr></table></span></td>
		<td<?php echo $View1->solution->CellAttributes() ?>><span id="el_View1_solution">
<span<?php echo $View1->solution->ViewAttributes() ?>>
<?php echo $View1->solution->ViewValue ?></span>
</span></td>
	</tr>
<?php } ?>
<?php if ($View1->priority->Visible) { // priority ?>
	<tr id="r_priority"<?php echo $View1->RowAttributes() ?>>
		<td class="ewTableHeader"><span id="elh_View1_priority"><table class="ewTableHeaderBtn"><tr><td><?php echo $View1->priority->FldCaption() ?></td></tr></table></span></td>
		<td<?php echo $View1->priority->CellAttributes() ?>><span id="el_View1_priority">
<span<?php echo $View1->priority->ViewAttributes() ?>>
<?php echo $View1->priority->ViewValue ?></span>
</span></td>
	</tr>
<?php } ?>
<?php if ($View1->contactTime->Visible) { // contactTime ?>
	<tr id="r_contactTime"<?php echo $View1->RowAttributes() ?>>
		<td class="ewTableHeader"><span id="elh_View1_contactTime"><table class="ewTableHeaderBtn"><tr><td><?php echo $View1->contactTime->FldCaption() ?></td></tr></table></span></td>
		<td<?php echo $View1->contactTime->CellAttributes() ?>><span id="el_View1_contactTime">
<span<?php echo $View1->contactTime->ViewAttributes() ?>>
<?php echo $View1->contactTime->ViewValue ?></span>
</span></td>
	</tr>
<?php } ?>
<?php if ($View1->contactDescription->Visible) { // contactDescription ?>
	<tr id="r_contactDescription"<?php echo $View1->RowAttributes() ?>>
		<td class="ewTableHeader"><span id="elh_View1_contactDescription"><table class="ewTableHeaderBtn"><tr><td><?php echo $View1->contactDescription->FldCaption() ?></td></tr></table></span></td>
		<td<?php echo $View1->contactDescription->CellAttributes() ?>><span id="el_View1_contactDescription">
<span<?php echo $View1->contactDescription->ViewAttributes() ?>>
<?php echo $View1->contactDescription->ViewValue ?></span>
</span></td>
	</tr>
<?php } ?>
<?php if ($View1->contactStaffName->Visible) { // contactStaffName ?>
	<tr id="r_contactStaffName"<?php echo $View1->RowAttributes() ?>>
		<td class="ewTableHeader"><span id="elh_View1_contactStaffName"><table class="ewTableHeaderBtn"><tr><td><?php echo $View1->contactStaffName->FldCaption() ?></td></tr></table></span></td>
		<td<?php echo $View1->contactStaffName->CellAttributes() ?>><span id="el_View1_contactStaffName">
<span<?php echo $View1->contactStaffName->ViewAttributes() ?>>
<?php echo $View1->contactStaffName->ViewValue ?></span>
</span></td>
	</tr>
<?php } ?>
<?php if ($View1->roleDescription->Visible) { // roleDescription ?>
	<tr id="r_roleDescription"<?php echo $View1->RowAttributes() ?>>
		<td class="ewTableHeader"><span id="elh_View1_roleDescription"><table class="ewTableHeaderBtn"><tr><td><?php echo $View1->roleDescription->FldCaption() ?></td></tr></table></span></td>
		<td<?php echo $View1->roleDescription->CellAttributes() ?>><span id="el_View1_roleDescription">
<span<?php echo $View1->roleDescription->ViewAttributes() ?>>
<?php echo $View1->roleDescription->ViewValue ?></span>
</span></td>
	</tr>
<?php } ?>
<?php if ($View1->callerName->Visible) { // callerName ?>
	<tr id="r_callerName"<?php echo $View1->RowAttributes() ?>>
		<td class="ewTableHeader"><span id="elh_View1_callerName"><table class="ewTableHeaderBtn"><tr><td><?php echo $View1->callerName->FldCaption() ?></td></tr></table></span></td>
		<td<?php echo $View1->callerName->CellAttributes() ?>><span id="el_View1_callerName">
<span<?php echo $View1->callerName->ViewAttributes() ?>>
<?php echo $View1->callerName->ViewValue ?></span>
</span></td>
	</tr>
<?php } ?>
<?php if ($View1->operatorName->Visible) { // operatorName ?>
	<tr id="r_operatorName"<?php echo $View1->RowAttributes() ?>>
		<td class="ewTableHeader"><span id="elh_View1_operatorName"><table class="ewTableHeaderBtn"><tr><td><?php echo $View1->operatorName->FldCaption() ?></td></tr></table></span></td>
		<td<?php echo $View1->operatorName->CellAttributes() ?>><span id="el_View1_operatorName">
<span<?php echo $View1->operatorName->ViewAttributes() ?>>
<?php echo $View1->operatorName->ViewValue ?></span>
</span></td>
	</tr>
<?php } ?>
<?php if ($View1->allocatedToName->Visible) { // allocatedToName ?>
	<tr id="r_allocatedToName"<?php echo $View1->RowAttributes() ?>>
		<td class="ewTableHeader"><span id="elh_View1_allocatedToName"><table class="ewTableHeaderBtn"><tr><td><?php echo $View1->allocatedToName->FldCaption() ?></td></tr></table></span></td>
		<td<?php echo $View1->allocatedToName->CellAttributes() ?>><span id="el_View1_allocatedToName">
<span<?php echo $View1->allocatedToName->ViewAttributes() ?>>
<?php echo $View1->allocatedToName->ViewValue ?></span>
</span></td>
	</tr>
<?php } ?>
<?php if ($View1->assetSerialNumber->Visible) { // assetSerialNumber ?>
	<tr id="r_assetSerialNumber"<?php echo $View1->RowAttributes() ?>>
		<td class="ewTableHeader"><span id="elh_View1_assetSerialNumber"><table class="ewTableHeaderBtn"><tr><td><?php echo $View1->assetSerialNumber->FldCaption() ?></td></tr></table></span></td>
		<td<?php echo $View1->assetSerialNumber->CellAttributes() ?>><span id="el_View1_assetSerialNumber">
<span<?php echo $View1->assetSerialNumber->ViewAttributes() ?>>
<?php echo $View1->assetSerialNumber->ViewValue ?></span>
</span></td>
	</tr>
<?php } ?>
<?php if ($View1->assetDescription->Visible) { // assetDescription ?>
	<tr id="r_assetDescription"<?php echo $View1->RowAttributes() ?>>
		<td class="ewTableHeader"><span id="elh_View1_assetDescription"><table class="ewTableHeaderBtn"><tr><td><?php echo $View1->assetDescription->FldCaption() ?></td></tr></table></span></td>
		<td<?php echo $View1->assetDescription->CellAttributes() ?>><span id="el_View1_assetDescription">
<span<?php echo $View1->assetDescription->ViewAttributes() ?>>
<?php echo $View1->assetDescription->ViewValue ?></span>
</span></td>
	</tr>
<?php } ?>
<?php if ($View1->vendorName->Visible) { // vendorName ?>
	<tr id="r_vendorName"<?php echo $View1->RowAttributes() ?>>
		<td class="ewTableHeader"><span id="elh_View1_vendorName"><table class="ewTableHeaderBtn"><tr><td><?php echo $View1->vendorName->FldCaption() ?></td></tr></table></span></td>
		<td<?php echo $View1->vendorName->CellAttributes() ?>><span id="el_View1_vendorName">
<span<?php echo $View1->vendorName->ViewAttributes() ?>>
<?php echo $View1->vendorName->ViewValue ?></span>
</span></td>
	</tr>
<?php } ?>
<?php if ($View1->vendorDetails->Visible) { // vendorDetails ?>
	<tr id="r_vendorDetails"<?php echo $View1->RowAttributes() ?>>
		<td class="ewTableHeader"><span id="elh_View1_vendorDetails"><table class="ewTableHeaderBtn"><tr><td><?php echo $View1->vendorDetails->FldCaption() ?></td></tr></table></span></td>
		<td<?php echo $View1->vendorDetails->CellAttributes() ?>><span id="el_View1_vendorDetails">
<span<?php echo $View1->vendorDetails->ViewAttributes() ?>>
<?php echo $View1->vendorDetails->ViewValue ?></span>
</span></td>
	</tr>
<?php } ?>
</table>
</div>
</td></tr></table>
</form>
<br>
<script type="text/javascript">
fView1view.Init();
</script>
<?php
$View1_view->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$View1_view->Page_Terminate();
?>
