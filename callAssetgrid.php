<?php include_once "staffinfo.php" ?>
<?php

// Create page object
if (!isset($callAsset_grid)) $callAsset_grid = new ccallAsset_grid();

// Page init
$callAsset_grid->Page_Init();

// Page main
$callAsset_grid->Page_Main();
?>
<?php if ($callAsset->Export == "") { ?>
<script type="text/javascript">

// Page object
var callAsset_grid = new ew_Page("callAsset_grid");
callAsset_grid.PageID = "grid"; // Page ID
var EW_PAGE_ID = callAsset_grid.PageID; // For backward compatibility

// Form object
var fcallAssetgrid = new ew_Form("fcallAssetgrid");

// Validate form
fcallAssetgrid.Validate = function(fobj) {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	fobj = fobj || this.Form;
	this.PostAutoSuggest();	
	if (fobj.a_confirm && fobj.a_confirm.value == "F")
		return true;
	var elm, aelm;
	var rowcnt = (fobj.key_count) ? Number(fobj.key_count.value) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // rowcnt == 0 => Inline-Add
	var addcnt = 0;
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = (fobj.key_count) ? String(i) : "";
		var checkrow = (fobj.a_list && fobj.a_list.value == "gridinsert") ? !this.EmptyRow(infix) : true;
		if (checkrow) {
			addcnt++;
		elm = fobj.elements["x" + infix + "_assetSerialNumber"];
		if (elm && !ew_HasValue(elm))
			return ew_OnError(this, elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($callAsset->assetSerialNumber->FldCaption()) ?>");

		// Set up row object
		ew_ElementsToRow(fobj, infix);

		// Fire Form_CustomValidate event
		if (!this.Form_CustomValidate(fobj))
			return false;
		} // End Grid Add checking
	}
	return true;
}

// Check empty row
fcallAssetgrid.EmptyRow = function(infix) {
	var fobj = this.Form;
	if (ew_ValueChanged(fobj, infix, "assetSerialNumber", false)) return false;
	return true;
}

// Form_CustomValidate event
fcallAssetgrid.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fcallAssetgrid.ValidateRequired = true;
<?php } else { ?>
fcallAssetgrid.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fcallAssetgrid.Lists["x_assetSerialNumber"] = {"LinkField":"x_serialNumber","Ajax":null,"AutoFill":false,"DisplayFields":["x_serialNumber","x_assetDescription","",""],"ParentFields":[],"FilterFields":[],"Options":[]};

// Form object for search
</script>
<?php } ?>
<?php
if ($callAsset->CurrentAction == "gridadd") {
	if ($callAsset->CurrentMode == "copy") {
		$bSelectLimit = EW_SELECT_LIMIT;
		if ($bSelectLimit) {
			$callAsset_grid->TotalRecs = $callAsset->SelectRecordCount();
			$callAsset_grid->Recordset = $callAsset_grid->LoadRecordset($callAsset_grid->StartRec-1, $callAsset_grid->DisplayRecs);
		} else {
			if ($callAsset_grid->Recordset = $callAsset_grid->LoadRecordset())
				$callAsset_grid->TotalRecs = $callAsset_grid->Recordset->RecordCount();
		}
		$callAsset_grid->StartRec = 1;
		$callAsset_grid->DisplayRecs = $callAsset_grid->TotalRecs;
	} else {
		$callAsset->CurrentFilter = "0=1";
		$callAsset_grid->StartRec = 1;
		$callAsset_grid->DisplayRecs = $callAsset->GridAddRowCount;
	}
	$callAsset_grid->TotalRecs = $callAsset_grid->DisplayRecs;
	$callAsset_grid->StopRec = $callAsset_grid->DisplayRecs;
} else {
	$bSelectLimit = EW_SELECT_LIMIT;
	if ($bSelectLimit) {
		$callAsset_grid->TotalRecs = $callAsset->SelectRecordCount();
	} else {
		if ($callAsset_grid->Recordset = $callAsset_grid->LoadRecordset())
			$callAsset_grid->TotalRecs = $callAsset_grid->Recordset->RecordCount();
	}
	$callAsset_grid->StartRec = 1;
	$callAsset_grid->DisplayRecs = $callAsset_grid->TotalRecs; // Display all records
	if ($bSelectLimit)
		$callAsset_grid->Recordset = $callAsset_grid->LoadRecordset($callAsset_grid->StartRec-1, $callAsset_grid->DisplayRecs);
}
?>
<p style="white-space: nowrap;"><span id="ewPageCaption" class="ewTitle ewTableTitle"><?php if ($callAsset->CurrentMode == "add" || $callAsset->CurrentMode == "copy") { ?><?php echo $Language->Phrase("Add") ?><?php } elseif ($callAsset->CurrentMode == "edit") { ?><?php echo $Language->Phrase("Edit") ?><?php } ?>&nbsp;<?php echo $Language->Phrase("TblTypeTABLE") ?><?php echo $callAsset->TableCaption() ?></span></p>
</p>
<?php $callAsset_grid->ShowPageHeader(); ?>
<?php
$callAsset_grid->ShowMessage();
?>
<br>
<table cellspacing="0" class="ewGrid"><tr><td class="ewGridContent">
<div id="fcallAssetgrid" class="ewForm">
<div id="gmp_callAsset" class="ewGridMiddlePanel">
<table id="tbl_callAssetgrid" class="ewTable ewTableSeparate">
<?php echo $callAsset->TableCustomInnerHtml ?>
<thead><!-- Table header -->
	<tr class="ewTableHeader">
<?php

// Render list options
$callAsset_grid->RenderListOptions();

// Render list options (header, left)
$callAsset_grid->ListOptions->Render("header", "left");
?>
<?php if ($callAsset->Id->Visible) { // Id ?>
	<?php if ($callAsset->SortUrl($callAsset->Id) == "") { ?>
		<td><span id="elh_callAsset_Id" class="callAsset_Id"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $callAsset->Id->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div><span id="elh_callAsset_Id" class="callAsset_Id">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $callAsset->Id->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($callAsset->Id->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($callAsset->Id->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($callAsset->assetSerialNumber->Visible) { // assetSerialNumber ?>
	<?php if ($callAsset->SortUrl($callAsset->assetSerialNumber) == "") { ?>
		<td><span id="elh_callAsset_assetSerialNumber" class="callAsset_assetSerialNumber"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $callAsset->assetSerialNumber->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div><span id="elh_callAsset_assetSerialNumber" class="callAsset_assetSerialNumber">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $callAsset->assetSerialNumber->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($callAsset->assetSerialNumber->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($callAsset->assetSerialNumber->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php

// Render list options (header, right)
$callAsset_grid->ListOptions->Render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
$callAsset_grid->StartRec = 1;
$callAsset_grid->StopRec = $callAsset_grid->TotalRecs; // Show all records

// Restore number of post back records
if ($objForm) {
	$objForm->Index = -1;
	if ($objForm->HasValue("key_count") && ($callAsset->CurrentAction == "gridadd" || $callAsset->CurrentAction == "gridedit" || $callAsset->CurrentAction == "F")) {
		$callAsset_grid->KeyCount = $objForm->GetValue("key_count");
		$callAsset_grid->StopRec = $callAsset_grid->KeyCount;
	}
}
$callAsset_grid->RecCnt = $callAsset_grid->StartRec - 1;
if ($callAsset_grid->Recordset && !$callAsset_grid->Recordset->EOF) {
	$callAsset_grid->Recordset->MoveFirst();
	if (!$bSelectLimit && $callAsset_grid->StartRec > 1)
		$callAsset_grid->Recordset->Move($callAsset_grid->StartRec - 1);
} elseif (!$callAsset->AllowAddDeleteRow && $callAsset_grid->StopRec == 0) {
	$callAsset_grid->StopRec = $callAsset->GridAddRowCount;
}

// Initialize aggregate
$callAsset->RowType = EW_ROWTYPE_AGGREGATEINIT;
$callAsset->ResetAttrs();
$callAsset_grid->RenderRow();
if ($callAsset->CurrentAction == "gridadd")
	$callAsset_grid->RowIndex = 0;
if ($callAsset->CurrentAction == "gridedit")
	$callAsset_grid->RowIndex = 0;
while ($callAsset_grid->RecCnt < $callAsset_grid->StopRec) {
	$callAsset_grid->RecCnt++;
	if (intval($callAsset_grid->RecCnt) >= intval($callAsset_grid->StartRec)) {
		$callAsset_grid->RowCnt++;
		if ($callAsset->CurrentAction == "gridadd" || $callAsset->CurrentAction == "gridedit" || $callAsset->CurrentAction == "F") {
			$callAsset_grid->RowIndex++;
			$objForm->Index = $callAsset_grid->RowIndex;
			if ($objForm->HasValue("k_action"))
				$callAsset_grid->RowAction = strval($objForm->GetValue("k_action"));
			elseif ($callAsset->CurrentAction == "gridadd")
				$callAsset_grid->RowAction = "insert";
			else
				$callAsset_grid->RowAction = "";
		}

		// Set up key count
		$callAsset_grid->KeyCount = $callAsset_grid->RowIndex;

		// Init row class and style
		$callAsset->ResetAttrs();
		$callAsset->CssClass = "";
		if ($callAsset->CurrentAction == "gridadd") {
			if ($callAsset->CurrentMode == "copy") {
				$callAsset_grid->LoadRowValues($callAsset_grid->Recordset); // Load row values
				$callAsset_grid->SetRecordKey($callAsset_grid->RowOldKey, $callAsset_grid->Recordset); // Set old record key
			} else {
				$callAsset_grid->LoadDefaultValues(); // Load default values
				$callAsset_grid->RowOldKey = ""; // Clear old key value
			}
		} elseif ($callAsset->CurrentAction == "gridedit") {
			$callAsset_grid->LoadRowValues($callAsset_grid->Recordset); // Load row values
		}
		$callAsset->RowType = EW_ROWTYPE_VIEW; // Render view
		if ($callAsset->CurrentAction == "gridadd") // Grid add
			$callAsset->RowType = EW_ROWTYPE_ADD; // Render add
		if ($callAsset->CurrentAction == "gridadd" && $callAsset->EventCancelled && !$objForm->HasValue("k_blankrow")) // Insert failed
			$callAsset_grid->RestoreCurrentRowFormValues($callAsset_grid->RowIndex); // Restore form values
		if ($callAsset->CurrentAction == "gridedit") { // Grid edit
			if ($callAsset->EventCancelled) {
				$callAsset_grid->RestoreCurrentRowFormValues($callAsset_grid->RowIndex); // Restore form values
			}
			if ($callAsset_grid->RowAction == "insert")
				$callAsset->RowType = EW_ROWTYPE_ADD; // Render add
			else
				$callAsset->RowType = EW_ROWTYPE_EDIT; // Render edit
		}
		if ($callAsset->CurrentAction == "gridedit" && ($callAsset->RowType == EW_ROWTYPE_EDIT || $callAsset->RowType == EW_ROWTYPE_ADD) && $callAsset->EventCancelled) // Update failed
			$callAsset_grid->RestoreCurrentRowFormValues($callAsset_grid->RowIndex); // Restore form values
		if ($callAsset->RowType == EW_ROWTYPE_EDIT) // Edit row
			$callAsset_grid->EditRowCnt++;
		if ($callAsset->CurrentAction == "F") // Confirm row
			$callAsset_grid->RestoreCurrentRowFormValues($callAsset_grid->RowIndex); // Restore form values

		// Set up row id / data-rowindex
		$callAsset->RowAttrs = array_merge($callAsset->RowAttrs, array('data-rowindex'=>$callAsset_grid->RowCnt, 'id'=>'r' . $callAsset_grid->RowCnt . '_callAsset', 'data-rowtype'=>$callAsset->RowType));

		// Render row
		$callAsset_grid->RenderRow();

		// Render list options
		$callAsset_grid->RenderListOptions();

		// Skip delete row / empty row for confirm page
		if ($callAsset_grid->RowAction <> "delete" && $callAsset_grid->RowAction <> "insertdelete" && !($callAsset_grid->RowAction == "insert" && $callAsset->CurrentAction == "F" && $callAsset_grid->EmptyRow())) {
?>
	<tr<?php echo $callAsset->RowAttributes() ?>>
<?php

// Render list options (body, left)
$callAsset_grid->ListOptions->Render("body", "left", $callAsset_grid->RowCnt);
?>
	<?php if ($callAsset->Id->Visible) { // Id ?>
		<td<?php echo $callAsset->Id->CellAttributes() ?>><span id="el<?php echo $callAsset_grid->RowCnt ?>_callAsset_Id" class="callAsset_Id">
<?php if ($callAsset->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="hidden" name="o<?php echo $callAsset_grid->RowIndex ?>_Id" id="o<?php echo $callAsset_grid->RowIndex ?>_Id" value="<?php echo ew_HtmlEncode($callAsset->Id->OldValue) ?>">
<?php } ?>
<?php if ($callAsset->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span<?php echo $callAsset->Id->ViewAttributes() ?>>
<?php echo $callAsset->Id->EditValue ?></span>
<input type="hidden" name="x<?php echo $callAsset_grid->RowIndex ?>_Id" id="x<?php echo $callAsset_grid->RowIndex ?>_Id" value="<?php echo ew_HtmlEncode($callAsset->Id->CurrentValue) ?>">
<?php } ?>
<?php if ($callAsset->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span<?php echo $callAsset->Id->ViewAttributes() ?>>
<?php echo $callAsset->Id->ListViewValue() ?></span>
<input type="hidden" name="x<?php echo $callAsset_grid->RowIndex ?>_Id" id="x<?php echo $callAsset_grid->RowIndex ?>_Id" value="<?php echo ew_HtmlEncode($callAsset->Id->FormValue) ?>">
<input type="hidden" name="o<?php echo $callAsset_grid->RowIndex ?>_Id" id="o<?php echo $callAsset_grid->RowIndex ?>_Id" value="<?php echo ew_HtmlEncode($callAsset->Id->OldValue) ?>">
<?php } ?>
</span><a id="<?php echo $callAsset_grid->PageObjName . "_row_" . $callAsset_grid->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($callAsset->assetSerialNumber->Visible) { // assetSerialNumber ?>
		<td<?php echo $callAsset->assetSerialNumber->CellAttributes() ?>><span id="el<?php echo $callAsset_grid->RowCnt ?>_callAsset_assetSerialNumber" class="callAsset_assetSerialNumber">
<?php if ($callAsset->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<select id="x<?php echo $callAsset_grid->RowIndex ?>_assetSerialNumber" name="x<?php echo $callAsset_grid->RowIndex ?>_assetSerialNumber"<?php echo $callAsset->assetSerialNumber->EditAttributes() ?>>
<?php
if (is_array($callAsset->assetSerialNumber->EditValue)) {
	$arwrk = $callAsset->assetSerialNumber->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($callAsset->assetSerialNumber->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " selected=\"selected\"" : "";
		if ($selwrk <> "") $emptywrk = FALSE;
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $arwrk[$rowcntwrk][1] ?>
<?php if ($arwrk[$rowcntwrk][2] <> "") { ?>
<?php echo ew_ValueSeparator(1,$callAsset->assetSerialNumber) ?><?php echo $arwrk[$rowcntwrk][2] ?>
<?php } ?>
</option>
<?php
	}
}
if (@$emptywrk) $callAsset->assetSerialNumber->OldValue = "";
?>
</select>
<script type="text/javascript">
fcallAssetgrid.Lists["x_assetSerialNumber"].Options = <?php echo (is_array($callAsset->assetSerialNumber->EditValue)) ? ew_ArrayToJson($callAsset->assetSerialNumber->EditValue, 1) : "[]" ?>;
</script>
<input type="hidden" name="o<?php echo $callAsset_grid->RowIndex ?>_assetSerialNumber" id="o<?php echo $callAsset_grid->RowIndex ?>_assetSerialNumber" value="<?php echo ew_HtmlEncode($callAsset->assetSerialNumber->OldValue) ?>">
<?php } ?>
<?php if ($callAsset->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<select id="x<?php echo $callAsset_grid->RowIndex ?>_assetSerialNumber" name="x<?php echo $callAsset_grid->RowIndex ?>_assetSerialNumber"<?php echo $callAsset->assetSerialNumber->EditAttributes() ?>>
<?php
if (is_array($callAsset->assetSerialNumber->EditValue)) {
	$arwrk = $callAsset->assetSerialNumber->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($callAsset->assetSerialNumber->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " selected=\"selected\"" : "";
		if ($selwrk <> "") $emptywrk = FALSE;
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $arwrk[$rowcntwrk][1] ?>
<?php if ($arwrk[$rowcntwrk][2] <> "") { ?>
<?php echo ew_ValueSeparator(1,$callAsset->assetSerialNumber) ?><?php echo $arwrk[$rowcntwrk][2] ?>
<?php } ?>
</option>
<?php
	}
}
if (@$emptywrk) $callAsset->assetSerialNumber->OldValue = "";
?>
</select>
<script type="text/javascript">
fcallAssetgrid.Lists["x_assetSerialNumber"].Options = <?php echo (is_array($callAsset->assetSerialNumber->EditValue)) ? ew_ArrayToJson($callAsset->assetSerialNumber->EditValue, 1) : "[]" ?>;
</script>
<?php } ?>
<?php if ($callAsset->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span<?php echo $callAsset->assetSerialNumber->ViewAttributes() ?>>
<?php echo $callAsset->assetSerialNumber->ListViewValue() ?></span>
<input type="hidden" name="x<?php echo $callAsset_grid->RowIndex ?>_assetSerialNumber" id="x<?php echo $callAsset_grid->RowIndex ?>_assetSerialNumber" value="<?php echo ew_HtmlEncode($callAsset->assetSerialNumber->FormValue) ?>">
<input type="hidden" name="o<?php echo $callAsset_grid->RowIndex ?>_assetSerialNumber" id="o<?php echo $callAsset_grid->RowIndex ?>_assetSerialNumber" value="<?php echo ew_HtmlEncode($callAsset->assetSerialNumber->OldValue) ?>">
<?php } ?>
</span><a id="<?php echo $callAsset_grid->PageObjName . "_row_" . $callAsset_grid->RowCnt ?>"></a></td>
	<?php } ?>
<?php

// Render list options (body, right)
$callAsset_grid->ListOptions->Render("body", "right", $callAsset_grid->RowCnt);
?>
	</tr>
<?php if ($callAsset->RowType == EW_ROWTYPE_ADD || $callAsset->RowType == EW_ROWTYPE_EDIT) { ?>
<script type="text/javascript">
fcallAssetgrid.UpdateOpts(<?php echo $callAsset_grid->RowIndex ?>);
</script>
<?php } ?>
<?php
	}
	} // End delete row checking
	if ($callAsset->CurrentAction <> "gridadd" || $callAsset->CurrentMode == "copy")
		if (!$callAsset_grid->Recordset->EOF) $callAsset_grid->Recordset->MoveNext();
}
?>
<?php
	if ($callAsset->CurrentMode == "add" || $callAsset->CurrentMode == "copy" || $callAsset->CurrentMode == "edit") {
		$callAsset_grid->RowIndex = '$rowindex$';
		$callAsset_grid->LoadDefaultValues();

		// Set row properties
		$callAsset->ResetAttrs();
		$callAsset->RowAttrs = array_merge($callAsset->RowAttrs, array('data-rowindex'=>$callAsset_grid->RowIndex, 'id'=>'r0_callAsset', 'data-rowtype'=>EW_ROWTYPE_ADD));
		ew_AppendClass($callAsset->RowAttrs["class"], "ewTemplate");
		$callAsset->RowType = EW_ROWTYPE_ADD;

		// Render row
		$callAsset_grid->RenderRow();

		// Render list options
		$callAsset_grid->RenderListOptions();
		$callAsset_grid->StartRowCnt = 0;
?>
	<tr<?php echo $callAsset->RowAttributes() ?>>
<?php

// Render list options (body, left)
$callAsset_grid->ListOptions->Render("body", "left", $callAsset_grid->RowIndex);
?>
	<?php if ($callAsset->Id->Visible) { // Id ?>
		<td><span id="el$rowindex$_callAsset_Id" class="callAsset_Id">
<?php if ($callAsset->CurrentAction <> "F") { ?>
<?php } else { ?>
<span<?php echo $callAsset->Id->ViewAttributes() ?>>
<?php echo $callAsset->Id->ViewValue ?></span>
<input type="hidden" name="x<?php echo $callAsset_grid->RowIndex ?>_Id" id="x<?php echo $callAsset_grid->RowIndex ?>_Id" value="<?php echo ew_HtmlEncode($callAsset->Id->FormValue) ?>">
<?php } ?>
<input type="hidden" name="o<?php echo $callAsset_grid->RowIndex ?>_Id" id="o<?php echo $callAsset_grid->RowIndex ?>_Id" value="<?php echo ew_HtmlEncode($callAsset->Id->OldValue) ?>">
</span></td>
	<?php } ?>
	<?php if ($callAsset->assetSerialNumber->Visible) { // assetSerialNumber ?>
		<td><span id="el$rowindex$_callAsset_assetSerialNumber" class="callAsset_assetSerialNumber">
<?php if ($callAsset->CurrentAction <> "F") { ?>
<select id="x<?php echo $callAsset_grid->RowIndex ?>_assetSerialNumber" name="x<?php echo $callAsset_grid->RowIndex ?>_assetSerialNumber"<?php echo $callAsset->assetSerialNumber->EditAttributes() ?>>
<?php
if (is_array($callAsset->assetSerialNumber->EditValue)) {
	$arwrk = $callAsset->assetSerialNumber->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($callAsset->assetSerialNumber->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " selected=\"selected\"" : "";
		if ($selwrk <> "") $emptywrk = FALSE;
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $arwrk[$rowcntwrk][1] ?>
<?php if ($arwrk[$rowcntwrk][2] <> "") { ?>
<?php echo ew_ValueSeparator(1,$callAsset->assetSerialNumber) ?><?php echo $arwrk[$rowcntwrk][2] ?>
<?php } ?>
</option>
<?php
	}
}
if (@$emptywrk) $callAsset->assetSerialNumber->OldValue = "";
?>
</select>
<script type="text/javascript">
fcallAssetgrid.Lists["x_assetSerialNumber"].Options = <?php echo (is_array($callAsset->assetSerialNumber->EditValue)) ? ew_ArrayToJson($callAsset->assetSerialNumber->EditValue, 1) : "[]" ?>;
</script>
<?php } else { ?>
<span<?php echo $callAsset->assetSerialNumber->ViewAttributes() ?>>
<?php echo $callAsset->assetSerialNumber->ViewValue ?></span>
<input type="hidden" name="x<?php echo $callAsset_grid->RowIndex ?>_assetSerialNumber" id="x<?php echo $callAsset_grid->RowIndex ?>_assetSerialNumber" value="<?php echo ew_HtmlEncode($callAsset->assetSerialNumber->FormValue) ?>">
<?php } ?>
<input type="hidden" name="o<?php echo $callAsset_grid->RowIndex ?>_assetSerialNumber" id="o<?php echo $callAsset_grid->RowIndex ?>_assetSerialNumber" value="<?php echo ew_HtmlEncode($callAsset->assetSerialNumber->OldValue) ?>">
</span></td>
	<?php } ?>
<?php

// Render list options (body, right)
$callAsset_grid->ListOptions->Render("body", "right", $callAsset_grid->RowCnt);
?>
<script type="text/javascript">
fcallAssetgrid.UpdateOpts(<?php echo $callAsset_grid->RowIndex ?>);
</script>
	</tr>
<?php
}
?>
</tbody>
</table>
<?php if ($callAsset->CurrentMode == "add" || $callAsset->CurrentMode == "copy") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridinsert">
<input type="hidden" name="key_count" id="key_count" value="<?php echo $callAsset_grid->KeyCount ?>">
<?php echo $callAsset_grid->MultiSelectKey ?>
<?php } ?>
<?php if ($callAsset->CurrentMode == "edit") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridupdate">
<input type="hidden" name="key_count" id="key_count" value="<?php echo $callAsset_grid->KeyCount ?>">
<?php echo $callAsset_grid->MultiSelectKey ?>
<?php } ?>
<?php if ($callAsset->CurrentMode == "") { ?>
<input type="hidden" name="a_list" id="a_list" value="">
<?php } ?>
<input type="hidden" name="detailpage" id="detailpage" value="fcallAssetgrid">
</div>
<?php

// Close recordset
if ($callAsset_grid->Recordset)
	$callAsset_grid->Recordset->Close();
?>
<?php if (($callAsset->CurrentMode == "add" || $callAsset->CurrentMode == "copy" || $callAsset->CurrentMode == "edit") && $callAsset->CurrentAction != "F") { // add/copy/edit mode ?>
<div class="ewGridLowerPanel">
</div>
<?php } ?>
</div>
</td></tr></table>
<?php if ($callAsset->Export == "") { ?>
<script type="text/javascript">
fcallAssetgrid.Init();
</script>
<?php } ?>
<?php
$callAsset_grid->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<?php
$callAsset_grid->Page_Terminate();
$Page = &$MasterPage;
?>
