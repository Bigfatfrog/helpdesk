<?php include_once "staffinfo.php" ?>
<?php

// Create page object
if (!isset($callContact_grid)) $callContact_grid = new ccallContact_grid();

// Page init
$callContact_grid->Page_Init();

// Page main
$callContact_grid->Page_Main();
?>
<?php if ($callContact->Export == "") { ?>
<script type="text/javascript">

// Page object
var callContact_grid = new ew_Page("callContact_grid");
callContact_grid.PageID = "grid"; // Page ID
var EW_PAGE_ID = callContact_grid.PageID; // For backward compatibility

// Form object
var fcallContactgrid = new ew_Form("fcallContactgrid");

// Validate form
fcallContactgrid.Validate = function(fobj) {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	fobj = fobj || this.Form;
	this.PostAutoSuggest();	
	if (fobj.a_confirm && fobj.a_confirm.value == "F")
		return true;
	var elm, aelm;
	var rowcnt = (fobj.key_count) ? Number(fobj.key_count.value) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // rowcnt == 0 => Inline-Add
	var addcnt = 0;
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = (fobj.key_count) ? String(i) : "";
		var checkrow = (fobj.a_list && fobj.a_list.value == "gridinsert") ? !this.EmptyRow(infix) : true;
		if (checkrow) {
			addcnt++;
		elm = fobj.elements["x" + infix + "_contactStaffId"];
		if (elm && !ew_HasValue(elm))
			return ew_OnError(this, elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($callContact->contactStaffId->FldCaption()) ?>");
		elm = fobj.elements["x" + infix + "_contactDescription"];
		if (elm && !ew_HasValue(elm))
			return ew_OnError(this, elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($callContact->contactDescription->FldCaption()) ?>");

		// Set up row object
		ew_ElementsToRow(fobj, infix);

		// Fire Form_CustomValidate event
		if (!this.Form_CustomValidate(fobj))
			return false;
		} // End Grid Add checking
	}
	return true;
}

// Check empty row
fcallContactgrid.EmptyRow = function(infix) {
	var fobj = this.Form;
	if (ew_ValueChanged(fobj, infix, "contactStaffId", false)) return false;
	if (ew_ValueChanged(fobj, infix, "contactDescription", false)) return false;
	return true;
}

// Form_CustomValidate event
fcallContactgrid.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fcallContactgrid.ValidateRequired = true;
<?php } else { ?>
fcallContactgrid.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fcallContactgrid.Lists["x_contactStaffId"] = {"LinkField":"x_id","Ajax":null,"AutoFill":false,"DisplayFields":["x_name","","",""],"ParentFields":[],"FilterFields":[],"Options":[]};

// Form object for search
</script>
<?php } ?>
<?php
if ($callContact->CurrentAction == "gridadd") {
	if ($callContact->CurrentMode == "copy") {
		$bSelectLimit = EW_SELECT_LIMIT;
		if ($bSelectLimit) {
			$callContact_grid->TotalRecs = $callContact->SelectRecordCount();
			$callContact_grid->Recordset = $callContact_grid->LoadRecordset($callContact_grid->StartRec-1, $callContact_grid->DisplayRecs);
		} else {
			if ($callContact_grid->Recordset = $callContact_grid->LoadRecordset())
				$callContact_grid->TotalRecs = $callContact_grid->Recordset->RecordCount();
		}
		$callContact_grid->StartRec = 1;
		$callContact_grid->DisplayRecs = $callContact_grid->TotalRecs;
	} else {
		$callContact->CurrentFilter = "0=1";
		$callContact_grid->StartRec = 1;
		$callContact_grid->DisplayRecs = $callContact->GridAddRowCount;
	}
	$callContact_grid->TotalRecs = $callContact_grid->DisplayRecs;
	$callContact_grid->StopRec = $callContact_grid->DisplayRecs;
} else {
	$bSelectLimit = EW_SELECT_LIMIT;
	if ($bSelectLimit) {
		$callContact_grid->TotalRecs = $callContact->SelectRecordCount();
	} else {
		if ($callContact_grid->Recordset = $callContact_grid->LoadRecordset())
			$callContact_grid->TotalRecs = $callContact_grid->Recordset->RecordCount();
	}
	$callContact_grid->StartRec = 1;
	$callContact_grid->DisplayRecs = $callContact_grid->TotalRecs; // Display all records
	if ($bSelectLimit)
		$callContact_grid->Recordset = $callContact_grid->LoadRecordset($callContact_grid->StartRec-1, $callContact_grid->DisplayRecs);
}
?>
<p style="white-space: nowrap;"><span id="ewPageCaption" class="ewTitle ewTableTitle"><?php if ($callContact->CurrentMode == "add" || $callContact->CurrentMode == "copy") { ?><?php echo $Language->Phrase("Add") ?><?php } elseif ($callContact->CurrentMode == "edit") { ?><?php echo $Language->Phrase("Edit") ?><?php } ?>&nbsp;<?php echo $Language->Phrase("TblTypeTABLE") ?><?php echo $callContact->TableCaption() ?></span></p>
</p>
<?php $callContact_grid->ShowPageHeader(); ?>
<?php
$callContact_grid->ShowMessage();
?>
<br>
<table cellspacing="0" class="ewGrid"><tr><td class="ewGridContent">
<div id="fcallContactgrid" class="ewForm">
<div id="gmp_callContact" class="ewGridMiddlePanel">
<table id="tbl_callContactgrid" class="ewTable ewTableSeparate">
<?php echo $callContact->TableCustomInnerHtml ?>
<thead><!-- Table header -->
	<tr class="ewTableHeader">
<?php

// Render list options
$callContact_grid->RenderListOptions();

// Render list options (header, left)
$callContact_grid->ListOptions->Render("header", "left");
?>
<?php if ($callContact->contactStaffId->Visible) { // contactStaffId ?>
	<?php if ($callContact->SortUrl($callContact->contactStaffId) == "") { ?>
		<td><span id="elh_callContact_contactStaffId" class="callContact_contactStaffId"><table class="ewTableHeaderBtn" style="white-space: nowrap;"><thead><tr><td><?php echo $callContact->contactStaffId->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div><span id="elh_callContact_contactStaffId" class="callContact_contactStaffId">
			<table class="ewTableHeaderBtn" style="white-space: nowrap;"><thead><tr><td class="ewTableHeaderCaption"><?php echo $callContact->contactStaffId->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($callContact->contactStaffId->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($callContact->contactStaffId->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($callContact->contactDescription->Visible) { // contactDescription ?>
	<?php if ($callContact->SortUrl($callContact->contactDescription) == "") { ?>
		<td><span id="elh_callContact_contactDescription" class="callContact_contactDescription"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $callContact->contactDescription->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div><span id="elh_callContact_contactDescription" class="callContact_contactDescription">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $callContact->contactDescription->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($callContact->contactDescription->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($callContact->contactDescription->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($callContact->contactTime->Visible) { // contactTime ?>
	<?php if ($callContact->SortUrl($callContact->contactTime) == "") { ?>
		<td><span id="elh_callContact_contactTime" class="callContact_contactTime"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $callContact->contactTime->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div><span id="elh_callContact_contactTime" class="callContact_contactTime">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $callContact->contactTime->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($callContact->contactTime->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($callContact->contactTime->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php

// Render list options (header, right)
$callContact_grid->ListOptions->Render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
$callContact_grid->StartRec = 1;
$callContact_grid->StopRec = $callContact_grid->TotalRecs; // Show all records

// Restore number of post back records
if ($objForm) {
	$objForm->Index = -1;
	if ($objForm->HasValue("key_count") && ($callContact->CurrentAction == "gridadd" || $callContact->CurrentAction == "gridedit" || $callContact->CurrentAction == "F")) {
		$callContact_grid->KeyCount = $objForm->GetValue("key_count");
		$callContact_grid->StopRec = $callContact_grid->KeyCount;
	}
}
$callContact_grid->RecCnt = $callContact_grid->StartRec - 1;
if ($callContact_grid->Recordset && !$callContact_grid->Recordset->EOF) {
	$callContact_grid->Recordset->MoveFirst();
	if (!$bSelectLimit && $callContact_grid->StartRec > 1)
		$callContact_grid->Recordset->Move($callContact_grid->StartRec - 1);
} elseif (!$callContact->AllowAddDeleteRow && $callContact_grid->StopRec == 0) {
	$callContact_grid->StopRec = $callContact->GridAddRowCount;
}

// Initialize aggregate
$callContact->RowType = EW_ROWTYPE_AGGREGATEINIT;
$callContact->ResetAttrs();
$callContact_grid->RenderRow();
if ($callContact->CurrentAction == "gridadd")
	$callContact_grid->RowIndex = 0;
if ($callContact->CurrentAction == "gridedit")
	$callContact_grid->RowIndex = 0;
while ($callContact_grid->RecCnt < $callContact_grid->StopRec) {
	$callContact_grid->RecCnt++;
	if (intval($callContact_grid->RecCnt) >= intval($callContact_grid->StartRec)) {
		$callContact_grid->RowCnt++;
		if ($callContact->CurrentAction == "gridadd" || $callContact->CurrentAction == "gridedit" || $callContact->CurrentAction == "F") {
			$callContact_grid->RowIndex++;
			$objForm->Index = $callContact_grid->RowIndex;
			if ($objForm->HasValue("k_action"))
				$callContact_grid->RowAction = strval($objForm->GetValue("k_action"));
			elseif ($callContact->CurrentAction == "gridadd")
				$callContact_grid->RowAction = "insert";
			else
				$callContact_grid->RowAction = "";
		}

		// Set up key count
		$callContact_grid->KeyCount = $callContact_grid->RowIndex;

		// Init row class and style
		$callContact->ResetAttrs();
		$callContact->CssClass = "";
		if ($callContact->CurrentAction == "gridadd") {
			if ($callContact->CurrentMode == "copy") {
				$callContact_grid->LoadRowValues($callContact_grid->Recordset); // Load row values
				$callContact_grid->SetRecordKey($callContact_grid->RowOldKey, $callContact_grid->Recordset); // Set old record key
			} else {
				$callContact_grid->LoadDefaultValues(); // Load default values
				$callContact_grid->RowOldKey = ""; // Clear old key value
			}
		} elseif ($callContact->CurrentAction == "gridedit") {
			$callContact_grid->LoadRowValues($callContact_grid->Recordset); // Load row values
		}
		$callContact->RowType = EW_ROWTYPE_VIEW; // Render view
		if ($callContact->CurrentAction == "gridadd") // Grid add
			$callContact->RowType = EW_ROWTYPE_ADD; // Render add
		if ($callContact->CurrentAction == "gridadd" && $callContact->EventCancelled && !$objForm->HasValue("k_blankrow")) // Insert failed
			$callContact_grid->RestoreCurrentRowFormValues($callContact_grid->RowIndex); // Restore form values
		if ($callContact->CurrentAction == "gridedit") { // Grid edit
			if ($callContact->EventCancelled) {
				$callContact_grid->RestoreCurrentRowFormValues($callContact_grid->RowIndex); // Restore form values
			}
			if ($callContact_grid->RowAction == "insert")
				$callContact->RowType = EW_ROWTYPE_ADD; // Render add
			else
				$callContact->RowType = EW_ROWTYPE_EDIT; // Render edit
		}
		if ($callContact->CurrentAction == "gridedit" && ($callContact->RowType == EW_ROWTYPE_EDIT || $callContact->RowType == EW_ROWTYPE_ADD) && $callContact->EventCancelled) // Update failed
			$callContact_grid->RestoreCurrentRowFormValues($callContact_grid->RowIndex); // Restore form values
		if ($callContact->RowType == EW_ROWTYPE_EDIT) // Edit row
			$callContact_grid->EditRowCnt++;
		if ($callContact->CurrentAction == "F") // Confirm row
			$callContact_grid->RestoreCurrentRowFormValues($callContact_grid->RowIndex); // Restore form values

		// Set up row id / data-rowindex
		$callContact->RowAttrs = array_merge($callContact->RowAttrs, array('data-rowindex'=>$callContact_grid->RowCnt, 'id'=>'r' . $callContact_grid->RowCnt . '_callContact', 'data-rowtype'=>$callContact->RowType));

		// Render row
		$callContact_grid->RenderRow();

		// Render list options
		$callContact_grid->RenderListOptions();

		// Skip delete row / empty row for confirm page
		if ($callContact_grid->RowAction <> "delete" && $callContact_grid->RowAction <> "insertdelete" && !($callContact_grid->RowAction == "insert" && $callContact->CurrentAction == "F" && $callContact_grid->EmptyRow())) {
?>
	<tr<?php echo $callContact->RowAttributes() ?>>
<?php

// Render list options (body, left)
$callContact_grid->ListOptions->Render("body", "left", $callContact_grid->RowCnt);
?>
	<?php if ($callContact->contactStaffId->Visible) { // contactStaffId ?>
		<td<?php echo $callContact->contactStaffId->CellAttributes() ?>><span id="el<?php echo $callContact_grid->RowCnt ?>_callContact_contactStaffId" class="callContact_contactStaffId">
<?php if ($callContact->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<select id="x<?php echo $callContact_grid->RowIndex ?>_contactStaffId" name="x<?php echo $callContact_grid->RowIndex ?>_contactStaffId"<?php echo $callContact->contactStaffId->EditAttributes() ?>>
<?php
if (is_array($callContact->contactStaffId->EditValue)) {
	$arwrk = $callContact->contactStaffId->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($callContact->contactStaffId->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " selected=\"selected\"" : "";
		if ($selwrk <> "") $emptywrk = FALSE;
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $arwrk[$rowcntwrk][1] ?>
</option>
<?php
	}
}
if (@$emptywrk) $callContact->contactStaffId->OldValue = "";
?>
</select>
<script type="text/javascript">
fcallContactgrid.Lists["x_contactStaffId"].Options = <?php echo (is_array($callContact->contactStaffId->EditValue)) ? ew_ArrayToJson($callContact->contactStaffId->EditValue, 1) : "[]" ?>;
</script>
<input type="hidden" name="o<?php echo $callContact_grid->RowIndex ?>_contactStaffId" id="o<?php echo $callContact_grid->RowIndex ?>_contactStaffId" value="<?php echo ew_HtmlEncode($callContact->contactStaffId->OldValue) ?>">
<?php } ?>
<?php if ($callContact->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<select id="x<?php echo $callContact_grid->RowIndex ?>_contactStaffId" name="x<?php echo $callContact_grid->RowIndex ?>_contactStaffId"<?php echo $callContact->contactStaffId->EditAttributes() ?>>
<?php
if (is_array($callContact->contactStaffId->EditValue)) {
	$arwrk = $callContact->contactStaffId->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($callContact->contactStaffId->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " selected=\"selected\"" : "";
		if ($selwrk <> "") $emptywrk = FALSE;
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $arwrk[$rowcntwrk][1] ?>
</option>
<?php
	}
}
if (@$emptywrk) $callContact->contactStaffId->OldValue = "";
?>
</select>
<script type="text/javascript">
fcallContactgrid.Lists["x_contactStaffId"].Options = <?php echo (is_array($callContact->contactStaffId->EditValue)) ? ew_ArrayToJson($callContact->contactStaffId->EditValue, 1) : "[]" ?>;
</script>
<?php } ?>
<?php if ($callContact->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span<?php echo $callContact->contactStaffId->ViewAttributes() ?>>
<?php echo $callContact->contactStaffId->ListViewValue() ?></span>
<input type="hidden" name="x<?php echo $callContact_grid->RowIndex ?>_contactStaffId" id="x<?php echo $callContact_grid->RowIndex ?>_contactStaffId" value="<?php echo ew_HtmlEncode($callContact->contactStaffId->FormValue) ?>">
<input type="hidden" name="o<?php echo $callContact_grid->RowIndex ?>_contactStaffId" id="o<?php echo $callContact_grid->RowIndex ?>_contactStaffId" value="<?php echo ew_HtmlEncode($callContact->contactStaffId->OldValue) ?>">
<?php } ?>
</span><a id="<?php echo $callContact_grid->PageObjName . "_row_" . $callContact_grid->RowCnt ?>"></a></td>
	<?php } ?>
<?php if ($callContact->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="hidden" name="x<?php echo $callContact_grid->RowIndex ?>_id" id="x<?php echo $callContact_grid->RowIndex ?>_id" value="<?php echo ew_HtmlEncode($callContact->id->CurrentValue) ?>">
<input type="hidden" name="o<?php echo $callContact_grid->RowIndex ?>_id" id="o<?php echo $callContact_grid->RowIndex ?>_id" value="<?php echo ew_HtmlEncode($callContact->id->OldValue) ?>">
<?php } ?>
<?php if ($callContact->RowType == EW_ROWTYPE_EDIT || $callContact->CurrentMode == "edit") { ?>
<input type="hidden" name="x<?php echo $callContact_grid->RowIndex ?>_id" id="x<?php echo $callContact_grid->RowIndex ?>_id" value="<?php echo ew_HtmlEncode($callContact->id->CurrentValue) ?>">
<?php } ?>
	<?php if ($callContact->contactDescription->Visible) { // contactDescription ?>
		<td<?php echo $callContact->contactDescription->CellAttributes() ?>><span id="el<?php echo $callContact_grid->RowCnt ?>_callContact_contactDescription" class="callContact_contactDescription">
<?php if ($callContact->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<textarea name="x<?php echo $callContact_grid->RowIndex ?>_contactDescription" id="x<?php echo $callContact_grid->RowIndex ?>_contactDescription" cols="35" rows="4"<?php echo $callContact->contactDescription->EditAttributes() ?>><?php echo $callContact->contactDescription->EditValue ?></textarea>
<input type="hidden" name="o<?php echo $callContact_grid->RowIndex ?>_contactDescription" id="o<?php echo $callContact_grid->RowIndex ?>_contactDescription" value="<?php echo ew_HtmlEncode($callContact->contactDescription->OldValue) ?>">
<?php } ?>
<?php if ($callContact->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<textarea name="x<?php echo $callContact_grid->RowIndex ?>_contactDescription" id="x<?php echo $callContact_grid->RowIndex ?>_contactDescription" cols="35" rows="4"<?php echo $callContact->contactDescription->EditAttributes() ?>><?php echo $callContact->contactDescription->EditValue ?></textarea>
<?php } ?>
<?php if ($callContact->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span<?php echo $callContact->contactDescription->ViewAttributes() ?>>
<?php echo $callContact->contactDescription->ListViewValue() ?></span>
<input type="hidden" name="x<?php echo $callContact_grid->RowIndex ?>_contactDescription" id="x<?php echo $callContact_grid->RowIndex ?>_contactDescription" value="<?php echo ew_HtmlEncode($callContact->contactDescription->FormValue) ?>">
<input type="hidden" name="o<?php echo $callContact_grid->RowIndex ?>_contactDescription" id="o<?php echo $callContact_grid->RowIndex ?>_contactDescription" value="<?php echo ew_HtmlEncode($callContact->contactDescription->OldValue) ?>">
<?php } ?>
</span><a id="<?php echo $callContact_grid->PageObjName . "_row_" . $callContact_grid->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($callContact->contactTime->Visible) { // contactTime ?>
		<td<?php echo $callContact->contactTime->CellAttributes() ?>><span id="el<?php echo $callContact_grid->RowCnt ?>_callContact_contactTime" class="callContact_contactTime">
<?php if ($callContact->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="hidden" name="o<?php echo $callContact_grid->RowIndex ?>_contactTime" id="o<?php echo $callContact_grid->RowIndex ?>_contactTime" value="<?php echo ew_HtmlEncode($callContact->contactTime->OldValue) ?>">
<?php } ?>
<?php if ($callContact->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<?php } ?>
<?php if ($callContact->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span<?php echo $callContact->contactTime->ViewAttributes() ?>>
<?php echo $callContact->contactTime->ListViewValue() ?></span>
<input type="hidden" name="x<?php echo $callContact_grid->RowIndex ?>_contactTime" id="x<?php echo $callContact_grid->RowIndex ?>_contactTime" value="<?php echo ew_HtmlEncode($callContact->contactTime->FormValue) ?>">
<input type="hidden" name="o<?php echo $callContact_grid->RowIndex ?>_contactTime" id="o<?php echo $callContact_grid->RowIndex ?>_contactTime" value="<?php echo ew_HtmlEncode($callContact->contactTime->OldValue) ?>">
<?php } ?>
</span><a id="<?php echo $callContact_grid->PageObjName . "_row_" . $callContact_grid->RowCnt ?>"></a></td>
	<?php } ?>
<?php

// Render list options (body, right)
$callContact_grid->ListOptions->Render("body", "right", $callContact_grid->RowCnt);
?>
	</tr>
<?php if ($callContact->RowType == EW_ROWTYPE_ADD || $callContact->RowType == EW_ROWTYPE_EDIT) { ?>
<script type="text/javascript">
fcallContactgrid.UpdateOpts(<?php echo $callContact_grid->RowIndex ?>);
</script>
<?php } ?>
<?php
	}
	} // End delete row checking
	if ($callContact->CurrentAction <> "gridadd" || $callContact->CurrentMode == "copy")
		if (!$callContact_grid->Recordset->EOF) $callContact_grid->Recordset->MoveNext();
}
?>
<?php
	if ($callContact->CurrentMode == "add" || $callContact->CurrentMode == "copy" || $callContact->CurrentMode == "edit") {
		$callContact_grid->RowIndex = '$rowindex$';
		$callContact_grid->LoadDefaultValues();

		// Set row properties
		$callContact->ResetAttrs();
		$callContact->RowAttrs = array_merge($callContact->RowAttrs, array('data-rowindex'=>$callContact_grid->RowIndex, 'id'=>'r0_callContact', 'data-rowtype'=>EW_ROWTYPE_ADD));
		ew_AppendClass($callContact->RowAttrs["class"], "ewTemplate");
		$callContact->RowType = EW_ROWTYPE_ADD;

		// Render row
		$callContact_grid->RenderRow();

		// Render list options
		$callContact_grid->RenderListOptions();
		$callContact_grid->StartRowCnt = 0;
?>
	<tr<?php echo $callContact->RowAttributes() ?>>
<?php

// Render list options (body, left)
$callContact_grid->ListOptions->Render("body", "left", $callContact_grid->RowIndex);
?>
	<?php if ($callContact->contactStaffId->Visible) { // contactStaffId ?>
		<td><span id="el$rowindex$_callContact_contactStaffId" class="callContact_contactStaffId">
<?php if ($callContact->CurrentAction <> "F") { ?>
<select id="x<?php echo $callContact_grid->RowIndex ?>_contactStaffId" name="x<?php echo $callContact_grid->RowIndex ?>_contactStaffId"<?php echo $callContact->contactStaffId->EditAttributes() ?>>
<?php
if (is_array($callContact->contactStaffId->EditValue)) {
	$arwrk = $callContact->contactStaffId->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($callContact->contactStaffId->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " selected=\"selected\"" : "";
		if ($selwrk <> "") $emptywrk = FALSE;
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $arwrk[$rowcntwrk][1] ?>
</option>
<?php
	}
}
if (@$emptywrk) $callContact->contactStaffId->OldValue = "";
?>
</select>
<script type="text/javascript">
fcallContactgrid.Lists["x_contactStaffId"].Options = <?php echo (is_array($callContact->contactStaffId->EditValue)) ? ew_ArrayToJson($callContact->contactStaffId->EditValue, 1) : "[]" ?>;
</script>
<?php } else { ?>
<span<?php echo $callContact->contactStaffId->ViewAttributes() ?>>
<?php echo $callContact->contactStaffId->ViewValue ?></span>
<input type="hidden" name="x<?php echo $callContact_grid->RowIndex ?>_contactStaffId" id="x<?php echo $callContact_grid->RowIndex ?>_contactStaffId" value="<?php echo ew_HtmlEncode($callContact->contactStaffId->FormValue) ?>">
<?php } ?>
<input type="hidden" name="o<?php echo $callContact_grid->RowIndex ?>_contactStaffId" id="o<?php echo $callContact_grid->RowIndex ?>_contactStaffId" value="<?php echo ew_HtmlEncode($callContact->contactStaffId->OldValue) ?>">
</span></td>
	<?php } ?>
	<?php if ($callContact->contactDescription->Visible) { // contactDescription ?>
		<td><span id="el$rowindex$_callContact_contactDescription" class="callContact_contactDescription">
<?php if ($callContact->CurrentAction <> "F") { ?>
<textarea name="x<?php echo $callContact_grid->RowIndex ?>_contactDescription" id="x<?php echo $callContact_grid->RowIndex ?>_contactDescription" cols="35" rows="4"<?php echo $callContact->contactDescription->EditAttributes() ?>><?php echo $callContact->contactDescription->EditValue ?></textarea>
<?php } else { ?>
<span<?php echo $callContact->contactDescription->ViewAttributes() ?>>
<?php echo $callContact->contactDescription->ViewValue ?></span>
<input type="hidden" name="x<?php echo $callContact_grid->RowIndex ?>_contactDescription" id="x<?php echo $callContact_grid->RowIndex ?>_contactDescription" value="<?php echo ew_HtmlEncode($callContact->contactDescription->FormValue) ?>">
<?php } ?>
<input type="hidden" name="o<?php echo $callContact_grid->RowIndex ?>_contactDescription" id="o<?php echo $callContact_grid->RowIndex ?>_contactDescription" value="<?php echo ew_HtmlEncode($callContact->contactDescription->OldValue) ?>">
</span></td>
	<?php } ?>
	<?php if ($callContact->contactTime->Visible) { // contactTime ?>
		<td><span id="el$rowindex$_callContact_contactTime" class="callContact_contactTime">
<?php if ($callContact->CurrentAction <> "F") { ?>
<?php } else { ?>
<span<?php echo $callContact->contactTime->ViewAttributes() ?>>
<?php echo $callContact->contactTime->ViewValue ?></span>
<input type="hidden" name="x<?php echo $callContact_grid->RowIndex ?>_contactTime" id="x<?php echo $callContact_grid->RowIndex ?>_contactTime" value="<?php echo ew_HtmlEncode($callContact->contactTime->FormValue) ?>">
<?php } ?>
<input type="hidden" name="o<?php echo $callContact_grid->RowIndex ?>_contactTime" id="o<?php echo $callContact_grid->RowIndex ?>_contactTime" value="<?php echo ew_HtmlEncode($callContact->contactTime->OldValue) ?>">
</span></td>
	<?php } ?>
<?php

// Render list options (body, right)
$callContact_grid->ListOptions->Render("body", "right", $callContact_grid->RowCnt);
?>
<script type="text/javascript">
fcallContactgrid.UpdateOpts(<?php echo $callContact_grid->RowIndex ?>);
</script>
	</tr>
<?php
}
?>
</tbody>
</table>
<?php if ($callContact->CurrentMode == "add" || $callContact->CurrentMode == "copy") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridinsert">
<input type="hidden" name="key_count" id="key_count" value="<?php echo $callContact_grid->KeyCount ?>">
<?php echo $callContact_grid->MultiSelectKey ?>
<?php } ?>
<?php if ($callContact->CurrentMode == "edit") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridupdate">
<input type="hidden" name="key_count" id="key_count" value="<?php echo $callContact_grid->KeyCount ?>">
<?php echo $callContact_grid->MultiSelectKey ?>
<?php } ?>
<?php if ($callContact->CurrentMode == "") { ?>
<input type="hidden" name="a_list" id="a_list" value="">
<?php } ?>
<input type="hidden" name="detailpage" id="detailpage" value="fcallContactgrid">
</div>
<?php

// Close recordset
if ($callContact_grid->Recordset)
	$callContact_grid->Recordset->Close();
?>
<?php if (($callContact->CurrentMode == "add" || $callContact->CurrentMode == "copy" || $callContact->CurrentMode == "edit") && $callContact->CurrentAction != "F") { // add/copy/edit mode ?>
<div class="ewGridLowerPanel">
</div>
<?php } ?>
</div>
</td></tr></table>
<?php if ($callContact->Export == "") { ?>
<script type="text/javascript">
fcallContactgrid.Init();
</script>
<?php } ?>
<?php
$callContact_grid->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<?php
$callContact_grid->Page_Terminate();
$Page = &$MasterPage;
?>
