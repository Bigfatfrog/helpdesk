<?php include_once "staffinfo.php" ?>
<?php

// Create page object
if (!isset($call_asset_grid)) $call_asset_grid = new ccall_asset_grid();

// Page init
$call_asset_grid->Page_Init();

// Page main
$call_asset_grid->Page_Main();
?>
<?php if ($call_asset->Export == "") { ?>
<script type="text/javascript">

// Page object
var call_asset_grid = new ew_Page("call_asset_grid");
call_asset_grid.PageID = "grid"; // Page ID
var EW_PAGE_ID = call_asset_grid.PageID; // For backward compatibility

// Form object
var fcall_assetgrid = new ew_Form("fcall_assetgrid");

// Validate form
fcall_assetgrid.Validate = function(fobj) {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	fobj = fobj || this.Form;
	this.PostAutoSuggest();	
	if (fobj.a_confirm && fobj.a_confirm.value == "F")
		return true;
	var elm, aelm;
	var rowcnt = (fobj.key_count) ? Number(fobj.key_count.value) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // rowcnt == 0 => Inline-Add
	var addcnt = 0;
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = (fobj.key_count) ? String(i) : "";
		var checkrow = (fobj.a_list && fobj.a_list.value == "gridinsert") ? !this.EmptyRow(infix) : true;
		if (checkrow) {
			addcnt++;
		elm = fobj.elements["x" + infix + "_asset_serial_number"];
		if (elm && !ew_HasValue(elm))
			return ew_OnError(this, elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($call_asset->asset_serial_number->FldCaption()) ?>");
		elm = fobj.elements["x" + infix + "_asset_serial_number"];
		if (elm && !ew_CheckInteger(elm.value))
			return ew_OnError(this, elm, "<?php echo ew_JsEncode2($call_asset->asset_serial_number->FldErrMsg()) ?>");

		// Set up row object
		ew_ElementsToRow(fobj, infix);

		// Fire Form_CustomValidate event
		if (!this.Form_CustomValidate(fobj))
			return false;
		} // End Grid Add checking
	}
	return true;
}

// Check empty row
fcall_assetgrid.EmptyRow = function(infix) {
	var fobj = this.Form;
	if (ew_ValueChanged(fobj, infix, "asset_serial_number", false)) return false;
	return true;
}

// Form_CustomValidate event
fcall_assetgrid.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fcall_assetgrid.ValidateRequired = true;
<?php } else { ?>
fcall_assetgrid.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
// Form object for search

</script>
<?php } ?>
<?php
if ($call_asset->CurrentAction == "gridadd") {
	if ($call_asset->CurrentMode == "copy") {
		$bSelectLimit = EW_SELECT_LIMIT;
		if ($bSelectLimit) {
			$call_asset_grid->TotalRecs = $call_asset->SelectRecordCount();
			$call_asset_grid->Recordset = $call_asset_grid->LoadRecordset($call_asset_grid->StartRec-1, $call_asset_grid->DisplayRecs);
		} else {
			if ($call_asset_grid->Recordset = $call_asset_grid->LoadRecordset())
				$call_asset_grid->TotalRecs = $call_asset_grid->Recordset->RecordCount();
		}
		$call_asset_grid->StartRec = 1;
		$call_asset_grid->DisplayRecs = $call_asset_grid->TotalRecs;
	} else {
		$call_asset->CurrentFilter = "0=1";
		$call_asset_grid->StartRec = 1;
		$call_asset_grid->DisplayRecs = $call_asset->GridAddRowCount;
	}
	$call_asset_grid->TotalRecs = $call_asset_grid->DisplayRecs;
	$call_asset_grid->StopRec = $call_asset_grid->DisplayRecs;
} else {
	$bSelectLimit = EW_SELECT_LIMIT;
	if ($bSelectLimit) {
		$call_asset_grid->TotalRecs = $call_asset->SelectRecordCount();
	} else {
		if ($call_asset_grid->Recordset = $call_asset_grid->LoadRecordset())
			$call_asset_grid->TotalRecs = $call_asset_grid->Recordset->RecordCount();
	}
	$call_asset_grid->StartRec = 1;
	$call_asset_grid->DisplayRecs = $call_asset_grid->TotalRecs; // Display all records
	if ($bSelectLimit)
		$call_asset_grid->Recordset = $call_asset_grid->LoadRecordset($call_asset_grid->StartRec-1, $call_asset_grid->DisplayRecs);
}
?>
<p style="white-space: nowrap;"><span id="ewPageCaption" class="ewTitle ewTableTitle"><?php if ($call_asset->CurrentMode == "add" || $call_asset->CurrentMode == "copy") { ?><?php echo $Language->Phrase("Add") ?><?php } elseif ($call_asset->CurrentMode == "edit") { ?><?php echo $Language->Phrase("Edit") ?><?php } ?>&nbsp;<?php echo $Language->Phrase("TblTypeTABLE") ?><?php echo $call_asset->TableCaption() ?></span></p>
</p>
<?php $call_asset_grid->ShowPageHeader(); ?>
<?php
$call_asset_grid->ShowMessage();
?>
<br>
<table cellspacing="0" class="ewGrid"><tr><td class="ewGridContent">
<div id="fcall_assetgrid" class="ewForm">
<div id="gmp_call_asset" class="ewGridMiddlePanel">
<table id="tbl_call_assetgrid" class="ewTable ewTableSeparate">
<?php echo $call_asset->TableCustomInnerHtml ?>
<thead><!-- Table header -->
	<tr class="ewTableHeader">
<?php

// Render list options
$call_asset_grid->RenderListOptions();

// Render list options (header, left)
$call_asset_grid->ListOptions->Render("header", "left");
?>
<?php if ($call_asset->id->Visible) { // id ?>
	<?php if ($call_asset->SortUrl($call_asset->id) == "") { ?>
		<td><span id="elh_call_asset_id" class="call_asset_id"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $call_asset->id->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div><span id="elh_call_asset_id" class="call_asset_id">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $call_asset->id->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($call_asset->id->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($call_asset->id->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($call_asset->asset_serial_number->Visible) { // asset_serial_number ?>
	<?php if ($call_asset->SortUrl($call_asset->asset_serial_number) == "") { ?>
		<td><span id="elh_call_asset_asset_serial_number" class="call_asset_asset_serial_number"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $call_asset->asset_serial_number->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div><span id="elh_call_asset_asset_serial_number" class="call_asset_asset_serial_number">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $call_asset->asset_serial_number->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($call_asset->asset_serial_number->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($call_asset->asset_serial_number->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php

// Render list options (header, right)
$call_asset_grid->ListOptions->Render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
$call_asset_grid->StartRec = 1;
$call_asset_grid->StopRec = $call_asset_grid->TotalRecs; // Show all records

// Restore number of post back records
if ($objForm) {
	$objForm->Index = -1;
	if ($objForm->HasValue("key_count") && ($call_asset->CurrentAction == "gridadd" || $call_asset->CurrentAction == "gridedit" || $call_asset->CurrentAction == "F")) {
		$call_asset_grid->KeyCount = $objForm->GetValue("key_count");
		$call_asset_grid->StopRec = $call_asset_grid->KeyCount;
	}
}
$call_asset_grid->RecCnt = $call_asset_grid->StartRec - 1;
if ($call_asset_grid->Recordset && !$call_asset_grid->Recordset->EOF) {
	$call_asset_grid->Recordset->MoveFirst();
	if (!$bSelectLimit && $call_asset_grid->StartRec > 1)
		$call_asset_grid->Recordset->Move($call_asset_grid->StartRec - 1);
} elseif (!$call_asset->AllowAddDeleteRow && $call_asset_grid->StopRec == 0) {
	$call_asset_grid->StopRec = $call_asset->GridAddRowCount;
}

// Initialize aggregate
$call_asset->RowType = EW_ROWTYPE_AGGREGATEINIT;
$call_asset->ResetAttrs();
$call_asset_grid->RenderRow();
if ($call_asset->CurrentAction == "gridadd")
	$call_asset_grid->RowIndex = 0;
if ($call_asset->CurrentAction == "gridedit")
	$call_asset_grid->RowIndex = 0;
while ($call_asset_grid->RecCnt < $call_asset_grid->StopRec) {
	$call_asset_grid->RecCnt++;
	if (intval($call_asset_grid->RecCnt) >= intval($call_asset_grid->StartRec)) {
		$call_asset_grid->RowCnt++;
		if ($call_asset->CurrentAction == "gridadd" || $call_asset->CurrentAction == "gridedit" || $call_asset->CurrentAction == "F") {
			$call_asset_grid->RowIndex++;
			$objForm->Index = $call_asset_grid->RowIndex;
			if ($objForm->HasValue("k_action"))
				$call_asset_grid->RowAction = strval($objForm->GetValue("k_action"));
			elseif ($call_asset->CurrentAction == "gridadd")
				$call_asset_grid->RowAction = "insert";
			else
				$call_asset_grid->RowAction = "";
		}

		// Set up key count
		$call_asset_grid->KeyCount = $call_asset_grid->RowIndex;

		// Init row class and style
		$call_asset->ResetAttrs();
		$call_asset->CssClass = "";
		if ($call_asset->CurrentAction == "gridadd") {
			if ($call_asset->CurrentMode == "copy") {
				$call_asset_grid->LoadRowValues($call_asset_grid->Recordset); // Load row values
				$call_asset_grid->SetRecordKey($call_asset_grid->RowOldKey, $call_asset_grid->Recordset); // Set old record key
			} else {
				$call_asset_grid->LoadDefaultValues(); // Load default values
				$call_asset_grid->RowOldKey = ""; // Clear old key value
			}
		} elseif ($call_asset->CurrentAction == "gridedit") {
			$call_asset_grid->LoadRowValues($call_asset_grid->Recordset); // Load row values
		}
		$call_asset->RowType = EW_ROWTYPE_VIEW; // Render view
		if ($call_asset->CurrentAction == "gridadd") // Grid add
			$call_asset->RowType = EW_ROWTYPE_ADD; // Render add
		if ($call_asset->CurrentAction == "gridadd" && $call_asset->EventCancelled && !$objForm->HasValue("k_blankrow")) // Insert failed
			$call_asset_grid->RestoreCurrentRowFormValues($call_asset_grid->RowIndex); // Restore form values
		if ($call_asset->CurrentAction == "gridedit") { // Grid edit
			if ($call_asset->EventCancelled) {
				$call_asset_grid->RestoreCurrentRowFormValues($call_asset_grid->RowIndex); // Restore form values
			}
			if ($call_asset_grid->RowAction == "insert")
				$call_asset->RowType = EW_ROWTYPE_ADD; // Render add
			else
				$call_asset->RowType = EW_ROWTYPE_EDIT; // Render edit
		}
		if ($call_asset->CurrentAction == "gridedit" && ($call_asset->RowType == EW_ROWTYPE_EDIT || $call_asset->RowType == EW_ROWTYPE_ADD) && $call_asset->EventCancelled) // Update failed
			$call_asset_grid->RestoreCurrentRowFormValues($call_asset_grid->RowIndex); // Restore form values
		if ($call_asset->RowType == EW_ROWTYPE_EDIT) // Edit row
			$call_asset_grid->EditRowCnt++;
		if ($call_asset->CurrentAction == "F") // Confirm row
			$call_asset_grid->RestoreCurrentRowFormValues($call_asset_grid->RowIndex); // Restore form values

		// Set up row id / data-rowindex
		$call_asset->RowAttrs = array_merge($call_asset->RowAttrs, array('data-rowindex'=>$call_asset_grid->RowCnt, 'id'=>'r' . $call_asset_grid->RowCnt . '_call_asset', 'data-rowtype'=>$call_asset->RowType));

		// Render row
		$call_asset_grid->RenderRow();

		// Render list options
		$call_asset_grid->RenderListOptions();

		// Skip delete row / empty row for confirm page
		if ($call_asset_grid->RowAction <> "delete" && $call_asset_grid->RowAction <> "insertdelete" && !($call_asset_grid->RowAction == "insert" && $call_asset->CurrentAction == "F" && $call_asset_grid->EmptyRow())) {
?>
	<tr<?php echo $call_asset->RowAttributes() ?>>
<?php

// Render list options (body, left)
$call_asset_grid->ListOptions->Render("body", "left", $call_asset_grid->RowCnt);
?>
	<?php if ($call_asset->id->Visible) { // id ?>
		<td<?php echo $call_asset->id->CellAttributes() ?>><span id="el<?php echo $call_asset_grid->RowCnt ?>_call_asset_id" class="call_asset_id">
<?php if ($call_asset->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="hidden" name="o<?php echo $call_asset_grid->RowIndex ?>_id" id="o<?php echo $call_asset_grid->RowIndex ?>_id" value="<?php echo ew_HtmlEncode($call_asset->id->OldValue) ?>">
<?php } ?>
<?php if ($call_asset->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span<?php echo $call_asset->id->ViewAttributes() ?>>
<?php echo $call_asset->id->EditValue ?></span>
<input type="hidden" name="x<?php echo $call_asset_grid->RowIndex ?>_id" id="x<?php echo $call_asset_grid->RowIndex ?>_id" value="<?php echo ew_HtmlEncode($call_asset->id->CurrentValue) ?>">
<?php } ?>
<?php if ($call_asset->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span<?php echo $call_asset->id->ViewAttributes() ?>>
<?php echo $call_asset->id->ListViewValue() ?></span>
<input type="hidden" name="x<?php echo $call_asset_grid->RowIndex ?>_id" id="x<?php echo $call_asset_grid->RowIndex ?>_id" value="<?php echo ew_HtmlEncode($call_asset->id->FormValue) ?>">
<input type="hidden" name="o<?php echo $call_asset_grid->RowIndex ?>_id" id="o<?php echo $call_asset_grid->RowIndex ?>_id" value="<?php echo ew_HtmlEncode($call_asset->id->OldValue) ?>">
<?php } ?>
</span><a id="<?php echo $call_asset_grid->PageObjName . "_row_" . $call_asset_grid->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($call_asset->asset_serial_number->Visible) { // asset_serial_number ?>
		<td<?php echo $call_asset->asset_serial_number->CellAttributes() ?>><span id="el<?php echo $call_asset_grid->RowCnt ?>_call_asset_asset_serial_number" class="call_asset_asset_serial_number">
<?php if ($call_asset->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="text" name="x<?php echo $call_asset_grid->RowIndex ?>_asset_serial_number" id="x<?php echo $call_asset_grid->RowIndex ?>_asset_serial_number" size="30" value="<?php echo $call_asset->asset_serial_number->EditValue ?>"<?php echo $call_asset->asset_serial_number->EditAttributes() ?>>
<input type="hidden" name="o<?php echo $call_asset_grid->RowIndex ?>_asset_serial_number" id="o<?php echo $call_asset_grid->RowIndex ?>_asset_serial_number" value="<?php echo ew_HtmlEncode($call_asset->asset_serial_number->OldValue) ?>">
<?php } ?>
<?php if ($call_asset->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<input type="text" name="x<?php echo $call_asset_grid->RowIndex ?>_asset_serial_number" id="x<?php echo $call_asset_grid->RowIndex ?>_asset_serial_number" size="30" value="<?php echo $call_asset->asset_serial_number->EditValue ?>"<?php echo $call_asset->asset_serial_number->EditAttributes() ?>>
<?php } ?>
<?php if ($call_asset->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span<?php echo $call_asset->asset_serial_number->ViewAttributes() ?>>
<?php echo $call_asset->asset_serial_number->ListViewValue() ?></span>
<input type="hidden" name="x<?php echo $call_asset_grid->RowIndex ?>_asset_serial_number" id="x<?php echo $call_asset_grid->RowIndex ?>_asset_serial_number" value="<?php echo ew_HtmlEncode($call_asset->asset_serial_number->FormValue) ?>">
<input type="hidden" name="o<?php echo $call_asset_grid->RowIndex ?>_asset_serial_number" id="o<?php echo $call_asset_grid->RowIndex ?>_asset_serial_number" value="<?php echo ew_HtmlEncode($call_asset->asset_serial_number->OldValue) ?>">
<?php } ?>
</span><a id="<?php echo $call_asset_grid->PageObjName . "_row_" . $call_asset_grid->RowCnt ?>"></a></td>
	<?php } ?>
<?php

// Render list options (body, right)
$call_asset_grid->ListOptions->Render("body", "right", $call_asset_grid->RowCnt);
?>
	</tr>
<?php if ($call_asset->RowType == EW_ROWTYPE_ADD || $call_asset->RowType == EW_ROWTYPE_EDIT) { ?>
<script type="text/javascript">
fcall_assetgrid.UpdateOpts(<?php echo $call_asset_grid->RowIndex ?>);
</script>
<?php } ?>
<?php
	}
	} // End delete row checking
	if ($call_asset->CurrentAction <> "gridadd" || $call_asset->CurrentMode == "copy")
		if (!$call_asset_grid->Recordset->EOF) $call_asset_grid->Recordset->MoveNext();
}
?>
<?php
	if ($call_asset->CurrentMode == "add" || $call_asset->CurrentMode == "copy" || $call_asset->CurrentMode == "edit") {
		$call_asset_grid->RowIndex = '$rowindex$';
		$call_asset_grid->LoadDefaultValues();

		// Set row properties
		$call_asset->ResetAttrs();
		$call_asset->RowAttrs = array_merge($call_asset->RowAttrs, array('data-rowindex'=>$call_asset_grid->RowIndex, 'id'=>'r0_call_asset', 'data-rowtype'=>EW_ROWTYPE_ADD));
		ew_AppendClass($call_asset->RowAttrs["class"], "ewTemplate");
		$call_asset->RowType = EW_ROWTYPE_ADD;

		// Render row
		$call_asset_grid->RenderRow();

		// Render list options
		$call_asset_grid->RenderListOptions();
		$call_asset_grid->StartRowCnt = 0;
?>
	<tr<?php echo $call_asset->RowAttributes() ?>>
<?php

// Render list options (body, left)
$call_asset_grid->ListOptions->Render("body", "left", $call_asset_grid->RowIndex);
?>
	<?php if ($call_asset->id->Visible) { // id ?>
		<td><span id="el$rowindex$_call_asset_id" class="call_asset_id">
<?php if ($call_asset->CurrentAction <> "F") { ?>
<?php } else { ?>
<span<?php echo $call_asset->id->ViewAttributes() ?>>
<?php echo $call_asset->id->ViewValue ?></span>
<input type="hidden" name="x<?php echo $call_asset_grid->RowIndex ?>_id" id="x<?php echo $call_asset_grid->RowIndex ?>_id" value="<?php echo ew_HtmlEncode($call_asset->id->FormValue) ?>">
<?php } ?>
<input type="hidden" name="o<?php echo $call_asset_grid->RowIndex ?>_id" id="o<?php echo $call_asset_grid->RowIndex ?>_id" value="<?php echo ew_HtmlEncode($call_asset->id->OldValue) ?>">
</span></td>
	<?php } ?>
	<?php if ($call_asset->asset_serial_number->Visible) { // asset_serial_number ?>
		<td><span id="el$rowindex$_call_asset_asset_serial_number" class="call_asset_asset_serial_number">
<?php if ($call_asset->CurrentAction <> "F") { ?>
<input type="text" name="x<?php echo $call_asset_grid->RowIndex ?>_asset_serial_number" id="x<?php echo $call_asset_grid->RowIndex ?>_asset_serial_number" size="30" value="<?php echo $call_asset->asset_serial_number->EditValue ?>"<?php echo $call_asset->asset_serial_number->EditAttributes() ?>>
<?php } else { ?>
<span<?php echo $call_asset->asset_serial_number->ViewAttributes() ?>>
<?php echo $call_asset->asset_serial_number->ViewValue ?></span>
<input type="hidden" name="x<?php echo $call_asset_grid->RowIndex ?>_asset_serial_number" id="x<?php echo $call_asset_grid->RowIndex ?>_asset_serial_number" value="<?php echo ew_HtmlEncode($call_asset->asset_serial_number->FormValue) ?>">
<?php } ?>
<input type="hidden" name="o<?php echo $call_asset_grid->RowIndex ?>_asset_serial_number" id="o<?php echo $call_asset_grid->RowIndex ?>_asset_serial_number" value="<?php echo ew_HtmlEncode($call_asset->asset_serial_number->OldValue) ?>">
</span></td>
	<?php } ?>
<?php

// Render list options (body, right)
$call_asset_grid->ListOptions->Render("body", "right", $call_asset_grid->RowCnt);
?>
<script type="text/javascript">
fcall_assetgrid.UpdateOpts(<?php echo $call_asset_grid->RowIndex ?>);
</script>
	</tr>
<?php
}
?>
</tbody>
</table>
<?php if ($call_asset->CurrentMode == "add" || $call_asset->CurrentMode == "copy") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridinsert">
<input type="hidden" name="key_count" id="key_count" value="<?php echo $call_asset_grid->KeyCount ?>">
<?php echo $call_asset_grid->MultiSelectKey ?>
<?php } ?>
<?php if ($call_asset->CurrentMode == "edit") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridupdate">
<input type="hidden" name="key_count" id="key_count" value="<?php echo $call_asset_grid->KeyCount ?>">
<?php echo $call_asset_grid->MultiSelectKey ?>
<?php } ?>
<?php if ($call_asset->CurrentMode == "") { ?>
<input type="hidden" name="a_list" id="a_list" value="">
<?php } ?>
<input type="hidden" name="detailpage" id="detailpage" value="fcall_assetgrid">
</div>
<?php

// Close recordset
if ($call_asset_grid->Recordset)
	$call_asset_grid->Recordset->Close();
?>
<?php if (($call_asset->CurrentMode == "add" || $call_asset->CurrentMode == "copy" || $call_asset->CurrentMode == "edit") && $call_asset->CurrentAction != "F") { // add/copy/edit mode ?>
<div class="ewGridLowerPanel">
</div>
<?php } ?>
</div>
</td></tr></table>
<?php if ($call_asset->Export == "") { ?>
<script type="text/javascript">
fcall_assetgrid.Init();
</script>
<?php } ?>
<?php
$call_asset_grid->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<?php
$call_asset_grid->Page_Terminate();
$Page = &$MasterPage;
?>
