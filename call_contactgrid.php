<?php include_once "staffinfo.php" ?>
<?php

// Create page object
if (!isset($call_contact_grid)) $call_contact_grid = new ccall_contact_grid();

// Page init
$call_contact_grid->Page_Init();

// Page main
$call_contact_grid->Page_Main();
?>
<?php if ($call_contact->Export == "") { ?>
<script type="text/javascript">

// Page object
var call_contact_grid = new ew_Page("call_contact_grid");
call_contact_grid.PageID = "grid"; // Page ID
var EW_PAGE_ID = call_contact_grid.PageID; // For backward compatibility

// Form object
var fcall_contactgrid = new ew_Form("fcall_contactgrid");

// Validate form
fcall_contactgrid.Validate = function(fobj) {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	fobj = fobj || this.Form;
	this.PostAutoSuggest();	
	if (fobj.a_confirm && fobj.a_confirm.value == "F")
		return true;
	var elm, aelm;
	var rowcnt = (fobj.key_count) ? Number(fobj.key_count.value) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // rowcnt == 0 => Inline-Add
	var addcnt = 0;
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = (fobj.key_count) ? String(i) : "";
		var checkrow = (fobj.a_list && fobj.a_list.value == "gridinsert") ? !this.EmptyRow(infix) : true;
		if (checkrow) {
			addcnt++;
		elm = fobj.elements["x" + infix + "_contact_time"];
		if (elm && !ew_CheckEuroDate(elm.value))
			return ew_OnError(this, elm, "<?php echo ew_JsEncode2($call_contact->contact_time->FldErrMsg()) ?>");
		elm = fobj.elements["x" + infix + "_operator_id"];
		if (elm && !ew_HasValue(elm))
			return ew_OnError(this, elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($call_contact->operator_id->FldCaption()) ?>");
		elm = fobj.elements["x" + infix + "_operator_id"];
		if (elm && !ew_CheckInteger(elm.value))
			return ew_OnError(this, elm, "<?php echo ew_JsEncode2($call_contact->operator_id->FldErrMsg()) ?>");

		// Set up row object
		ew_ElementsToRow(fobj, infix);

		// Fire Form_CustomValidate event
		if (!this.Form_CustomValidate(fobj))
			return false;
		} // End Grid Add checking
	}
	return true;
}

// Check empty row
fcall_contactgrid.EmptyRow = function(infix) {
	var fobj = this.Form;
	if (ew_ValueChanged(fobj, infix, "contact_time", false)) return false;
	if (ew_ValueChanged(fobj, infix, "operator_id", false)) return false;
	return true;
}

// Form_CustomValidate event
fcall_contactgrid.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fcall_contactgrid.ValidateRequired = true;
<?php } else { ?>
fcall_contactgrid.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
// Form object for search

</script>
<?php } ?>
<?php
if ($call_contact->CurrentAction == "gridadd") {
	if ($call_contact->CurrentMode == "copy") {
		$bSelectLimit = EW_SELECT_LIMIT;
		if ($bSelectLimit) {
			$call_contact_grid->TotalRecs = $call_contact->SelectRecordCount();
			$call_contact_grid->Recordset = $call_contact_grid->LoadRecordset($call_contact_grid->StartRec-1, $call_contact_grid->DisplayRecs);
		} else {
			if ($call_contact_grid->Recordset = $call_contact_grid->LoadRecordset())
				$call_contact_grid->TotalRecs = $call_contact_grid->Recordset->RecordCount();
		}
		$call_contact_grid->StartRec = 1;
		$call_contact_grid->DisplayRecs = $call_contact_grid->TotalRecs;
	} else {
		$call_contact->CurrentFilter = "0=1";
		$call_contact_grid->StartRec = 1;
		$call_contact_grid->DisplayRecs = $call_contact->GridAddRowCount;
	}
	$call_contact_grid->TotalRecs = $call_contact_grid->DisplayRecs;
	$call_contact_grid->StopRec = $call_contact_grid->DisplayRecs;
} else {
	$bSelectLimit = EW_SELECT_LIMIT;
	if ($bSelectLimit) {
		$call_contact_grid->TotalRecs = $call_contact->SelectRecordCount();
	} else {
		if ($call_contact_grid->Recordset = $call_contact_grid->LoadRecordset())
			$call_contact_grid->TotalRecs = $call_contact_grid->Recordset->RecordCount();
	}
	$call_contact_grid->StartRec = 1;
	$call_contact_grid->DisplayRecs = $call_contact_grid->TotalRecs; // Display all records
	if ($bSelectLimit)
		$call_contact_grid->Recordset = $call_contact_grid->LoadRecordset($call_contact_grid->StartRec-1, $call_contact_grid->DisplayRecs);
}
?>
<p style="white-space: nowrap;"><span id="ewPageCaption" class="ewTitle ewTableTitle"><?php if ($call_contact->CurrentMode == "add" || $call_contact->CurrentMode == "copy") { ?><?php echo $Language->Phrase("Add") ?><?php } elseif ($call_contact->CurrentMode == "edit") { ?><?php echo $Language->Phrase("Edit") ?><?php } ?>&nbsp;<?php echo $Language->Phrase("TblTypeTABLE") ?><?php echo $call_contact->TableCaption() ?></span></p>
</p>
<?php $call_contact_grid->ShowPageHeader(); ?>
<?php
$call_contact_grid->ShowMessage();
?>
<br>
<table cellspacing="0" class="ewGrid"><tr><td class="ewGridContent">
<div id="fcall_contactgrid" class="ewForm">
<div id="gmp_call_contact" class="ewGridMiddlePanel">
<table id="tbl_call_contactgrid" class="ewTable ewTableSeparate">
<?php echo $call_contact->TableCustomInnerHtml ?>
<thead><!-- Table header -->
	<tr class="ewTableHeader">
<?php

// Render list options
$call_contact_grid->RenderListOptions();

// Render list options (header, left)
$call_contact_grid->ListOptions->Render("header", "left");
?>
<?php if ($call_contact->id->Visible) { // id ?>
	<?php if ($call_contact->SortUrl($call_contact->id) == "") { ?>
		<td><span id="elh_call_contact_id" class="call_contact_id"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $call_contact->id->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div><span id="elh_call_contact_id" class="call_contact_id">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $call_contact->id->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($call_contact->id->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($call_contact->id->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($call_contact->contact_time->Visible) { // contact_time ?>
	<?php if ($call_contact->SortUrl($call_contact->contact_time) == "") { ?>
		<td><span id="elh_call_contact_contact_time" class="call_contact_contact_time"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $call_contact->contact_time->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div><span id="elh_call_contact_contact_time" class="call_contact_contact_time">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $call_contact->contact_time->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($call_contact->contact_time->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($call_contact->contact_time->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($call_contact->operator_id->Visible) { // operator_id ?>
	<?php if ($call_contact->SortUrl($call_contact->operator_id) == "") { ?>
		<td><span id="elh_call_contact_operator_id" class="call_contact_operator_id"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $call_contact->operator_id->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div><span id="elh_call_contact_operator_id" class="call_contact_operator_id">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $call_contact->operator_id->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($call_contact->operator_id->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($call_contact->operator_id->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php

// Render list options (header, right)
$call_contact_grid->ListOptions->Render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
$call_contact_grid->StartRec = 1;
$call_contact_grid->StopRec = $call_contact_grid->TotalRecs; // Show all records

// Restore number of post back records
if ($objForm) {
	$objForm->Index = -1;
	if ($objForm->HasValue("key_count") && ($call_contact->CurrentAction == "gridadd" || $call_contact->CurrentAction == "gridedit" || $call_contact->CurrentAction == "F")) {
		$call_contact_grid->KeyCount = $objForm->GetValue("key_count");
		$call_contact_grid->StopRec = $call_contact_grid->KeyCount;
	}
}
$call_contact_grid->RecCnt = $call_contact_grid->StartRec - 1;
if ($call_contact_grid->Recordset && !$call_contact_grid->Recordset->EOF) {
	$call_contact_grid->Recordset->MoveFirst();
	if (!$bSelectLimit && $call_contact_grid->StartRec > 1)
		$call_contact_grid->Recordset->Move($call_contact_grid->StartRec - 1);
} elseif (!$call_contact->AllowAddDeleteRow && $call_contact_grid->StopRec == 0) {
	$call_contact_grid->StopRec = $call_contact->GridAddRowCount;
}

// Initialize aggregate
$call_contact->RowType = EW_ROWTYPE_AGGREGATEINIT;
$call_contact->ResetAttrs();
$call_contact_grid->RenderRow();
if ($call_contact->CurrentAction == "gridadd")
	$call_contact_grid->RowIndex = 0;
if ($call_contact->CurrentAction == "gridedit")
	$call_contact_grid->RowIndex = 0;
while ($call_contact_grid->RecCnt < $call_contact_grid->StopRec) {
	$call_contact_grid->RecCnt++;
	if (intval($call_contact_grid->RecCnt) >= intval($call_contact_grid->StartRec)) {
		$call_contact_grid->RowCnt++;
		if ($call_contact->CurrentAction == "gridadd" || $call_contact->CurrentAction == "gridedit" || $call_contact->CurrentAction == "F") {
			$call_contact_grid->RowIndex++;
			$objForm->Index = $call_contact_grid->RowIndex;
			if ($objForm->HasValue("k_action"))
				$call_contact_grid->RowAction = strval($objForm->GetValue("k_action"));
			elseif ($call_contact->CurrentAction == "gridadd")
				$call_contact_grid->RowAction = "insert";
			else
				$call_contact_grid->RowAction = "";
		}

		// Set up key count
		$call_contact_grid->KeyCount = $call_contact_grid->RowIndex;

		// Init row class and style
		$call_contact->ResetAttrs();
		$call_contact->CssClass = "";
		if ($call_contact->CurrentAction == "gridadd") {
			if ($call_contact->CurrentMode == "copy") {
				$call_contact_grid->LoadRowValues($call_contact_grid->Recordset); // Load row values
				$call_contact_grid->SetRecordKey($call_contact_grid->RowOldKey, $call_contact_grid->Recordset); // Set old record key
			} else {
				$call_contact_grid->LoadDefaultValues(); // Load default values
				$call_contact_grid->RowOldKey = ""; // Clear old key value
			}
		} elseif ($call_contact->CurrentAction == "gridedit") {
			$call_contact_grid->LoadRowValues($call_contact_grid->Recordset); // Load row values
		}
		$call_contact->RowType = EW_ROWTYPE_VIEW; // Render view
		if ($call_contact->CurrentAction == "gridadd") // Grid add
			$call_contact->RowType = EW_ROWTYPE_ADD; // Render add
		if ($call_contact->CurrentAction == "gridadd" && $call_contact->EventCancelled && !$objForm->HasValue("k_blankrow")) // Insert failed
			$call_contact_grid->RestoreCurrentRowFormValues($call_contact_grid->RowIndex); // Restore form values
		if ($call_contact->CurrentAction == "gridedit") { // Grid edit
			if ($call_contact->EventCancelled) {
				$call_contact_grid->RestoreCurrentRowFormValues($call_contact_grid->RowIndex); // Restore form values
			}
			if ($call_contact_grid->RowAction == "insert")
				$call_contact->RowType = EW_ROWTYPE_ADD; // Render add
			else
				$call_contact->RowType = EW_ROWTYPE_EDIT; // Render edit
		}
		if ($call_contact->CurrentAction == "gridedit" && ($call_contact->RowType == EW_ROWTYPE_EDIT || $call_contact->RowType == EW_ROWTYPE_ADD) && $call_contact->EventCancelled) // Update failed
			$call_contact_grid->RestoreCurrentRowFormValues($call_contact_grid->RowIndex); // Restore form values
		if ($call_contact->RowType == EW_ROWTYPE_EDIT) // Edit row
			$call_contact_grid->EditRowCnt++;
		if ($call_contact->CurrentAction == "F") // Confirm row
			$call_contact_grid->RestoreCurrentRowFormValues($call_contact_grid->RowIndex); // Restore form values

		// Set up row id / data-rowindex
		$call_contact->RowAttrs = array_merge($call_contact->RowAttrs, array('data-rowindex'=>$call_contact_grid->RowCnt, 'id'=>'r' . $call_contact_grid->RowCnt . '_call_contact', 'data-rowtype'=>$call_contact->RowType));

		// Render row
		$call_contact_grid->RenderRow();

		// Render list options
		$call_contact_grid->RenderListOptions();

		// Skip delete row / empty row for confirm page
		if ($call_contact_grid->RowAction <> "delete" && $call_contact_grid->RowAction <> "insertdelete" && !($call_contact_grid->RowAction == "insert" && $call_contact->CurrentAction == "F" && $call_contact_grid->EmptyRow())) {
?>
	<tr<?php echo $call_contact->RowAttributes() ?>>
<?php

// Render list options (body, left)
$call_contact_grid->ListOptions->Render("body", "left", $call_contact_grid->RowCnt);
?>
	<?php if ($call_contact->id->Visible) { // id ?>
		<td<?php echo $call_contact->id->CellAttributes() ?>><span id="el<?php echo $call_contact_grid->RowCnt ?>_call_contact_id" class="call_contact_id">
<?php if ($call_contact->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="hidden" name="o<?php echo $call_contact_grid->RowIndex ?>_id" id="o<?php echo $call_contact_grid->RowIndex ?>_id" value="<?php echo ew_HtmlEncode($call_contact->id->OldValue) ?>">
<?php } ?>
<?php if ($call_contact->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span<?php echo $call_contact->id->ViewAttributes() ?>>
<?php echo $call_contact->id->EditValue ?></span>
<input type="hidden" name="x<?php echo $call_contact_grid->RowIndex ?>_id" id="x<?php echo $call_contact_grid->RowIndex ?>_id" value="<?php echo ew_HtmlEncode($call_contact->id->CurrentValue) ?>">
<?php } ?>
<?php if ($call_contact->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span<?php echo $call_contact->id->ViewAttributes() ?>>
<?php echo $call_contact->id->ListViewValue() ?></span>
<input type="hidden" name="x<?php echo $call_contact_grid->RowIndex ?>_id" id="x<?php echo $call_contact_grid->RowIndex ?>_id" value="<?php echo ew_HtmlEncode($call_contact->id->FormValue) ?>">
<input type="hidden" name="o<?php echo $call_contact_grid->RowIndex ?>_id" id="o<?php echo $call_contact_grid->RowIndex ?>_id" value="<?php echo ew_HtmlEncode($call_contact->id->OldValue) ?>">
<?php } ?>
</span><a id="<?php echo $call_contact_grid->PageObjName . "_row_" . $call_contact_grid->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($call_contact->contact_time->Visible) { // contact_time ?>
		<td<?php echo $call_contact->contact_time->CellAttributes() ?>><span id="el<?php echo $call_contact_grid->RowCnt ?>_call_contact_contact_time" class="call_contact_contact_time">
<?php if ($call_contact->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<input type="text" name="x<?php echo $call_contact_grid->RowIndex ?>_contact_time" id="x<?php echo $call_contact_grid->RowIndex ?>_contact_time" value="<?php echo $call_contact->contact_time->EditValue ?>"<?php echo $call_contact->contact_time->EditAttributes() ?>>
<?php if (!$call_contact->contact_time->ReadOnly && !$call_contact->contact_time->Disabled && @$call_contact->contact_time->EditAttrs["readonly"] == "" && @$call_contact->contact_time->EditAttrs["disabled"] == "") { ?>
&nbsp;<img src="phpimages/calendar.png" id="fcall_contactgrid$x<?php echo $call_contact_grid->RowIndex ?>_contact_time$" name="fcall_contactgrid$x<?php echo $call_contact_grid->RowIndex ?>_contact_time$" alt="<?php echo $Language->Phrase("PickDate") ?>" title="<?php echo $Language->Phrase("PickDate") ?>" class="ewCalendar" style="border: 0;">
<script type="text/javascript">
ew_CreateCalendar("fcall_contactgrid", "x<?php echo $call_contact_grid->RowIndex ?>_contact_time", "%d/%m/%Y");
</script>
<?php } ?>
<input type="hidden" name="o<?php echo $call_contact_grid->RowIndex ?>_contact_time" id="o<?php echo $call_contact_grid->RowIndex ?>_contact_time" value="<?php echo ew_HtmlEncode($call_contact->contact_time->OldValue) ?>">
<?php } ?>
<?php if ($call_contact->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<input type="text" name="x<?php echo $call_contact_grid->RowIndex ?>_contact_time" id="x<?php echo $call_contact_grid->RowIndex ?>_contact_time" value="<?php echo $call_contact->contact_time->EditValue ?>"<?php echo $call_contact->contact_time->EditAttributes() ?>>
<?php if (!$call_contact->contact_time->ReadOnly && !$call_contact->contact_time->Disabled && @$call_contact->contact_time->EditAttrs["readonly"] == "" && @$call_contact->contact_time->EditAttrs["disabled"] == "") { ?>
&nbsp;<img src="phpimages/calendar.png" id="fcall_contactgrid$x<?php echo $call_contact_grid->RowIndex ?>_contact_time$" name="fcall_contactgrid$x<?php echo $call_contact_grid->RowIndex ?>_contact_time$" alt="<?php echo $Language->Phrase("PickDate") ?>" title="<?php echo $Language->Phrase("PickDate") ?>" class="ewCalendar" style="border: 0;">
<script type="text/javascript">
ew_CreateCalendar("fcall_contactgrid", "x<?php echo $call_contact_grid->RowIndex ?>_contact_time", "%d/%m/%Y");
</script>
<?php } ?>
<?php } ?>
<?php if ($call_contact->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span<?php echo $call_contact->contact_time->ViewAttributes() ?>>
<?php echo $call_contact->contact_time->ListViewValue() ?></span>
<input type="hidden" name="x<?php echo $call_contact_grid->RowIndex ?>_contact_time" id="x<?php echo $call_contact_grid->RowIndex ?>_contact_time" value="<?php echo ew_HtmlEncode($call_contact->contact_time->FormValue) ?>">
<input type="hidden" name="o<?php echo $call_contact_grid->RowIndex ?>_contact_time" id="o<?php echo $call_contact_grid->RowIndex ?>_contact_time" value="<?php echo ew_HtmlEncode($call_contact->contact_time->OldValue) ?>">
<?php } ?>
</span><a id="<?php echo $call_contact_grid->PageObjName . "_row_" . $call_contact_grid->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($call_contact->operator_id->Visible) { // operator_id ?>
		<td<?php echo $call_contact->operator_id->CellAttributes() ?>><span id="el<?php echo $call_contact_grid->RowCnt ?>_call_contact_operator_id" class="call_contact_operator_id">
<?php if ($call_contact->RowType == EW_ROWTYPE_ADD) { // Add record ?>
<?php if ($call_contact->operator_id->getSessionValue() <> "") { ?>
<span<?php echo $call_contact->operator_id->ViewAttributes() ?>>
<?php echo $call_contact->operator_id->ListViewValue() ?></span>
<input type="hidden" id="x<?php echo $call_contact_grid->RowIndex ?>_operator_id" name="x<?php echo $call_contact_grid->RowIndex ?>_operator_id" value="<?php echo ew_HtmlEncode($call_contact->operator_id->CurrentValue) ?>">
<?php } else { ?>
<input type="text" name="x<?php echo $call_contact_grid->RowIndex ?>_operator_id" id="x<?php echo $call_contact_grid->RowIndex ?>_operator_id" size="30" value="<?php echo $call_contact->operator_id->EditValue ?>"<?php echo $call_contact->operator_id->EditAttributes() ?>>
<?php } ?>
<input type="hidden" name="o<?php echo $call_contact_grid->RowIndex ?>_operator_id" id="o<?php echo $call_contact_grid->RowIndex ?>_operator_id" value="<?php echo ew_HtmlEncode($call_contact->operator_id->OldValue) ?>">
<?php } ?>
<?php if ($call_contact->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<?php if ($call_contact->operator_id->getSessionValue() <> "") { ?>
<span<?php echo $call_contact->operator_id->ViewAttributes() ?>>
<?php echo $call_contact->operator_id->ListViewValue() ?></span>
<input type="hidden" id="x<?php echo $call_contact_grid->RowIndex ?>_operator_id" name="x<?php echo $call_contact_grid->RowIndex ?>_operator_id" value="<?php echo ew_HtmlEncode($call_contact->operator_id->CurrentValue) ?>">
<?php } else { ?>
<input type="text" name="x<?php echo $call_contact_grid->RowIndex ?>_operator_id" id="x<?php echo $call_contact_grid->RowIndex ?>_operator_id" size="30" value="<?php echo $call_contact->operator_id->EditValue ?>"<?php echo $call_contact->operator_id->EditAttributes() ?>>
<?php } ?>
<?php } ?>
<?php if ($call_contact->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span<?php echo $call_contact->operator_id->ViewAttributes() ?>>
<?php echo $call_contact->operator_id->ListViewValue() ?></span>
<input type="hidden" name="x<?php echo $call_contact_grid->RowIndex ?>_operator_id" id="x<?php echo $call_contact_grid->RowIndex ?>_operator_id" value="<?php echo ew_HtmlEncode($call_contact->operator_id->FormValue) ?>">
<input type="hidden" name="o<?php echo $call_contact_grid->RowIndex ?>_operator_id" id="o<?php echo $call_contact_grid->RowIndex ?>_operator_id" value="<?php echo ew_HtmlEncode($call_contact->operator_id->OldValue) ?>">
<?php } ?>
</span><a id="<?php echo $call_contact_grid->PageObjName . "_row_" . $call_contact_grid->RowCnt ?>"></a></td>
	<?php } ?>
<?php

// Render list options (body, right)
$call_contact_grid->ListOptions->Render("body", "right", $call_contact_grid->RowCnt);
?>
	</tr>
<?php if ($call_contact->RowType == EW_ROWTYPE_ADD || $call_contact->RowType == EW_ROWTYPE_EDIT) { ?>
<script type="text/javascript">
fcall_contactgrid.UpdateOpts(<?php echo $call_contact_grid->RowIndex ?>);
</script>
<?php } ?>
<?php
	}
	} // End delete row checking
	if ($call_contact->CurrentAction <> "gridadd" || $call_contact->CurrentMode == "copy")
		if (!$call_contact_grid->Recordset->EOF) $call_contact_grid->Recordset->MoveNext();
}
?>
<?php
	if ($call_contact->CurrentMode == "add" || $call_contact->CurrentMode == "copy" || $call_contact->CurrentMode == "edit") {
		$call_contact_grid->RowIndex = '$rowindex$';
		$call_contact_grid->LoadDefaultValues();

		// Set row properties
		$call_contact->ResetAttrs();
		$call_contact->RowAttrs = array_merge($call_contact->RowAttrs, array('data-rowindex'=>$call_contact_grid->RowIndex, 'id'=>'r0_call_contact', 'data-rowtype'=>EW_ROWTYPE_ADD));
		ew_AppendClass($call_contact->RowAttrs["class"], "ewTemplate");
		$call_contact->RowType = EW_ROWTYPE_ADD;

		// Render row
		$call_contact_grid->RenderRow();

		// Render list options
		$call_contact_grid->RenderListOptions();
		$call_contact_grid->StartRowCnt = 0;
?>
	<tr<?php echo $call_contact->RowAttributes() ?>>
<?php

// Render list options (body, left)
$call_contact_grid->ListOptions->Render("body", "left", $call_contact_grid->RowIndex);
?>
	<?php if ($call_contact->id->Visible) { // id ?>
		<td><span id="el$rowindex$_call_contact_id" class="call_contact_id">
<?php if ($call_contact->CurrentAction <> "F") { ?>
<?php } else { ?>
<span<?php echo $call_contact->id->ViewAttributes() ?>>
<?php echo $call_contact->id->ViewValue ?></span>
<input type="hidden" name="x<?php echo $call_contact_grid->RowIndex ?>_id" id="x<?php echo $call_contact_grid->RowIndex ?>_id" value="<?php echo ew_HtmlEncode($call_contact->id->FormValue) ?>">
<?php } ?>
<input type="hidden" name="o<?php echo $call_contact_grid->RowIndex ?>_id" id="o<?php echo $call_contact_grid->RowIndex ?>_id" value="<?php echo ew_HtmlEncode($call_contact->id->OldValue) ?>">
</span></td>
	<?php } ?>
	<?php if ($call_contact->contact_time->Visible) { // contact_time ?>
		<td><span id="el$rowindex$_call_contact_contact_time" class="call_contact_contact_time">
<?php if ($call_contact->CurrentAction <> "F") { ?>
<input type="text" name="x<?php echo $call_contact_grid->RowIndex ?>_contact_time" id="x<?php echo $call_contact_grid->RowIndex ?>_contact_time" value="<?php echo $call_contact->contact_time->EditValue ?>"<?php echo $call_contact->contact_time->EditAttributes() ?>>
<?php if (!$call_contact->contact_time->ReadOnly && !$call_contact->contact_time->Disabled && @$call_contact->contact_time->EditAttrs["readonly"] == "" && @$call_contact->contact_time->EditAttrs["disabled"] == "") { ?>
&nbsp;<img src="phpimages/calendar.png" id="fcall_contactgrid$x<?php echo $call_contact_grid->RowIndex ?>_contact_time$" name="fcall_contactgrid$x<?php echo $call_contact_grid->RowIndex ?>_contact_time$" alt="<?php echo $Language->Phrase("PickDate") ?>" title="<?php echo $Language->Phrase("PickDate") ?>" class="ewCalendar" style="border: 0;">
<script type="text/javascript">
ew_CreateCalendar("fcall_contactgrid", "x<?php echo $call_contact_grid->RowIndex ?>_contact_time", "%d/%m/%Y");
</script>
<?php } ?>
<?php } else { ?>
<span<?php echo $call_contact->contact_time->ViewAttributes() ?>>
<?php echo $call_contact->contact_time->ViewValue ?></span>
<input type="hidden" name="x<?php echo $call_contact_grid->RowIndex ?>_contact_time" id="x<?php echo $call_contact_grid->RowIndex ?>_contact_time" value="<?php echo ew_HtmlEncode($call_contact->contact_time->FormValue) ?>">
<?php } ?>
<input type="hidden" name="o<?php echo $call_contact_grid->RowIndex ?>_contact_time" id="o<?php echo $call_contact_grid->RowIndex ?>_contact_time" value="<?php echo ew_HtmlEncode($call_contact->contact_time->OldValue) ?>">
</span></td>
	<?php } ?>
	<?php if ($call_contact->operator_id->Visible) { // operator_id ?>
		<td><span id="el$rowindex$_call_contact_operator_id" class="call_contact_operator_id">
<?php if ($call_contact->CurrentAction <> "F") { ?>
<?php if ($call_contact->operator_id->getSessionValue() <> "") { ?>
<span<?php echo $call_contact->operator_id->ViewAttributes() ?>>
<?php echo $call_contact->operator_id->ListViewValue() ?></span>
<input type="hidden" id="x<?php echo $call_contact_grid->RowIndex ?>_operator_id" name="x<?php echo $call_contact_grid->RowIndex ?>_operator_id" value="<?php echo ew_HtmlEncode($call_contact->operator_id->CurrentValue) ?>">
<?php } else { ?>
<input type="text" name="x<?php echo $call_contact_grid->RowIndex ?>_operator_id" id="x<?php echo $call_contact_grid->RowIndex ?>_operator_id" size="30" value="<?php echo $call_contact->operator_id->EditValue ?>"<?php echo $call_contact->operator_id->EditAttributes() ?>>
<?php } ?>
<?php } else { ?>
<span<?php echo $call_contact->operator_id->ViewAttributes() ?>>
<?php echo $call_contact->operator_id->ViewValue ?></span>
<input type="hidden" name="x<?php echo $call_contact_grid->RowIndex ?>_operator_id" id="x<?php echo $call_contact_grid->RowIndex ?>_operator_id" value="<?php echo ew_HtmlEncode($call_contact->operator_id->FormValue) ?>">
<?php } ?>
<input type="hidden" name="o<?php echo $call_contact_grid->RowIndex ?>_operator_id" id="o<?php echo $call_contact_grid->RowIndex ?>_operator_id" value="<?php echo ew_HtmlEncode($call_contact->operator_id->OldValue) ?>">
</span></td>
	<?php } ?>
<?php

// Render list options (body, right)
$call_contact_grid->ListOptions->Render("body", "right", $call_contact_grid->RowCnt);
?>
<script type="text/javascript">
fcall_contactgrid.UpdateOpts(<?php echo $call_contact_grid->RowIndex ?>);
</script>
	</tr>
<?php
}
?>
</tbody>
</table>
<?php if ($call_contact->CurrentMode == "add" || $call_contact->CurrentMode == "copy") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridinsert">
<input type="hidden" name="key_count" id="key_count" value="<?php echo $call_contact_grid->KeyCount ?>">
<?php echo $call_contact_grid->MultiSelectKey ?>
<?php } ?>
<?php if ($call_contact->CurrentMode == "edit") { ?>
<input type="hidden" name="a_list" id="a_list" value="gridupdate">
<input type="hidden" name="key_count" id="key_count" value="<?php echo $call_contact_grid->KeyCount ?>">
<?php echo $call_contact_grid->MultiSelectKey ?>
<?php } ?>
<?php if ($call_contact->CurrentMode == "") { ?>
<input type="hidden" name="a_list" id="a_list" value="">
<?php } ?>
<input type="hidden" name="detailpage" id="detailpage" value="fcall_contactgrid">
</div>
<?php

// Close recordset
if ($call_contact_grid->Recordset)
	$call_contact_grid->Recordset->Close();
?>
<?php if (($call_contact->CurrentMode == "add" || $call_contact->CurrentMode == "copy" || $call_contact->CurrentMode == "edit") && $call_contact->CurrentAction != "F") { // add/copy/edit mode ?>
<div class="ewGridLowerPanel">
</div>
<?php } ?>
</div>
</td></tr></table>
<?php if ($call_contact->Export == "") { ?>
<script type="text/javascript">
fcall_contactgrid.Init();
</script>
<?php } ?>
<?php
$call_contact_grid->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<?php
$call_contact_grid->Page_Terminate();
$Page = &$MasterPage;
?>
