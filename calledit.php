<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg9.php" ?>
<?php include_once "ewmysql9.php" ?>
<?php include_once "phpfn9.php" ?>
<?php include_once "callinfo.php" ?>
<?php include_once "staffinfo.php" ?>
<?php include_once "callAssetgridcls.php" ?>
<?php include_once "callContactgridcls.php" ?>
<?php include_once "userfn9.php" ?>
<?php

//
// Page class
//

$call_edit = NULL; // Initialize page object first

class ccall_edit extends ccall {

	// Page ID
	var $PageID = 'edit';

	// Project ID
	var $ProjectID = "{4F01D370-BA3A-4C91-9445-0AA347261657}";

	// Table name
	var $TableName = 'call';

	// Page object name
	var $PageObjName = 'call_edit';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}
	var $AuditTrailOnEdit = TRUE;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			$html .= "<p class=\"ewMessage\">" . $sMessage . "</p>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			$html .= "<table class=\"ewMessageTable\"><tr><td class=\"ewWarningIcon\"></td><td class=\"ewWarningMessage\">" . $sWarningMessage . "</td></tr></table>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			$html .= "<table class=\"ewMessageTable\"><tr><td class=\"ewSuccessIcon\"></td><td class=\"ewSuccessMessage\">" . $sSuccessMessage . "</td></tr></table>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			$html .= "<table class=\"ewMessageTable\"><tr><td class=\"ewErrorIcon\"></td><td class=\"ewErrorMessage\">" . $sErrorMessage . "</td></tr></table>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p class=\"phpmaker\">" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Fotoer exists, display
			echo "<p class=\"phpmaker\">" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language, $UserAgent;

		// User agent
		$UserAgent = ew_UserAgent();
		$GLOBALS["Page"] = &$this;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (call)
		if (!isset($GLOBALS["call"])) {
			$GLOBALS["call"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["call"];
		}

		// Table object (staff)
		if (!isset($GLOBALS['staff'])) $GLOBALS['staff'] = new cstaff();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'edit', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'call', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect();
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if (!$Security->IsLoggedIn()) {
			$Security->SaveLastUrl();
			$this->Page_Terminate("login.php");
		}

		// Create form object
		$objForm = new cFormObj();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"];
		$this->id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $conn;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();
		$this->Page_Redirecting($url);

		 // Close connection
		$conn->Close();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $DbMasterFilter;
	var $DbDetailFilter;

	// 
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError;

		// Load key from QueryString
		if (@$_GET["id"] <> "")
			$this->id->setQueryStringValue($_GET["id"]);

		// Process form if post back
		if (@$_POST["a_edit"] <> "") {
			$this->CurrentAction = $_POST["a_edit"]; // Get action code
			$this->LoadFormValues(); // Get form values
		} else {
			$this->CurrentAction = "I"; // Default action is display
		}

		// Check if valid key
		if ($this->id->CurrentValue == "")
			$this->Page_Terminate("calllist.php"); // Invalid key, return to list

		// Set up detail parameters
		$this->SetUpDetailParms();

		// Validate form if post back
		if (@$_POST["a_edit"] <> "") {
			if (!$this->ValidateForm()) {
				$this->CurrentAction = ""; // Form error, reset action
				$this->setFailureMessage($gsFormError);
				$this->EventCancelled = TRUE; // Event cancelled
				$this->RestoreFormValues();
			}
		}
		switch ($this->CurrentAction) {
			case "I": // Get a record to display
				if (!$this->LoadRow()) { // Load record based on key
					if ($this->getFailureMessage() == "") $this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
					$this->Page_Terminate("calllist.php"); // No matching record, return to list
				}
				break;
			Case "U": // Update
				$this->SendEmail = TRUE; // Send email on update success
				if ($this->EditRow()) { // Update record based on key
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("UpdateSuccess")); // Update success
					if ($this->getCurrentDetailTable() <> "") // Master/detail edit
						$sReturnUrl = $this->GetDetailUrl();
					else
						$sReturnUrl = $this->getReturnUrl();
					$this->Page_Terminate($sReturnUrl); // Return to caller
				} else {
					$this->EventCancelled = TRUE; // Event cancelled
					$this->RestoreFormValues(); // Restore form values if update failed
				}
		}

		// Render the record
		$this->RowType = EW_ROWTYPE_EDIT; // Render as Edit
		$this->ResetAttrs();
		$this->RenderRow();
	}

	// Get upload files
	function GetUploadFiles() {
		global $objForm;

		// Get upload data
		$index = $objForm->Index; // Save form index
		$objForm->Index = -1;
		$confirmPage = (strval($objForm->GetValue("a_confirm")) <> "");
		$objForm->Index = $index; // Restore form index
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->id->FldIsDetailKey)
			$this->id->setFormValue($objForm->GetValue("x_id"));
		if (!$this->callerId->FldIsDetailKey) {
			$this->callerId->setFormValue($objForm->GetValue("x_callerId"));
		}
		if (!$this->operatorId->FldIsDetailKey) {
			$this->operatorId->setFormValue($objForm->GetValue("x_operatorId"));
		}
		if (!$this->priority->FldIsDetailKey) {
			$this->priority->setFormValue($objForm->GetValue("x_priority"));
		}
		if (!$this->allocatedToId->FldIsDetailKey) {
			$this->allocatedToId->setFormValue($objForm->GetValue("x_allocatedToId"));
		}
		if (!$this->timeOfCall->FldIsDetailKey) {
			$this->timeOfCall->setFormValue($objForm->GetValue("x_timeOfCall"));
			$this->timeOfCall->CurrentValue = ew_UnFormatDateTime($this->timeOfCall->CurrentValue, 7);
		}
		if (!$this->callDescription->FldIsDetailKey) {
			$this->callDescription->setFormValue($objForm->GetValue("x_callDescription"));
		}
		if (!$this->status->FldIsDetailKey) {
			$this->status->setFormValue($objForm->GetValue("x_status"));
		}
		if (!$this->solution->FldIsDetailKey) {
			$this->solution->setFormValue($objForm->GetValue("x_solution"));
		}
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		$this->LoadRow();
		$this->id->CurrentValue = $this->id->FormValue;
		$this->callerId->CurrentValue = $this->callerId->FormValue;
		$this->operatorId->CurrentValue = $this->operatorId->FormValue;
		$this->priority->CurrentValue = $this->priority->FormValue;
		$this->allocatedToId->CurrentValue = $this->allocatedToId->FormValue;
		$this->timeOfCall->CurrentValue = $this->timeOfCall->FormValue;
		$this->timeOfCall->CurrentValue = ew_UnFormatDateTime($this->timeOfCall->CurrentValue, 7);
		$this->callDescription->CurrentValue = $this->callDescription->FormValue;
		$this->status->CurrentValue = $this->status->FormValue;
		$this->solution->CurrentValue = $this->solution->FormValue;
	}

	// Load row based on key values
	function LoadRow() {
		global $conn, $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		global $conn;
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->id->setDbValue($rs->fields('id'));
		$this->callerId->setDbValue($rs->fields('callerId'));
		$this->operatorId->setDbValue($rs->fields('operatorId'));
		$this->priority->setDbValue($rs->fields('priority'));
		$this->allocatedToId->setDbValue($rs->fields('allocatedToId'));
		$this->timeOfCall->setDbValue($rs->fields('timeOfCall'));
		$this->callDescription->setDbValue($rs->fields('callDescription'));
		$this->status->setDbValue($rs->fields('status'));
		$this->solution->setDbValue($rs->fields('solution'));
	}

	// Render row values based on field settings
	function RenderRow() {
		global $conn, $Security, $Language;
		global $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// id
		// callerId
		// operatorId
		// priority
		// allocatedToId
		// timeOfCall
		// callDescription
		// status
		// solution

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

			// id
			$this->id->ViewValue = $this->id->CurrentValue;
			$this->id->ViewCustomAttributes = "";

			// callerId
			if (strval($this->callerId->CurrentValue) <> "") {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->callerId->CurrentValue, EW_DATATYPE_NUMBER);
			$sSqlWrk = "SELECT `id`, `id` AS `DispFld`, `name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `staff`";
			$sWhereWrk = "";
			$lookuptblfilter = "`active`=true";
			if (strval($lookuptblfilter) <> "") {
				ew_AddFilter($sWhereWrk, $lookuptblfilter);
			}
			if ($sFilterWrk <> "") {
				ew_AddFilter($sWhereWrk, $sFilterWrk);
			}
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
				$rswrk = $conn->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$this->callerId->ViewValue = $rswrk->fields('DispFld');
					$this->callerId->ViewValue .= ew_ValueSeparator(1,$this->callerId) . $rswrk->fields('Disp2Fld');
					$rswrk->Close();
				} else {
					$this->callerId->ViewValue = $this->callerId->CurrentValue;
				}
			} else {
				$this->callerId->ViewValue = NULL;
			}
			$this->callerId->ViewCustomAttributes = "";

			// operatorId
			if (strval($this->operatorId->CurrentValue) <> "") {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->operatorId->CurrentValue, EW_DATATYPE_NUMBER);
			$sSqlWrk = "SELECT `id`, `id` AS `DispFld`, `name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `staff`";
			$sWhereWrk = "";
			$lookuptblfilter = "`active`=true && `roleId` =1";
			if (strval($lookuptblfilter) <> "") {
				ew_AddFilter($sWhereWrk, $lookuptblfilter);
			}
			if ($sFilterWrk <> "") {
				ew_AddFilter($sWhereWrk, $sFilterWrk);
			}
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
				$rswrk = $conn->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$this->operatorId->ViewValue = $rswrk->fields('DispFld');
					$this->operatorId->ViewValue .= ew_ValueSeparator(1,$this->operatorId) . $rswrk->fields('Disp2Fld');
					$rswrk->Close();
				} else {
					$this->operatorId->ViewValue = $this->operatorId->CurrentValue;
				}
			} else {
				$this->operatorId->ViewValue = NULL;
			}
			$this->operatorId->ViewCustomAttributes = "";

			// priority
			if (strval($this->priority->CurrentValue) <> "") {
				switch ($this->priority->CurrentValue) {
					case $this->priority->FldTagValue(1):
						$this->priority->ViewValue = $this->priority->FldTagCaption(1) <> "" ? $this->priority->FldTagCaption(1) : $this->priority->CurrentValue;
						break;
					case $this->priority->FldTagValue(2):
						$this->priority->ViewValue = $this->priority->FldTagCaption(2) <> "" ? $this->priority->FldTagCaption(2) : $this->priority->CurrentValue;
						break;
					case $this->priority->FldTagValue(3):
						$this->priority->ViewValue = $this->priority->FldTagCaption(3) <> "" ? $this->priority->FldTagCaption(3) : $this->priority->CurrentValue;
						break;
					case $this->priority->FldTagValue(4):
						$this->priority->ViewValue = $this->priority->FldTagCaption(4) <> "" ? $this->priority->FldTagCaption(4) : $this->priority->CurrentValue;
						break;
					case $this->priority->FldTagValue(5):
						$this->priority->ViewValue = $this->priority->FldTagCaption(5) <> "" ? $this->priority->FldTagCaption(5) : $this->priority->CurrentValue;
						break;
					default:
						$this->priority->ViewValue = $this->priority->CurrentValue;
				}
			} else {
				$this->priority->ViewValue = NULL;
			}
			$this->priority->ViewCustomAttributes = "";

			// allocatedToId
			if (strval($this->allocatedToId->CurrentValue) <> "") {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->allocatedToId->CurrentValue, EW_DATATYPE_NUMBER);
			$sSqlWrk = "SELECT `id`, `id` AS `DispFld`, `name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `staff`";
			$sWhereWrk = "";
			$lookuptblfilter = "`active`=true && `roleId` =1";
			if (strval($lookuptblfilter) <> "") {
				ew_AddFilter($sWhereWrk, $lookuptblfilter);
			}
			if ($sFilterWrk <> "") {
				ew_AddFilter($sWhereWrk, $sFilterWrk);
			}
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
				$rswrk = $conn->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$this->allocatedToId->ViewValue = $rswrk->fields('DispFld');
					$this->allocatedToId->ViewValue .= ew_ValueSeparator(1,$this->allocatedToId) . $rswrk->fields('Disp2Fld');
					$rswrk->Close();
				} else {
					$this->allocatedToId->ViewValue = $this->allocatedToId->CurrentValue;
				}
			} else {
				$this->allocatedToId->ViewValue = NULL;
			}
			$this->allocatedToId->ViewCustomAttributes = "";

			// timeOfCall
			$this->timeOfCall->ViewValue = $this->timeOfCall->CurrentValue;
			$this->timeOfCall->ViewValue = ew_FormatDateTime($this->timeOfCall->ViewValue, 7);
			$this->timeOfCall->ViewCustomAttributes = "";

			// callDescription
			$this->callDescription->ViewValue = $this->callDescription->CurrentValue;
			$this->callDescription->ViewCustomAttributes = "";

			// status
			if (strval($this->status->CurrentValue) <> "") {
				switch ($this->status->CurrentValue) {
					case $this->status->FldTagValue(1):
						$this->status->ViewValue = $this->status->FldTagCaption(1) <> "" ? $this->status->FldTagCaption(1) : $this->status->CurrentValue;
						break;
					case $this->status->FldTagValue(2):
						$this->status->ViewValue = $this->status->FldTagCaption(2) <> "" ? $this->status->FldTagCaption(2) : $this->status->CurrentValue;
						break;
					case $this->status->FldTagValue(3):
						$this->status->ViewValue = $this->status->FldTagCaption(3) <> "" ? $this->status->FldTagCaption(3) : $this->status->CurrentValue;
						break;
					default:
						$this->status->ViewValue = $this->status->CurrentValue;
				}
			} else {
				$this->status->ViewValue = NULL;
			}
			$this->status->ViewValue = ew_FormatDateTime($this->status->ViewValue, 7);
			$this->status->ViewCustomAttributes = "";

			// solution
			$this->solution->ViewValue = $this->solution->CurrentValue;
			$this->solution->ViewCustomAttributes = "";

			// id
			$this->id->LinkCustomAttributes = "";
			$this->id->HrefValue = "";
			$this->id->TooltipValue = "";

			// callerId
			$this->callerId->LinkCustomAttributes = "";
			$this->callerId->HrefValue = "";
			$this->callerId->TooltipValue = "";

			// operatorId
			$this->operatorId->LinkCustomAttributes = "";
			$this->operatorId->HrefValue = "";
			$this->operatorId->TooltipValue = "";

			// priority
			$this->priority->LinkCustomAttributes = "";
			$this->priority->HrefValue = "";
			$this->priority->TooltipValue = "";

			// allocatedToId
			$this->allocatedToId->LinkCustomAttributes = "";
			$this->allocatedToId->HrefValue = "";
			$this->allocatedToId->TooltipValue = "";

			// timeOfCall
			$this->timeOfCall->LinkCustomAttributes = "";
			$this->timeOfCall->HrefValue = "";
			$this->timeOfCall->TooltipValue = "";

			// callDescription
			$this->callDescription->LinkCustomAttributes = "";
			$this->callDescription->HrefValue = "";
			$this->callDescription->TooltipValue = "";

			// status
			$this->status->LinkCustomAttributes = "";
			$this->status->HrefValue = "";
			$this->status->TooltipValue = "";

			// solution
			$this->solution->LinkCustomAttributes = "";
			$this->solution->HrefValue = "";
			$this->solution->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_EDIT) { // Edit row

			// id
			$this->id->EditCustomAttributes = "";
			$this->id->EditValue = $this->id->CurrentValue;
			$this->id->ViewCustomAttributes = "";

			// callerId
			$this->callerId->EditCustomAttributes = "";
			if (strval($this->callerId->CurrentValue) <> "") {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->callerId->CurrentValue, EW_DATATYPE_NUMBER);
			$sSqlWrk = "SELECT `id`, `id` AS `DispFld`, `name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `staff`";
			$sWhereWrk = "";
			$lookuptblfilter = "`active`=true";
			if (strval($lookuptblfilter) <> "") {
				ew_AddFilter($sWhereWrk, $lookuptblfilter);
			}
			if ($sFilterWrk <> "") {
				ew_AddFilter($sWhereWrk, $sFilterWrk);
			}
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
				$rswrk = $conn->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$this->callerId->EditValue = $rswrk->fields('DispFld');
					$this->callerId->EditValue .= ew_ValueSeparator(1,$this->callerId) . $rswrk->fields('Disp2Fld');
					$rswrk->Close();
				} else {
					$this->callerId->EditValue = $this->callerId->CurrentValue;
				}
			} else {
				$this->callerId->EditValue = NULL;
			}
			$this->callerId->ViewCustomAttributes = "";

			// operatorId
			$this->operatorId->EditCustomAttributes = "";
			if (strval($this->operatorId->CurrentValue) <> "") {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->operatorId->CurrentValue, EW_DATATYPE_NUMBER);
			$sSqlWrk = "SELECT `id`, `id` AS `DispFld`, `name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `staff`";
			$sWhereWrk = "";
			$lookuptblfilter = "`active`=true && `roleId` =1";
			if (strval($lookuptblfilter) <> "") {
				ew_AddFilter($sWhereWrk, $lookuptblfilter);
			}
			if ($sFilterWrk <> "") {
				ew_AddFilter($sWhereWrk, $sFilterWrk);
			}
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
				$rswrk = $conn->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$this->operatorId->EditValue = $rswrk->fields('DispFld');
					$this->operatorId->EditValue .= ew_ValueSeparator(1,$this->operatorId) . $rswrk->fields('Disp2Fld');
					$rswrk->Close();
				} else {
					$this->operatorId->EditValue = $this->operatorId->CurrentValue;
				}
			} else {
				$this->operatorId->EditValue = NULL;
			}
			$this->operatorId->ViewCustomAttributes = "";

			// priority
			$this->priority->EditCustomAttributes = "";
			$arwrk = array();
			$arwrk[] = array($this->priority->FldTagValue(1), $this->priority->FldTagCaption(1) <> "" ? $this->priority->FldTagCaption(1) : $this->priority->FldTagValue(1));
			$arwrk[] = array($this->priority->FldTagValue(2), $this->priority->FldTagCaption(2) <> "" ? $this->priority->FldTagCaption(2) : $this->priority->FldTagValue(2));
			$arwrk[] = array($this->priority->FldTagValue(3), $this->priority->FldTagCaption(3) <> "" ? $this->priority->FldTagCaption(3) : $this->priority->FldTagValue(3));
			$arwrk[] = array($this->priority->FldTagValue(4), $this->priority->FldTagCaption(4) <> "" ? $this->priority->FldTagCaption(4) : $this->priority->FldTagValue(4));
			$arwrk[] = array($this->priority->FldTagValue(5), $this->priority->FldTagCaption(5) <> "" ? $this->priority->FldTagCaption(5) : $this->priority->FldTagValue(5));
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect")));
			$this->priority->EditValue = $arwrk;

			// allocatedToId
			$this->allocatedToId->EditCustomAttributes = "";
			$sFilterWrk = "";
			$sSqlWrk = "SELECT `id`, `id` AS `DispFld`, `name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `staff`";
			$sWhereWrk = "";
			$lookuptblfilter = "`active`=true && `roleId` =1";
			if (strval($lookuptblfilter) <> "") {
				ew_AddFilter($sWhereWrk, $lookuptblfilter);
			}
			if ($sFilterWrk <> "") {
				ew_AddFilter($sWhereWrk, $sFilterWrk);
			}
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = $conn->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->allocatedToId->EditValue = $arwrk;

			// timeOfCall
			// callDescription

			$this->callDescription->EditCustomAttributes = "";
			$this->callDescription->EditValue = ew_HtmlEncode($this->callDescription->CurrentValue);

			// status
			$this->status->EditCustomAttributes = "";
			$arwrk = array();
			$arwrk[] = array($this->status->FldTagValue(1), $this->status->FldTagCaption(1) <> "" ? $this->status->FldTagCaption(1) : $this->status->FldTagValue(1));
			$arwrk[] = array($this->status->FldTagValue(2), $this->status->FldTagCaption(2) <> "" ? $this->status->FldTagCaption(2) : $this->status->FldTagValue(2));
			$arwrk[] = array($this->status->FldTagValue(3), $this->status->FldTagCaption(3) <> "" ? $this->status->FldTagCaption(3) : $this->status->FldTagValue(3));
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect")));
			$this->status->EditValue = $arwrk;

			// solution
			$this->solution->EditCustomAttributes = "";
			$this->solution->EditValue = ew_HtmlEncode($this->solution->CurrentValue);

			// Edit refer script
			// id

			$this->id->HrefValue = "";

			// callerId
			$this->callerId->HrefValue = "";

			// operatorId
			$this->operatorId->HrefValue = "";

			// priority
			$this->priority->HrefValue = "";

			// allocatedToId
			$this->allocatedToId->HrefValue = "";

			// timeOfCall
			$this->timeOfCall->HrefValue = "";

			// callDescription
			$this->callDescription->HrefValue = "";

			// status
			$this->status->HrefValue = "";

			// solution
			$this->solution->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!is_null($this->priority->FormValue) && $this->priority->FormValue == "") {
			ew_AddMessage($gsFormError, $Language->Phrase("EnterRequiredField") . " - " . $this->priority->FldCaption());
		}
		if (!is_null($this->allocatedToId->FormValue) && $this->allocatedToId->FormValue == "") {
			ew_AddMessage($gsFormError, $Language->Phrase("EnterRequiredField") . " - " . $this->allocatedToId->FldCaption());
		}
		if (!is_null($this->callDescription->FormValue) && $this->callDescription->FormValue == "") {
			ew_AddMessage($gsFormError, $Language->Phrase("EnterRequiredField") . " - " . $this->callDescription->FldCaption());
		}

		// Validate detail grid
		if ($this->getCurrentDetailTable() == "callAsset" && $GLOBALS["callAsset"]->DetailEdit) {
			if (!isset($GLOBALS["callAsset_grid"])) $GLOBALS["callAsset_grid"] = new ccallAsset_grid(); // get detail page object
			$GLOBALS["callAsset_grid"]->ValidateGridForm();
		}
		if ($this->getCurrentDetailTable() == "callContact" && $GLOBALS["callContact"]->DetailEdit) {
			if (!isset($GLOBALS["callContact_grid"])) $GLOBALS["callContact_grid"] = new ccallContact_grid(); // get detail page object
			$GLOBALS["callContact_grid"]->ValidateGridForm();
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Update record based on key values
	function EditRow() {
		global $conn, $Security, $Language;
		$sFilter = $this->KeyFilter();
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = 'ew_ErrorFn';
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE)
			return FALSE;
		if ($rs->EOF) {
			$EditRow = FALSE; // Update Failed
		} else {

			// Begin transaction
			if ($this->getCurrentDetailTable() <> "")
				$conn->BeginTrans();

			// Save old values
			$rsold = &$rs->fields;
			$rsnew = array();

			// priority
			$this->priority->SetDbValueDef($rsnew, $this->priority->CurrentValue, NULL, $this->priority->ReadOnly);

			// allocatedToId
			$this->allocatedToId->SetDbValueDef($rsnew, $this->allocatedToId->CurrentValue, 0, $this->allocatedToId->ReadOnly);

			// callDescription
			$this->callDescription->SetDbValueDef($rsnew, $this->callDescription->CurrentValue, NULL, $this->callDescription->ReadOnly);

			// status
			$this->status->SetDbValueDef($rsnew, $this->status->CurrentValue, NULL, $this->status->ReadOnly);

			// solution
			$this->solution->SetDbValueDef($rsnew, $this->solution->CurrentValue, NULL, $this->solution->ReadOnly);

			// Call Row Updating event
			$bUpdateRow = $this->Row_Updating($rsold, $rsnew);
			if ($bUpdateRow) {
				$conn->raiseErrorFn = 'ew_ErrorFn';
				if (count($rsnew) > 0)
					$EditRow = $this->Update($rsnew);
				else
					$EditRow = TRUE; // No field to update
				$conn->raiseErrorFn = '';

				// Update detail records
				if ($EditRow) {
					if ($this->getCurrentDetailTable() == "callAsset" && $GLOBALS["callAsset"]->DetailEdit) {
						if (!isset($GLOBALS["callAsset_grid"])) $GLOBALS["callAsset_grid"] = new ccallAsset_grid(); // get detail page object
						$EditRow = $GLOBALS["callAsset_grid"]->GridUpdate();
					}
					if ($this->getCurrentDetailTable() == "callContact" && $GLOBALS["callContact"]->DetailEdit) {
						if (!isset($GLOBALS["callContact_grid"])) $GLOBALS["callContact_grid"] = new ccallContact_grid(); // get detail page object
						$EditRow = $GLOBALS["callContact_grid"]->GridUpdate();
					}
				}

				// Commit/Rollback transaction
				if ($this->getCurrentDetailTable() <> "") {
					if ($EditRow) {
						$conn->CommitTrans(); // Commit transaction
					} else {
						$conn->RollbackTrans(); // Rollback transaction
					}
				}
			} else {
				if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

					// Use the message, do nothing
				} elseif ($this->CancelMessage <> "") {
					$this->setFailureMessage($this->CancelMessage);
					$this->CancelMessage = "";
				} else {
					$this->setFailureMessage($Language->Phrase("UpdateCancelled"));
				}
				$EditRow = FALSE;
			}
		}

		// Call Row_Updated event
		if ($EditRow)
			$this->Row_Updated($rsold, $rsnew);
		if ($EditRow) {
			$this->WriteAuditTrailOnEdit($rsold, $rsnew);
		}
		$rs->Close();
		return $EditRow;
	}

	// Set up detail parms based on QueryString
	function SetUpDetailParms() {

		// Get the keys for master table
		if (isset($_GET[EW_TABLE_SHOW_DETAIL])) {
			$sDetailTblVar = $_GET[EW_TABLE_SHOW_DETAIL];
			$this->setCurrentDetailTable($sDetailTblVar);
		} else {
			$sDetailTblVar = $this->getCurrentDetailTable();
		}
		if ($sDetailTblVar <> "") {
			if ($sDetailTblVar == "callAsset") {
				if (!isset($GLOBALS["callAsset_grid"]))
					$GLOBALS["callAsset_grid"] = new ccallAsset_grid;
				if ($GLOBALS["callAsset_grid"]->DetailEdit) {
					$GLOBALS["callAsset_grid"]->CurrentMode = "edit";
					$GLOBALS["callAsset_grid"]->CurrentAction = "gridedit";

					// Save current master table to detail table
					$GLOBALS["callAsset_grid"]->setCurrentMasterTable($this->TableVar);
					$GLOBALS["callAsset_grid"]->setStartRecordNumber(1);
					$GLOBALS["callAsset_grid"]->callId->FldIsDetailKey = TRUE;
					$GLOBALS["callAsset_grid"]->callId->CurrentValue = $this->id->CurrentValue;
					$GLOBALS["callAsset_grid"]->callId->setSessionValue($GLOBALS["callAsset_grid"]->callId->CurrentValue);
				}
			}
			if ($sDetailTblVar == "callContact") {
				if (!isset($GLOBALS["callContact_grid"]))
					$GLOBALS["callContact_grid"] = new ccallContact_grid;
				if ($GLOBALS["callContact_grid"]->DetailEdit) {
					$GLOBALS["callContact_grid"]->CurrentMode = "edit";
					$GLOBALS["callContact_grid"]->CurrentAction = "gridedit";

					// Save current master table to detail table
					$GLOBALS["callContact_grid"]->setCurrentMasterTable($this->TableVar);
					$GLOBALS["callContact_grid"]->setStartRecordNumber(1);
					$GLOBALS["callContact_grid"]->callId->FldIsDetailKey = TRUE;
					$GLOBALS["callContact_grid"]->callId->CurrentValue = $this->id->CurrentValue;
					$GLOBALS["callContact_grid"]->callId->setSessionValue($GLOBALS["callContact_grid"]->callId->CurrentValue);
				}
			}
		}
	}

	// Write Audit Trail start/end for grid update
	function WriteAuditTrailDummy($typ) {
		$table = 'call';
	  $usr = CurrentUserName();
		ew_WriteAuditTrail("log", ew_StdCurrentDateTime(), ew_ScriptName(), $usr, $typ, $table, "", "", "", "");
	}

	// Write Audit Trail (edit page)
	function WriteAuditTrailOnEdit(&$rsold, &$rsnew) {
		if (!$this->AuditTrailOnEdit) return;
		$table = 'call';

		// Get key value
		$key = "";
		if ($key <> "") $key .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
		$key .= $rsold['id'];

		// Write Audit Trail
		$dt = ew_StdCurrentDateTime();
		$id = ew_ScriptName();
	  $usr = CurrentUserName();
		foreach (array_keys($rsnew) as $fldname) {
			if ($this->fields[$fldname]->FldDataType <> EW_DATATYPE_BLOB) { // Ignore BLOB fields
				if ($this->fields[$fldname]->FldDataType == EW_DATATYPE_DATE) { // DateTime field
					$modified = (ew_FormatDateTime($rsold[$fldname], 0) <> ew_FormatDateTime($rsnew[$fldname], 0));
				} else {
					$modified = !ew_CompareValue($rsold[$fldname], $rsnew[$fldname]);
				}
				if ($modified) {
					if ($this->fields[$fldname]->FldDataType == EW_DATATYPE_MEMO) { // Memo field
						if (EW_AUDIT_TRAIL_TO_DATABASE) {
							$oldvalue = $rsold[$fldname];
							$newvalue = $rsnew[$fldname];
						} else {
							$oldvalue = "[MEMO]";
							$newvalue = "[MEMO]";
						}
					} elseif ($this->fields[$fldname]->FldDataType == EW_DATATYPE_XML) { // XML field
						$oldvalue = "[XML]";
						$newvalue = "[XML]";
					} else {
						$oldvalue = $rsold[$fldname];
						$newvalue = $rsnew[$fldname];
					}
					ew_WriteAuditTrail("log", $dt, $id, $usr, "U", $table, $fldname, $key, $oldvalue, $newvalue);
				}
			}
		}
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($call_edit)) $call_edit = new ccall_edit();

// Page init
$call_edit->Page_Init();

// Page main
$call_edit->Page_Main();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Page object
var call_edit = new ew_Page("call_edit");
call_edit.PageID = "edit"; // Page ID
var EW_PAGE_ID = call_edit.PageID; // For backward compatibility

// Form object
var fcalledit = new ew_Form("fcalledit");

// Validate form
fcalledit.Validate = function(fobj) {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	fobj = fobj || this.Form;
	this.PostAutoSuggest();	
	if (fobj.a_confirm && fobj.a_confirm.value == "F")
		return true;
	var elm, aelm;
	var rowcnt = 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // rowcnt == 0 => Inline-Add
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = "";
		elm = fobj.elements["x" + infix + "_priority"];
		if (elm && !ew_HasValue(elm))
			return ew_OnError(this, elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($call->priority->FldCaption()) ?>");
		elm = fobj.elements["x" + infix + "_allocatedToId"];
		if (elm && !ew_HasValue(elm))
			return ew_OnError(this, elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($call->allocatedToId->FldCaption()) ?>");
		elm = fobj.elements["x" + infix + "_callDescription"];
		if (elm && !ew_HasValue(elm))
			return ew_OnError(this, elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($call->callDescription->FldCaption()) ?>");

		// Set up row object
		ew_ElementsToRow(fobj, infix);

		// Fire Form_CustomValidate event
		if (!this.Form_CustomValidate(fobj))
			return false;
	}

	// Process detail page
	if (fobj.detailpage && fobj.detailpage.value && ewForms[fobj.detailpage.value])
		return ewForms[fobj.detailpage.value].Validate(fobj);
	return true;
}

// Form_CustomValidate event
fcalledit.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fcalledit.ValidateRequired = true;
<?php } else { ?>
fcalledit.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fcalledit.Lists["x_callerId"] = {"LinkField":"x_id","Ajax":null,"AutoFill":false,"DisplayFields":["x_id","x_name","",""],"ParentFields":[],"FilterFields":[],"Options":[]};
fcalledit.Lists["x_operatorId"] = {"LinkField":"x_id","Ajax":null,"AutoFill":false,"DisplayFields":["x_id","x_name","",""],"ParentFields":[],"FilterFields":[],"Options":[]};
fcalledit.Lists["x_allocatedToId"] = {"LinkField":"x_id","Ajax":null,"AutoFill":false,"DisplayFields":["x_id","x_name","",""],"ParentFields":[],"FilterFields":[],"Options":[]};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<p><span id="ewPageCaption" class="ewTitle ewTableTitle"><?php echo $Language->Phrase("Edit") ?>&nbsp;<?php echo $Language->Phrase("TblTypeTABLE") ?><?php echo $call->TableCaption() ?></span></p>
<p class="phpmaker"><a href="<?php echo $call->getReturnUrl() ?>" id="a_GoBack" class="ewLink"><?php echo $Language->Phrase("GoBack") ?></a></p>
<?php $call_edit->ShowPageHeader(); ?>
<?php
$call_edit->ShowMessage();
?>
<form name="fcalledit" id="fcalledit" class="ewForm" action="<?php echo ew_CurrentPage() ?>" method="post" onsubmit="return ewForms[this.id].Submit();">
<br>
<input type="hidden" name="t" value="call">
<input type="hidden" name="a_edit" id="a_edit" value="U">
<table cellspacing="0" class="ewGrid"><tr><td class="ewGridContent">
<div class="ewGridMiddlePanel">
<table id="tbl_calledit" class="ewTable">
<?php if ($call->id->Visible) { // id ?>
	<tr id="r_id"<?php echo $call->RowAttributes() ?>>
		<td class="ewTableHeader"><span id="elh_call_id"><table class="ewTableHeaderBtn"><tr><td><?php echo $call->id->FldCaption() ?></td></tr></table></span></td>
		<td<?php echo $call->id->CellAttributes() ?>><span id="el_call_id">
<span<?php echo $call->id->ViewAttributes() ?>>
<?php echo $call->id->EditValue ?></span>
<input type="hidden" name="x_id" id="x_id" value="<?php echo ew_HtmlEncode($call->id->CurrentValue) ?>">
</span><?php echo $call->id->CustomMsg ?></td>
	</tr>
<?php } ?>
<?php if ($call->callerId->Visible) { // callerId ?>
	<tr id="r_callerId"<?php echo $call->RowAttributes() ?>>
		<td class="ewTableHeader"><span id="elh_call_callerId"><table class="ewTableHeaderBtn"><tr><td><?php echo $call->callerId->FldCaption() ?></td></tr></table></span></td>
		<td<?php echo $call->callerId->CellAttributes() ?>><span id="el_call_callerId">
<span<?php echo $call->callerId->ViewAttributes() ?>>
<?php echo $call->callerId->EditValue ?></span>
<input type="hidden" name="x_callerId" id="x_callerId" value="<?php echo ew_HtmlEncode($call->callerId->CurrentValue) ?>">
</span><?php echo $call->callerId->CustomMsg ?></td>
	</tr>
<?php } ?>
<?php if ($call->operatorId->Visible) { // operatorId ?>
	<tr id="r_operatorId"<?php echo $call->RowAttributes() ?>>
		<td class="ewTableHeader"><span id="elh_call_operatorId"><table class="ewTableHeaderBtn"><tr><td><?php echo $call->operatorId->FldCaption() ?></td></tr></table></span></td>
		<td<?php echo $call->operatorId->CellAttributes() ?>><span id="el_call_operatorId">
<span<?php echo $call->operatorId->ViewAttributes() ?>>
<?php echo $call->operatorId->EditValue ?></span>
<input type="hidden" name="x_operatorId" id="x_operatorId" value="<?php echo ew_HtmlEncode($call->operatorId->CurrentValue) ?>">
</span><?php echo $call->operatorId->CustomMsg ?></td>
	</tr>
<?php } ?>
<?php if ($call->priority->Visible) { // priority ?>
	<tr id="r_priority"<?php echo $call->RowAttributes() ?>>
		<td class="ewTableHeader"><span id="elh_call_priority"><table class="ewTableHeaderBtn"><tr><td><?php echo $call->priority->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></td></tr></table></span></td>
		<td<?php echo $call->priority->CellAttributes() ?>><span id="el_call_priority">
<select id="x_priority" name="x_priority"<?php echo $call->priority->EditAttributes() ?>>
<?php
if (is_array($call->priority->EditValue)) {
	$arwrk = $call->priority->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($call->priority->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " selected=\"selected\"" : "";
		if ($selwrk <> "") $emptywrk = FALSE;
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $arwrk[$rowcntwrk][1] ?>
</option>
<?php
	}
}
?>
</select>
</span><?php echo $call->priority->CustomMsg ?></td>
	</tr>
<?php } ?>
<?php if ($call->allocatedToId->Visible) { // allocatedToId ?>
	<tr id="r_allocatedToId"<?php echo $call->RowAttributes() ?>>
		<td class="ewTableHeader"><span id="elh_call_allocatedToId"><table class="ewTableHeaderBtn"><tr><td><?php echo $call->allocatedToId->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></td></tr></table></span></td>
		<td<?php echo $call->allocatedToId->CellAttributes() ?>><span id="el_call_allocatedToId">
<select id="x_allocatedToId" name="x_allocatedToId"<?php echo $call->allocatedToId->EditAttributes() ?>>
<?php
if (is_array($call->allocatedToId->EditValue)) {
	$arwrk = $call->allocatedToId->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($call->allocatedToId->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " selected=\"selected\"" : "";
		if ($selwrk <> "") $emptywrk = FALSE;
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $arwrk[$rowcntwrk][1] ?>
<?php if ($arwrk[$rowcntwrk][2] <> "") { ?>
<?php echo ew_ValueSeparator(1,$call->allocatedToId) ?><?php echo $arwrk[$rowcntwrk][2] ?>
<?php } ?>
</option>
<?php
	}
}
?>
</select>
<script type="text/javascript">
fcalledit.Lists["x_allocatedToId"].Options = <?php echo (is_array($call->allocatedToId->EditValue)) ? ew_ArrayToJson($call->allocatedToId->EditValue, 1) : "[]" ?>;
</script>
</span><?php echo $call->allocatedToId->CustomMsg ?></td>
	</tr>
<?php } ?>
<?php if ($call->callDescription->Visible) { // callDescription ?>
	<tr id="r_callDescription"<?php echo $call->RowAttributes() ?>>
		<td class="ewTableHeader"><span id="elh_call_callDescription"><table class="ewTableHeaderBtn"><tr><td><?php echo $call->callDescription->FldCaption() ?><?php echo $Language->Phrase("FieldRequiredIndicator") ?></td></tr></table></span></td>
		<td<?php echo $call->callDescription->CellAttributes() ?>><span id="el_call_callDescription">
<textarea name="x_callDescription" id="x_callDescription" cols="35" rows="4"<?php echo $call->callDescription->EditAttributes() ?>><?php echo $call->callDescription->EditValue ?></textarea>
</span><?php echo $call->callDescription->CustomMsg ?></td>
	</tr>
<?php } ?>
<?php if ($call->status->Visible) { // status ?>
	<tr id="r_status"<?php echo $call->RowAttributes() ?>>
		<td class="ewTableHeader"><span id="elh_call_status"><table class="ewTableHeaderBtn"><tr><td><?php echo $call->status->FldCaption() ?></td></tr></table></span></td>
		<td<?php echo $call->status->CellAttributes() ?>><span id="el_call_status">
<select id="x_status" name="x_status"<?php echo $call->status->EditAttributes() ?>>
<?php
if (is_array($call->status->EditValue)) {
	$arwrk = $call->status->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($call->status->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " selected=\"selected\"" : "";
		if ($selwrk <> "") $emptywrk = FALSE;
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $arwrk[$rowcntwrk][1] ?>
</option>
<?php
	}
}
?>
</select>
</span><?php echo $call->status->CustomMsg ?></td>
	</tr>
<?php } ?>
<?php if ($call->solution->Visible) { // solution ?>
	<tr id="r_solution"<?php echo $call->RowAttributes() ?>>
		<td class="ewTableHeader"><span id="elh_call_solution"><table class="ewTableHeaderBtn"><tr><td><?php echo $call->solution->FldCaption() ?></td></tr></table></span></td>
		<td<?php echo $call->solution->CellAttributes() ?>><span id="el_call_solution">
<textarea name="x_solution" id="x_solution" cols="35" rows="4"<?php echo $call->solution->EditAttributes() ?>><?php echo $call->solution->EditValue ?></textarea>
</span><?php echo $call->solution->CustomMsg ?></td>
	</tr>
<?php } ?>
</table>
</div>
</td></tr></table>
<br>
<?php if ($call->getCurrentDetailTable() == "callAsset" && $callAsset->DetailEdit) { ?>
<br>
<?php include_once "callAssetgrid.php" ?>
<br>
<?php } ?>
<?php if ($call->getCurrentDetailTable() == "callContact" && $callContact->DetailEdit) { ?>
<br>
<?php include_once "callContactgrid.php" ?>
<br>
<?php } ?>
<input type="submit" name="btnAction" id="btnAction" value="<?php echo ew_BtnCaption($Language->Phrase("EditBtn")) ?>">
</form>
<script type="text/javascript">
fcalledit.Init();
</script>
<?php
$call_edit->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$call_edit->Page_Terminate();
?>
