<?php

// Global variable for table object
$call = NULL;

//
// Table class for call
//
class ccall extends cTable {
	var $id;
	var $callerId;
	var $operatorId;
	var $priority;
	var $allocatedToId;
	var $timeOfCall;
	var $callDescription;
	var $status;
	var $solution;

	//
	// Table class constructor
	//
	function __construct() {
		global $Language;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();
		$this->TableVar = 'call';
		$this->TableName = 'call';
		$this->TableType = 'TABLE';
		$this->ExportAll = TRUE;
		$this->ExportPageBreakCount = 0; // Page break per every n record (PDF only)
		$this->ExportPageOrientation = "portrait"; // Page orientation (PDF only)
		$this->ExportPageSize = "a4"; // Page size (PDF only)
		$this->DetailAdd = TRUE; // Allow detail add
		$this->DetailEdit = TRUE; // Allow detail edit
		$this->GridAddRowCount = 5;
		$this->AllowAddDeleteRow = ew_AllowAddDeleteRow(); // Allow add/delete row
		$this->UserIDAllowSecurity = 0; // User ID Allow
		$this->BasicSearch = new cBasicSearch($this->TableVar);

		// id
		$this->id = new cField('call', 'call', 'x_id', 'id', '`id`', '`id`', 3, -1, FALSE, '`id`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->id->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['id'] = &$this->id;

		// callerId
		$this->callerId = new cField('call', 'call', 'x_callerId', 'callerId', '`callerId`', '`callerId`', 3, -1, FALSE, '`callerId`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->callerId->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['callerId'] = &$this->callerId;

		// operatorId
		$this->operatorId = new cField('call', 'call', 'x_operatorId', 'operatorId', '`operatorId`', '`operatorId`', 3, -1, FALSE, '`operatorId`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->operatorId->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['operatorId'] = &$this->operatorId;

		// priority
		$this->priority = new cField('call', 'call', 'x_priority', 'priority', '`priority`', '`priority`', 3, -1, FALSE, '`priority`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->priority->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['priority'] = &$this->priority;

		// allocatedToId
		$this->allocatedToId = new cField('call', 'call', 'x_allocatedToId', 'allocatedToId', '`allocatedToId`', '`allocatedToId`', 3, -1, FALSE, '`allocatedToId`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->allocatedToId->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['allocatedToId'] = &$this->allocatedToId;

		// timeOfCall
		$this->timeOfCall = new cField('call', 'call', 'x_timeOfCall', 'timeOfCall', '`timeOfCall`', 'DATE_FORMAT(`timeOfCall`, \'%d/%m/%Y %H:%i:%s\')', 135, 7, FALSE, '`timeOfCall`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->timeOfCall->FldDefaultErrMsg = str_replace("%s", "/", $Language->Phrase("IncorrectDateDMY"));
		$this->fields['timeOfCall'] = &$this->timeOfCall;

		// callDescription
		$this->callDescription = new cField('call', 'call', 'x_callDescription', 'callDescription', '`callDescription`', '`callDescription`', 201, -1, FALSE, '`callDescription`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->fields['callDescription'] = &$this->callDescription;

		// status
		$this->status = new cField('call', 'call', 'x_status', 'status', '`status`', '`status`', 3, 7, FALSE, '`status`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->status->FldDefaultErrMsg = $Language->Phrase("IncorrectInteger");
		$this->fields['status'] = &$this->status;

		// solution
		$this->solution = new cField('call', 'call', 'x_solution', 'solution', '`solution`', '`solution`', 201, -1, FALSE, '`solution`', FALSE, FALSE, FALSE, 'FORMATTED TEXT');
		$this->fields['solution'] = &$this->solution;
	}

	// Single column sort
	function UpdateSort(&$ofld) {
		if ($this->CurrentOrder == $ofld->FldName) {
			$sSortField = $ofld->FldExpression;
			$sLastSort = $ofld->getSort();
			if ($this->CurrentOrderType == "ASC" || $this->CurrentOrderType == "DESC") {
				$sThisSort = $this->CurrentOrderType;
			} else {
				$sThisSort = ($sLastSort == "ASC") ? "DESC" : "ASC";
			}
			$ofld->setSort($sThisSort);
			$this->setSessionOrderBy($sSortField . " " . $sThisSort); // Save to Session
		} else {
			$ofld->setSort("");
		}
	}

	// Current detail table name
	function getCurrentDetailTable() {
		return @$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_DETAIL_TABLE];
	}

	function setCurrentDetailTable($v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_DETAIL_TABLE] = $v;
	}

	// Get detail url
	function GetDetailUrl() {

		// Detail url
		$sDetailUrl = "";
		if ($this->getCurrentDetailTable() == "callAsset") {
			$sDetailUrl = $GLOBALS["callAsset"]->GetListUrl() . "?showmaster=" . $this->TableVar;
			$sDetailUrl .= "&callId=" . $this->id->CurrentValue;
		}
		if ($this->getCurrentDetailTable() == "callContact") {
			$sDetailUrl = $GLOBALS["callContact"]->GetListUrl() . "?showmaster=" . $this->TableVar;
			$sDetailUrl .= "&callId=" . $this->id->CurrentValue;
		}
		return $sDetailUrl;
	}

	// Table level SQL
	function SqlFrom() { // From
		return "`call`";
	}

	function SqlSelect() { // Select
		return "SELECT * FROM " . $this->SqlFrom();
	}

	function SqlWhere() { // Where
		$sWhere = "";
		$this->TableFilter = "";
		ew_AddFilter($sWhere, $this->TableFilter);
		return $sWhere;
	}

	function SqlGroupBy() { // Group By
		return "";
	}

	function SqlHaving() { // Having
		return "";
	}

	function SqlOrderBy() { // Order By
		return "";
	}

	// Check if Anonymous User is allowed
	function AllowAnonymousUser() {
		switch (@$this->PageID) {
			case "add":
			case "register":
			case "addopt":
				return FALSE;
			case "edit":
			case "update":
			case "changepwd":
			case "forgotpwd":
				return FALSE;
			case "delete":
				return FALSE;
			case "view":
				return FALSE;
			case "search":
				return FALSE;
			default:
				return FALSE;
		}
	}

	// Apply User ID filters
	function ApplyUserIDFilters($sFilter) {
		return $sFilter;
	}

	// Check if User ID security allows view all
	function UserIDAllow($id = "") {
		return TRUE;
	}

	// Get SQL
	function GetSQL($where, $orderby) {
		return ew_BuildSelectSql($this->SqlSelect(), $this->SqlWhere(),
			$this->SqlGroupBy(), $this->SqlHaving(), $this->SqlOrderBy(),
			$where, $orderby);
	}

	// Table SQL
	function SQL() {
		$sFilter = $this->CurrentFilter;
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->SqlSelect(), $this->SqlWhere(),
			$this->SqlGroupBy(), $this->SqlHaving(), $this->SqlOrderBy(),
			$sFilter, $sSort);
	}

	// Table SQL with List page filter
	function SelectSQL() {
		$sFilter = $this->getSessionWhere();
		ew_AddFilter($sFilter, $this->CurrentFilter);
		$sFilter = $this->ApplyUserIDFilters($sFilter);
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql($this->SqlSelect(), $this->SqlWhere(), $this->SqlGroupBy(),
			$this->SqlHaving(), $this->SqlOrderBy(), $sFilter, $sSort);
	}

	// Get ORDER BY clause
	function GetOrderBy() {
		$sSort = $this->getSessionOrderBy();
		return ew_BuildSelectSql("", "", "", "", $this->SqlOrderBy(), "", $sSort);
	}

	// Try to get record count
	function TryGetRecordCount($sSql) {
		global $conn;
		$cnt = -1;
		if ($this->TableType == 'TABLE' || $this->TableType == 'VIEW') {
			$sSql = "SELECT COUNT(*) FROM" . substr($sSql, 13);
			$sOrderBy = $this->GetOrderBy();
			if (substr($sSql, strlen($sOrderBy) * -1) == $sOrderBy)
				$sSql = substr($sSql, 0, strlen($sSql) - strlen($sOrderBy)); // Remove ORDER BY clause
		} else {
			$sSql = "SELECT COUNT(*) FROM (" . $sSql . ") EW_COUNT_TABLE";
		}
		if ($rs = $conn->Execute($sSql)) {
			if (!$rs->EOF && $rs->FieldCount() > 0) {
				$cnt = $rs->fields[0];
				$rs->Close();
			}
		}
		return intval($cnt);
	}

	// Get record count based on filter (for detail record count in master table pages)
	function LoadRecordCount($sFilter) {
		$origFilter = $this->CurrentFilter;
		$this->CurrentFilter = $sFilter;
		$this->Recordset_Selecting($this->CurrentFilter);

		//$sSql = $this->SQL();
		$sSql = $this->GetSQL($this->CurrentFilter, "");
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			if ($rs = $this->LoadRs($this->CurrentFilter)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		$this->CurrentFilter = $origFilter;
		return intval($cnt);
	}

	// Get record count (for current List page)
	function SelectRecordCount() {
		global $conn;
		$origFilter = $this->CurrentFilter;
		$this->Recordset_Selecting($this->CurrentFilter);
		$sSql = $this->SelectSQL();
		$cnt = $this->TryGetRecordCount($sSql);
		if ($cnt == -1) {
			if ($rs = $conn->Execute($sSql)) {
				$cnt = $rs->RecordCount();
				$rs->Close();
			}
		}
		$this->CurrentFilter = $origFilter;
		return intval($cnt);
	}

	// Update Table
	var $UpdateTable = "`call`";

	// INSERT statement
	function InsertSQL(&$rs) {
		global $conn;
		$names = "";
		$values = "";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]))
				continue;
			$names .= $this->fields[$name]->FldExpression . ",";
			$values .= ew_QuotedValue($value, $this->fields[$name]->FldDataType) . ",";
		}
		while (substr($names, -1) == ",")
			$names = substr($names, 0, -1);
		while (substr($values, -1) == ",")
			$values = substr($values, 0, -1);
		return "INSERT INTO " . $this->UpdateTable . " ($names) VALUES ($values)";
	}

	// Insert
	function Insert(&$rs) {
		global $conn;
		return $conn->Execute($this->InsertSQL($rs));
	}

	// UPDATE statement
	function UpdateSQL(&$rs, $where = "") {
		$sql = "UPDATE " . $this->UpdateTable . " SET ";
		foreach ($rs as $name => $value) {
			if (!isset($this->fields[$name]))
				continue;
			$sql .= $this->fields[$name]->FldExpression . "=";
			$sql .= ew_QuotedValue($value, $this->fields[$name]->FldDataType) . ",";
		}
		while (substr($sql, -1) == ",")
			$sql = substr($sql, 0, -1);
		$filter = $this->CurrentFilter;
		ew_AddFilter($filter, $where);
		if ($filter <> "")	$sql .= " WHERE " . $filter;
		return $sql;
	}

	// Update
	function Update(&$rs, $where = "") {
		global $conn;
		return $conn->Execute($this->UpdateSQL($rs, $where));
	}

	// DELETE statement
	function DeleteSQL(&$rs, $where = "") {
		$sql = "DELETE FROM " . $this->UpdateTable . " WHERE ";
		if ($rs) {
			$sql .= ew_QuotedName('id') . '=' . ew_QuotedValue($rs['id'], $this->id->FldDataType) . ' AND ';
		}
		if (substr($sql, -5) == " AND ") $sql = substr($sql, 0, -5);
		$filter = $this->CurrentFilter;
		ew_AddFilter($filter, $where);
		if ($filter <> "")	$sql .= " AND " . $filter;
		return $sql;
	}

	// Delete
	function Delete(&$rs, $where = "") {
		global $conn;
		return $conn->Execute($this->DeleteSQL($rs, $where));
	}

	// Key filter WHERE clause
	function SqlKeyFilter() {
		return "`id` = @id@";
	}

	// Key filter
	function KeyFilter() {
		$sKeyFilter = $this->SqlKeyFilter();
		if (!is_numeric($this->id->CurrentValue))
			$sKeyFilter = "0=1"; // Invalid key
		$sKeyFilter = str_replace("@id@", ew_AdjustSql($this->id->CurrentValue), $sKeyFilter); // Replace key value
		return $sKeyFilter;
	}

	// Return page URL
	function getReturnUrl() {
		$name = EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL;

		// Get referer URL automatically
		if (ew_ServerVar("HTTP_REFERER") <> "" && ew_ReferPage() <> ew_CurrentPage() && ew_ReferPage() <> "login.php") // Referer not same page or login page
			$_SESSION[$name] = ew_ServerVar("HTTP_REFERER"); // Save to Session
		if (@$_SESSION[$name] <> "") {
			return $_SESSION[$name];
		} else {
			return "calllist.php";
		}
	}

	function setReturnUrl($v) {
		$_SESSION[EW_PROJECT_NAME . "_" . $this->TableVar . "_" . EW_TABLE_RETURN_URL] = $v;
	}

	// List URL
	function GetListUrl() {
		return "calllist.php";
	}

	// View URL
	function GetViewUrl() {
		return $this->KeyUrl("callview.php", $this->UrlParm());
	}

	// Add URL
	function GetAddUrl() {
		return "calladd.php";
	}

	// Edit URL
	function GetEditUrl($parm = "") {
		if ($parm <> "")
			return $this->KeyUrl("calledit.php", $this->UrlParm($parm));
		else
			return $this->KeyUrl("calledit.php", $this->UrlParm(EW_TABLE_SHOW_DETAIL . "="));
	}

	// Inline edit URL
	function GetInlineEditUrl() {
		return $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=edit"));
	}

	// Copy URL
	function GetCopyUrl($parm = "") {
		if ($parm <> "")
			return $this->KeyUrl("calladd.php", $this->UrlParm($parm));
		else
			return $this->KeyUrl("calladd.php", $this->UrlParm(EW_TABLE_SHOW_DETAIL . "="));
	}

	// Inline copy URL
	function GetInlineCopyUrl() {
		return $this->KeyUrl(ew_CurrentPage(), $this->UrlParm("a=copy"));
	}

	// Delete URL
	function GetDeleteUrl() {
		return $this->KeyUrl("calldelete.php", $this->UrlParm());
	}

	// Add key value to URL
	function KeyUrl($url, $parm = "") {
		$sUrl = $url . "?";
		if ($parm <> "") $sUrl .= $parm . "&";
		if (!is_null($this->id->CurrentValue)) {
			$sUrl .= "id=" . urlencode($this->id->CurrentValue);
		} else {
			return "javascript:alert(ewLanguage.Phrase('InvalidRecord'));";
		}
		return $sUrl;
	}

	// Sort URL
	function SortUrl(&$fld) {
		if ($this->CurrentAction <> "" || $this->Export <> "" ||
			in_array($fld->FldType, array(128, 204, 205))) { // Unsortable data type
				return "";
		} elseif ($fld->Sortable) {
			$sUrlParm = $this->UrlParm("order=" . urlencode($fld->FldName) . "&ordertype=" . $fld->ReverseSort());
			return ew_CurrentPage() . "?" . $sUrlParm;
		} else {
			return "";
		}
	}

	// Get record keys from $_POST/$_GET/$_SESSION
	function GetRecordKeys() {
		global $EW_COMPOSITE_KEY_SEPARATOR;
		$arKeys = array();
		$arKey = array();
		if (isset($_POST["key_m"])) {
			$arKeys = ew_StripSlashes($_POST["key_m"]);
			$cnt = count($arKeys);
		} elseif (isset($_GET["key_m"])) {
			$arKeys = ew_StripSlashes($_GET["key_m"]);
			$cnt = count($arKeys);
		} elseif (isset($_GET)) {
			$arKeys[] = @$_GET["id"]; // id

			//return $arKeys; // do not return yet, so the values will also be checked by the following code
		}

		// check keys
		$ar = array();
		foreach ($arKeys as $key) {
			if (!is_numeric($key))
				continue;
			$ar[] = $key;
		}
		return $ar;
	}

	// Get key filter
	function GetKeyFilter() {
		$arKeys = $this->GetRecordKeys();
		$sKeyFilter = "";
		foreach ($arKeys as $key) {
			if ($sKeyFilter <> "") $sKeyFilter .= " OR ";
			$this->id->CurrentValue = $key;
			$sKeyFilter .= "(" . $this->KeyFilter() . ")";
		}
		return $sKeyFilter;
	}

	// Load rows based on filter
	function &LoadRs($sFilter) {
		global $conn;

		// Set up filter (SQL WHERE clause) and get return SQL
		//$this->CurrentFilter = $sFilter;
		//$sSql = $this->SQL();

		$sSql = $this->GetSQL($sFilter, "");
		$rs = $conn->Execute($sSql);
		return $rs;
	}

	// Load row values from recordset
	function LoadListRowValues(&$rs) {
		$this->id->setDbValue($rs->fields('id'));
		$this->callerId->setDbValue($rs->fields('callerId'));
		$this->operatorId->setDbValue($rs->fields('operatorId'));
		$this->priority->setDbValue($rs->fields('priority'));
		$this->allocatedToId->setDbValue($rs->fields('allocatedToId'));
		$this->timeOfCall->setDbValue($rs->fields('timeOfCall'));
		$this->callDescription->setDbValue($rs->fields('callDescription'));
		$this->status->setDbValue($rs->fields('status'));
		$this->solution->setDbValue($rs->fields('solution'));
	}

	// Render list row values
	function RenderListRow() {
		global $conn, $Security;

		// Call Row Rendering event
		$this->Row_Rendering();

   // Common render codes
		// id
		// callerId
		// operatorId
		// priority
		// allocatedToId
		// timeOfCall
		// callDescription
		// status
		// solution
		// id

		$this->id->ViewValue = $this->id->CurrentValue;
		$this->id->ViewCustomAttributes = "";

		// callerId
		if (strval($this->callerId->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->callerId->CurrentValue, EW_DATATYPE_NUMBER);
		$sSqlWrk = "SELECT `id`, `id` AS `DispFld`, `name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `staff`";
		$sWhereWrk = "";
		$lookuptblfilter = "`active`=true";
		if (strval($lookuptblfilter) <> "") {
			ew_AddFilter($sWhereWrk, $lookuptblfilter);
		}
		if ($sFilterWrk <> "") {
			ew_AddFilter($sWhereWrk, $sFilterWrk);
		}
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = $conn->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$this->callerId->ViewValue = $rswrk->fields('DispFld');
				$this->callerId->ViewValue .= ew_ValueSeparator(1,$this->callerId) . $rswrk->fields('Disp2Fld');
				$rswrk->Close();
			} else {
				$this->callerId->ViewValue = $this->callerId->CurrentValue;
			}
		} else {
			$this->callerId->ViewValue = NULL;
		}
		$this->callerId->ViewCustomAttributes = "";

		// operatorId
		if (strval($this->operatorId->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->operatorId->CurrentValue, EW_DATATYPE_NUMBER);
		$sSqlWrk = "SELECT `id`, `id` AS `DispFld`, `name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `staff`";
		$sWhereWrk = "";
		$lookuptblfilter = "`active`=true && `roleId` =1";
		if (strval($lookuptblfilter) <> "") {
			ew_AddFilter($sWhereWrk, $lookuptblfilter);
		}
		if ($sFilterWrk <> "") {
			ew_AddFilter($sWhereWrk, $sFilterWrk);
		}
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = $conn->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$this->operatorId->ViewValue = $rswrk->fields('DispFld');
				$this->operatorId->ViewValue .= ew_ValueSeparator(1,$this->operatorId) . $rswrk->fields('Disp2Fld');
				$rswrk->Close();
			} else {
				$this->operatorId->ViewValue = $this->operatorId->CurrentValue;
			}
		} else {
			$this->operatorId->ViewValue = NULL;
		}
		$this->operatorId->ViewCustomAttributes = "";

		// priority
		if (strval($this->priority->CurrentValue) <> "") {
			switch ($this->priority->CurrentValue) {
				case $this->priority->FldTagValue(1):
					$this->priority->ViewValue = $this->priority->FldTagCaption(1) <> "" ? $this->priority->FldTagCaption(1) : $this->priority->CurrentValue;
					break;
				case $this->priority->FldTagValue(2):
					$this->priority->ViewValue = $this->priority->FldTagCaption(2) <> "" ? $this->priority->FldTagCaption(2) : $this->priority->CurrentValue;
					break;
				case $this->priority->FldTagValue(3):
					$this->priority->ViewValue = $this->priority->FldTagCaption(3) <> "" ? $this->priority->FldTagCaption(3) : $this->priority->CurrentValue;
					break;
				case $this->priority->FldTagValue(4):
					$this->priority->ViewValue = $this->priority->FldTagCaption(4) <> "" ? $this->priority->FldTagCaption(4) : $this->priority->CurrentValue;
					break;
				case $this->priority->FldTagValue(5):
					$this->priority->ViewValue = $this->priority->FldTagCaption(5) <> "" ? $this->priority->FldTagCaption(5) : $this->priority->CurrentValue;
					break;
				default:
					$this->priority->ViewValue = $this->priority->CurrentValue;
			}
		} else {
			$this->priority->ViewValue = NULL;
		}
		$this->priority->ViewCustomAttributes = "";

		// allocatedToId
		if (strval($this->allocatedToId->CurrentValue) <> "") {
			$sFilterWrk = "`id`" . ew_SearchString("=", $this->allocatedToId->CurrentValue, EW_DATATYPE_NUMBER);
		$sSqlWrk = "SELECT `id`, `id` AS `DispFld`, `name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `staff`";
		$sWhereWrk = "";
		$lookuptblfilter = "`active`=true && `roleId` =1";
		if (strval($lookuptblfilter) <> "") {
			ew_AddFilter($sWhereWrk, $lookuptblfilter);
		}
		if ($sFilterWrk <> "") {
			ew_AddFilter($sWhereWrk, $sFilterWrk);
		}
		if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = $conn->Execute($sSqlWrk);
			if ($rswrk && !$rswrk->EOF) { // Lookup values found
				$this->allocatedToId->ViewValue = $rswrk->fields('DispFld');
				$this->allocatedToId->ViewValue .= ew_ValueSeparator(1,$this->allocatedToId) . $rswrk->fields('Disp2Fld');
				$rswrk->Close();
			} else {
				$this->allocatedToId->ViewValue = $this->allocatedToId->CurrentValue;
			}
		} else {
			$this->allocatedToId->ViewValue = NULL;
		}
		$this->allocatedToId->ViewCustomAttributes = "";

		// timeOfCall
		$this->timeOfCall->ViewValue = $this->timeOfCall->CurrentValue;
		$this->timeOfCall->ViewValue = ew_FormatDateTime($this->timeOfCall->ViewValue, 7);
		$this->timeOfCall->ViewCustomAttributes = "";

		// callDescription
		$this->callDescription->ViewValue = $this->callDescription->CurrentValue;
		$this->callDescription->ViewCustomAttributes = "";

		// status
		if (strval($this->status->CurrentValue) <> "") {
			switch ($this->status->CurrentValue) {
				case $this->status->FldTagValue(1):
					$this->status->ViewValue = $this->status->FldTagCaption(1) <> "" ? $this->status->FldTagCaption(1) : $this->status->CurrentValue;
					break;
				case $this->status->FldTagValue(2):
					$this->status->ViewValue = $this->status->FldTagCaption(2) <> "" ? $this->status->FldTagCaption(2) : $this->status->CurrentValue;
					break;
				case $this->status->FldTagValue(3):
					$this->status->ViewValue = $this->status->FldTagCaption(3) <> "" ? $this->status->FldTagCaption(3) : $this->status->CurrentValue;
					break;
				default:
					$this->status->ViewValue = $this->status->CurrentValue;
			}
		} else {
			$this->status->ViewValue = NULL;
		}
		$this->status->ViewValue = ew_FormatDateTime($this->status->ViewValue, 7);
		$this->status->ViewCustomAttributes = "";

		// solution
		$this->solution->ViewValue = $this->solution->CurrentValue;
		$this->solution->ViewCustomAttributes = "";

		// id
		$this->id->LinkCustomAttributes = "";
		$this->id->HrefValue = "";
		$this->id->TooltipValue = "";

		// callerId
		$this->callerId->LinkCustomAttributes = "";
		$this->callerId->HrefValue = "";
		$this->callerId->TooltipValue = "";

		// operatorId
		$this->operatorId->LinkCustomAttributes = "";
		$this->operatorId->HrefValue = "";
		$this->operatorId->TooltipValue = "";

		// priority
		$this->priority->LinkCustomAttributes = "";
		$this->priority->HrefValue = "";
		$this->priority->TooltipValue = "";

		// allocatedToId
		$this->allocatedToId->LinkCustomAttributes = "";
		$this->allocatedToId->HrefValue = "";
		$this->allocatedToId->TooltipValue = "";

		// timeOfCall
		$this->timeOfCall->LinkCustomAttributes = "";
		$this->timeOfCall->HrefValue = "";
		$this->timeOfCall->TooltipValue = "";

		// callDescription
		$this->callDescription->LinkCustomAttributes = "";
		$this->callDescription->HrefValue = "";
		$this->callDescription->TooltipValue = "";

		// status
		$this->status->LinkCustomAttributes = "";
		$this->status->HrefValue = "";
		$this->status->TooltipValue = "";

		// solution
		$this->solution->LinkCustomAttributes = "";
		$this->solution->HrefValue = "";
		$this->solution->TooltipValue = "";

		// Call Row Rendered event
		$this->Row_Rendered();
	}

	// Aggregate list row values
	function AggregateListRowValues() {
	}

	// Aggregate list row (for rendering)
	function AggregateListRow() {
	}

	// Export data in HTML/CSV/Word/Excel/Email/PDF format
	function ExportDocument(&$Doc, &$Recordset, $StartRec, $StopRec, $ExportPageType = "") {
		if (!$Recordset || !$Doc)
			return;

		// Write header
		$Doc->ExportTableHeader();
		if ($Doc->Horizontal) { // Horizontal format, write header
			$Doc->BeginExportRow();
			if ($ExportPageType == "view") {
				if ($this->id->Exportable) $Doc->ExportCaption($this->id);
				if ($this->callerId->Exportable) $Doc->ExportCaption($this->callerId);
				if ($this->operatorId->Exportable) $Doc->ExportCaption($this->operatorId);
				if ($this->priority->Exportable) $Doc->ExportCaption($this->priority);
				if ($this->allocatedToId->Exportable) $Doc->ExportCaption($this->allocatedToId);
				if ($this->timeOfCall->Exportable) $Doc->ExportCaption($this->timeOfCall);
				if ($this->callDescription->Exportable) $Doc->ExportCaption($this->callDescription);
				if ($this->status->Exportable) $Doc->ExportCaption($this->status);
				if ($this->solution->Exportable) $Doc->ExportCaption($this->solution);
			} else {
				if ($this->id->Exportable) $Doc->ExportCaption($this->id);
				if ($this->callerId->Exportable) $Doc->ExportCaption($this->callerId);
				if ($this->operatorId->Exportable) $Doc->ExportCaption($this->operatorId);
				if ($this->priority->Exportable) $Doc->ExportCaption($this->priority);
				if ($this->allocatedToId->Exportable) $Doc->ExportCaption($this->allocatedToId);
				if ($this->timeOfCall->Exportable) $Doc->ExportCaption($this->timeOfCall);
				if ($this->callDescription->Exportable) $Doc->ExportCaption($this->callDescription);
				if ($this->status->Exportable) $Doc->ExportCaption($this->status);
				if ($this->solution->Exportable) $Doc->ExportCaption($this->solution);
			}
			$Doc->EndExportRow();
		}

		// Move to first record
		$RecCnt = $StartRec - 1;
		if (!$Recordset->EOF) {
			$Recordset->MoveFirst();
			if ($StartRec > 1)
				$Recordset->Move($StartRec - 1);
		}
		while (!$Recordset->EOF && $RecCnt < $StopRec) {
			$RecCnt++;
			if (intval($RecCnt) >= intval($StartRec)) {
				$RowCnt = intval($RecCnt) - intval($StartRec) + 1;

				// Page break
				if ($this->ExportPageBreakCount > 0) {
					if ($RowCnt > 1 && ($RowCnt - 1) % $this->ExportPageBreakCount == 0)
						$Doc->ExportPageBreak();
				}
				$this->LoadListRowValues($Recordset);

				// Render row
				$this->RowType = EW_ROWTYPE_VIEW; // Render view
				$this->ResetAttrs();
				$this->RenderListRow();
				$Doc->BeginExportRow($RowCnt); // Allow CSS styles if enabled
				if ($ExportPageType == "view") {
					if ($this->id->Exportable) $Doc->ExportField($this->id);
					if ($this->callerId->Exportable) $Doc->ExportField($this->callerId);
					if ($this->operatorId->Exportable) $Doc->ExportField($this->operatorId);
					if ($this->priority->Exportable) $Doc->ExportField($this->priority);
					if ($this->allocatedToId->Exportable) $Doc->ExportField($this->allocatedToId);
					if ($this->timeOfCall->Exportable) $Doc->ExportField($this->timeOfCall);
					if ($this->callDescription->Exportable) $Doc->ExportField($this->callDescription);
					if ($this->status->Exportable) $Doc->ExportField($this->status);
					if ($this->solution->Exportable) $Doc->ExportField($this->solution);
				} else {
					if ($this->id->Exportable) $Doc->ExportField($this->id);
					if ($this->callerId->Exportable) $Doc->ExportField($this->callerId);
					if ($this->operatorId->Exportable) $Doc->ExportField($this->operatorId);
					if ($this->priority->Exportable) $Doc->ExportField($this->priority);
					if ($this->allocatedToId->Exportable) $Doc->ExportField($this->allocatedToId);
					if ($this->timeOfCall->Exportable) $Doc->ExportField($this->timeOfCall);
					if ($this->callDescription->Exportable) $Doc->ExportField($this->callDescription);
					if ($this->status->Exportable) $Doc->ExportField($this->status);
					if ($this->solution->Exportable) $Doc->ExportField($this->solution);
				}
				$Doc->EndExportRow();
			}
			$Recordset->MoveNext();
		}
		$Doc->ExportTableFooter();
	}

	// Table level events
	// Recordset Selecting event
	function Recordset_Selecting(&$filter) {

		// Enter your code here	
	}

	// Recordset Selected event
	function Recordset_Selected(&$rs) {

		//echo "Recordset Selected";
	}

	// Recordset Search Validated event
	function Recordset_SearchValidated() {

		// Example:
		//$this->MyField1->AdvancedSearch->SearchValue = "your search criteria"; // Search value

	}

	// Recordset Searching event
	function Recordset_Searching(&$filter) {

		// Enter your code here	
	}

	// Row_Selecting event
	function Row_Selecting(&$filter) {

		// Enter your code here	
	}

	// Row Selected event
	function Row_Selected(&$rs) {

		//echo "Row Selected";
	}

	// Row Inserting event
	function Row_Inserting($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Inserted event
	function Row_Inserted($rsold, &$rsnew) {

		//echo "Row Inserted"
	}

	// Row Updating event
	function Row_Updating($rsold, &$rsnew) {

		// Enter your code here
		// To cancel, set return value to FALSE

		return TRUE;
	}

	// Row Updated event
	function Row_Updated($rsold, &$rsnew) {

		//echo "Row Updated";
	}

	// Row Update Conflict event
	function Row_UpdateConflict($rsold, &$rsnew) {

		// Enter your code here
		// To ignore conflict, set return value to FALSE

		return TRUE;
	}

	// Row Deleting event
	function Row_Deleting(&$rs) {

		// Enter your code here
		// To cancel, set return value to False

		return TRUE;
	}

	// Row Deleted event
	function Row_Deleted(&$rs) {

		//echo "Row Deleted";
	}

	// Email Sending event
	function Email_Sending(&$Email, &$Args) {

		//var_dump($Email); var_dump($Args); exit();
		return TRUE;
	}

	// Row Rendering event
	function Row_Rendering() {

		// Enter your code here	
	}

	// Row Rendered event
	function Row_Rendered() {

		// To view properties of field class, use:
		//var_dump($this-><FieldName>); 

	}

	// User ID Filtering event
	function UserID_Filtering(&$filter) {

		// Enter your code here
	}
}
?>
