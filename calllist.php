<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg9.php" ?>
<?php include_once "ewmysql9.php" ?>
<?php include_once "phpfn9.php" ?>
<?php include_once "callinfo.php" ?>
<?php include_once "staffinfo.php" ?>
<?php include_once "callAssetgridcls.php" ?>
<?php include_once "callContactgridcls.php" ?>
<?php include_once "userfn9.php" ?>
<?php

//
// Page class
//

$call_list = NULL; // Initialize page object first

class ccall_list extends ccall {

	// Page ID
	var $PageID = 'list';

	// Project ID
	var $ProjectID = "{4F01D370-BA3A-4C91-9445-0AA347261657}";

	// Table name
	var $TableName = 'call';

	// Page object name
	var $PageObjName = 'call_list';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Page URLs
	var $AddUrl;
	var $EditUrl;
	var $CopyUrl;
	var $DeleteUrl;
	var $ViewUrl;
	var $ListUrl;

	// Export URLs
	var $ExportPrintUrl;
	var $ExportHtmlUrl;
	var $ExportExcelUrl;
	var $ExportWordUrl;
	var $ExportXmlUrl;
	var $ExportCsvUrl;
	var $ExportPdfUrl;

	// Update URLs
	var $InlineAddUrl;
	var $InlineCopyUrl;
	var $InlineEditUrl;
	var $GridAddUrl;
	var $GridEditUrl;
	var $MultiDeleteUrl;
	var $MultiUpdateUrl;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			$html .= "<p class=\"ewMessage\">" . $sMessage . "</p>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			$html .= "<table class=\"ewMessageTable\"><tr><td class=\"ewWarningIcon\"></td><td class=\"ewWarningMessage\">" . $sWarningMessage . "</td></tr></table>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			$html .= "<table class=\"ewMessageTable\"><tr><td class=\"ewSuccessIcon\"></td><td class=\"ewSuccessMessage\">" . $sSuccessMessage . "</td></tr></table>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			$html .= "<table class=\"ewMessageTable\"><tr><td class=\"ewErrorIcon\"></td><td class=\"ewErrorMessage\">" . $sErrorMessage . "</td></tr></table>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p class=\"phpmaker\">" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Fotoer exists, display
			echo "<p class=\"phpmaker\">" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language, $UserAgent;

		// User agent
		$UserAgent = ew_UserAgent();
		$GLOBALS["Page"] = &$this;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (call)
		if (!isset($GLOBALS["call"])) {
			$GLOBALS["call"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["call"];
		}

		// Initialize URLs
		$this->ExportPrintUrl = $this->PageUrl() . "export=print";
		$this->ExportExcelUrl = $this->PageUrl() . "export=excel";
		$this->ExportWordUrl = $this->PageUrl() . "export=word";
		$this->ExportHtmlUrl = $this->PageUrl() . "export=html";
		$this->ExportXmlUrl = $this->PageUrl() . "export=xml";
		$this->ExportCsvUrl = $this->PageUrl() . "export=csv";
		$this->ExportPdfUrl = $this->PageUrl() . "export=pdf";
		$this->AddUrl = "calladd.php?" . EW_TABLE_SHOW_DETAIL . "=";
		$this->InlineAddUrl = $this->PageUrl() . "a=add";
		$this->GridAddUrl = $this->PageUrl() . "a=gridadd";
		$this->GridEditUrl = $this->PageUrl() . "a=gridedit";
		$this->MultiDeleteUrl = "calldelete.php";
		$this->MultiUpdateUrl = "callupdate.php";

		// Table object (staff)
		if (!isset($GLOBALS['staff'])) $GLOBALS['staff'] = new cstaff();

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'list', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'call', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect();

		// List options
		$this->ListOptions = new cListOptions();
		$this->ListOptions->TableVar = $this->TableVar;

		// Export options
		$this->ExportOptions = new cListOptions();
		$this->ExportOptions->Tag = "span";
		$this->ExportOptions->TagClassName = "ewExportOption";
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if (!$Security->IsLoggedIn()) {
			$Security->SaveLastUrl();
			$this->Page_Terminate("login.php");
		}

		// Get export parameters
		if (@$_GET["export"] <> "") {
			$this->Export = $_GET["export"];
		} elseif (ew_IsHttpPost()) {
			if (@$_POST["exporttype"] <> "")
				$this->Export = $_POST["exporttype"];
		} else {
			$this->setExportReturnUrl(ew_CurrentUrl());
		}
		$gsExport = $this->Export; // Get export parameter, used in header
		$gsExportFile = $this->TableVar; // Get export file, used in header

		// Get grid add count
		$gridaddcnt = @$_GET[EW_TABLE_GRID_ADD_ROW_COUNT];
		if (is_numeric($gridaddcnt) && $gridaddcnt > 0)
			$this->GridAddRowCount = $gridaddcnt;

		// Set up list options
		$this->SetupListOptions();

		// Setup export options
		$this->SetupExportOptions();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"];
		$this->id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();
		$this->timeOfCall->Visible = !$this->IsAddOrEdit();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $conn;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();
		$this->Page_Redirecting($url);

		 // Close connection
		$conn->Close();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}

	// Class variables
	var $ListOptions; // List options
	var $ExportOptions; // Export options
	var $DisplayRecs = 20;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $Pager;
	var $SearchWhere = ""; // Search WHERE clause
	var $RecCnt = 0; // Record count
	var $EditRowCnt;
	var $StartRowCnt = 1;
	var $RowCnt = 0;
	var $Attrs = array(); // Row attributes and cell attributes
	var $RowIndex = 0; // Row index
	var $KeyCount = 0; // Key count
	var $RowAction = ""; // Row action
	var $RowOldKey = ""; // Row old key (for copy)
	var $RecPerRow = 0;
	var $ColCnt = 0;
	var $DbMasterFilter = ""; // Master filter
	var $DbDetailFilter = ""; // Detail filter
	var $MasterRecordExists;	
	var $MultiSelectKey;
	var $Command;
	var $callAsset_Count;
	var $callContact_Count;
	var $Recordset;
	var $OldRecordset;

	//
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError, $gsSearchError, $Security;

		// Search filters
		$sSrchAdvanced = ""; // Advanced search filter
		$sSrchBasic = ""; // Basic search filter
		$sFilter = "";

		// Get command
		$this->Command = strtolower(@$_GET["cmd"]);
		if ($this->IsPageRequest()) { // Validate request

			// Handle reset command
			$this->ResetCmd();

			// Hide all options
			if ($this->Export <> "" ||
				$this->CurrentAction == "gridadd" ||
				$this->CurrentAction == "gridedit") {
				$this->ListOptions->HideAllOptions();
				$this->ExportOptions->HideAllOptions();
			}

			// Set up sorting order
			$this->SetUpSortOrder();
		}

		// Restore display records
		if ($this->getRecordsPerPage() <> "") {
			$this->DisplayRecs = $this->getRecordsPerPage(); // Restore from Session
		} else {
			$this->DisplayRecs = 20; // Load default
		}

		// Load Sorting Order
		$this->LoadSortOrder();

		// Build filter
		$sFilter = "";
		ew_AddFilter($sFilter, $this->DbDetailFilter);
		ew_AddFilter($sFilter, $this->SearchWhere);

		// Set up filter in session
		$this->setSessionWhere($sFilter);
		$this->CurrentFilter = "";

		// Export data only
		if (in_array($this->Export, array("html","word","excel","xml","csv","email","pdf"))) {
			$this->ExportData();
			if ($this->Export == "email")
				$this->Page_Terminate($this->ExportReturnUrl());
			else
				$this->Page_Terminate(); // Terminate response
			exit();
		}
	}

	// Build filter for all keys
	function BuildKeyFilter() {
		global $objForm;
		$sWrkFilter = "";

		// Update row index and get row key
		$rowindex = 1;
		$objForm->Index = $rowindex;
		$sThisKey = strval($objForm->GetValue("k_key"));
		while ($sThisKey <> "") {
			if ($this->SetupKeyValues($sThisKey)) {
				$sFilter = $this->KeyFilter();
				if ($sWrkFilter <> "") $sWrkFilter .= " OR ";
				$sWrkFilter .= $sFilter;
			} else {
				$sWrkFilter = "0=1";
				break;
			}

			// Update row index and get row key
			$rowindex++; // next row
			$objForm->Index = $rowindex;
			$sThisKey = strval($objForm->GetValue("k_key"));
		}
		return $sWrkFilter;
	}

	// Set up key values
	function SetupKeyValues($key) {
		$arrKeyFlds = explode($GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"], $key);
		if (count($arrKeyFlds) >= 1) {
			$this->id->setFormValue($arrKeyFlds[0]);
			if (!is_numeric($this->id->FormValue))
				return FALSE;
		}
		return TRUE;
	}

	// Set up sort parameters
	function SetUpSortOrder() {

		// Check for "order" parameter
		if (@$_GET["order"] <> "") {
			$this->CurrentOrder = ew_StripSlashes(@$_GET["order"]);
			$this->CurrentOrderType = @$_GET["ordertype"];
			$this->UpdateSort($this->id); // id
			$this->UpdateSort($this->callerId); // callerId
			$this->UpdateSort($this->operatorId); // operatorId
			$this->UpdateSort($this->priority); // priority
			$this->UpdateSort($this->allocatedToId); // allocatedToId
			$this->UpdateSort($this->timeOfCall); // timeOfCall
			$this->UpdateSort($this->callDescription); // callDescription
			$this->UpdateSort($this->status); // status
			$this->UpdateSort($this->solution); // solution
			$this->setStartRecordNumber(1); // Reset start position
		}
	}

	// Load sort order parameters
	function LoadSortOrder() {
		$sOrderBy = $this->getSessionOrderBy(); // Get ORDER BY from Session
		if ($sOrderBy == "") {
			if ($this->SqlOrderBy() <> "") {
				$sOrderBy = $this->SqlOrderBy();
				$this->setSessionOrderBy($sOrderBy);
			}
		}
	}

	// Reset command
	// cmd=reset (Reset search parameters)
	// cmd=resetall (Reset search and master/detail parameters)
	// cmd=resetsort (Reset sort parameters)
	function ResetCmd() {

		// Check if reset command
		if (substr($this->Command,0,5) == "reset") {

			// Reset sorting order
			if ($this->Command == "resetsort") {
				$sOrderBy = "";
				$this->setSessionOrderBy($sOrderBy);
				$this->id->setSort("");
				$this->callerId->setSort("");
				$this->operatorId->setSort("");
				$this->priority->setSort("");
				$this->allocatedToId->setSort("");
				$this->timeOfCall->setSort("");
				$this->callDescription->setSort("");
				$this->status->setSort("");
				$this->solution->setSort("");
			}

			// Reset start position
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Set up list options
	function SetupListOptions() {
		global $Security, $Language;

		// "edit"
		$item = &$this->ListOptions->Add("edit");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->IsLoggedIn();
		$item->OnLeft = FALSE;

		// "detail_callAsset"
		$item = &$this->ListOptions->Add("detail_callAsset");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->IsLoggedIn();
		$item->OnLeft = FALSE;
		if (!isset($GLOBALS["callAsset_grid"])) $GLOBALS["callAsset_grid"] = new ccallAsset_grid;

		// "detail_callContact"
		$item = &$this->ListOptions->Add("detail_callContact");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->IsLoggedIn();
		$item->OnLeft = FALSE;
		if (!isset($GLOBALS["callContact_grid"])) $GLOBALS["callContact_grid"] = new ccallContact_grid;

		// Call ListOptions_Load event
		$this->ListOptions_Load();
	}

	// Render list options
	function RenderListOptions() {
		global $Security, $Language, $objForm;
		$this->ListOptions->LoadDefault();

		// "edit"
		$oListOpt = &$this->ListOptions->Items["edit"];
		if ($Security->IsLoggedIn()) {
			$oListOpt->Body = "<a class=\"ewRowLink\" href=\"" . $this->EditUrl . "\">" . $Language->Phrase("EditLink") . "</a>";
		}

		// "detail_callAsset"
		$oListOpt = &$this->ListOptions->Items["detail_callAsset"];
		if ($Security->IsLoggedIn()) {
			$oListOpt->Body = $Language->Phrase("DetailLink") . $Language->TablePhrase("callAsset", "TblCaption");
			$oListOpt->Body .= str_replace("%c", $this->callAsset_Count, $Language->Phrase("DetailCount"));
			$oListOpt->Body = "<a class=\"ewRowLink\" href=\"callAssetlist.php?" . EW_TABLE_SHOW_MASTER . "=call&id=" . urlencode(strval($this->id->CurrentValue)) . "\">" . $oListOpt->Body . "</a>";
			$links = "";
			if ($GLOBALS["callAsset_grid"]->DetailEdit && $Security->IsLoggedIn() && $Security->IsLoggedIn())
				$links .= "<a class=\"ewRowLink\" href=\"" . $this->GetEditUrl(EW_TABLE_SHOW_DETAIL . "=callAsset") . "\">" . $Language->Phrase("EditLink") . "</a>&nbsp;";
			if ($links <> "") $oListOpt->Body .= "<br>" . $links;
		}

		// "detail_callContact"
		$oListOpt = &$this->ListOptions->Items["detail_callContact"];
		if ($Security->IsLoggedIn()) {
			$oListOpt->Body = $Language->Phrase("DetailLink") . $Language->TablePhrase("callContact", "TblCaption");
			$oListOpt->Body .= str_replace("%c", $this->callContact_Count, $Language->Phrase("DetailCount"));
			$oListOpt->Body = "<a class=\"ewRowLink\" href=\"callContactlist.php?" . EW_TABLE_SHOW_MASTER . "=call&id=" . urlencode(strval($this->id->CurrentValue)) . "\">" . $oListOpt->Body . "</a>";
			$links = "";
			if ($GLOBALS["callContact_grid"]->DetailEdit && $Security->IsLoggedIn() && $Security->IsLoggedIn())
				$links .= "<a class=\"ewRowLink\" href=\"" . $this->GetEditUrl(EW_TABLE_SHOW_DETAIL . "=callContact") . "\">" . $Language->Phrase("EditLink") . "</a>&nbsp;";
			if ($links <> "") $oListOpt->Body .= "<br>" . $links;
		}
		$this->RenderListOptionsExt();

		// Call ListOptions_Rendered event
		$this->ListOptions_Rendered();
	}

	function RenderListOptionsExt() {
		global $Security, $Language;
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {
		global $conn;

		// Call Recordset Selecting event
		$this->Recordset_Selecting($this->CurrentFilter);

		// Load List page SQL
		$sSql = $this->SelectSQL();
		if ($offset > -1 && $rowcnt > -1)
			$sSql .= " LIMIT $rowcnt OFFSET $offset";

		// Load recordset
		$rs = ew_LoadRecordset($sSql);

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $conn, $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		global $conn;
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->id->setDbValue($rs->fields('id'));
		$this->callerId->setDbValue($rs->fields('callerId'));
		$this->operatorId->setDbValue($rs->fields('operatorId'));
		$this->priority->setDbValue($rs->fields('priority'));
		$this->allocatedToId->setDbValue($rs->fields('allocatedToId'));
		$this->timeOfCall->setDbValue($rs->fields('timeOfCall'));
		$this->callDescription->setDbValue($rs->fields('callDescription'));
		$this->status->setDbValue($rs->fields('status'));
		$this->solution->setDbValue($rs->fields('solution'));
		if (!isset($GLOBALS["callAsset_grid"])) $GLOBALS["callAsset_grid"] = new ccallAsset_grid;
		$sDetailFilter = $GLOBALS["callAsset"]->SqlDetailFilter_call();
		$sDetailFilter = str_replace("@callId@", ew_AdjustSql($this->id->DbValue), $sDetailFilter);
		$GLOBALS["callAsset"]->setCurrentMasterTable("call");
		$sDetailFilter = $GLOBALS["callAsset"]->ApplyUserIDFilters($sDetailFilter);
		$this->callAsset_Count = $GLOBALS["callAsset"]->LoadRecordCount($sDetailFilter);
		if (!isset($GLOBALS["callContact_grid"])) $GLOBALS["callContact_grid"] = new ccallContact_grid;
		$sDetailFilter = $GLOBALS["callContact"]->SqlDetailFilter_call();
		$sDetailFilter = str_replace("@callId@", ew_AdjustSql($this->id->DbValue), $sDetailFilter);
		$GLOBALS["callContact"]->setCurrentMasterTable("call");
		$sDetailFilter = $GLOBALS["callContact"]->ApplyUserIDFilters($sDetailFilter);
		$this->callContact_Count = $GLOBALS["callContact"]->LoadRecordCount($sDetailFilter);
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("id")) <> "")
			$this->id->CurrentValue = $this->getKey("id"); // id
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$this->OldRecordset = ew_LoadRecordset($sSql);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $conn, $Security, $Language;
		global $gsLanguage;

		// Initialize URLs
		$this->ViewUrl = $this->GetViewUrl();
		$this->EditUrl = $this->GetEditUrl();
		$this->InlineEditUrl = $this->GetInlineEditUrl();
		$this->CopyUrl = $this->GetCopyUrl();
		$this->InlineCopyUrl = $this->GetInlineCopyUrl();
		$this->DeleteUrl = $this->GetDeleteUrl();

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// id
		// callerId
		// operatorId
		// priority
		// allocatedToId
		// timeOfCall
		// callDescription
		// status
		// solution

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

			// id
			$this->id->ViewValue = $this->id->CurrentValue;
			$this->id->ViewCustomAttributes = "";

			// callerId
			if (strval($this->callerId->CurrentValue) <> "") {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->callerId->CurrentValue, EW_DATATYPE_NUMBER);
			$sSqlWrk = "SELECT `id`, `id` AS `DispFld`, `name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `staff`";
			$sWhereWrk = "";
			$lookuptblfilter = "`active`=true";
			if (strval($lookuptblfilter) <> "") {
				ew_AddFilter($sWhereWrk, $lookuptblfilter);
			}
			if ($sFilterWrk <> "") {
				ew_AddFilter($sWhereWrk, $sFilterWrk);
			}
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
				$rswrk = $conn->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$this->callerId->ViewValue = $rswrk->fields('DispFld');
					$this->callerId->ViewValue .= ew_ValueSeparator(1,$this->callerId) . $rswrk->fields('Disp2Fld');
					$rswrk->Close();
				} else {
					$this->callerId->ViewValue = $this->callerId->CurrentValue;
				}
			} else {
				$this->callerId->ViewValue = NULL;
			}
			$this->callerId->ViewCustomAttributes = "";

			// operatorId
			if (strval($this->operatorId->CurrentValue) <> "") {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->operatorId->CurrentValue, EW_DATATYPE_NUMBER);
			$sSqlWrk = "SELECT `id`, `id` AS `DispFld`, `name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `staff`";
			$sWhereWrk = "";
			$lookuptblfilter = "`active`=true && `roleId` =1";
			if (strval($lookuptblfilter) <> "") {
				ew_AddFilter($sWhereWrk, $lookuptblfilter);
			}
			if ($sFilterWrk <> "") {
				ew_AddFilter($sWhereWrk, $sFilterWrk);
			}
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
				$rswrk = $conn->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$this->operatorId->ViewValue = $rswrk->fields('DispFld');
					$this->operatorId->ViewValue .= ew_ValueSeparator(1,$this->operatorId) . $rswrk->fields('Disp2Fld');
					$rswrk->Close();
				} else {
					$this->operatorId->ViewValue = $this->operatorId->CurrentValue;
				}
			} else {
				$this->operatorId->ViewValue = NULL;
			}
			$this->operatorId->ViewCustomAttributes = "";

			// priority
			if (strval($this->priority->CurrentValue) <> "") {
				switch ($this->priority->CurrentValue) {
					case $this->priority->FldTagValue(1):
						$this->priority->ViewValue = $this->priority->FldTagCaption(1) <> "" ? $this->priority->FldTagCaption(1) : $this->priority->CurrentValue;
						break;
					case $this->priority->FldTagValue(2):
						$this->priority->ViewValue = $this->priority->FldTagCaption(2) <> "" ? $this->priority->FldTagCaption(2) : $this->priority->CurrentValue;
						break;
					case $this->priority->FldTagValue(3):
						$this->priority->ViewValue = $this->priority->FldTagCaption(3) <> "" ? $this->priority->FldTagCaption(3) : $this->priority->CurrentValue;
						break;
					case $this->priority->FldTagValue(4):
						$this->priority->ViewValue = $this->priority->FldTagCaption(4) <> "" ? $this->priority->FldTagCaption(4) : $this->priority->CurrentValue;
						break;
					case $this->priority->FldTagValue(5):
						$this->priority->ViewValue = $this->priority->FldTagCaption(5) <> "" ? $this->priority->FldTagCaption(5) : $this->priority->CurrentValue;
						break;
					default:
						$this->priority->ViewValue = $this->priority->CurrentValue;
				}
			} else {
				$this->priority->ViewValue = NULL;
			}
			$this->priority->ViewCustomAttributes = "";

			// allocatedToId
			if (strval($this->allocatedToId->CurrentValue) <> "") {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->allocatedToId->CurrentValue, EW_DATATYPE_NUMBER);
			$sSqlWrk = "SELECT `id`, `id` AS `DispFld`, `name` AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `staff`";
			$sWhereWrk = "";
			$lookuptblfilter = "`active`=true && `roleId` =1";
			if (strval($lookuptblfilter) <> "") {
				ew_AddFilter($sWhereWrk, $lookuptblfilter);
			}
			if ($sFilterWrk <> "") {
				ew_AddFilter($sWhereWrk, $sFilterWrk);
			}
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
				$rswrk = $conn->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$this->allocatedToId->ViewValue = $rswrk->fields('DispFld');
					$this->allocatedToId->ViewValue .= ew_ValueSeparator(1,$this->allocatedToId) . $rswrk->fields('Disp2Fld');
					$rswrk->Close();
				} else {
					$this->allocatedToId->ViewValue = $this->allocatedToId->CurrentValue;
				}
			} else {
				$this->allocatedToId->ViewValue = NULL;
			}
			$this->allocatedToId->ViewCustomAttributes = "";

			// timeOfCall
			$this->timeOfCall->ViewValue = $this->timeOfCall->CurrentValue;
			$this->timeOfCall->ViewValue = ew_FormatDateTime($this->timeOfCall->ViewValue, 7);
			$this->timeOfCall->ViewCustomAttributes = "";

			// callDescription
			$this->callDescription->ViewValue = $this->callDescription->CurrentValue;
			$this->callDescription->ViewCustomAttributes = "";

			// status
			if (strval($this->status->CurrentValue) <> "") {
				switch ($this->status->CurrentValue) {
					case $this->status->FldTagValue(1):
						$this->status->ViewValue = $this->status->FldTagCaption(1) <> "" ? $this->status->FldTagCaption(1) : $this->status->CurrentValue;
						break;
					case $this->status->FldTagValue(2):
						$this->status->ViewValue = $this->status->FldTagCaption(2) <> "" ? $this->status->FldTagCaption(2) : $this->status->CurrentValue;
						break;
					case $this->status->FldTagValue(3):
						$this->status->ViewValue = $this->status->FldTagCaption(3) <> "" ? $this->status->FldTagCaption(3) : $this->status->CurrentValue;
						break;
					default:
						$this->status->ViewValue = $this->status->CurrentValue;
				}
			} else {
				$this->status->ViewValue = NULL;
			}
			$this->status->ViewValue = ew_FormatDateTime($this->status->ViewValue, 7);
			$this->status->ViewCustomAttributes = "";

			// solution
			$this->solution->ViewValue = $this->solution->CurrentValue;
			$this->solution->ViewCustomAttributes = "";

			// id
			$this->id->LinkCustomAttributes = "";
			$this->id->HrefValue = "";
			$this->id->TooltipValue = "";

			// callerId
			$this->callerId->LinkCustomAttributes = "";
			$this->callerId->HrefValue = "";
			$this->callerId->TooltipValue = "";

			// operatorId
			$this->operatorId->LinkCustomAttributes = "";
			$this->operatorId->HrefValue = "";
			$this->operatorId->TooltipValue = "";

			// priority
			$this->priority->LinkCustomAttributes = "";
			$this->priority->HrefValue = "";
			$this->priority->TooltipValue = "";

			// allocatedToId
			$this->allocatedToId->LinkCustomAttributes = "";
			$this->allocatedToId->HrefValue = "";
			$this->allocatedToId->TooltipValue = "";

			// timeOfCall
			$this->timeOfCall->LinkCustomAttributes = "";
			$this->timeOfCall->HrefValue = "";
			$this->timeOfCall->TooltipValue = "";

			// callDescription
			$this->callDescription->LinkCustomAttributes = "";
			$this->callDescription->HrefValue = "";
			$this->callDescription->TooltipValue = "";

			// status
			$this->status->LinkCustomAttributes = "";
			$this->status->HrefValue = "";
			$this->status->TooltipValue = "";

			// solution
			$this->solution->LinkCustomAttributes = "";
			$this->solution->HrefValue = "";
			$this->solution->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Set up export options
	function SetupExportOptions() {
		global $Language;

		// Printer friendly
		$item = &$this->ExportOptions->Add("print");
		$item->Body = "<a href=\"" . $this->ExportPrintUrl . "\">" . $Language->Phrase("PrinterFriendly") . "</a>";
		$item->Visible = FALSE;

		// Export to Excel
		$item = &$this->ExportOptions->Add("excel");
		$item->Body = "<a href=\"" . $this->ExportExcelUrl . "\">" . $Language->Phrase("ExportToExcel") . "</a>";
		$item->Visible = FALSE;

		// Export to Word
		$item = &$this->ExportOptions->Add("word");
		$item->Body = "<a href=\"" . $this->ExportWordUrl . "\">" . $Language->Phrase("ExportToWord") . "</a>";
		$item->Visible = FALSE;

		// Export to Html
		$item = &$this->ExportOptions->Add("html");
		$item->Body = "<a href=\"" . $this->ExportHtmlUrl . "\">" . $Language->Phrase("ExportToHtml") . "</a>";
		$item->Visible = FALSE;

		// Export to Xml
		$item = &$this->ExportOptions->Add("xml");
		$item->Body = "<a href=\"" . $this->ExportXmlUrl . "\">" . $Language->Phrase("ExportToXml") . "</a>";
		$item->Visible = FALSE;

		// Export to Csv
		$item = &$this->ExportOptions->Add("csv");
		$item->Body = "<a href=\"" . $this->ExportCsvUrl . "\">" . $Language->Phrase("ExportToCsv") . "</a>";
		$item->Visible = TRUE;

		// Export to Pdf
		$item = &$this->ExportOptions->Add("pdf");
		$item->Body = "<a href=\"" . $this->ExportPdfUrl . "\">" . $Language->Phrase("ExportToPDF") . "</a>";
		$item->Visible = FALSE;

		// Export to Email
		$item = &$this->ExportOptions->Add("email");
		$item->Body = "<a id=\"emf_call\" href=\"javascript:void(0);\" onclick=\"ew_EmailDialogShow({lnk:'emf_call',hdr:ewLanguage.Phrase('ExportToEmail'),f:document.fcalllist,sel:false});\">" . $Language->Phrase("ExportToEmail") . "</a>";
		$item->Visible = FALSE;

		// Hide options for export/action
		if ($this->Export <> "" || $this->CurrentAction <> "")
			$this->ExportOptions->HideAllOptions();
	}

	// Export data in HTML/CSV/Word/Excel/XML/Email/PDF format
	function ExportData() {
		$utf8 = (strtolower(EW_CHARSET) == "utf-8");
		$bSelectLimit = EW_SELECT_LIMIT;

		// Load recordset
		if ($bSelectLimit) {
			$this->TotalRecs = $this->SelectRecordCount();
		} else {
			if ($rs = $this->LoadRecordset())
				$this->TotalRecs = $rs->RecordCount();
		}
		$this->StartRec = 1;

		// Export all
		if ($this->ExportAll) {
			set_time_limit(EW_EXPORT_ALL_TIME_LIMIT);
			$this->DisplayRecs = $this->TotalRecs;
			$this->StopRec = $this->TotalRecs;
		} else { // Export one page only
			$this->SetUpStartRec(); // Set up start record position

			// Set the last record to display
			if ($this->DisplayRecs <= 0) {
				$this->StopRec = $this->TotalRecs;
			} else {
				$this->StopRec = $this->StartRec + $this->DisplayRecs - 1;
			}
		}
		if ($bSelectLimit)
			$rs = $this->LoadRecordset($this->StartRec-1, $this->DisplayRecs <= 0 ? $this->TotalRecs : $this->DisplayRecs);
		if (!$rs) {
			header("Content-Type:"); // Remove header
			header("Content-Disposition:");
			$this->ShowMessage();
			return;
		}
		$ExportDoc = ew_ExportDocument($this, "h");
		$ParentTable = "";
		if ($bSelectLimit) {
			$StartRec = 1;
			$StopRec = $this->DisplayRecs <= 0 ? $this->TotalRecs : $this->DisplayRecs;
		} else {
			$StartRec = $this->StartRec;
			$StopRec = $this->StopRec;
		}
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		$ExportDoc->Text .= $sHeader;
		$this->ExportDocument($ExportDoc, $rs, $StartRec, $StopRec, "");
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		$ExportDoc->Text .= $sFooter;

		// Close recordset
		$rs->Close();

		// Export header and footer
		$ExportDoc->ExportHeaderAndFooter();

		// Clean output buffer
		if (!EW_DEBUG_ENABLED && ob_get_length())
			ob_end_clean();

		// Write debug message if enabled
		if (EW_DEBUG_ENABLED)
			echo ew_DebugMsg();

		// Output data
		$ExportDoc->Export();
	}

	// Write Audit Trail start/end for grid update
	function WriteAuditTrailDummy($typ) {
		$table = 'call';
	  $usr = CurrentUserName();
		ew_WriteAuditTrail("log", ew_StdCurrentDateTime(), ew_ScriptName(), $usr, $typ, $table, "", "", "", "");
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}

	// ListOptions Load event
	function ListOptions_Load() {

		// Example:
		//$opt = &$this->ListOptions->Add("new");
		//$opt->Header = "xxx";
		//$opt->OnLeft = TRUE; // Link on left
		//$opt->MoveTo(0); // Move to first column

	}

	// ListOptions Rendered event
	function ListOptions_Rendered() {

		// Example: 
		//$this->ListOptions->Items["new"]->Body = "xxx";

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($call_list)) $call_list = new ccall_list();

// Page init
$call_list->Page_Init();

// Page main
$call_list->Page_Main();
?>
<?php include_once "header.php" ?>
<?php if ($call->Export == "") { ?>
<script type="text/javascript">

// Page object
var call_list = new ew_Page("call_list");
call_list.PageID = "list"; // Page ID
var EW_PAGE_ID = call_list.PageID; // For backward compatibility

// Form object
var fcalllist = new ew_Form("fcalllist");

// Form_CustomValidate event
fcalllist.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fcalllist.ValidateRequired = true;
<?php } else { ?>
fcalllist.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fcalllist.Lists["x_callerId"] = {"LinkField":"x_id","Ajax":null,"AutoFill":false,"DisplayFields":["x_id","x_name","",""],"ParentFields":[],"FilterFields":[],"Options":[]};
fcalllist.Lists["x_operatorId"] = {"LinkField":"x_id","Ajax":null,"AutoFill":false,"DisplayFields":["x_id","x_name","",""],"ParentFields":[],"FilterFields":[],"Options":[]};
fcalllist.Lists["x_allocatedToId"] = {"LinkField":"x_id","Ajax":null,"AutoFill":false,"DisplayFields":["x_id","x_name","",""],"ParentFields":[],"FilterFields":[],"Options":[]};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php } ?>
<?php
	$bSelectLimit = EW_SELECT_LIMIT;
	if ($bSelectLimit) {
		$call_list->TotalRecs = $call->SelectRecordCount();
	} else {
		if ($call_list->Recordset = $call_list->LoadRecordset())
			$call_list->TotalRecs = $call_list->Recordset->RecordCount();
	}
	$call_list->StartRec = 1;
	if ($call_list->DisplayRecs <= 0 || ($call->Export <> "" && $call->ExportAll)) // Display all records
		$call_list->DisplayRecs = $call_list->TotalRecs;
	if (!($call->Export <> "" && $call->ExportAll))
		$call_list->SetUpStartRec(); // Set up start record position
	if ($bSelectLimit)
		$call_list->Recordset = $call_list->LoadRecordset($call_list->StartRec-1, $call_list->DisplayRecs);
?>
<p style="white-space: nowrap;"><span id="ewPageCaption" class="ewTitle ewTableTitle"><?php echo $Language->Phrase("TblTypeTABLE") ?><?php echo $call->TableCaption() ?>&nbsp;&nbsp;</span>
<?php $call_list->ExportOptions->Render("body"); ?>
</p>
<?php $call_list->ShowPageHeader(); ?>
<?php
$call_list->ShowMessage();
?>
<br>
<table cellspacing="0" class="ewGrid"><tr><td class="ewGridContent">
<form name="fcalllist" id="fcalllist" class="ewForm" action="" method="post">
<input type="hidden" name="t" value="call">
<div id="gmp_call" class="ewGridMiddlePanel">
<?php if ($call_list->TotalRecs > 0) { ?>
<table id="tbl_calllist" class="ewTable ewTableSeparate">
<?php echo $call->TableCustomInnerHtml ?>
<thead><!-- Table header -->
	<tr class="ewTableHeader">
<?php

// Render list options
$call_list->RenderListOptions();

// Render list options (header, left)
$call_list->ListOptions->Render("header", "left");
?>
<?php if ($call->id->Visible) { // id ?>
	<?php if ($call->SortUrl($call->id) == "") { ?>
		<td><span id="elh_call_id" class="call_id"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $call->id->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div onmousedown="ew_Sort(event,'<?php echo $call->SortUrl($call->id) ?>',1);"><span id="elh_call_id" class="call_id">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $call->id->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($call->id->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($call->id->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($call->callerId->Visible) { // callerId ?>
	<?php if ($call->SortUrl($call->callerId) == "") { ?>
		<td><span id="elh_call_callerId" class="call_callerId"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $call->callerId->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div onmousedown="ew_Sort(event,'<?php echo $call->SortUrl($call->callerId) ?>',1);"><span id="elh_call_callerId" class="call_callerId">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $call->callerId->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($call->callerId->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($call->callerId->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($call->operatorId->Visible) { // operatorId ?>
	<?php if ($call->SortUrl($call->operatorId) == "") { ?>
		<td><span id="elh_call_operatorId" class="call_operatorId"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $call->operatorId->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div onmousedown="ew_Sort(event,'<?php echo $call->SortUrl($call->operatorId) ?>',1);"><span id="elh_call_operatorId" class="call_operatorId">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $call->operatorId->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($call->operatorId->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($call->operatorId->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($call->priority->Visible) { // priority ?>
	<?php if ($call->SortUrl($call->priority) == "") { ?>
		<td><span id="elh_call_priority" class="call_priority"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $call->priority->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div onmousedown="ew_Sort(event,'<?php echo $call->SortUrl($call->priority) ?>',1);"><span id="elh_call_priority" class="call_priority">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $call->priority->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($call->priority->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($call->priority->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($call->allocatedToId->Visible) { // allocatedToId ?>
	<?php if ($call->SortUrl($call->allocatedToId) == "") { ?>
		<td><span id="elh_call_allocatedToId" class="call_allocatedToId"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $call->allocatedToId->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div onmousedown="ew_Sort(event,'<?php echo $call->SortUrl($call->allocatedToId) ?>',1);"><span id="elh_call_allocatedToId" class="call_allocatedToId">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $call->allocatedToId->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($call->allocatedToId->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($call->allocatedToId->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($call->timeOfCall->Visible) { // timeOfCall ?>
	<?php if ($call->SortUrl($call->timeOfCall) == "") { ?>
		<td><span id="elh_call_timeOfCall" class="call_timeOfCall"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $call->timeOfCall->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div onmousedown="ew_Sort(event,'<?php echo $call->SortUrl($call->timeOfCall) ?>',1);"><span id="elh_call_timeOfCall" class="call_timeOfCall">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $call->timeOfCall->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($call->timeOfCall->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($call->timeOfCall->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($call->callDescription->Visible) { // callDescription ?>
	<?php if ($call->SortUrl($call->callDescription) == "") { ?>
		<td><span id="elh_call_callDescription" class="call_callDescription"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $call->callDescription->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div onmousedown="ew_Sort(event,'<?php echo $call->SortUrl($call->callDescription) ?>',1);"><span id="elh_call_callDescription" class="call_callDescription">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $call->callDescription->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($call->callDescription->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($call->callDescription->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($call->status->Visible) { // status ?>
	<?php if ($call->SortUrl($call->status) == "") { ?>
		<td><span id="elh_call_status" class="call_status"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $call->status->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div onmousedown="ew_Sort(event,'<?php echo $call->SortUrl($call->status) ?>',1);"><span id="elh_call_status" class="call_status">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $call->status->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($call->status->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($call->status->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($call->solution->Visible) { // solution ?>
	<?php if ($call->SortUrl($call->solution) == "") { ?>
		<td><span id="elh_call_solution" class="call_solution"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $call->solution->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div onmousedown="ew_Sort(event,'<?php echo $call->SortUrl($call->solution) ?>',1);"><span id="elh_call_solution" class="call_solution">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $call->solution->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($call->solution->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($call->solution->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php

// Render list options (header, right)
$call_list->ListOptions->Render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
if ($call->ExportAll && $call->Export <> "") {
	$call_list->StopRec = $call_list->TotalRecs;
} else {

	// Set the last record to display
	if ($call_list->TotalRecs > $call_list->StartRec + $call_list->DisplayRecs - 1)
		$call_list->StopRec = $call_list->StartRec + $call_list->DisplayRecs - 1;
	else
		$call_list->StopRec = $call_list->TotalRecs;
}
$call_list->RecCnt = $call_list->StartRec - 1;
if ($call_list->Recordset && !$call_list->Recordset->EOF) {
	$call_list->Recordset->MoveFirst();
	if (!$bSelectLimit && $call_list->StartRec > 1)
		$call_list->Recordset->Move($call_list->StartRec - 1);
} elseif (!$call->AllowAddDeleteRow && $call_list->StopRec == 0) {
	$call_list->StopRec = $call->GridAddRowCount;
}

// Initialize aggregate
$call->RowType = EW_ROWTYPE_AGGREGATEINIT;
$call->ResetAttrs();
$call_list->RenderRow();
while ($call_list->RecCnt < $call_list->StopRec) {
	$call_list->RecCnt++;
	if (intval($call_list->RecCnt) >= intval($call_list->StartRec)) {
		$call_list->RowCnt++;

		// Set up key count
		$call_list->KeyCount = $call_list->RowIndex;

		// Init row class and style
		$call->ResetAttrs();
		$call->CssClass = "";
		if ($call->CurrentAction == "gridadd") {
		} else {
			$call_list->LoadRowValues($call_list->Recordset); // Load row values
		}
		$call->RowType = EW_ROWTYPE_VIEW; // Render view

		// Set up row id / data-rowindex
		$call->RowAttrs = array_merge($call->RowAttrs, array('data-rowindex'=>$call_list->RowCnt, 'id'=>'r' . $call_list->RowCnt . '_call', 'data-rowtype'=>$call->RowType));

		// Render row
		$call_list->RenderRow();

		// Render list options
		$call_list->RenderListOptions();
?>
	<tr<?php echo $call->RowAttributes() ?>>
<?php

// Render list options (body, left)
$call_list->ListOptions->Render("body", "left", $call_list->RowCnt);
?>
	<?php if ($call->id->Visible) { // id ?>
		<td<?php echo $call->id->CellAttributes() ?>><span id="el<?php echo $call_list->RowCnt ?>_call_id" class="call_id">
<span<?php echo $call->id->ViewAttributes() ?>>
<?php echo $call->id->ListViewValue() ?></span>
</span><a id="<?php echo $call_list->PageObjName . "_row_" . $call_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($call->callerId->Visible) { // callerId ?>
		<td<?php echo $call->callerId->CellAttributes() ?>><span id="el<?php echo $call_list->RowCnt ?>_call_callerId" class="call_callerId">
<span<?php echo $call->callerId->ViewAttributes() ?>>
<?php echo $call->callerId->ListViewValue() ?></span>
</span><a id="<?php echo $call_list->PageObjName . "_row_" . $call_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($call->operatorId->Visible) { // operatorId ?>
		<td<?php echo $call->operatorId->CellAttributes() ?>><span id="el<?php echo $call_list->RowCnt ?>_call_operatorId" class="call_operatorId">
<span<?php echo $call->operatorId->ViewAttributes() ?>>
<?php echo $call->operatorId->ListViewValue() ?></span>
</span><a id="<?php echo $call_list->PageObjName . "_row_" . $call_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($call->priority->Visible) { // priority ?>
		<td<?php echo $call->priority->CellAttributes() ?>><span id="el<?php echo $call_list->RowCnt ?>_call_priority" class="call_priority">
<span<?php echo $call->priority->ViewAttributes() ?>>
<?php echo $call->priority->ListViewValue() ?></span>
</span><a id="<?php echo $call_list->PageObjName . "_row_" . $call_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($call->allocatedToId->Visible) { // allocatedToId ?>
		<td<?php echo $call->allocatedToId->CellAttributes() ?>><span id="el<?php echo $call_list->RowCnt ?>_call_allocatedToId" class="call_allocatedToId">
<span<?php echo $call->allocatedToId->ViewAttributes() ?>>
<?php echo $call->allocatedToId->ListViewValue() ?></span>
</span><a id="<?php echo $call_list->PageObjName . "_row_" . $call_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($call->timeOfCall->Visible) { // timeOfCall ?>
		<td<?php echo $call->timeOfCall->CellAttributes() ?>><span id="el<?php echo $call_list->RowCnt ?>_call_timeOfCall" class="call_timeOfCall">
<span<?php echo $call->timeOfCall->ViewAttributes() ?>>
<?php echo $call->timeOfCall->ListViewValue() ?></span>
</span><a id="<?php echo $call_list->PageObjName . "_row_" . $call_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($call->callDescription->Visible) { // callDescription ?>
		<td<?php echo $call->callDescription->CellAttributes() ?>><span id="el<?php echo $call_list->RowCnt ?>_call_callDescription" class="call_callDescription">
<span<?php echo $call->callDescription->ViewAttributes() ?>>
<?php echo $call->callDescription->ListViewValue() ?></span>
</span><a id="<?php echo $call_list->PageObjName . "_row_" . $call_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($call->status->Visible) { // status ?>
		<td<?php echo $call->status->CellAttributes() ?>><span id="el<?php echo $call_list->RowCnt ?>_call_status" class="call_status">
<span<?php echo $call->status->ViewAttributes() ?>>
<?php echo $call->status->ListViewValue() ?></span>
</span><a id="<?php echo $call_list->PageObjName . "_row_" . $call_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($call->solution->Visible) { // solution ?>
		<td<?php echo $call->solution->CellAttributes() ?>><span id="el<?php echo $call_list->RowCnt ?>_call_solution" class="call_solution">
<span<?php echo $call->solution->ViewAttributes() ?>>
<?php echo $call->solution->ListViewValue() ?></span>
</span><a id="<?php echo $call_list->PageObjName . "_row_" . $call_list->RowCnt ?>"></a></td>
	<?php } ?>
<?php

// Render list options (body, right)
$call_list->ListOptions->Render("body", "right", $call_list->RowCnt);
?>
	</tr>
<?php
	}
	if ($call->CurrentAction <> "gridadd")
		$call_list->Recordset->MoveNext();
}
?>
</tbody>
</table>
<?php } ?>
<?php if ($call->CurrentAction == "") { ?>
<input type="hidden" name="a_list" id="a_list" value="">
<?php } ?>
</div>
</form>
<?php

// Close recordset
if ($call_list->Recordset)
	$call_list->Recordset->Close();
?>
<?php if ($call->Export == "") { ?>
<div class="ewGridLowerPanel">
<?php if ($call->CurrentAction <> "gridadd" && $call->CurrentAction <> "gridedit") { ?>
<form name="ewpagerform" id="ewpagerform" class="ewForm" action="<?php echo ew_CurrentPage() ?>">
<table class="ewPager"><tr><td>
<?php if (!isset($call_list->Pager)) $call_list->Pager = new cPrevNextPager($call_list->StartRec, $call_list->DisplayRecs, $call_list->TotalRecs) ?>
<?php if ($call_list->Pager->RecordCount > 0) { ?>
	<table cellspacing="0" class="ewStdTable"><tbody><tr><td><span class="phpmaker"><?php echo $Language->Phrase("Page") ?>&nbsp;</span></td>
<!--first page button-->
	<?php if ($call_list->Pager->FirstButton->Enabled) { ?>
	<td><a href="<?php echo $call_list->PageUrl() ?>start=<?php echo $call_list->Pager->FirstButton->Start ?>"><img src="phpimages/first.gif" alt="<?php echo $Language->Phrase("PagerFirst") ?>" width="16" height="16" style="border: 0;"></a></td>
	<?php } else { ?>
	<td><img src="phpimages/firstdisab.gif" alt="<?php echo $Language->Phrase("PagerFirst") ?>" width="16" height="16" style="border: 0;"></td>
	<?php } ?>
<!--previous page button-->
	<?php if ($call_list->Pager->PrevButton->Enabled) { ?>
	<td><a href="<?php echo $call_list->PageUrl() ?>start=<?php echo $call_list->Pager->PrevButton->Start ?>"><img src="phpimages/prev.gif" alt="<?php echo $Language->Phrase("PagerPrevious") ?>" width="16" height="16" style="border: 0;"></a></td>
	<?php } else { ?>
	<td><img src="phpimages/prevdisab.gif" alt="<?php echo $Language->Phrase("PagerPrevious") ?>" width="16" height="16" style="border: 0;"></td>
	<?php } ?>
<!--current page number-->
	<td><input type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" id="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $call_list->Pager->CurrentPage ?>" size="4"></td>
<!--next page button-->
	<?php if ($call_list->Pager->NextButton->Enabled) { ?>
	<td><a href="<?php echo $call_list->PageUrl() ?>start=<?php echo $call_list->Pager->NextButton->Start ?>"><img src="phpimages/next.gif" alt="<?php echo $Language->Phrase("PagerNext") ?>" width="16" height="16" style="border: 0;"></a></td>	
	<?php } else { ?>
	<td><img src="phpimages/nextdisab.gif" alt="<?php echo $Language->Phrase("PagerNext") ?>" width="16" height="16" style="border: 0;"></td>
	<?php } ?>
<!--last page button-->
	<?php if ($call_list->Pager->LastButton->Enabled) { ?>
	<td><a href="<?php echo $call_list->PageUrl() ?>start=<?php echo $call_list->Pager->LastButton->Start ?>"><img src="phpimages/last.gif" alt="<?php echo $Language->Phrase("PagerLast") ?>" width="16" height="16" style="border: 0;"></a></td>	
	<?php } else { ?>
	<td><img src="phpimages/lastdisab.gif" alt="<?php echo $Language->Phrase("PagerLast") ?>" width="16" height="16" style="border: 0;"></td>
	<?php } ?>
	<td><span class="phpmaker">&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $call_list->Pager->PageCount ?></span></td>
	</tr></tbody></table>
	</td>	
	<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td>
	<span class="phpmaker"><?php echo $Language->Phrase("Record") ?>&nbsp;<?php echo $call_list->Pager->FromIndex ?>&nbsp;<?php echo $Language->Phrase("To") ?>&nbsp;<?php echo $call_list->Pager->ToIndex ?>&nbsp;<?php echo $Language->Phrase("Of") ?>&nbsp;<?php echo $call_list->Pager->RecordCount ?></span>
<?php } else { ?>
	<?php if ($call_list->SearchWhere == "0=101") { ?>
	<span class="phpmaker"><?php echo $Language->Phrase("EnterSearchCriteria") ?></span>
	<?php } else { ?>
	<span class="phpmaker"><?php echo $Language->Phrase("NoRecord") ?></span>
	<?php } ?>
<?php } ?>
	</td>
</tr></table>
</form>
<?php } ?>
<span class="phpmaker">
<?php if ($Security->IsLoggedIn()) { ?>
<?php if ($call_list->AddUrl <> "") { ?>
<a class="ewGridLink" href="<?php echo $call_list->AddUrl ?>"><?php echo $Language->Phrase("AddLink") ?></a>&nbsp;&nbsp;
<?php } ?>
<?php if ($callAsset_grid->DetailAdd && $Security->IsLoggedIn()) { ?>
<a class="ewGridLink" href="<?php echo $call->GetAddUrl() . "?" . EW_TABLE_SHOW_DETAIL . "=callAsset" ?>"><?php echo $Language->Phrase("AddLink") ?>&nbsp;<?php echo $call->TableCaption() ?>/<?php echo $callAsset->TableCaption() ?></a>&nbsp;&nbsp;
<?php } ?>
<?php if ($callContact_grid->DetailAdd && $Security->IsLoggedIn()) { ?>
<a class="ewGridLink" href="<?php echo $call->GetAddUrl() . "?" . EW_TABLE_SHOW_DETAIL . "=callContact" ?>"><?php echo $Language->Phrase("AddLink") ?>&nbsp;<?php echo $call->TableCaption() ?>/<?php echo $callContact->TableCaption() ?></a>&nbsp;&nbsp;
<?php } ?>
<?php } ?>
</span>
</div>
<?php } ?>
</td></tr></table>
<?php if ($call->Export == "") { ?>
<script type="text/javascript">
fcalllist.Init();
</script>
<?php } ?>
<?php
$call_list->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<?php if ($call->Export == "") { ?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php } ?>
<?php include_once "footer.php" ?>
<?php
$call_list->Page_Terminate();
?>
