<?php

// id
// callerId
// operatorId
// priority
// allocatedToId
// timeOfCall
// callDescription
// status
// solution

?>
<?php if ($call->Visible) { ?>
<table cellspacing="0" id="t_call" class="ewGrid"><tr><td class="ewGridContent">
<div class="ewGridMiddlePanel">
<table id="tbl_callmaster" class="ewTable ewTableSeparate">
	<tbody>
<?php if ($call->id->Visible) { // id ?>
		<tr id="r_id">
			<td class="ewTableHeader"><table class="ewTableHeaderBtn"><tr><td><?php echo $call->id->FldCaption() ?></td></tr></table></td>
			<td<?php echo $call->id->CellAttributes() ?>><span id="el_call_id">
<span<?php echo $call->id->ViewAttributes() ?>>
<?php echo $call->id->ListViewValue() ?></span>
</span></td>
		</tr>
<?php } ?>
<?php if ($call->callerId->Visible) { // callerId ?>
		<tr id="r_callerId">
			<td class="ewTableHeader"><table class="ewTableHeaderBtn"><tr><td><?php echo $call->callerId->FldCaption() ?></td></tr></table></td>
			<td<?php echo $call->callerId->CellAttributes() ?>><span id="el_call_callerId">
<span<?php echo $call->callerId->ViewAttributes() ?>>
<?php echo $call->callerId->ListViewValue() ?></span>
</span></td>
		</tr>
<?php } ?>
<?php if ($call->operatorId->Visible) { // operatorId ?>
		<tr id="r_operatorId">
			<td class="ewTableHeader"><table class="ewTableHeaderBtn"><tr><td><?php echo $call->operatorId->FldCaption() ?></td></tr></table></td>
			<td<?php echo $call->operatorId->CellAttributes() ?>><span id="el_call_operatorId">
<span<?php echo $call->operatorId->ViewAttributes() ?>>
<?php echo $call->operatorId->ListViewValue() ?></span>
</span></td>
		</tr>
<?php } ?>
<?php if ($call->priority->Visible) { // priority ?>
		<tr id="r_priority">
			<td class="ewTableHeader"><table class="ewTableHeaderBtn"><tr><td><?php echo $call->priority->FldCaption() ?></td></tr></table></td>
			<td<?php echo $call->priority->CellAttributes() ?>><span id="el_call_priority">
<span<?php echo $call->priority->ViewAttributes() ?>>
<?php echo $call->priority->ListViewValue() ?></span>
</span></td>
		</tr>
<?php } ?>
<?php if ($call->allocatedToId->Visible) { // allocatedToId ?>
		<tr id="r_allocatedToId">
			<td class="ewTableHeader"><table class="ewTableHeaderBtn"><tr><td><?php echo $call->allocatedToId->FldCaption() ?></td></tr></table></td>
			<td<?php echo $call->allocatedToId->CellAttributes() ?>><span id="el_call_allocatedToId">
<span<?php echo $call->allocatedToId->ViewAttributes() ?>>
<?php echo $call->allocatedToId->ListViewValue() ?></span>
</span></td>
		</tr>
<?php } ?>
<?php if ($call->timeOfCall->Visible) { // timeOfCall ?>
		<tr id="r_timeOfCall">
			<td class="ewTableHeader"><table class="ewTableHeaderBtn"><tr><td><?php echo $call->timeOfCall->FldCaption() ?></td></tr></table></td>
			<td<?php echo $call->timeOfCall->CellAttributes() ?>><span id="el_call_timeOfCall">
<span<?php echo $call->timeOfCall->ViewAttributes() ?>>
<?php echo $call->timeOfCall->ListViewValue() ?></span>
</span></td>
		</tr>
<?php } ?>
<?php if ($call->callDescription->Visible) { // callDescription ?>
		<tr id="r_callDescription">
			<td class="ewTableHeader"><table class="ewTableHeaderBtn"><tr><td><?php echo $call->callDescription->FldCaption() ?></td></tr></table></td>
			<td<?php echo $call->callDescription->CellAttributes() ?>><span id="el_call_callDescription">
<span<?php echo $call->callDescription->ViewAttributes() ?>>
<?php echo $call->callDescription->ListViewValue() ?></span>
</span></td>
		</tr>
<?php } ?>
<?php if ($call->status->Visible) { // status ?>
		<tr id="r_status">
			<td class="ewTableHeader"><table class="ewTableHeaderBtn"><tr><td><?php echo $call->status->FldCaption() ?></td></tr></table></td>
			<td<?php echo $call->status->CellAttributes() ?>><span id="el_call_status">
<span<?php echo $call->status->ViewAttributes() ?>>
<?php echo $call->status->ListViewValue() ?></span>
</span></td>
		</tr>
<?php } ?>
<?php if ($call->solution->Visible) { // solution ?>
		<tr id="r_solution">
			<td class="ewTableHeader"><table class="ewTableHeaderBtn"><tr><td><?php echo $call->solution->FldCaption() ?></td></tr></table></td>
			<td<?php echo $call->solution->CellAttributes() ?>><span id="el_call_solution">
<span<?php echo $call->solution->ViewAttributes() ?>>
<?php echo $call->solution->ListViewValue() ?></span>
</span></td>
		</tr>
<?php } ?>
	</tbody>
</table>
</div>
</td></tr></table>
<br>
<?php } ?>
