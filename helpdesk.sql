-- --------------------------------------------------------
-- Host:                         213.171.200.83
-- Server version:               5.6.26-log - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.4999
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for information_schema


-- Dumping database structure for helpdesk
CREATE DATABASE IF NOT EXISTS `helpdesk` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `helpdesk`;


-- Dumping structure for table helpdesk.address
CREATE TABLE IF NOT EXISTS `address` (
  `postcode` varchar(50) DEFAULT NULL,
  `address1` varchar(50) DEFAULT NULL,
  `address2` varchar(50) DEFAULT NULL,
  `address3` varchar(50) DEFAULT NULL,
  `address4` varchar(50) DEFAULT NULL,
  KEY `Index 1` (`postcode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table helpdesk.address: ~5 rows (approximately)
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
REPLACE INTO `address` (`postcode`, `address1`, `address2`, `address3`, `address4`) VALUES
	('m33 2lx', 'Station Road', 'Anytown', 'Any county', 'Any Country'),
	('ng1 3fx', 'Any road', 'Big Town', NULL, NULL),
	('w1a 4qt', 'Oxford Street', 'Mayfair', 'London', 'Greater London'),
	('ng1 7qw', 'Swan Hill', 'Big Town', 'Big Area', 'Big County'),
	('al1 7tr', 'Main Street', 'Mainton', 'city', 'County');
/*!40000 ALTER TABLE `address` ENABLE KEYS */;


-- Dumping structure for table helpdesk.asset
CREATE TABLE IF NOT EXISTS `asset` (
  `serialNumber` int(11) NOT NULL DEFAULT '0',
  `assetDescription` text,
  `vendorId` int(11) DEFAULT NULL,
  PRIMARY KEY (`serialNumber`),
  UNIQUE KEY `serial_number` (`serialNumber`),
  KEY `FK_asset_vendor` (`vendorId`),
  CONSTRAINT `FK_asset_vendor` FOREIGN KEY (`vendorId`) REFERENCES `vendor` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table helpdesk.asset: ~6 rows (approximately)
/*!40000 ALTER TABLE `asset` DISABLE KEYS */;
REPLACE INTO `asset` (`serialNumber`, `assetDescription`, `vendorId`) VALUES
	(678, 'Type 2 bolier', 1),
	(56785, 'Type 7a Bolier', 2),
	(123456, 'Boiler1', 1),
	(234567, 'Boiler2', 2),
	(4561234, 'boiler3', 3),
	(8679458, 'Type 7a boiler', 2);
/*!40000 ALTER TABLE `asset` ENABLE KEYS */;


-- Dumping structure for table helpdesk.audittrail
CREATE TABLE IF NOT EXISTS `audittrail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datetime` datetime NOT NULL,
  `script` varchar(255) DEFAULT NULL,
  `user` varchar(255) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `table` varchar(255) DEFAULT NULL,
  `field` varchar(255) DEFAULT NULL,
  `keyvalue` longtext,
  `oldvalue` longtext,
  `newvalue` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=latin1;

-- Dumping data for table helpdesk.audittrail: ~110 rows (approximately)
/*!40000 ALTER TABLE `audittrail` DISABLE KEYS */;
REPLACE INTO `audittrail` (`id`, `datetime`, `script`, `user`, `action`, `table`, `field`, `keyvalue`, `oldvalue`, `newvalue`) VALUES
	(1, '2016-04-24 17:05:22', '/helpdesk/calledit.php', 'Joe Bloggs', 'U', 'call', 'priority', '1', NULL, '1'),
	(2, '2016-04-24 17:20:56', '/helpdesk/calledit.php', 'Joe Bloggs', 'U', 'call', 'status', '1', '0', '1'),
	(3, '2016-05-11 23:21:57', '/helpdesk/calledit.php', 'Joe Bloggs', 'U', 'call', 'solution', '1', NULL, 'test solution'),
	(4, '2016-05-20 05:33:01', '/stafflist.php', 'Joe Bloggs', 'A', 'staff', 'name', '4', '', 'Brian Griffen'),
	(5, '2016-05-20 05:33:01', '/stafflist.php', 'Joe Bloggs', 'A', 'staff', 'roleId', '4', '', '2'),
	(6, '2016-05-20 05:33:01', '/stafflist.php', 'Joe Bloggs', 'A', 'staff', 'active', '4', '', 'Y'),
	(7, '2016-05-20 05:33:01', '/stafflist.php', 'Joe Bloggs', 'A', 'staff', 'password', '4', '', NULL),
	(8, '2016-05-20 05:33:01', '/stafflist.php', 'Joe Bloggs', 'A', 'staff', 'telephone', '4', '', '98765432'),
	(9, '2016-05-20 05:33:01', '/stafflist.php', 'Joe Bloggs', 'A', 'staff', 'addressNo', '4', '', '16'),
	(10, '2016-05-20 05:33:01', '/stafflist.php', 'Joe Bloggs', 'A', 'staff', 'addressPostcode', '4', '', 'w1a 4qt'),
	(11, '2016-05-20 05:33:01', '/stafflist.php', 'Joe Bloggs', 'A', 'staff', 'id', '4', '', '4'),
	(12, '2016-05-20 05:34:50', '/stafflist.php', 'Joe Bloggs', 'A', 'staff', 'name', '5', '', 'Frank Spenser'),
	(13, '2016-05-20 05:34:50', '/stafflist.php', 'Joe Bloggs', 'A', 'staff', 'roleId', '5', '', '2'),
	(14, '2016-05-20 05:34:50', '/stafflist.php', 'Joe Bloggs', 'A', 'staff', 'active', '5', '', 'N'),
	(15, '2016-05-20 05:34:50', '/stafflist.php', 'Joe Bloggs', 'A', 'staff', 'password', '5', '', NULL),
	(16, '2016-05-20 05:34:50', '/stafflist.php', 'Joe Bloggs', 'A', 'staff', 'telephone', '5', '', '875468'),
	(17, '2016-05-20 05:34:50', '/stafflist.php', 'Joe Bloggs', 'A', 'staff', 'addressNo', '5', '', '25'),
	(18, '2016-05-20 05:34:50', '/stafflist.php', 'Joe Bloggs', 'A', 'staff', 'addressPostcode', '5', '', 'ng1 3fx'),
	(19, '2016-05-20 05:34:50', '/stafflist.php', 'Joe Bloggs', 'A', 'staff', 'id', '5', '', '5'),
	(20, '2016-05-20 05:38:10', '/stafflist.php', 'Joe Bloggs', 'U', 'staff', 'active', '5', 'N', 'Y'),
	(21, '2016-05-20 05:39:34', '/stafflist.php', 'Joe Bloggs', 'U', 'staff', 'password', '5', NULL, 'password'),
	(22, '2016-05-20 05:42:17', '/vendorlist.php', 'Joe Bloggs', 'U', 'vendor', 'vendorName', '2', 'Vailient', 'Vailent'),
	(23, '2016-05-20 05:42:17', '/vendorlist.php', 'Joe Bloggs', 'U', 'vendor', 'vendorDetails', '2', 'Vailient details', 'Vailent details'),
	(24, '2016-05-20 05:43:59', '/vendorlist.php', 'Joe Bloggs', 'A', 'vendor', 'vendorName', '4', '', 'Bosch'),
	(25, '2016-05-20 05:43:59', '/vendorlist.php', 'Joe Bloggs', 'A', 'vendor', 'vendorDetails', '4', '', 'German manufaturer'),
	(26, '2016-05-20 05:43:59', '/vendorlist.php', 'Joe Bloggs', 'A', 'vendor', 'id', '4', '', '4'),
	(27, '2016-05-20 05:48:47', '/assetlist.php', 'Joe Bloggs', 'A', 'asset', 'serialNumber', '0', '', '0'),
	(28, '2016-05-20 05:48:47', '/assetlist.php', 'Joe Bloggs', 'A', 'asset', 'assetDescription', '0', '', 'Type 7a boiler'),
	(29, '2016-05-20 05:48:47', '/assetlist.php', 'Joe Bloggs', 'A', 'asset', 'vendorId', '0', '', '2'),
	(30, '2016-05-20 05:55:17', '/assetlist.php', 'Joe Bloggs', 'A', 'asset', 'serialNumber', '56785', '', '56785'),
	(31, '2016-05-20 05:55:17', '/assetlist.php', 'Joe Bloggs', 'A', 'asset', 'assetDescription', '56785', '', 'Type 7a Bolier'),
	(32, '2016-05-20 05:55:17', '/assetlist.php', 'Joe Bloggs', 'A', 'asset', 'vendorId', '56785', '', '2'),
	(33, '2016-05-20 06:01:21', '/calledit.php', 'Joe Bloggs', 'U', 'call', 'allocatedToId', '1', '2', '1'),
	(34, '2016-05-20 06:19:52', '/helpdesk/callContactlist.php', 'Joe Bloggs', 'U', 'callContact', 'contactStaffId', '5', '3', '1'),
	(35, '2016-05-20 06:24:05', '/helpdesk/stafflist.php', 'Joe Bloggs', 'U', 'staff', 'active', '5', 'Y', 'N'),
	(36, '2016-05-20 06:24:40', '/helpdesk/stafflist.php', 'Joe Bloggs', 'A', 'staff', 'name', '6', '', 'Paul Riley'),
	(37, '2016-05-20 06:24:40', '/helpdesk/stafflist.php', 'Joe Bloggs', 'A', 'staff', 'roleId', '6', '', '1'),
	(38, '2016-05-20 06:24:40', '/helpdesk/stafflist.php', 'Joe Bloggs', 'A', 'staff', 'active', '6', '', 'Y'),
	(39, '2016-05-20 06:24:40', '/helpdesk/stafflist.php', 'Joe Bloggs', 'A', 'staff', 'password', '6', '', 'password'),
	(40, '2016-05-20 06:24:40', '/helpdesk/stafflist.php', 'Joe Bloggs', 'A', 'staff', 'telephone', '6', '', '6475895'),
	(41, '2016-05-20 06:24:40', '/helpdesk/stafflist.php', 'Joe Bloggs', 'A', 'staff', 'addressNo', '6', '', '7'),
	(42, '2016-05-20 06:24:40', '/helpdesk/stafflist.php', 'Joe Bloggs', 'A', 'staff', 'addressPostcode', '6', '', 'm33 2lx'),
	(43, '2016-05-20 06:24:40', '/helpdesk/stafflist.php', 'Joe Bloggs', 'A', 'staff', 'id', '6', '', '6'),
	(44, '2016-05-20 06:27:39', '/helpdesk/callContactlist.php', 'Joe Bloggs', 'U', 'callContact', 'contactStaffId', '5', '1', '6'),
	(45, '2016-05-20 06:28:54', '/helpdesk/callContactlist.php', 'Joe Bloggs', 'A', 'callContact', 'contactStaffId', '6', '', '1'),
	(46, '2016-05-20 06:28:54', '/helpdesk/callContactlist.php', 'Joe Bloggs', 'A', 'callContact', 'contactDescription', '6', '', 'call solved'),
	(47, '2016-05-20 06:28:54', '/helpdesk/callContactlist.php', 'Joe Bloggs', 'A', 'callContact', 'contactTime', '6', '', '2016-05-20 06:28:54'),
	(48, '2016-05-20 06:28:54', '/helpdesk/callContactlist.php', 'Joe Bloggs', 'A', 'callContact', 'callId', '6', '', '1'),
	(49, '2016-05-20 06:28:54', '/helpdesk/callContactlist.php', 'Joe Bloggs', 'A', 'callContact', 'id', '6', '', '6'),
	(50, '2016-05-20 06:29:37', '/helpdesk/calledit.php', 'Joe Bloggs', 'U', 'call', 'status', '1', '1', '2'),
	(51, '2016-05-20 06:32:00', '/helpdesk/calladd.php', 'Joe Bloggs', 'A', 'call', 'callerId', '2', '', '4'),
	(52, '2016-05-20 06:32:00', '/helpdesk/calladd.php', 'Joe Bloggs', 'A', 'call', 'operatorId', '2', '', '1'),
	(53, '2016-05-20 06:32:00', '/helpdesk/calladd.php', 'Joe Bloggs', 'A', 'call', 'priority', '2', '', '5'),
	(54, '2016-05-20 06:32:00', '/helpdesk/calladd.php', 'Joe Bloggs', 'A', 'call', 'allocatedToId', '2', '', '1'),
	(55, '2016-05-20 06:32:00', '/helpdesk/calladd.php', 'Joe Bloggs', 'A', 'call', 'timeOfCall', '2', '', '2016-05-20'),
	(56, '2016-05-20 06:32:00', '/helpdesk/calladd.php', 'Joe Bloggs', 'A', 'call', 'callDescription', '2', '', 'Bolier Stopped working'),
	(57, '2016-05-20 06:32:00', '/helpdesk/calladd.php', 'Joe Bloggs', 'A', 'call', 'status', '2', '', '1'),
	(58, '2016-05-20 06:32:00', '/helpdesk/calladd.php', 'Joe Bloggs', 'A', 'call', 'id', '2', '', '2'),
	(59, '2016-05-20 06:32:24', '/helpdesk/callAssetlist.php', 'Joe Bloggs', 'A', 'callAsset', 'assetSerialNumber', '2', '', '8679458'),
	(60, '2016-05-20 06:32:24', '/helpdesk/callAssetlist.php', 'Joe Bloggs', 'A', 'callAsset', 'callId', '2', '', '2'),
	(61, '2016-05-20 06:32:24', '/helpdesk/callAssetlist.php', 'Joe Bloggs', 'A', 'callAsset', 'Id', '2', '', '2'),
	(62, '2016-05-20 06:32:55', '/helpdesk/callAssetlist.php', 'Joe Bloggs', 'A', 'callAsset', 'assetSerialNumber', '3', '', '8679458'),
	(63, '2016-05-20 06:32:55', '/helpdesk/callAssetlist.php', 'Joe Bloggs', 'A', 'callAsset', 'callId', '3', '', '2'),
	(64, '2016-05-20 06:32:55', '/helpdesk/callAssetlist.php', 'Joe Bloggs', 'A', 'callAsset', 'Id', '3', '', '3'),
	(65, '2016-05-20 06:35:50', '/helpdesk/calledit.php', 'Joe Bloggs', 'U', 'call', 'callDescription', '2', 'Bolier Stopped working', 'Boilers Stopped working'),
	(66, '2016-05-20 11:20:26', '/stafflist.php', 'Joe Bloggs', 'A', 'staff', 'name', '7', '', 'test Staff'),
	(67, '2016-05-20 11:20:26', '/stafflist.php', 'Joe Bloggs', 'A', 'staff', 'roleId', '7', '', '1'),
	(68, '2016-05-20 11:20:26', '/stafflist.php', 'Joe Bloggs', 'A', 'staff', 'active', '7', '', 'N'),
	(69, '2016-05-20 11:20:26', '/stafflist.php', 'Joe Bloggs', 'A', 'staff', 'password', '7', '', NULL),
	(70, '2016-05-20 11:20:26', '/stafflist.php', 'Joe Bloggs', 'A', 'staff', 'telephone', '7', '', NULL),
	(71, '2016-05-20 11:20:26', '/stafflist.php', 'Joe Bloggs', 'A', 'staff', 'addressNo', '7', '', NULL),
	(72, '2016-05-20 11:20:26', '/stafflist.php', 'Joe Bloggs', 'A', 'staff', 'addressPostcode', '7', '', NULL),
	(73, '2016-05-20 11:20:26', '/stafflist.php', 'Joe Bloggs', 'A', 'staff', 'id', '7', '', '7'),
	(74, '2016-05-20 11:21:24', '/assetlist.php', 'Joe Bloggs', 'A', 'asset', 'serialNumber', '678', '', '678'),
	(75, '2016-05-20 11:21:24', '/assetlist.php', 'Joe Bloggs', 'A', 'asset', 'assetDescription', '678', '', 'Type 2 bolier'),
	(76, '2016-05-20 11:21:24', '/assetlist.php', 'Joe Bloggs', 'A', 'asset', 'vendorId', '678', '', '1'),
	(77, '2016-05-20 11:22:26', '/vendorlist.php', 'Joe Bloggs', 'A', 'vendor', 'vendorName', '5', '', 'Bullseye'),
	(78, '2016-05-20 11:22:26', '/vendorlist.php', 'Joe Bloggs', 'A', 'vendor', 'vendorDetails', '5', '', NULL),
	(79, '2016-05-20 11:22:26', '/vendorlist.php', 'Joe Bloggs', 'A', 'vendor', 'id', '5', '', '5'),
	(80, '2016-05-20 11:23:29', '/calladd.php', 'Joe Bloggs', 'A', 'call', 'callerId', '3', '', '1'),
	(81, '2016-05-20 11:23:29', '/calladd.php', 'Joe Bloggs', 'A', 'call', 'operatorId', '3', '', '6'),
	(82, '2016-05-20 11:23:29', '/calladd.php', 'Joe Bloggs', 'A', 'call', 'priority', '3', '', '4'),
	(83, '2016-05-20 11:23:29', '/calladd.php', 'Joe Bloggs', 'A', 'call', 'allocatedToId', '3', '', '1'),
	(84, '2016-05-20 11:23:29', '/calladd.php', 'Joe Bloggs', 'A', 'call', 'timeOfCall', '3', '', '2016-05-20'),
	(85, '2016-05-20 11:23:29', '/calladd.php', 'Joe Bloggs', 'A', 'call', 'callDescription', '3', '', 'This is a big problem. Nothing works'),
	(86, '2016-05-20 11:23:29', '/calladd.php', 'Joe Bloggs', 'A', 'call', 'status', '3', '', '1'),
	(87, '2016-05-20 11:23:29', '/calladd.php', 'Joe Bloggs', 'A', 'call', 'id', '3', '', '3'),
	(88, '2016-05-20 11:23:44', '/callAssetlist.php', 'Joe Bloggs', 'A', 'callAsset', 'assetSerialNumber', '4', '', '678'),
	(89, '2016-05-20 11:23:44', '/callAssetlist.php', 'Joe Bloggs', 'A', 'callAsset', 'callId', '4', '', '3'),
	(90, '2016-05-20 11:23:44', '/callAssetlist.php', 'Joe Bloggs', 'A', 'callAsset', 'Id', '4', '', '4'),
	(91, '2016-05-20 11:24:35', '/callContactlist.php', 'Joe Bloggs', 'A', 'callContact', 'contactStaffId', '7', '', '1'),
	(92, '2016-05-20 11:24:35', '/callContactlist.php', 'Joe Bloggs', 'A', 'callContact', 'contactDescription', '7', '', 'Call received and nothing is working'),
	(93, '2016-05-20 11:24:35', '/callContactlist.php', 'Joe Bloggs', 'A', 'callContact', 'contactTime', '7', '', '2016-05-20 11:24:35'),
	(94, '2016-05-20 11:24:35', '/callContactlist.php', 'Joe Bloggs', 'A', 'callContact', 'callId', '7', '', '3'),
	(95, '2016-05-20 11:24:35', '/callContactlist.php', 'Joe Bloggs', 'A', 'callContact', 'id', '7', '', '7'),
	(96, '2016-05-20 11:25:31', '/callContactlist.php', 'Joe Bloggs', 'A', 'callContact', 'contactStaffId', '8', '', '1'),
	(97, '2016-05-20 11:25:31', '/callContactlist.php', 'Joe Bloggs', 'A', 'callContact', 'contactDescription', '8', '', 'Check with manufacture - Our problem'),
	(98, '2016-05-20 11:25:31', '/callContactlist.php', 'Joe Bloggs', 'A', 'callContact', 'contactTime', '8', '', '2016-05-20 11:25:31'),
	(99, '2016-05-20 11:25:31', '/callContactlist.php', 'Joe Bloggs', 'A', 'callContact', 'callId', '8', '', '3'),
	(100, '2016-05-20 11:25:31', '/callContactlist.php', 'Joe Bloggs', 'A', 'callContact', 'id', '8', '', '8'),
	(101, '2016-05-20 11:25:55', '/callContactlist.php', 'Joe Bloggs', 'A', 'callContact', 'contactStaffId', '9', '', '6'),
	(102, '2016-05-20 11:25:55', '/callContactlist.php', 'Joe Bloggs', 'A', 'callContact', 'contactDescription', '9', '', 'Check with engineer - their problem'),
	(103, '2016-05-20 11:25:55', '/callContactlist.php', 'Joe Bloggs', 'A', 'callContact', 'contactTime', '9', '', '2016-05-20 11:25:55'),
	(104, '2016-05-20 11:25:55', '/callContactlist.php', 'Joe Bloggs', 'A', 'callContact', 'callId', '9', '', '3'),
	(105, '2016-05-20 11:25:55', '/callContactlist.php', 'Joe Bloggs', 'A', 'callContact', 'id', '9', '', '9'),
	(106, '2016-05-20 11:26:27', '/callContactlist.php', 'Joe Bloggs', 'A', 'callContact', 'contactStaffId', '10', '', '1'),
	(107, '2016-05-20 11:26:27', '/callContactlist.php', 'Joe Bloggs', 'A', 'callContact', 'contactDescription', '10', '', 'Are they all plugged in'),
	(108, '2016-05-20 11:26:27', '/callContactlist.php', 'Joe Bloggs', 'A', 'callContact', 'contactTime', '10', '', '2016-05-20 11:26:27'),
	(109, '2016-05-20 11:26:27', '/callContactlist.php', 'Joe Bloggs', 'A', 'callContact', 'callId', '10', '', '3'),
	(110, '2016-05-20 11:26:27', '/callContactlist.php', 'Joe Bloggs', 'A', 'callContact', 'id', '10', '', '10'),
	(111, '2016-05-20 15:21:34', '/calledit.php', 'Joe Bloggs', 'U', 'call', 'priority', '1', '1', '2'),
	(112, '2016-05-20 15:21:54', '/callContactlist.php', 'Joe Bloggs', 'A', 'callContact', 'contactStaffId', '11', '', '1'),
	(113, '2016-05-20 15:21:54', '/callContactlist.php', 'Joe Bloggs', 'A', 'callContact', 'contactDescription', '11', '', 'another contact'),
	(114, '2016-05-20 15:21:54', '/callContactlist.php', 'Joe Bloggs', 'A', 'callContact', 'contactTime', '11', '', '2016-05-20 15:21:54'),
	(115, '2016-05-20 15:21:54', '/callContactlist.php', 'Joe Bloggs', 'A', 'callContact', 'callId', '11', '', '1'),
	(116, '2016-05-20 15:21:54', '/callContactlist.php', 'Joe Bloggs', 'A', 'callContact', 'id', '11', '', '11');
/*!40000 ALTER TABLE `audittrail` ENABLE KEYS */;


-- Dumping structure for table helpdesk.call
CREATE TABLE IF NOT EXISTS `call` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `callerId` int(11) NOT NULL DEFAULT '0',
  `operatorId` int(11) NOT NULL DEFAULT '0',
  `allocatedToId` int(11) NOT NULL DEFAULT '0',
  `timeOfCall` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) DEFAULT '1',
  `callDescription` text,
  `solution` text,
  `priority` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_call_staff` (`callerId`),
  KEY `FK_call_staff_2` (`operatorId`),
  KEY `FK_call_staff_3` (`allocatedToId`),
  CONSTRAINT `FK_call_staff` FOREIGN KEY (`callerId`) REFERENCES `staff` (`id`),
  CONSTRAINT `FK_call_staff_2` FOREIGN KEY (`operatorId`) REFERENCES `staff` (`id`),
  CONSTRAINT `FK_call_staff_3` FOREIGN KEY (`allocatedToId`) REFERENCES `staff` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table helpdesk.call: ~3 rows (approximately)
/*!40000 ALTER TABLE `call` DISABLE KEYS */;
REPLACE INTO `call` (`id`, `callerId`, `operatorId`, `allocatedToId`, `timeOfCall`, `status`, `callDescription`, `solution`, `priority`) VALUES
	(1, 1, 1, 1, '2016-04-02 00:00:00', 2, 'Test call', 'test solution', 2),
	(2, 4, 1, 1, '2016-05-20 00:00:00', 1, 'Boilers Stopped working', NULL, 5),
	(3, 1, 6, 1, '2016-05-20 00:00:00', 1, 'This is a big problem. Nothing works', NULL, 4);
/*!40000 ALTER TABLE `call` ENABLE KEYS */;


-- Dumping structure for table helpdesk.callAsset
CREATE TABLE IF NOT EXISTS `callAsset` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `callId` int(11) DEFAULT '0',
  `assetSerialNumber` int(11) DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `FK_call_asset_call` (`callId`),
  KEY `FK_call_asset_asset` (`assetSerialNumber`),
  CONSTRAINT `FK_call_asset_call` FOREIGN KEY (`callId`) REFERENCES `call` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Dumping data for table helpdesk.callAsset: ~4 rows (approximately)
/*!40000 ALTER TABLE `callAsset` DISABLE KEYS */;
REPLACE INTO `callAsset` (`Id`, `callId`, `assetSerialNumber`) VALUES
	(1, 1, 234567),
	(2, 2, 8679458),
	(3, 2, 8679458),
	(4, 3, 678);
/*!40000 ALTER TABLE `callAsset` ENABLE KEYS */;


-- Dumping structure for table helpdesk.callContact
CREATE TABLE IF NOT EXISTS `callContact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contactDescription` text NOT NULL,
  `callId` int(11) DEFAULT '0',
  `contactTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `contactStaffId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_call_contact_call` (`callId`),
  KEY `FK_call_contact_staff` (`contactStaffId`),
  CONSTRAINT `FK_call_contact_call` FOREIGN KEY (`callId`) REFERENCES `call` (`id`),
  CONSTRAINT `FK_call_contact_staff` FOREIGN KEY (`contactStaffId`) REFERENCES `staff` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- Dumping data for table helpdesk.callContact: ~7 rows (approximately)
/*!40000 ALTER TABLE `callContact` DISABLE KEYS */;
REPLACE INTO `callContact` (`id`, `contactDescription`, `callId`, `contactTime`, `contactStaffId`) VALUES
	(4, 'test contact', 1, '2016-04-24 17:57:59', 1),
	(5, 'Test contact 2', 1, '2016-05-11 23:19:33', 6),
	(6, 'call solved', 1, '2016-05-20 06:28:54', 1),
	(7, 'Call received and nothing is working', 3, '2016-05-20 11:24:35', 1),
	(8, 'Check with manufacture - Our problem', 3, '2016-05-20 11:25:31', 1),
	(9, 'Check with engineer - their problem', 3, '2016-05-20 11:25:55', 6),
	(10, 'Are they all plugged in', 3, '2016-05-20 11:26:27', 1),
	(11, 'another contact', 1, '2016-05-20 15:21:54', 1);
/*!40000 ALTER TABLE `callContact` ENABLE KEYS */;


-- Dumping structure for view helpdesk.EnquiryCallAsset
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `EnquiryCallAsset` (
	`callId` INT(11) NULL,
	`assetSerialNumber` INT(11) NULL,
	`assetDescription` TEXT NULL COLLATE 'utf8_general_ci',
	`vendorName` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`vendorDetails` VARCHAR(250) NULL COLLATE 'utf8_general_ci'
) ENGINE=MyISAM;


-- Dumping structure for view helpdesk.EnquirycallContact
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `EnquirycallContact` (
	`callId` INT(11) NULL,
	`name` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`roleDescription` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`contactDescription` TEXT NOT NULL COLLATE 'utf8_general_ci',
	`contactTime` TIMESTAMP NULL
) ENGINE=MyISAM;


-- Dumping structure for view helpdesk.EnquiryMaster
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `EnquiryMaster` (
	`CallId` INT(11) NOT NULL,
	`Caller Details` CHAR(0) NOT NULL COLLATE 'utf8_general_ci',
	`CallerName` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`OperatorName` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`Problem Details` CHAR(0) NOT NULL COLLATE 'utf8_general_ci',
	`AllocatedTo` VARCHAR(50) NULL COLLATE 'utf8_general_ci',
	`timeOfCall` TIMESTAMP NOT NULL,
	`callDescription` TEXT NULL COLLATE 'utf8_general_ci',
	`Resolution Status` CHAR(0) NOT NULL COLLATE 'utf8_general_ci',
	`status` INT(11) NULL,
	`solution` TEXT NULL COLLATE 'utf8_general_ci',
	`CallerTel` VARCHAR(15) NULL COLLATE 'utf8_general_ci',
	`OperatorTel` VARCHAR(15) NULL COLLATE 'utf8_general_ci',
	`AllocatedTel` VARCHAR(15) NULL COLLATE 'utf8_general_ci',
	`Caller Id` INT(11) NOT NULL,
	`Operator Id` INT(11) NOT NULL,
	`Allocated To Id` INT(11) NOT NULL,
	`Priority` VARCHAR(50) NULL COLLATE 'latin1_swedish_ci'
) ENGINE=MyISAM;


-- Dumping structure for table helpdesk.priority
CREATE TABLE IF NOT EXISTS `priority` (
  `id` int(11) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  KEY `Index 1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table helpdesk.priority: ~5 rows (approximately)
/*!40000 ALTER TABLE `priority` DISABLE KEYS */;
REPLACE INTO `priority` (`id`, `description`) VALUES
	(1, 'Very Low'),
	(2, 'Low'),
	(3, 'Normal'),
	(4, 'High'),
	(5, 'Very High');
/*!40000 ALTER TABLE `priority` ENABLE KEYS */;


-- Dumping structure for table helpdesk.role
CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roleDescription` varchar(50) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table helpdesk.role: ~2 rows (approximately)
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
REPLACE INTO `role` (`id`, `roleDescription`) VALUES
	(1, 'Help Desk Operative'),
	(2, 'Boiler Engineer');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;


-- Dumping structure for table helpdesk.staff
CREATE TABLE IF NOT EXISTS `staff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `roleId` int(11) DEFAULT NULL,
  `active` enum('Y','N') DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `telephone` varchar(15) DEFAULT NULL,
  `addressNo` int(11) DEFAULT NULL,
  `addressPostcode` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_staff_role` (`roleId`),
  CONSTRAINT `FK_staff_role` FOREIGN KEY (`roleId`) REFERENCES `role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Dumping data for table helpdesk.staff: ~7 rows (approximately)
/*!40000 ALTER TABLE `staff` DISABLE KEYS */;
REPLACE INTO `staff` (`id`, `name`, `roleId`, `active`, `password`, `telephone`, `addressNo`, `addressPostcode`) VALUES
	(1, 'Joe Bloggs', 1, 'Y', 'password', '1234567', 1, 'm33 2lx'),
	(2, 'Jim Bowen', 2, 'Y', NULL, '1234567', 4, 'ng1 3fx'),
	(3, 'Peter Griffin', 1, 'N', NULL, '1234567', 16, 'w1a 4qt'),
	(4, 'Brian Griffen', 2, 'Y', NULL, '98765432', 16, 'w1a 4qt'),
	(5, 'Frank Spenser', 2, 'N', 'password', '875468', 25, 'ng1 3fx'),
	(6, 'Paul Riley', 1, 'Y', 'password', '6475895', 7, 'm33 2lx'),
	(7, 'test Staff', 1, 'N', NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `staff` ENABLE KEYS */;


-- Dumping structure for table helpdesk.vendor
CREATE TABLE IF NOT EXISTS `vendor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vendorName` varchar(50) DEFAULT NULL,
  `vendorDetails` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Dumping data for table helpdesk.vendor: ~5 rows (approximately)
/*!40000 ALTER TABLE `vendor` DISABLE KEYS */;
REPLACE INTO `vendor` (`id`, `vendorName`, `vendorDetails`) VALUES
	(1, 'Hotpoint', 'Hotpoint vendor details'),
	(2, 'Vailent', 'Vailent details'),
	(3, 'Gloworm', 'Gloworm details'),
	(4, 'Bosch', 'German manufaturer'),
	(5, 'Bullseye', NULL);
/*!40000 ALTER TABLE `vendor` ENABLE KEYS */;


-- Dumping structure for view helpdesk.EnquiryCallAsset
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `EnquiryCallAsset`;
CREATE ALGORITHM=UNDEFINED DEFINER=`helpdesk`@`%` SQL SECURITY DEFINER VIEW `EnquiryCallAsset` AS select `callAsset`.`callId` AS `callId`,`callAsset`.`assetSerialNumber` AS `assetSerialNumber`,`asset`.`assetDescription` AS `assetDescription`,`vendor`.`vendorName` AS `vendorName`,`vendor`.`vendorDetails` AS `vendorDetails` from ((`callAsset` left join `asset` on((`callAsset`.`assetSerialNumber` = `asset`.`serialNumber`))) left join `vendor` on((`asset`.`vendorId` = `vendor`.`id`)));


-- Dumping structure for view helpdesk.EnquirycallContact
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `EnquirycallContact`;
CREATE ALGORITHM=UNDEFINED DEFINER=`helpdesk`@`%` SQL SECURITY DEFINER VIEW `EnquirycallContact` AS select `callContact`.`callId` AS `callId`,`staff`.`name` AS `name`,`role`.`roleDescription` AS `roleDescription`,`callContact`.`contactDescription` AS `contactDescription`,`callContact`.`contactTime` AS `contactTime` from ((`callContact` left join `staff` on((`callContact`.`contactStaffId` = `staff`.`id`))) left join `role` on((`staff`.`roleId` = `role`.`id`)));


-- Dumping structure for view helpdesk.EnquiryMaster
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `EnquiryMaster`;
CREATE ALGORITHM=UNDEFINED DEFINER=`helpdesk`@`%` SQL SECURITY DEFINER VIEW `EnquiryMaster` AS select `call`.`id` AS `CallId`,'' AS `Caller Details`,`staff`.`name` AS `CallerName`,`staff1`.`name` AS `OperatorName`,'' AS `Problem Details`,`staff2`.`name` AS `AllocatedTo`,`call`.`timeOfCall` AS `timeOfCall`,`call`.`callDescription` AS `callDescription`,'' AS `Resolution Status`,`call`.`status` AS `status`,`call`.`solution` AS `solution`,`staff`.`telephone` AS `CallerTel`,`staff2`.`telephone` AS `OperatorTel`,`staff1`.`telephone` AS `AllocatedTel`,`call`.`callerId` AS `Caller Id`,`call`.`operatorId` AS `Operator Id`,`call`.`allocatedToId` AS `Allocated To Id`,`priority`.`description` AS `Priority` from ((((`call` join `staff` on((`call`.`callerId` = `staff`.`id`))) join `staff` `staff1` on((`call`.`operatorId` = `staff1`.`id`))) join `staff` `staff2` on((`call`.`allocatedToId` = `staff2`.`id`))) join `priority` on((`call`.`priority` = `priority`.`id`)));
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
