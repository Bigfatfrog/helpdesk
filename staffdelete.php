<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg9.php" ?>
<?php include_once "ewmysql9.php" ?>
<?php include_once "phpfn9.php" ?>
<?php include_once "staffinfo.php" ?>
<?php include_once "userfn9.php" ?>
<?php

//
// Page class
//

$staff_delete = NULL; // Initialize page object first

class cstaff_delete extends cstaff {

	// Page ID
	var $PageID = 'delete';

	// Project ID
	var $ProjectID = "{4F01D370-BA3A-4C91-9445-0AA347261657}";

	// Table name
	var $TableName = 'staff';

	// Page object name
	var $PageObjName = 'staff_delete';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}
	var $AuditTrailOnDelete = TRUE;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			$html .= "<p class=\"ewMessage\">" . $sMessage . "</p>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			$html .= "<table class=\"ewMessageTable\"><tr><td class=\"ewWarningIcon\"></td><td class=\"ewWarningMessage\">" . $sWarningMessage . "</td></tr></table>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			$html .= "<table class=\"ewMessageTable\"><tr><td class=\"ewSuccessIcon\"></td><td class=\"ewSuccessMessage\">" . $sSuccessMessage . "</td></tr></table>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			$html .= "<table class=\"ewMessageTable\"><tr><td class=\"ewErrorIcon\"></td><td class=\"ewErrorMessage\">" . $sErrorMessage . "</td></tr></table>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p class=\"phpmaker\">" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Fotoer exists, display
			echo "<p class=\"phpmaker\">" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language, $UserAgent;

		// User agent
		$UserAgent = ew_UserAgent();
		$GLOBALS["Page"] = &$this;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (staff)
		if (!isset($GLOBALS["staff"])) {
			$GLOBALS["staff"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["staff"];
		}

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'delete', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'staff', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect();
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if (!$Security->IsLoggedIn()) {
			$Security->SaveLastUrl();
			$this->Page_Terminate("login.php");
		}
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"];
		$this->id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $conn;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();
		$this->Page_Redirecting($url);

		 // Close connection
		$conn->Close();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}
	var $TotalRecs = 0;
	var $RecCnt;
	var $RecKeys = array();
	var $Recordset;
	var $StartRowCnt = 1;
	var $RowCnt = 0;

	//
	// Page main
	//
	function Page_Main() {
		global $Language;

		// Load key parameters
		$this->RecKeys = $this->GetRecordKeys(); // Load record keys
		$sFilter = $this->GetKeyFilter();
		if ($sFilter == "")
			$this->Page_Terminate("stafflist.php"); // Prevent SQL injection, return to list

		// Set up filter (SQL WHHERE clause) and get return SQL
		// SQL constructor in staff class, staffinfo.php

		$this->CurrentFilter = $sFilter;

		// Get action
		if (@$_POST["a_delete"] <> "") {
			$this->CurrentAction = $_POST["a_delete"];
		} else {
			$this->CurrentAction = "I"; // Display record
		}
		switch ($this->CurrentAction) {
			case "D": // Delete
				$this->SendEmail = TRUE; // Send email on delete success
				if ($this->DeleteRows()) { // delete rows
					if ($this->getSuccessMessage() == "")
						$this->setSuccessMessage($Language->Phrase("DeleteSuccess")); // Set up success message
					$this->Page_Terminate($this->getReturnUrl()); // Return to caller
				}
		}
	}

// No functions
	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {
		global $conn;

		// Call Recordset Selecting event
		$this->Recordset_Selecting($this->CurrentFilter);

		// Load List page SQL
		$sSql = $this->SelectSQL();
		if ($offset > -1 && $rowcnt > -1)
			$sSql .= " LIMIT $rowcnt OFFSET $offset";

		// Load recordset
		$rs = ew_LoadRecordset($sSql);

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $conn, $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		global $conn;
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->id->setDbValue($rs->fields('id'));
		$this->name->setDbValue($rs->fields('name'));
		$this->roleId->setDbValue($rs->fields('roleId'));
		$this->active->setDbValue($rs->fields('active'));
		$this->password->setDbValue($rs->fields('password'));
		$this->telephone->setDbValue($rs->fields('telephone'));
		$this->addressNo->setDbValue($rs->fields('addressNo'));
		$this->addressPostcode->setDbValue($rs->fields('addressPostcode'));
	}

	// Render row values based on field settings
	function RenderRow() {
		global $conn, $Security, $Language;
		global $gsLanguage;

		// Initialize URLs
		// Call Row_Rendering event

		$this->Row_Rendering();

		// Common render codes for all row types
		// id
		// name
		// roleId
		// active
		// password
		// telephone
		// addressNo
		// addressPostcode

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

			// id
			$this->id->ViewValue = $this->id->CurrentValue;
			$this->id->ViewCustomAttributes = "";

			// name
			$this->name->ViewValue = $this->name->CurrentValue;
			$this->name->ViewCustomAttributes = "";

			// roleId
			if (strval($this->roleId->CurrentValue) <> "") {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->roleId->CurrentValue, EW_DATATYPE_NUMBER);
			$sSqlWrk = "SELECT `id`, `roleDescription` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `role`";
			$sWhereWrk = "";
			if ($sFilterWrk <> "") {
				ew_AddFilter($sWhereWrk, $sFilterWrk);
			}
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
				$rswrk = $conn->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$this->roleId->ViewValue = $rswrk->fields('DispFld');
					$rswrk->Close();
				} else {
					$this->roleId->ViewValue = $this->roleId->CurrentValue;
				}
			} else {
				$this->roleId->ViewValue = NULL;
			}
			$this->roleId->ViewCustomAttributes = "";

			// active
			if (ew_ConvertToBool($this->active->CurrentValue)) {
				$this->active->ViewValue = $this->active->FldTagCaption(1) <> "" ? $this->active->FldTagCaption(1) : "Y";
			} else {
				$this->active->ViewValue = $this->active->FldTagCaption(2) <> "" ? $this->active->FldTagCaption(2) : "N";
			}
			$this->active->ViewCustomAttributes = "";

			// password
			$this->password->ViewValue = $this->password->CurrentValue;
			$this->password->ViewCustomAttributes = "";

			// telephone
			$this->telephone->ViewValue = $this->telephone->CurrentValue;
			$this->telephone->ViewCustomAttributes = "";

			// addressNo
			$this->addressNo->ViewValue = $this->addressNo->CurrentValue;
			$this->addressNo->ViewCustomAttributes = "";

			// addressPostcode
			if (strval($this->addressPostcode->CurrentValue) <> "") {
				$sFilterWrk = "`postcode`" . ew_SearchString("=", $this->addressPostcode->CurrentValue, EW_DATATYPE_STRING);
			$sSqlWrk = "SELECT `postcode`, `postcode` AS `DispFld`, `address1` AS `Disp2Fld`, `address2` AS `Disp3Fld`, `address3` AS `Disp4Fld` FROM `address`";
			$sWhereWrk = "";
			if ($sFilterWrk <> "") {
				ew_AddFilter($sWhereWrk, $sFilterWrk);
			}
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
				$rswrk = $conn->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$this->addressPostcode->ViewValue = $rswrk->fields('DispFld');
					$this->addressPostcode->ViewValue .= ew_ValueSeparator(1,$this->addressPostcode) . $rswrk->fields('Disp2Fld');
					$this->addressPostcode->ViewValue .= ew_ValueSeparator(2,$this->addressPostcode) . $rswrk->fields('Disp3Fld');
					$this->addressPostcode->ViewValue .= ew_ValueSeparator(3,$this->addressPostcode) . $rswrk->fields('Disp4Fld');
					$rswrk->Close();
				} else {
					$this->addressPostcode->ViewValue = $this->addressPostcode->CurrentValue;
				}
			} else {
				$this->addressPostcode->ViewValue = NULL;
			}
			$this->addressPostcode->ViewCustomAttributes = "";

			// id
			$this->id->LinkCustomAttributes = "";
			$this->id->HrefValue = "";
			$this->id->TooltipValue = "";

			// name
			$this->name->LinkCustomAttributes = "";
			$this->name->HrefValue = "";
			$this->name->TooltipValue = "";

			// roleId
			$this->roleId->LinkCustomAttributes = "";
			$this->roleId->HrefValue = "";
			$this->roleId->TooltipValue = "";

			// active
			$this->active->LinkCustomAttributes = "";
			$this->active->HrefValue = "";
			$this->active->TooltipValue = "";

			// password
			$this->password->LinkCustomAttributes = "";
			$this->password->HrefValue = "";
			$this->password->TooltipValue = "";

			// telephone
			$this->telephone->LinkCustomAttributes = "";
			$this->telephone->HrefValue = "";
			$this->telephone->TooltipValue = "";

			// addressNo
			$this->addressNo->LinkCustomAttributes = "";
			$this->addressNo->HrefValue = "";
			$this->addressNo->TooltipValue = "";

			// addressPostcode
			$this->addressPostcode->LinkCustomAttributes = "";
			$this->addressPostcode->HrefValue = "";
			$this->addressPostcode->TooltipValue = "";
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	//
	// Delete records based on current filter
	//
	function DeleteRows() {
		global $conn, $Language, $Security;
		$DeleteRows = TRUE;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = 'ew_ErrorFn';
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE) {
			return FALSE;
		} elseif ($rs->EOF) {
			$this->setFailureMessage($Language->Phrase("NoRecord")); // No record found
			$rs->Close();
			return FALSE;
		} else {
			$this->LoadRowValues($rs); // Load row values
		}
		$conn->BeginTrans();
		if ($this->AuditTrailOnDelete) $this->WriteAuditTrailDummy($Language->Phrase("BatchDeleteBegin")); // Batch delete begin

		// Clone old rows
		$rsold = ($rs) ? $rs->GetRows() : array();
		if ($rs)
			$rs->Close();

		// Call row deleting event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$DeleteRows = $this->Row_Deleting($row);
				if (!$DeleteRows) break;
			}
		}
		if ($DeleteRows) {
			$sKey = "";
			foreach ($rsold as $row) {
				$sThisKey = "";
				if ($sThisKey <> "") $sThisKey .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
				$sThisKey .= $row['id'];
				$conn->raiseErrorFn = 'ew_ErrorFn';
				$DeleteRows = $this->Delete($row); // Delete
				$conn->raiseErrorFn = '';
				if ($DeleteRows === FALSE)
					break;
				if ($sKey <> "") $sKey .= ", ";
				$sKey .= $sThisKey;
			}
		} else {

			// Set up error message
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("DeleteCancelled"));
			}
		}
		if ($DeleteRows) {
			$conn->CommitTrans(); // Commit the changes
			if ($DeleteRows) {
				foreach ($rsold as $row)
					$this->WriteAuditTrailOnDelete($row);
			}
			if ($this->AuditTrailOnDelete) $this->WriteAuditTrailDummy($Language->Phrase("BatchDeleteSuccess")); // Batch delete success
		} else {
			$conn->RollbackTrans(); // Rollback changes
			if ($this->AuditTrailOnDelete) $this->WriteAuditTrailDummy($Language->Phrase("BatchDeleteRollback")); // Batch delete rollback
		}

		// Call Row Deleted event
		if ($DeleteRows) {
			foreach ($rsold as $row) {
				$this->Row_Deleted($row);
			}
		}
		return $DeleteRows;
	}

	// Write Audit Trail start/end for grid update
	function WriteAuditTrailDummy($typ) {
		$table = 'staff';
	  $usr = CurrentUserName();
		ew_WriteAuditTrail("log", ew_StdCurrentDateTime(), ew_ScriptName(), $usr, $typ, $table, "", "", "", "");
	}

	// Write Audit Trail (delete page)
	function WriteAuditTrailOnDelete(&$rs) {
		if (!$this->AuditTrailOnDelete) return;
		$table = 'staff';

		// Get key value
		$key = "";
		if ($key <> "")
			$key .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
		$key .= $rs['id'];

		// Write Audit Trail
		$dt = ew_StdCurrentDateTime();
		$id = ew_ScriptName();
	  $curUser = CurrentUserName();
		foreach (array_keys($rs) as $fldname) {
			if (array_key_exists($fldname, $this->fields) && $this->fields[$fldname]->FldDataType <> EW_DATATYPE_BLOB) { // Ignore BLOB fields
				if ($this->fields[$fldname]->FldDataType == EW_DATATYPE_MEMO) {
					if (EW_AUDIT_TRAIL_TO_DATABASE)
						$oldvalue = $rs[$fldname];
					else
						$oldvalue = "[MEMO]"; // Memo field
				} elseif ($this->fields[$fldname]->FldDataType == EW_DATATYPE_XML) {
					$oldvalue = "[XML]"; // XML field
				} else {
					$oldvalue = $rs[$fldname];
				}
				ew_WriteAuditTrail("log", $dt, $id, $curUser, "D", $table, $fldname, $key, $oldvalue, "");
			}
		}
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($staff_delete)) $staff_delete = new cstaff_delete();

// Page init
$staff_delete->Page_Init();

// Page main
$staff_delete->Page_Main();
?>
<?php include_once "header.php" ?>
<script type="text/javascript">

// Page object
var staff_delete = new ew_Page("staff_delete");
staff_delete.PageID = "delete"; // Page ID
var EW_PAGE_ID = staff_delete.PageID; // For backward compatibility

// Form object
var fstaffdelete = new ew_Form("fstaffdelete");

// Form_CustomValidate event
fstaffdelete.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fstaffdelete.ValidateRequired = true;
<?php } else { ?>
fstaffdelete.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fstaffdelete.Lists["x_roleId"] = {"LinkField":"x_id","Ajax":null,"AutoFill":false,"DisplayFields":["x_roleDescription","","",""],"ParentFields":[],"FilterFields":[],"Options":[]};
fstaffdelete.Lists["x_addressPostcode"] = {"LinkField":"x_postcode","Ajax":null,"AutoFill":false,"DisplayFields":["x_postcode","x_address1","x_address2","x_address3"],"ParentFields":[],"FilterFields":[],"Options":[]};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php

// Load records for display
if ($staff_delete->Recordset = $staff_delete->LoadRecordset())
	$staff_deleteTotalRecs = $staff_delete->Recordset->RecordCount(); // Get record count
if ($staff_deleteTotalRecs <= 0) { // No record found, exit
	if ($staff_delete->Recordset)
		$staff_delete->Recordset->Close();
	$staff_delete->Page_Terminate("stafflist.php"); // Return to list
}
?>
<p><span id="ewPageCaption" class="ewTitle ewTableTitle"><?php echo $Language->Phrase("Delete") ?>&nbsp;<?php echo $Language->Phrase("TblTypeTABLE") ?><?php echo $staff->TableCaption() ?></span></p>
<p class="phpmaker"><a href="<?php echo $staff->getReturnUrl() ?>" id="a_GoBack" class="ewLink"><?php echo $Language->Phrase("GoBack") ?></a></p>
<?php $staff_delete->ShowPageHeader(); ?>
<?php
$staff_delete->ShowMessage();
?>
<form name="fstaffdelete" id="fstaffdelete" class="ewForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<br>
<input type="hidden" name="t" value="staff">
<input type="hidden" name="a_delete" id="a_delete" value="D">
<?php foreach ($staff_delete->RecKeys as $key) { ?>
<?php $keyvalue = is_array($key) ? implode($EW_COMPOSITE_KEY_SEPARATOR, $key) : $key; ?>
<input type="hidden" name="key_m[]" value="<?php echo ew_HtmlEncode($keyvalue) ?>">
<?php } ?>
<table cellspacing="0" class="ewGrid"><tr><td class="ewGridContent">
<div class="ewGridMiddlePanel">
<table id="tbl_staffdelete" class="ewTable ewTableSeparate">
<?php echo $staff->TableCustomInnerHtml ?>
	<thead>
	<tr class="ewTableHeader">
		<td><span id="elh_staff_id" class="staff_id"><table class="ewTableHeaderBtn"><tr><td><?php echo $staff->id->FldCaption() ?></td></tr></table></span></td>
		<td><span id="elh_staff_name" class="staff_name"><table class="ewTableHeaderBtn"><tr><td><?php echo $staff->name->FldCaption() ?></td></tr></table></span></td>
		<td><span id="elh_staff_roleId" class="staff_roleId"><table class="ewTableHeaderBtn"><tr><td><?php echo $staff->roleId->FldCaption() ?></td></tr></table></span></td>
		<td><span id="elh_staff_active" class="staff_active"><table class="ewTableHeaderBtn"><tr><td><?php echo $staff->active->FldCaption() ?></td></tr></table></span></td>
		<td><span id="elh_staff_password" class="staff_password"><table class="ewTableHeaderBtn"><tr><td><?php echo $staff->password->FldCaption() ?></td></tr></table></span></td>
		<td><span id="elh_staff_telephone" class="staff_telephone"><table class="ewTableHeaderBtn"><tr><td><?php echo $staff->telephone->FldCaption() ?></td></tr></table></span></td>
		<td><span id="elh_staff_addressNo" class="staff_addressNo"><table class="ewTableHeaderBtn"><tr><td><?php echo $staff->addressNo->FldCaption() ?></td></tr></table></span></td>
		<td><span id="elh_staff_addressPostcode" class="staff_addressPostcode"><table class="ewTableHeaderBtn"><tr><td><?php echo $staff->addressPostcode->FldCaption() ?></td></tr></table></span></td>
	</tr>
	</thead>
	<tbody>
<?php
$staff_delete->RecCnt = 0;
$i = 0;
while (!$staff_delete->Recordset->EOF) {
	$staff_delete->RecCnt++;
	$staff_delete->RowCnt++;

	// Set row properties
	$staff->ResetAttrs();
	$staff->RowType = EW_ROWTYPE_VIEW; // View

	// Get the field contents
	$staff_delete->LoadRowValues($staff_delete->Recordset);

	// Render row
	$staff_delete->RenderRow();
?>
	<tr<?php echo $staff->RowAttributes() ?>>
		<td<?php echo $staff->id->CellAttributes() ?>><span id="el<?php echo $staff_delete->RowCnt ?>_staff_id" class="staff_id">
<span<?php echo $staff->id->ViewAttributes() ?>>
<?php echo $staff->id->ListViewValue() ?></span>
</span></td>
		<td<?php echo $staff->name->CellAttributes() ?>><span id="el<?php echo $staff_delete->RowCnt ?>_staff_name" class="staff_name">
<span<?php echo $staff->name->ViewAttributes() ?>>
<?php echo $staff->name->ListViewValue() ?></span>
</span></td>
		<td<?php echo $staff->roleId->CellAttributes() ?>><span id="el<?php echo $staff_delete->RowCnt ?>_staff_roleId" class="staff_roleId">
<span<?php echo $staff->roleId->ViewAttributes() ?>>
<?php echo $staff->roleId->ListViewValue() ?></span>
</span></td>
		<td<?php echo $staff->active->CellAttributes() ?>><span id="el<?php echo $staff_delete->RowCnt ?>_staff_active" class="staff_active">
<span<?php echo $staff->active->ViewAttributes() ?>>
<?php if (ew_ConvertToBool($staff->active->CurrentValue)) { ?>
<input type="checkbox" value="<?php echo $staff->active->ListViewValue() ?>" checked="checked" onclick="this.form.reset();" disabled="disabled">
<?php } else { ?>
<input type="checkbox" value="<?php echo $staff->active->ListViewValue() ?>" onclick="this.form.reset();" disabled="disabled">
<?php } ?></span>
</span></td>
		<td<?php echo $staff->password->CellAttributes() ?>><span id="el<?php echo $staff_delete->RowCnt ?>_staff_password" class="staff_password">
<span<?php echo $staff->password->ViewAttributes() ?>>
<?php echo $staff->password->ListViewValue() ?></span>
</span></td>
		<td<?php echo $staff->telephone->CellAttributes() ?>><span id="el<?php echo $staff_delete->RowCnt ?>_staff_telephone" class="staff_telephone">
<span<?php echo $staff->telephone->ViewAttributes() ?>>
<?php echo $staff->telephone->ListViewValue() ?></span>
</span></td>
		<td<?php echo $staff->addressNo->CellAttributes() ?>><span id="el<?php echo $staff_delete->RowCnt ?>_staff_addressNo" class="staff_addressNo">
<span<?php echo $staff->addressNo->ViewAttributes() ?>>
<?php echo $staff->addressNo->ListViewValue() ?></span>
</span></td>
		<td<?php echo $staff->addressPostcode->CellAttributes() ?>><span id="el<?php echo $staff_delete->RowCnt ?>_staff_addressPostcode" class="staff_addressPostcode">
<span<?php echo $staff->addressPostcode->ViewAttributes() ?>>
<?php echo $staff->addressPostcode->ListViewValue() ?></span>
</span></td>
	</tr>
<?php
	$staff_delete->Recordset->MoveNext();
}
$staff_delete->Recordset->Close();
?>
</tbody>
</table>
</div>
</td></tr></table>
<br>
<input type="submit" name="Action" value="<?php echo ew_BtnCaption($Language->Phrase("DeleteBtn")) ?>">
</form>
<script type="text/javascript">
fstaffdelete.Init();
</script>
<?php
$staff_delete->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php include_once "footer.php" ?>
<?php
$staff_delete->Page_Terminate();
?>
