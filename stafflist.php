<?php
if (session_id() == "") session_start(); // Initialize Session data
ob_start(); // Turn on output buffering
?>
<?php include_once "ewcfg9.php" ?>
<?php include_once "ewmysql9.php" ?>
<?php include_once "phpfn9.php" ?>
<?php include_once "staffinfo.php" ?>
<?php include_once "userfn9.php" ?>
<?php

//
// Page class
//

$staff_list = NULL; // Initialize page object first

class cstaff_list extends cstaff {

	// Page ID
	var $PageID = 'list';

	// Project ID
	var $ProjectID = "{4F01D370-BA3A-4C91-9445-0AA347261657}";

	// Table name
	var $TableName = 'staff';

	// Page object name
	var $PageObjName = 'staff_list';

	// Page name
	function PageName() {
		return ew_CurrentPage();
	}

	// Page URL
	function PageUrl() {
		$PageUrl = ew_CurrentPage() . "?";
		if ($this->UseTokenInUrl) $PageUrl .= "t=" . $this->TableVar . "&"; // Add page token
		return $PageUrl;
	}

	// Page URLs
	var $AddUrl;
	var $EditUrl;
	var $CopyUrl;
	var $DeleteUrl;
	var $ViewUrl;
	var $ListUrl;

	// Export URLs
	var $ExportPrintUrl;
	var $ExportHtmlUrl;
	var $ExportExcelUrl;
	var $ExportWordUrl;
	var $ExportXmlUrl;
	var $ExportCsvUrl;
	var $ExportPdfUrl;

	// Update URLs
	var $InlineAddUrl;
	var $InlineCopyUrl;
	var $InlineEditUrl;
	var $GridAddUrl;
	var $GridEditUrl;
	var $MultiDeleteUrl;
	var $MultiUpdateUrl;
	var $AuditTrailOnAdd = TRUE;
	var $AuditTrailOnEdit = TRUE;
	var $AuditTrailOnDelete = TRUE;

	// Message
	function getMessage() {
		return @$_SESSION[EW_SESSION_MESSAGE];
	}

	function setMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_MESSAGE], $v);
	}

	function getFailureMessage() {
		return @$_SESSION[EW_SESSION_FAILURE_MESSAGE];
	}

	function setFailureMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_FAILURE_MESSAGE], $v);
	}

	function getSuccessMessage() {
		return @$_SESSION[EW_SESSION_SUCCESS_MESSAGE];
	}

	function setSuccessMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_SUCCESS_MESSAGE], $v);
	}

	function getWarningMessage() {
		return @$_SESSION[EW_SESSION_WARNING_MESSAGE];
	}

	function setWarningMessage($v) {
		ew_AddMessage($_SESSION[EW_SESSION_WARNING_MESSAGE], $v);
	}

	// Show message
	function ShowMessage() {
		$hidden = FALSE;
		$html = "";

		// Message
		$sMessage = $this->getMessage();
		$this->Message_Showing($sMessage, "");
		if ($sMessage <> "") { // Message in Session, display
			$html .= "<p class=\"ewMessage\">" . $sMessage . "</p>";
			$_SESSION[EW_SESSION_MESSAGE] = ""; // Clear message in Session
		}

		// Warning message
		$sWarningMessage = $this->getWarningMessage();
		$this->Message_Showing($sWarningMessage, "warning");
		if ($sWarningMessage <> "") { // Message in Session, display
			$html .= "<table class=\"ewMessageTable\"><tr><td class=\"ewWarningIcon\"></td><td class=\"ewWarningMessage\">" . $sWarningMessage . "</td></tr></table>";
			$_SESSION[EW_SESSION_WARNING_MESSAGE] = ""; // Clear message in Session
		}

		// Success message
		$sSuccessMessage = $this->getSuccessMessage();
		$this->Message_Showing($sSuccessMessage, "success");
		if ($sSuccessMessage <> "") { // Message in Session, display
			$html .= "<table class=\"ewMessageTable\"><tr><td class=\"ewSuccessIcon\"></td><td class=\"ewSuccessMessage\">" . $sSuccessMessage . "</td></tr></table>";
			$_SESSION[EW_SESSION_SUCCESS_MESSAGE] = ""; // Clear message in Session
		}

		// Failure message
		$sErrorMessage = $this->getFailureMessage();
		$this->Message_Showing($sErrorMessage, "failure");
		if ($sErrorMessage <> "") { // Message in Session, display
			$html .= "<table class=\"ewMessageTable\"><tr><td class=\"ewErrorIcon\"></td><td class=\"ewErrorMessage\">" . $sErrorMessage . "</td></tr></table>";
			$_SESSION[EW_SESSION_FAILURE_MESSAGE] = ""; // Clear message in Session
		}
		echo "<div class=\"ewMessageDialog\"" . (($hidden) ? " style=\"display: none;\"" : "") . ">" . $html . "</div>";
	}
	var $PageHeader;
	var $PageFooter;

	// Show Page Header
	function ShowPageHeader() {
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		if ($sHeader <> "") { // Header exists, display
			echo "<p class=\"phpmaker\">" . $sHeader . "</p>";
		}
	}

	// Show Page Footer
	function ShowPageFooter() {
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		if ($sFooter <> "") { // Fotoer exists, display
			echo "<p class=\"phpmaker\">" . $sFooter . "</p>";
		}
	}

	// Validate page request
	function IsPageRequest() {
		global $objForm;
		if ($this->UseTokenInUrl) {
			if ($objForm)
				return ($this->TableVar == $objForm->GetValue("t"));
			if (@$_GET["t"] <> "")
				return ($this->TableVar == $_GET["t"]);
		} else {
			return TRUE;
		}
	}

	//
	// Page class constructor
	//
	function __construct() {
		global $conn, $Language, $UserAgent;

		// User agent
		$UserAgent = ew_UserAgent();
		$GLOBALS["Page"] = &$this;

		// Language object
		if (!isset($Language)) $Language = new cLanguage();

		// Parent constuctor
		parent::__construct();

		// Table object (staff)
		if (!isset($GLOBALS["staff"])) {
			$GLOBALS["staff"] = &$this;
			$GLOBALS["Table"] = &$GLOBALS["staff"];
		}

		// Initialize URLs
		$this->ExportPrintUrl = $this->PageUrl() . "export=print";
		$this->ExportExcelUrl = $this->PageUrl() . "export=excel";
		$this->ExportWordUrl = $this->PageUrl() . "export=word";
		$this->ExportHtmlUrl = $this->PageUrl() . "export=html";
		$this->ExportXmlUrl = $this->PageUrl() . "export=xml";
		$this->ExportCsvUrl = $this->PageUrl() . "export=csv";
		$this->ExportPdfUrl = $this->PageUrl() . "export=pdf";
		$this->AddUrl = "staffadd.php";
		$this->InlineAddUrl = $this->PageUrl() . "a=add";
		$this->GridAddUrl = $this->PageUrl() . "a=gridadd";
		$this->GridEditUrl = $this->PageUrl() . "a=gridedit";
		$this->MultiDeleteUrl = "staffdelete.php";
		$this->MultiUpdateUrl = "staffupdate.php";

		// Page ID
		if (!defined("EW_PAGE_ID"))
			define("EW_PAGE_ID", 'list', TRUE);

		// Table name (for backward compatibility)
		if (!defined("EW_TABLE_NAME"))
			define("EW_TABLE_NAME", 'staff', TRUE);

		// Start timer
		if (!isset($GLOBALS["gTimer"])) $GLOBALS["gTimer"] = new cTimer();

		// Open connection
		if (!isset($conn)) $conn = ew_Connect();

		// List options
		$this->ListOptions = new cListOptions();
		$this->ListOptions->TableVar = $this->TableVar;

		// Export options
		$this->ExportOptions = new cListOptions();
		$this->ExportOptions->Tag = "span";
		$this->ExportOptions->TagClassName = "ewExportOption";
	}

	// 
	//  Page_Init
	//
	function Page_Init() {
		global $gsExport, $gsExportFile, $UserProfile, $Language, $Security, $objForm;

		// Security
		$Security = new cAdvancedSecurity();
		if (!$Security->IsLoggedIn()) $Security->AutoLogin();
		if (!$Security->IsLoggedIn()) {
			$Security->SaveLastUrl();
			$this->Page_Terminate("login.php");
		}

		// Create form object
		$objForm = new cFormObj();

		// Get export parameters
		if (@$_GET["export"] <> "") {
			$this->Export = $_GET["export"];
		} elseif (ew_IsHttpPost()) {
			if (@$_POST["exporttype"] <> "")
				$this->Export = $_POST["exporttype"];
		} else {
			$this->setExportReturnUrl(ew_CurrentUrl());
		}
		$gsExport = $this->Export; // Get export parameter, used in header
		$gsExportFile = $this->TableVar; // Get export file, used in header

		// Get grid add count
		$gridaddcnt = @$_GET[EW_TABLE_GRID_ADD_ROW_COUNT];
		if (is_numeric($gridaddcnt) && $gridaddcnt > 0)
			$this->GridAddRowCount = $gridaddcnt;

		// Set up list options
		$this->SetupListOptions();

		// Setup export options
		$this->SetupExportOptions();
		$this->CurrentAction = (@$_GET["a"] <> "") ? $_GET["a"] : @$_POST["a_list"];
		$this->id->Visible = !$this->IsAdd() && !$this->IsCopy() && !$this->IsGridAdd();

		// Global Page Loading event (in userfn*.php)
		Page_Loading();

		// Page Load event
		$this->Page_Load();
	}

	//
	// Page_Terminate
	//
	function Page_Terminate($url = "") {
		global $conn;

		// Page Unload event
		$this->Page_Unload();

		// Global Page Unloaded event (in userfn*.php)
		Page_Unloaded();
		$this->Page_Redirecting($url);

		 // Close connection
		$conn->Close();

		// Go to URL if specified
		if ($url <> "") {
			if (!EW_DEBUG_ENABLED && ob_get_length())
				ob_end_clean();
			header("Location: " . $url);
		}
		exit();
	}

	// Class variables
	var $ListOptions; // List options
	var $ExportOptions; // Export options
	var $DisplayRecs = 20;
	var $StartRec;
	var $StopRec;
	var $TotalRecs = 0;
	var $RecRange = 10;
	var $Pager;
	var $SearchWhere = ""; // Search WHERE clause
	var $RecCnt = 0; // Record count
	var $EditRowCnt;
	var $StartRowCnt = 1;
	var $RowCnt = 0;
	var $Attrs = array(); // Row attributes and cell attributes
	var $RowIndex = 0; // Row index
	var $KeyCount = 0; // Key count
	var $RowAction = ""; // Row action
	var $RowOldKey = ""; // Row old key (for copy)
	var $RecPerRow = 0;
	var $ColCnt = 0;
	var $DbMasterFilter = ""; // Master filter
	var $DbDetailFilter = ""; // Detail filter
	var $MasterRecordExists;	
	var $MultiSelectKey;
	var $Command;
	var $Recordset;
	var $OldRecordset;

	//
	// Page main
	//
	function Page_Main() {
		global $objForm, $Language, $gsFormError, $gsSearchError, $Security;

		// Search filters
		$sSrchAdvanced = ""; // Advanced search filter
		$sSrchBasic = ""; // Basic search filter
		$sFilter = "";

		// Get command
		$this->Command = strtolower(@$_GET["cmd"]);
		if ($this->IsPageRequest()) { // Validate request

			// Handle reset command
			$this->ResetCmd();

			// Check QueryString parameters
			if (@$_GET["a"] <> "") {
				$this->CurrentAction = $_GET["a"];

				// Clear inline mode
				if ($this->CurrentAction == "cancel")
					$this->ClearInlineMode();

				// Switch to inline edit mode
				if ($this->CurrentAction == "edit")
					$this->InlineEditMode();

				// Switch to inline add mode
				if ($this->CurrentAction == "add" || $this->CurrentAction == "copy")
					$this->InlineAddMode();
			} else {
				if (@$_POST["a_list"] <> "") {
					$this->CurrentAction = $_POST["a_list"]; // Get action

					// Inline Update
					if (($this->CurrentAction == "update" || $this->CurrentAction == "overwrite") && @$_SESSION[EW_SESSION_INLINE_MODE] == "edit")
						$this->InlineUpdate();

					// Insert Inline
					if ($this->CurrentAction == "insert" && @$_SESSION[EW_SESSION_INLINE_MODE] == "add")
						$this->InlineInsert();
				}
			}

			// Hide all options
			if ($this->Export <> "" ||
				$this->CurrentAction == "gridadd" ||
				$this->CurrentAction == "gridedit") {
				$this->ListOptions->HideAllOptions();
				$this->ExportOptions->HideAllOptions();
			}

			// Set up sorting order
			$this->SetUpSortOrder();
		}

		// Restore display records
		if ($this->getRecordsPerPage() <> "") {
			$this->DisplayRecs = $this->getRecordsPerPage(); // Restore from Session
		} else {
			$this->DisplayRecs = 20; // Load default
		}

		// Load Sorting Order
		$this->LoadSortOrder();

		// Build filter
		$sFilter = "";
		ew_AddFilter($sFilter, $this->DbDetailFilter);
		ew_AddFilter($sFilter, $this->SearchWhere);

		// Set up filter in session
		$this->setSessionWhere($sFilter);
		$this->CurrentFilter = "";

		// Export data only
		if (in_array($this->Export, array("html","word","excel","xml","csv","email","pdf"))) {
			$this->ExportData();
			if ($this->Export == "email")
				$this->Page_Terminate($this->ExportReturnUrl());
			else
				$this->Page_Terminate(); // Terminate response
			exit();
		}
	}

	//  Exit inline mode
	function ClearInlineMode() {
		$this->setKey("id", ""); // Clear inline edit key
		$this->LastAction = $this->CurrentAction; // Save last action
		$this->CurrentAction = ""; // Clear action
		$_SESSION[EW_SESSION_INLINE_MODE] = ""; // Clear inline mode
	}

	// Switch to Inline Edit mode
	function InlineEditMode() {
		global $Security, $Language;
		$bInlineEdit = TRUE;
		if (@$_GET["id"] <> "") {
			$this->id->setQueryStringValue($_GET["id"]);
		} else {
			$bInlineEdit = FALSE;
		}
		if ($bInlineEdit) {
			if ($this->LoadRow()) {
				$this->setKey("id", $this->id->CurrentValue); // Set up inline edit key
				$_SESSION[EW_SESSION_INLINE_MODE] = "edit"; // Enable inline edit
			}
		}
	}

	// Perform update to Inline Edit record
	function InlineUpdate() {
		global $Language, $objForm, $gsFormError;
		$objForm->Index = 1; 
		$this->LoadFormValues(); // Get form values

		// Validate form
		$bInlineUpdate = TRUE;
		if (!$this->ValidateForm()) {	
			$bInlineUpdate = FALSE; // Form error, reset action
			$this->setFailureMessage($gsFormError);
		} else {
			$bInlineUpdate = FALSE;
			$rowkey = strval($objForm->GetValue("k_key"));
			if ($this->SetupKeyValues($rowkey)) { // Set up key values
				if ($this->CheckInlineEditKey()) { // Check key
					$this->SendEmail = TRUE; // Send email on update success
					$bInlineUpdate = $this->EditRow(); // Update record
				} else {
					$bInlineUpdate = FALSE;
				}
			}
		}
		if ($bInlineUpdate) { // Update success
			if ($this->getSuccessMessage() == "")
				$this->setSuccessMessage($Language->Phrase("UpdateSuccess")); // Set up success message
			$this->ClearInlineMode(); // Clear inline edit mode
		} else {
			if ($this->getFailureMessage() == "")
				$this->setFailureMessage($Language->Phrase("UpdateFailed")); // Set update failed message
			$this->EventCancelled = TRUE; // Cancel event
			$this->CurrentAction = "edit"; // Stay in edit mode
		}
	}

	// Check Inline Edit key
	function CheckInlineEditKey() {

		//CheckInlineEditKey = True
		if (strval($this->getKey("id")) <> strval($this->id->CurrentValue))
			return FALSE;
		return TRUE;
	}

	// Switch to Inline Add mode
	function InlineAddMode() {
		global $Security, $Language;
		$this->CurrentAction = "add";
		$_SESSION[EW_SESSION_INLINE_MODE] = "add"; // Enable inline add
	}

	// Perform update to Inline Add/Copy record
	function InlineInsert() {
		global $Language, $objForm, $gsFormError;
		$this->LoadOldRecord(); // Load old recordset
		$objForm->Index = 0;
		$this->LoadFormValues(); // Get form values

		// Validate form
		if (!$this->ValidateForm()) {
			$this->setFailureMessage($gsFormError); // Set validation error message
			$this->EventCancelled = TRUE; // Set event cancelled
			$this->CurrentAction = "add"; // Stay in add mode
			return;
		}
		$this->SendEmail = TRUE; // Send email on add success
		if ($this->AddRow($this->OldRecordset)) { // Add record
			if ($this->getSuccessMessage() == "")
				$this->setSuccessMessage($Language->Phrase("AddSuccess")); // Set up add success message
			$this->ClearInlineMode(); // Clear inline add mode
		} else { // Add failed
			$this->EventCancelled = TRUE; // Set event cancelled
			$this->CurrentAction = "add"; // Stay in add mode
		}
	}

	// Build filter for all keys
	function BuildKeyFilter() {
		global $objForm;
		$sWrkFilter = "";

		// Update row index and get row key
		$rowindex = 1;
		$objForm->Index = $rowindex;
		$sThisKey = strval($objForm->GetValue("k_key"));
		while ($sThisKey <> "") {
			if ($this->SetupKeyValues($sThisKey)) {
				$sFilter = $this->KeyFilter();
				if ($sWrkFilter <> "") $sWrkFilter .= " OR ";
				$sWrkFilter .= $sFilter;
			} else {
				$sWrkFilter = "0=1";
				break;
			}

			// Update row index and get row key
			$rowindex++; // next row
			$objForm->Index = $rowindex;
			$sThisKey = strval($objForm->GetValue("k_key"));
		}
		return $sWrkFilter;
	}

	// Set up key values
	function SetupKeyValues($key) {
		$arrKeyFlds = explode($GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"], $key);
		if (count($arrKeyFlds) >= 1) {
			$this->id->setFormValue($arrKeyFlds[0]);
			if (!is_numeric($this->id->FormValue))
				return FALSE;
		}
		return TRUE;
	}

	// Set up sort parameters
	function SetUpSortOrder() {

		// Check for "order" parameter
		if (@$_GET["order"] <> "") {
			$this->CurrentOrder = ew_StripSlashes(@$_GET["order"]);
			$this->CurrentOrderType = @$_GET["ordertype"];
			$this->UpdateSort($this->id); // id
			$this->UpdateSort($this->name); // name
			$this->UpdateSort($this->roleId); // roleId
			$this->UpdateSort($this->active); // active
			$this->UpdateSort($this->password); // password
			$this->UpdateSort($this->telephone); // telephone
			$this->UpdateSort($this->addressNo); // addressNo
			$this->UpdateSort($this->addressPostcode); // addressPostcode
			$this->setStartRecordNumber(1); // Reset start position
		}
	}

	// Load sort order parameters
	function LoadSortOrder() {
		$sOrderBy = $this->getSessionOrderBy(); // Get ORDER BY from Session
		if ($sOrderBy == "") {
			if ($this->SqlOrderBy() <> "") {
				$sOrderBy = $this->SqlOrderBy();
				$this->setSessionOrderBy($sOrderBy);
			}
		}
	}

	// Reset command
	// cmd=reset (Reset search parameters)
	// cmd=resetall (Reset search and master/detail parameters)
	// cmd=resetsort (Reset sort parameters)
	function ResetCmd() {

		// Check if reset command
		if (substr($this->Command,0,5) == "reset") {

			// Reset sorting order
			if ($this->Command == "resetsort") {
				$sOrderBy = "";
				$this->setSessionOrderBy($sOrderBy);
				$this->id->setSort("");
				$this->name->setSort("");
				$this->roleId->setSort("");
				$this->active->setSort("");
				$this->password->setSort("");
				$this->telephone->setSort("");
				$this->addressNo->setSort("");
				$this->addressPostcode->setSort("");
			}

			// Reset start position
			$this->StartRec = 1;
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Set up list options
	function SetupListOptions() {
		global $Security, $Language;

		// "edit"
		$item = &$this->ListOptions->Add("edit");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->IsLoggedIn();
		$item->OnLeft = FALSE;

		// "copy"
		$item = &$this->ListOptions->Add("copy");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->IsLoggedIn() && ($this->CurrentAction == "add");
		$item->OnLeft = FALSE;

		// "delete"
		$item = &$this->ListOptions->Add("delete");
		$item->CssStyle = "white-space: nowrap;";
		$item->Visible = $Security->IsLoggedIn();
		$item->OnLeft = FALSE;

		// Call ListOptions_Load event
		$this->ListOptions_Load();
	}

	// Render list options
	function RenderListOptions() {
		global $Security, $Language, $objForm;
		$this->ListOptions->LoadDefault();

		// Set up row action and key
		if (is_numeric($this->RowIndex)) {
			$objForm->Index = $this->RowIndex;
			if ($this->RowAction <> "")
				$this->MultiSelectKey .= "<input type=\"hidden\" name=\"k" . $this->RowIndex . "_action\" id=\"k" . $this->RowIndex . "_action\" value=\"" . $this->RowAction . "\">";
			if ($this->RowAction == "delete") {
				$rowkey = $objForm->GetValue("k_key");
				$this->SetupKeyValues($rowkey);
			}
			if ($this->RowAction == "insert" && $this->CurrentAction == "F" && $this->EmptyRow())
				$this->MultiSelectKey .= "<input type=\"hidden\" name=\"k" . $this->RowIndex . "_blankrow\" id=\"k" . $this->RowIndex . "_blankrow\" value=\"1\">";
		}

		// "copy"
		$oListOpt = &$this->ListOptions->Items["copy"];
		if (($this->CurrentAction == "add" || $this->CurrentAction == "copy") && $this->RowType == EW_ROWTYPE_ADD) { // Inline Add/Copy
			$this->ListOptions->CustomItem = "copy"; // Show copy column only
			$oListOpt->Body = "<div" . (($oListOpt->OnLeft) ? " style=\"text-align: right\"" : "") . ">" .
				"<a class=\"ewGridLink\" href=\"\" onclick=\"return ewForms['fstafflist'].Submit();\">" . $Language->Phrase("InsertLink") . "</a>&nbsp;" .
				"<a class=\"ewGridLink\" href=\"" . $this->PageUrl() . "a=cancel\">" . $Language->Phrase("CancelLink") . "</a>" .
				"<input type=\"hidden\" name=\"a_list\" id=\"a_list\" value=\"insert\"></div>";
			return;
		}

		// "edit"
		$oListOpt = &$this->ListOptions->Items["edit"];
		if ($this->CurrentAction == "edit" && $this->RowType == EW_ROWTYPE_EDIT) { // Inline-Edit
			$this->ListOptions->CustomItem = "edit"; // Show edit column only
				$oListOpt->Body = "<div" . (($oListOpt->OnLeft) ? " style=\"text-align: right\"" : "") . ">" .
					"<a class=\"ewGridLink\" href=\"\" onclick=\"return ewForms['fstafflist'].Submit('" . ew_GetHashUrl($this->PageName(), $this->PageObjName . "_row_" . $this->RowCnt) . "');\">" . $Language->Phrase("UpdateLink") . "</a>&nbsp;" .
					"<a class=\"ewGridLink\" href=\"" . $this->PageUrl() . "a=cancel\">" . $Language->Phrase("CancelLink") . "</a>" .
					"<input type=\"hidden\" name=\"a_list\" id=\"a_list\" value=\"update\"></div>";
			$oListOpt->Body .= "<input type=\"hidden\" name=\"k" . $this->RowIndex . "_key\" id=\"k" . $this->RowIndex . "_key\" value=\"" . ew_HtmlEncode($this->id->CurrentValue) . "\">";
			return;
		}

		// "edit"
		$oListOpt = &$this->ListOptions->Items["edit"];
		if ($Security->IsLoggedIn()) {
			$oListOpt->Body .= "<a class=\"ewRowLink\" href=\"" . ew_GetHashUrl($this->InlineEditUrl, $this->PageObjName . "_row_" . $this->RowCnt) . "\">" . $Language->Phrase("InlineEditLink") . "</a>";
		}

		// "delete"
		$oListOpt = &$this->ListOptions->Items["delete"];
		if ($Security->IsLoggedIn())
			$oListOpt->Body = "<a class=\"ewRowLink\"" . "" . " href=\"" . $this->DeleteUrl . "\">" . $Language->Phrase("DeleteLink") . "</a>";
		$this->RenderListOptionsExt();

		// Call ListOptions_Rendered event
		$this->ListOptions_Rendered();
	}

	function RenderListOptionsExt() {
		global $Security, $Language;
	}

	// Set up starting record parameters
	function SetUpStartRec() {
		if ($this->DisplayRecs == 0)
			return;
		if ($this->IsPageRequest()) { // Validate request
			if (@$_GET[EW_TABLE_START_REC] <> "") { // Check for "start" parameter
				$this->StartRec = $_GET[EW_TABLE_START_REC];
				$this->setStartRecordNumber($this->StartRec);
			} elseif (@$_GET[EW_TABLE_PAGE_NO] <> "") {
				$PageNo = $_GET[EW_TABLE_PAGE_NO];
				if (is_numeric($PageNo)) {
					$this->StartRec = ($PageNo-1)*$this->DisplayRecs+1;
					if ($this->StartRec <= 0) {
						$this->StartRec = 1;
					} elseif ($this->StartRec >= intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1) {
						$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1;
					}
					$this->setStartRecordNumber($this->StartRec);
				}
			}
		}
		$this->StartRec = $this->getStartRecordNumber();

		// Check if correct start record counter
		if (!is_numeric($this->StartRec) || $this->StartRec == "") { // Avoid invalid start record counter
			$this->StartRec = 1; // Reset start record counter
			$this->setStartRecordNumber($this->StartRec);
		} elseif (intval($this->StartRec) > intval($this->TotalRecs)) { // Avoid starting record > total records
			$this->StartRec = intval(($this->TotalRecs-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to last page first record
			$this->setStartRecordNumber($this->StartRec);
		} elseif (($this->StartRec-1) % $this->DisplayRecs <> 0) {
			$this->StartRec = intval(($this->StartRec-1)/$this->DisplayRecs)*$this->DisplayRecs+1; // Point to page boundary
			$this->setStartRecordNumber($this->StartRec);
		}
	}

	// Load default values
	function LoadDefaultValues() {
		$this->id->CurrentValue = NULL;
		$this->id->OldValue = $this->id->CurrentValue;
		$this->name->CurrentValue = NULL;
		$this->name->OldValue = $this->name->CurrentValue;
		$this->roleId->CurrentValue = NULL;
		$this->roleId->OldValue = $this->roleId->CurrentValue;
		$this->active->CurrentValue = NULL;
		$this->active->OldValue = $this->active->CurrentValue;
		$this->password->CurrentValue = NULL;
		$this->password->OldValue = $this->password->CurrentValue;
		$this->telephone->CurrentValue = NULL;
		$this->telephone->OldValue = $this->telephone->CurrentValue;
		$this->addressNo->CurrentValue = NULL;
		$this->addressNo->OldValue = $this->addressNo->CurrentValue;
		$this->addressPostcode->CurrentValue = NULL;
		$this->addressPostcode->OldValue = $this->addressPostcode->CurrentValue;
	}

	// Load form values
	function LoadFormValues() {

		// Load from form
		global $objForm;
		if (!$this->id->FldIsDetailKey && $this->CurrentAction <> "gridadd" && $this->CurrentAction <> "add")
			$this->id->setFormValue($objForm->GetValue("x_id"));
		if (!$this->name->FldIsDetailKey) {
			$this->name->setFormValue($objForm->GetValue("x_name"));
		}
		if (!$this->roleId->FldIsDetailKey) {
			$this->roleId->setFormValue($objForm->GetValue("x_roleId"));
		}
		if (!$this->active->FldIsDetailKey) {
			$this->active->setFormValue($objForm->GetValue("x_active"));
		}
		if (!$this->password->FldIsDetailKey) {
			$this->password->setFormValue($objForm->GetValue("x_password"));
		}
		if (!$this->telephone->FldIsDetailKey) {
			$this->telephone->setFormValue($objForm->GetValue("x_telephone"));
		}
		if (!$this->addressNo->FldIsDetailKey) {
			$this->addressNo->setFormValue($objForm->GetValue("x_addressNo"));
		}
		if (!$this->addressPostcode->FldIsDetailKey) {
			$this->addressPostcode->setFormValue($objForm->GetValue("x_addressPostcode"));
		}
	}

	// Restore form values
	function RestoreFormValues() {
		global $objForm;
		if ($this->CurrentAction <> "gridadd" && $this->CurrentAction <> "add")
			$this->id->CurrentValue = $this->id->FormValue;
		$this->name->CurrentValue = $this->name->FormValue;
		$this->roleId->CurrentValue = $this->roleId->FormValue;
		$this->active->CurrentValue = $this->active->FormValue;
		$this->password->CurrentValue = $this->password->FormValue;
		$this->telephone->CurrentValue = $this->telephone->FormValue;
		$this->addressNo->CurrentValue = $this->addressNo->FormValue;
		$this->addressPostcode->CurrentValue = $this->addressPostcode->FormValue;
	}

	// Load recordset
	function LoadRecordset($offset = -1, $rowcnt = -1) {
		global $conn;

		// Call Recordset Selecting event
		$this->Recordset_Selecting($this->CurrentFilter);

		// Load List page SQL
		$sSql = $this->SelectSQL();
		if ($offset > -1 && $rowcnt > -1)
			$sSql .= " LIMIT $rowcnt OFFSET $offset";

		// Load recordset
		$rs = ew_LoadRecordset($sSql);

		// Call Recordset Selected event
		$this->Recordset_Selected($rs);
		return $rs;
	}

	// Load row based on key values
	function LoadRow() {
		global $conn, $Security, $Language;
		$sFilter = $this->KeyFilter();

		// Call Row Selecting event
		$this->Row_Selecting($sFilter);

		// Load SQL based on filter
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$res = FALSE;
		$rs = ew_LoadRecordset($sSql);
		if ($rs && !$rs->EOF) {
			$res = TRUE;
			$this->LoadRowValues($rs); // Load row values
			$rs->Close();
		}
		return $res;
	}

	// Load row values from recordset
	function LoadRowValues(&$rs) {
		global $conn;
		if (!$rs || $rs->EOF) return;

		// Call Row Selected event
		$row = &$rs->fields;
		$this->Row_Selected($row);
		$this->id->setDbValue($rs->fields('id'));
		$this->name->setDbValue($rs->fields('name'));
		$this->roleId->setDbValue($rs->fields('roleId'));
		$this->active->setDbValue($rs->fields('active'));
		$this->password->setDbValue($rs->fields('password'));
		$this->telephone->setDbValue($rs->fields('telephone'));
		$this->addressNo->setDbValue($rs->fields('addressNo'));
		$this->addressPostcode->setDbValue($rs->fields('addressPostcode'));
	}

	// Load old record
	function LoadOldRecord() {

		// Load key values from Session
		$bValidKey = TRUE;
		if (strval($this->getKey("id")) <> "")
			$this->id->CurrentValue = $this->getKey("id"); // id
		else
			$bValidKey = FALSE;

		// Load old recordset
		if ($bValidKey) {
			$this->CurrentFilter = $this->KeyFilter();
			$sSql = $this->SQL();
			$this->OldRecordset = ew_LoadRecordset($sSql);
			$this->LoadRowValues($this->OldRecordset); // Load row values
		} else {
			$this->OldRecordset = NULL;
		}
		return $bValidKey;
	}

	// Render row values based on field settings
	function RenderRow() {
		global $conn, $Security, $Language;
		global $gsLanguage;

		// Initialize URLs
		$this->ViewUrl = $this->GetViewUrl();
		$this->EditUrl = $this->GetEditUrl();
		$this->InlineEditUrl = $this->GetInlineEditUrl();
		$this->CopyUrl = $this->GetCopyUrl();
		$this->InlineCopyUrl = $this->GetInlineCopyUrl();
		$this->DeleteUrl = $this->GetDeleteUrl();

		// Call Row_Rendering event
		$this->Row_Rendering();

		// Common render codes for all row types
		// id
		// name
		// roleId
		// active
		// password
		// telephone
		// addressNo
		// addressPostcode

		if ($this->RowType == EW_ROWTYPE_VIEW) { // View row

			// id
			$this->id->ViewValue = $this->id->CurrentValue;
			$this->id->ViewCustomAttributes = "";

			// name
			$this->name->ViewValue = $this->name->CurrentValue;
			$this->name->ViewCustomAttributes = "";

			// roleId
			if (strval($this->roleId->CurrentValue) <> "") {
				$sFilterWrk = "`id`" . ew_SearchString("=", $this->roleId->CurrentValue, EW_DATATYPE_NUMBER);
			$sSqlWrk = "SELECT `id`, `roleDescription` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld` FROM `role`";
			$sWhereWrk = "";
			if ($sFilterWrk <> "") {
				ew_AddFilter($sWhereWrk, $sFilterWrk);
			}
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
				$rswrk = $conn->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$this->roleId->ViewValue = $rswrk->fields('DispFld');
					$rswrk->Close();
				} else {
					$this->roleId->ViewValue = $this->roleId->CurrentValue;
				}
			} else {
				$this->roleId->ViewValue = NULL;
			}
			$this->roleId->ViewCustomAttributes = "";

			// active
			if (ew_ConvertToBool($this->active->CurrentValue)) {
				$this->active->ViewValue = $this->active->FldTagCaption(1) <> "" ? $this->active->FldTagCaption(1) : "Y";
			} else {
				$this->active->ViewValue = $this->active->FldTagCaption(2) <> "" ? $this->active->FldTagCaption(2) : "N";
			}
			$this->active->ViewCustomAttributes = "";

			// password
			$this->password->ViewValue = $this->password->CurrentValue;
			$this->password->ViewCustomAttributes = "";

			// telephone
			$this->telephone->ViewValue = $this->telephone->CurrentValue;
			$this->telephone->ViewCustomAttributes = "";

			// addressNo
			$this->addressNo->ViewValue = $this->addressNo->CurrentValue;
			$this->addressNo->ViewCustomAttributes = "";

			// addressPostcode
			if (strval($this->addressPostcode->CurrentValue) <> "") {
				$sFilterWrk = "`postcode`" . ew_SearchString("=", $this->addressPostcode->CurrentValue, EW_DATATYPE_STRING);
			$sSqlWrk = "SELECT `postcode`, `postcode` AS `DispFld`, `address1` AS `Disp2Fld`, `address2` AS `Disp3Fld`, `address3` AS `Disp4Fld` FROM `address`";
			$sWhereWrk = "";
			if ($sFilterWrk <> "") {
				ew_AddFilter($sWhereWrk, $sFilterWrk);
			}
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
				$rswrk = $conn->Execute($sSqlWrk);
				if ($rswrk && !$rswrk->EOF) { // Lookup values found
					$this->addressPostcode->ViewValue = $rswrk->fields('DispFld');
					$this->addressPostcode->ViewValue .= ew_ValueSeparator(1,$this->addressPostcode) . $rswrk->fields('Disp2Fld');
					$this->addressPostcode->ViewValue .= ew_ValueSeparator(2,$this->addressPostcode) . $rswrk->fields('Disp3Fld');
					$this->addressPostcode->ViewValue .= ew_ValueSeparator(3,$this->addressPostcode) . $rswrk->fields('Disp4Fld');
					$rswrk->Close();
				} else {
					$this->addressPostcode->ViewValue = $this->addressPostcode->CurrentValue;
				}
			} else {
				$this->addressPostcode->ViewValue = NULL;
			}
			$this->addressPostcode->ViewCustomAttributes = "";

			// id
			$this->id->LinkCustomAttributes = "";
			$this->id->HrefValue = "";
			$this->id->TooltipValue = "";

			// name
			$this->name->LinkCustomAttributes = "";
			$this->name->HrefValue = "";
			$this->name->TooltipValue = "";

			// roleId
			$this->roleId->LinkCustomAttributes = "";
			$this->roleId->HrefValue = "";
			$this->roleId->TooltipValue = "";

			// active
			$this->active->LinkCustomAttributes = "";
			$this->active->HrefValue = "";
			$this->active->TooltipValue = "";

			// password
			$this->password->LinkCustomAttributes = "";
			$this->password->HrefValue = "";
			$this->password->TooltipValue = "";

			// telephone
			$this->telephone->LinkCustomAttributes = "";
			$this->telephone->HrefValue = "";
			$this->telephone->TooltipValue = "";

			// addressNo
			$this->addressNo->LinkCustomAttributes = "";
			$this->addressNo->HrefValue = "";
			$this->addressNo->TooltipValue = "";

			// addressPostcode
			$this->addressPostcode->LinkCustomAttributes = "";
			$this->addressPostcode->HrefValue = "";
			$this->addressPostcode->TooltipValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_ADD) { // Add row

			// id
			// name

			$this->name->EditCustomAttributes = "";
			$this->name->EditValue = ew_HtmlEncode($this->name->CurrentValue);

			// roleId
			$this->roleId->EditCustomAttributes = "";
			$sFilterWrk = "";
			$sSqlWrk = "SELECT `id`, `roleDescription` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `role`";
			$sWhereWrk = "";
			if ($sFilterWrk <> "") {
				ew_AddFilter($sWhereWrk, $sFilterWrk);
			}
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = $conn->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->roleId->EditValue = $arwrk;

			// active
			$this->active->EditCustomAttributes = "";
			$arwrk = array();
			$arwrk[] = array($this->active->FldTagValue(1), $this->active->FldTagCaption(1) <> "" ? $this->active->FldTagCaption(1) : $this->active->FldTagValue(1));
			$arwrk[] = array($this->active->FldTagValue(2), $this->active->FldTagCaption(2) <> "" ? $this->active->FldTagCaption(2) : $this->active->FldTagValue(2));
			$this->active->EditValue = $arwrk;

			// password
			$this->password->EditCustomAttributes = "";
			$this->password->EditValue = ew_HtmlEncode($this->password->CurrentValue);

			// telephone
			$this->telephone->EditCustomAttributes = "";
			$this->telephone->EditValue = ew_HtmlEncode($this->telephone->CurrentValue);

			// addressNo
			$this->addressNo->EditCustomAttributes = "";
			$this->addressNo->EditValue = ew_HtmlEncode($this->addressNo->CurrentValue);

			// addressPostcode
			$this->addressPostcode->EditCustomAttributes = "";
			$sFilterWrk = "";
			$sSqlWrk = "SELECT `postcode`, `postcode` AS `DispFld`, `address1` AS `Disp2Fld`, `address2` AS `Disp3Fld`, `address3` AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `address`";
			$sWhereWrk = "";
			if ($sFilterWrk <> "") {
				ew_AddFilter($sWhereWrk, $sFilterWrk);
			}
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = $conn->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->addressPostcode->EditValue = $arwrk;

			// Edit refer script
			// id

			$this->id->HrefValue = "";

			// name
			$this->name->HrefValue = "";

			// roleId
			$this->roleId->HrefValue = "";

			// active
			$this->active->HrefValue = "";

			// password
			$this->password->HrefValue = "";

			// telephone
			$this->telephone->HrefValue = "";

			// addressNo
			$this->addressNo->HrefValue = "";

			// addressPostcode
			$this->addressPostcode->HrefValue = "";
		} elseif ($this->RowType == EW_ROWTYPE_EDIT) { // Edit row

			// id
			$this->id->EditCustomAttributes = "";
			$this->id->EditValue = $this->id->CurrentValue;
			$this->id->ViewCustomAttributes = "";

			// name
			$this->name->EditCustomAttributes = "";
			$this->name->EditValue = ew_HtmlEncode($this->name->CurrentValue);

			// roleId
			$this->roleId->EditCustomAttributes = "";
			$sFilterWrk = "";
			$sSqlWrk = "SELECT `id`, `roleDescription` AS `DispFld`, '' AS `Disp2Fld`, '' AS `Disp3Fld`, '' AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `role`";
			$sWhereWrk = "";
			if ($sFilterWrk <> "") {
				ew_AddFilter($sWhereWrk, $sFilterWrk);
			}
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = $conn->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->roleId->EditValue = $arwrk;

			// active
			$this->active->EditCustomAttributes = "";
			$arwrk = array();
			$arwrk[] = array($this->active->FldTagValue(1), $this->active->FldTagCaption(1) <> "" ? $this->active->FldTagCaption(1) : $this->active->FldTagValue(1));
			$arwrk[] = array($this->active->FldTagValue(2), $this->active->FldTagCaption(2) <> "" ? $this->active->FldTagCaption(2) : $this->active->FldTagValue(2));
			$this->active->EditValue = $arwrk;

			// password
			$this->password->EditCustomAttributes = "";
			$this->password->EditValue = ew_HtmlEncode($this->password->CurrentValue);

			// telephone
			$this->telephone->EditCustomAttributes = "";
			$this->telephone->EditValue = ew_HtmlEncode($this->telephone->CurrentValue);

			// addressNo
			$this->addressNo->EditCustomAttributes = "";
			$this->addressNo->EditValue = ew_HtmlEncode($this->addressNo->CurrentValue);

			// addressPostcode
			$this->addressPostcode->EditCustomAttributes = "";
			$sFilterWrk = "";
			$sSqlWrk = "SELECT `postcode`, `postcode` AS `DispFld`, `address1` AS `Disp2Fld`, `address2` AS `Disp3Fld`, `address3` AS `Disp4Fld`, '' AS `SelectFilterFld`, '' AS `SelectFilterFld2`, '' AS `SelectFilterFld3`, '' AS `SelectFilterFld4` FROM `address`";
			$sWhereWrk = "";
			if ($sFilterWrk <> "") {
				ew_AddFilter($sWhereWrk, $sFilterWrk);
			}
			if ($sWhereWrk <> "") $sSqlWrk .= " WHERE " . $sWhereWrk;
			$rswrk = $conn->Execute($sSqlWrk);
			$arwrk = ($rswrk) ? $rswrk->GetRows() : array();
			if ($rswrk) $rswrk->Close();
			array_unshift($arwrk, array("", $Language->Phrase("PleaseSelect"), "", "", "", "", "", "", ""));
			$this->addressPostcode->EditValue = $arwrk;

			// Edit refer script
			// id

			$this->id->HrefValue = "";

			// name
			$this->name->HrefValue = "";

			// roleId
			$this->roleId->HrefValue = "";

			// active
			$this->active->HrefValue = "";

			// password
			$this->password->HrefValue = "";

			// telephone
			$this->telephone->HrefValue = "";

			// addressNo
			$this->addressNo->HrefValue = "";

			// addressPostcode
			$this->addressPostcode->HrefValue = "";
		}
		if ($this->RowType == EW_ROWTYPE_ADD ||
			$this->RowType == EW_ROWTYPE_EDIT ||
			$this->RowType == EW_ROWTYPE_SEARCH) { // Add / Edit / Search row
			$this->SetupFieldTitles();
		}

		// Call Row Rendered event
		if ($this->RowType <> EW_ROWTYPE_AGGREGATEINIT)
			$this->Row_Rendered();
	}

	// Validate form
	function ValidateForm() {
		global $Language, $gsFormError;

		// Initialize form error message
		$gsFormError = "";

		// Check if validation required
		if (!EW_SERVER_VALIDATE)
			return ($gsFormError == "");
		if (!is_null($this->name->FormValue) && $this->name->FormValue == "") {
			ew_AddMessage($gsFormError, $Language->Phrase("EnterRequiredField") . " - " . $this->name->FldCaption());
		}
		if (!is_null($this->roleId->FormValue) && $this->roleId->FormValue == "") {
			ew_AddMessage($gsFormError, $Language->Phrase("EnterRequiredField") . " - " . $this->roleId->FldCaption());
		}
		if (!ew_CheckInteger($this->addressNo->FormValue)) {
			ew_AddMessage($gsFormError, $this->addressNo->FldErrMsg());
		}

		// Return validate result
		$ValidateForm = ($gsFormError == "");

		// Call Form_CustomValidate event
		$sFormCustomError = "";
		$ValidateForm = $ValidateForm && $this->Form_CustomValidate($sFormCustomError);
		if ($sFormCustomError <> "") {
			ew_AddMessage($gsFormError, $sFormCustomError);
		}
		return $ValidateForm;
	}

	// Update record based on key values
	function EditRow() {
		global $conn, $Security, $Language;
		$sFilter = $this->KeyFilter();
		$this->CurrentFilter = $sFilter;
		$sSql = $this->SQL();
		$conn->raiseErrorFn = 'ew_ErrorFn';
		$rs = $conn->Execute($sSql);
		$conn->raiseErrorFn = '';
		if ($rs === FALSE)
			return FALSE;
		if ($rs->EOF) {
			$EditRow = FALSE; // Update Failed
		} else {

			// Save old values
			$rsold = &$rs->fields;
			$rsnew = array();

			// name
			$this->name->SetDbValueDef($rsnew, $this->name->CurrentValue, NULL, $this->name->ReadOnly);

			// roleId
			$this->roleId->SetDbValueDef($rsnew, $this->roleId->CurrentValue, NULL, $this->roleId->ReadOnly);

			// active
			$tmpBool = $this->active->CurrentValue;
			if ($tmpBool <> "Y" && $tmpBool <> "N")
				$tmpBool = (!empty($tmpBool)) ? "Y" : "N";
			$this->active->SetDbValueDef($rsnew, $tmpBool, NULL, $this->active->ReadOnly);

			// password
			$this->password->SetDbValueDef($rsnew, $this->password->CurrentValue, NULL, $this->password->ReadOnly || (EW_ENCRYPTED_PASSWORD && $rs->fields('password') == $this->password->CurrentValue));

			// telephone
			$this->telephone->SetDbValueDef($rsnew, $this->telephone->CurrentValue, NULL, $this->telephone->ReadOnly);

			// addressNo
			$this->addressNo->SetDbValueDef($rsnew, $this->addressNo->CurrentValue, NULL, $this->addressNo->ReadOnly);

			// addressPostcode
			$this->addressPostcode->SetDbValueDef($rsnew, $this->addressPostcode->CurrentValue, NULL, $this->addressPostcode->ReadOnly);

			// Call Row Updating event
			$bUpdateRow = $this->Row_Updating($rsold, $rsnew);
			if ($bUpdateRow) {
				$conn->raiseErrorFn = 'ew_ErrorFn';
				if (count($rsnew) > 0)
					$EditRow = $this->Update($rsnew);
				else
					$EditRow = TRUE; // No field to update
				$conn->raiseErrorFn = '';
			} else {
				if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

					// Use the message, do nothing
				} elseif ($this->CancelMessage <> "") {
					$this->setFailureMessage($this->CancelMessage);
					$this->CancelMessage = "";
				} else {
					$this->setFailureMessage($Language->Phrase("UpdateCancelled"));
				}
				$EditRow = FALSE;
			}
		}

		// Call Row_Updated event
		if ($EditRow)
			$this->Row_Updated($rsold, $rsnew);
		if ($EditRow) {
			$this->WriteAuditTrailOnEdit($rsold, $rsnew);
		}
		$rs->Close();
		return $EditRow;
	}

	// Add record
	function AddRow($rsold = NULL) {
		global $conn, $Language, $Security;
		$rsnew = array();

		// name
		$this->name->SetDbValueDef($rsnew, $this->name->CurrentValue, NULL, FALSE);

		// roleId
		$this->roleId->SetDbValueDef($rsnew, $this->roleId->CurrentValue, NULL, FALSE);

		// active
		$tmpBool = $this->active->CurrentValue;
		if ($tmpBool <> "Y" && $tmpBool <> "N")
			$tmpBool = (!empty($tmpBool)) ? "Y" : "N";
		$this->active->SetDbValueDef($rsnew, $tmpBool, NULL, FALSE);

		// password
		$this->password->SetDbValueDef($rsnew, $this->password->CurrentValue, NULL, FALSE);

		// telephone
		$this->telephone->SetDbValueDef($rsnew, $this->telephone->CurrentValue, NULL, FALSE);

		// addressNo
		$this->addressNo->SetDbValueDef($rsnew, $this->addressNo->CurrentValue, NULL, FALSE);

		// addressPostcode
		$this->addressPostcode->SetDbValueDef($rsnew, $this->addressPostcode->CurrentValue, NULL, FALSE);

		// Call Row Inserting event
		$rs = ($rsold == NULL) ? NULL : $rsold->fields;
		$bInsertRow = $this->Row_Inserting($rs, $rsnew);
		if ($bInsertRow) {
			$conn->raiseErrorFn = 'ew_ErrorFn';
			$AddRow = $this->Insert($rsnew);
			$conn->raiseErrorFn = '';
			if ($AddRow) {
			}
		} else {
			if ($this->getSuccessMessage() <> "" || $this->getFailureMessage() <> "") {

				// Use the message, do nothing
			} elseif ($this->CancelMessage <> "") {
				$this->setFailureMessage($this->CancelMessage);
				$this->CancelMessage = "";
			} else {
				$this->setFailureMessage($Language->Phrase("InsertCancelled"));
			}
			$AddRow = FALSE;
		}

		// Get insert id if necessary
		if ($AddRow) {
			$this->id->setDbValue($conn->Insert_ID());
			$rsnew['id'] = $this->id->DbValue;
		}
		if ($AddRow) {

			// Call Row Inserted event
			$rs = ($rsold == NULL) ? NULL : $rsold->fields;
			$this->Row_Inserted($rs, $rsnew);
			$this->WriteAuditTrailOnAdd($rsnew);
		}
		return $AddRow;
	}

	// Set up export options
	function SetupExportOptions() {
		global $Language;

		// Printer friendly
		$item = &$this->ExportOptions->Add("print");
		$item->Body = "<a href=\"" . $this->ExportPrintUrl . "\">" . $Language->Phrase("PrinterFriendly") . "</a>";
		$item->Visible = FALSE;

		// Export to Excel
		$item = &$this->ExportOptions->Add("excel");
		$item->Body = "<a href=\"" . $this->ExportExcelUrl . "\">" . $Language->Phrase("ExportToExcel") . "</a>";
		$item->Visible = FALSE;

		// Export to Word
		$item = &$this->ExportOptions->Add("word");
		$item->Body = "<a href=\"" . $this->ExportWordUrl . "\">" . $Language->Phrase("ExportToWord") . "</a>";
		$item->Visible = FALSE;

		// Export to Html
		$item = &$this->ExportOptions->Add("html");
		$item->Body = "<a href=\"" . $this->ExportHtmlUrl . "\">" . $Language->Phrase("ExportToHtml") . "</a>";
		$item->Visible = FALSE;

		// Export to Xml
		$item = &$this->ExportOptions->Add("xml");
		$item->Body = "<a href=\"" . $this->ExportXmlUrl . "\">" . $Language->Phrase("ExportToXml") . "</a>";
		$item->Visible = FALSE;

		// Export to Csv
		$item = &$this->ExportOptions->Add("csv");
		$item->Body = "<a href=\"" . $this->ExportCsvUrl . "\">" . $Language->Phrase("ExportToCsv") . "</a>";
		$item->Visible = TRUE;

		// Export to Pdf
		$item = &$this->ExportOptions->Add("pdf");
		$item->Body = "<a href=\"" . $this->ExportPdfUrl . "\">" . $Language->Phrase("ExportToPDF") . "</a>";
		$item->Visible = FALSE;

		// Export to Email
		$item = &$this->ExportOptions->Add("email");
		$item->Body = "<a id=\"emf_staff\" href=\"javascript:void(0);\" onclick=\"ew_EmailDialogShow({lnk:'emf_staff',hdr:ewLanguage.Phrase('ExportToEmail'),f:document.fstafflist,sel:false});\">" . $Language->Phrase("ExportToEmail") . "</a>";
		$item->Visible = FALSE;

		// Hide options for export/action
		if ($this->Export <> "" || $this->CurrentAction <> "")
			$this->ExportOptions->HideAllOptions();
	}

	// Export data in HTML/CSV/Word/Excel/XML/Email/PDF format
	function ExportData() {
		$utf8 = (strtolower(EW_CHARSET) == "utf-8");
		$bSelectLimit = EW_SELECT_LIMIT;

		// Load recordset
		if ($bSelectLimit) {
			$this->TotalRecs = $this->SelectRecordCount();
		} else {
			if ($rs = $this->LoadRecordset())
				$this->TotalRecs = $rs->RecordCount();
		}
		$this->StartRec = 1;

		// Export all
		if ($this->ExportAll) {
			set_time_limit(EW_EXPORT_ALL_TIME_LIMIT);
			$this->DisplayRecs = $this->TotalRecs;
			$this->StopRec = $this->TotalRecs;
		} else { // Export one page only
			$this->SetUpStartRec(); // Set up start record position

			// Set the last record to display
			if ($this->DisplayRecs <= 0) {
				$this->StopRec = $this->TotalRecs;
			} else {
				$this->StopRec = $this->StartRec + $this->DisplayRecs - 1;
			}
		}
		if ($bSelectLimit)
			$rs = $this->LoadRecordset($this->StartRec-1, $this->DisplayRecs <= 0 ? $this->TotalRecs : $this->DisplayRecs);
		if (!$rs) {
			header("Content-Type:"); // Remove header
			header("Content-Disposition:");
			$this->ShowMessage();
			return;
		}
		$ExportDoc = ew_ExportDocument($this, "h");
		$ParentTable = "";
		if ($bSelectLimit) {
			$StartRec = 1;
			$StopRec = $this->DisplayRecs <= 0 ? $this->TotalRecs : $this->DisplayRecs;
		} else {
			$StartRec = $this->StartRec;
			$StopRec = $this->StopRec;
		}
		$sHeader = $this->PageHeader;
		$this->Page_DataRendering($sHeader);
		$ExportDoc->Text .= $sHeader;
		$this->ExportDocument($ExportDoc, $rs, $StartRec, $StopRec, "");
		$sFooter = $this->PageFooter;
		$this->Page_DataRendered($sFooter);
		$ExportDoc->Text .= $sFooter;

		// Close recordset
		$rs->Close();

		// Export header and footer
		$ExportDoc->ExportHeaderAndFooter();

		// Clean output buffer
		if (!EW_DEBUG_ENABLED && ob_get_length())
			ob_end_clean();

		// Write debug message if enabled
		if (EW_DEBUG_ENABLED)
			echo ew_DebugMsg();

		// Output data
		$ExportDoc->Export();
	}

	// Write Audit Trail start/end for grid update
	function WriteAuditTrailDummy($typ) {
		$table = 'staff';
	  $usr = CurrentUserName();
		ew_WriteAuditTrail("log", ew_StdCurrentDateTime(), ew_ScriptName(), $usr, $typ, $table, "", "", "", "");
	}

	// Write Audit Trail (add page)
	function WriteAuditTrailOnAdd(&$rs) {
		if (!$this->AuditTrailOnAdd) return;
		$table = 'staff';

		// Get key value
		$key = "";
		if ($key <> "") $key .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
		$key .= $rs['id'];

		// Write Audit Trail
		$dt = ew_StdCurrentDateTime();
		$id = ew_ScriptName();
	  $usr = CurrentUserName();
		foreach (array_keys($rs) as $fldname) {
			if ($this->fields[$fldname]->FldDataType <> EW_DATATYPE_BLOB) { // Ignore BLOB fields
				if ($this->fields[$fldname]->FldDataType == EW_DATATYPE_MEMO) {
					if (EW_AUDIT_TRAIL_TO_DATABASE)
						$newvalue = $rs[$fldname];
					else
						$newvalue = "[MEMO]"; // Memo Field
				} elseif ($this->fields[$fldname]->FldDataType == EW_DATATYPE_XML) {
					$newvalue = "[XML]"; // XML Field
				} else {
					$newvalue = $rs[$fldname];
				}
				ew_WriteAuditTrail("log", $dt, $id, $usr, "A", $table, $fldname, $key, "", $newvalue);
			}
		}
	}

	// Write Audit Trail (edit page)
	function WriteAuditTrailOnEdit(&$rsold, &$rsnew) {
		if (!$this->AuditTrailOnEdit) return;
		$table = 'staff';

		// Get key value
		$key = "";
		if ($key <> "") $key .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
		$key .= $rsold['id'];

		// Write Audit Trail
		$dt = ew_StdCurrentDateTime();
		$id = ew_ScriptName();
	  $usr = CurrentUserName();
		foreach (array_keys($rsnew) as $fldname) {
			if ($this->fields[$fldname]->FldDataType <> EW_DATATYPE_BLOB) { // Ignore BLOB fields
				if ($this->fields[$fldname]->FldDataType == EW_DATATYPE_DATE) { // DateTime field
					$modified = (ew_FormatDateTime($rsold[$fldname], 0) <> ew_FormatDateTime($rsnew[$fldname], 0));
				} else {
					$modified = !ew_CompareValue($rsold[$fldname], $rsnew[$fldname]);
				}
				if ($modified) {
					if ($this->fields[$fldname]->FldDataType == EW_DATATYPE_MEMO) { // Memo field
						if (EW_AUDIT_TRAIL_TO_DATABASE) {
							$oldvalue = $rsold[$fldname];
							$newvalue = $rsnew[$fldname];
						} else {
							$oldvalue = "[MEMO]";
							$newvalue = "[MEMO]";
						}
					} elseif ($this->fields[$fldname]->FldDataType == EW_DATATYPE_XML) { // XML field
						$oldvalue = "[XML]";
						$newvalue = "[XML]";
					} else {
						$oldvalue = $rsold[$fldname];
						$newvalue = $rsnew[$fldname];
					}
					ew_WriteAuditTrail("log", $dt, $id, $usr, "U", $table, $fldname, $key, $oldvalue, $newvalue);
				}
			}
		}
	}

	// Write Audit Trail (delete page)
	function WriteAuditTrailOnDelete(&$rs) {
		if (!$this->AuditTrailOnDelete) return;
		$table = 'staff';

		// Get key value
		$key = "";
		if ($key <> "")
			$key .= $GLOBALS["EW_COMPOSITE_KEY_SEPARATOR"];
		$key .= $rs['id'];

		// Write Audit Trail
		$dt = ew_StdCurrentDateTime();
		$id = ew_ScriptName();
	  $curUser = CurrentUserName();
		foreach (array_keys($rs) as $fldname) {
			if (array_key_exists($fldname, $this->fields) && $this->fields[$fldname]->FldDataType <> EW_DATATYPE_BLOB) { // Ignore BLOB fields
				if ($this->fields[$fldname]->FldDataType == EW_DATATYPE_MEMO) {
					if (EW_AUDIT_TRAIL_TO_DATABASE)
						$oldvalue = $rs[$fldname];
					else
						$oldvalue = "[MEMO]"; // Memo field
				} elseif ($this->fields[$fldname]->FldDataType == EW_DATATYPE_XML) {
					$oldvalue = "[XML]"; // XML field
				} else {
					$oldvalue = $rs[$fldname];
				}
				ew_WriteAuditTrail("log", $dt, $id, $curUser, "D", $table, $fldname, $key, $oldvalue, "");
			}
		}
	}

	// Page Load event
	function Page_Load() {

		//echo "Page Load";
	}

	// Page Unload event
	function Page_Unload() {

		//echo "Page Unload";
	}

	// Page Redirecting event
	function Page_Redirecting(&$url) {

		// Example:
		//$url = "your URL";

	}

	// Message Showing event
	// $type = ''|'success'|'failure'|'warning'
	function Message_Showing(&$msg, $type) {
		if ($type == 'success') {

			//$msg = "your success message";
		} elseif ($type == 'failure') {

			//$msg = "your failure message";
		} elseif ($type == 'warning') {

			//$msg = "your warning message";
		} else {

			//$msg = "your message";
		}
	}

	// Page Data Rendering event
	function Page_DataRendering(&$header) {

		// Example:
		//$header = "your header";

	}

	// Page Data Rendered event
	function Page_DataRendered(&$footer) {

		// Example:
		//$footer = "your footer";

	}

	// Form Custom Validate event
	function Form_CustomValidate(&$CustomError) {

		// Return error message in CustomError
		return TRUE;
	}

	// ListOptions Load event
	function ListOptions_Load() {

		// Example:
		//$opt = &$this->ListOptions->Add("new");
		//$opt->Header = "xxx";
		//$opt->OnLeft = TRUE; // Link on left
		//$opt->MoveTo(0); // Move to first column

	}

	// ListOptions Rendered event
	function ListOptions_Rendered() {

		// Example: 
		//$this->ListOptions->Items["new"]->Body = "xxx";

	}
}
?>
<?php ew_Header(FALSE) ?>
<?php

// Create page object
if (!isset($staff_list)) $staff_list = new cstaff_list();

// Page init
$staff_list->Page_Init();

// Page main
$staff_list->Page_Main();
?>
<?php include_once "header.php" ?>
<?php if ($staff->Export == "") { ?>
<script type="text/javascript">

// Page object
var staff_list = new ew_Page("staff_list");
staff_list.PageID = "list"; // Page ID
var EW_PAGE_ID = staff_list.PageID; // For backward compatibility

// Form object
var fstafflist = new ew_Form("fstafflist");

// Validate form
fstafflist.Validate = function(fobj) {
	if (!this.ValidateRequired)
		return true; // Ignore validation
	fobj = fobj || this.Form;
	this.PostAutoSuggest();	
	if (fobj.a_confirm && fobj.a_confirm.value == "F")
		return true;
	var elm, aelm;
	var rowcnt = (fobj.key_count) ? Number(fobj.key_count.value) : 1;
	var startcnt = (rowcnt == 0) ? 0 : 1; // rowcnt == 0 => Inline-Add
	for (var i = startcnt; i <= rowcnt; i++) {
		var infix = (fobj.key_count) ? String(i) : "";
		elm = fobj.elements["x" + infix + "_name"];
		if (elm && !ew_HasValue(elm))
			return ew_OnError(this, elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($staff->name->FldCaption()) ?>");
		elm = fobj.elements["x" + infix + "_roleId"];
		if (elm && !ew_HasValue(elm))
			return ew_OnError(this, elm, ewLanguage.Phrase("EnterRequiredField") + " - <?php echo ew_JsEncode2($staff->roleId->FldCaption()) ?>");
		elm = fobj.elements["x" + infix + "_addressNo"];
		if (elm && !ew_CheckInteger(elm.value))
			return ew_OnError(this, elm, "<?php echo ew_JsEncode2($staff->addressNo->FldErrMsg()) ?>");

		// Set up row object
		ew_ElementsToRow(fobj, infix);

		// Fire Form_CustomValidate event
		if (!this.Form_CustomValidate(fobj))
			return false;
	}
	return true;
}

// Form_CustomValidate event
fstafflist.Form_CustomValidate = 
 function(fobj) { // DO NOT CHANGE THIS LINE!

 	// Your custom validation code here, return false if invalid. 
 	return true;
 }

// Use JavaScript validation or not
<?php if (EW_CLIENT_VALIDATE) { ?>
fstafflist.ValidateRequired = true;
<?php } else { ?>
fstafflist.ValidateRequired = false; 
<?php } ?>

// Dynamic selection lists
fstafflist.Lists["x_roleId"] = {"LinkField":"x_id","Ajax":null,"AutoFill":false,"DisplayFields":["x_roleDescription","","",""],"ParentFields":[],"FilterFields":[],"Options":[]};
fstafflist.Lists["x_addressPostcode"] = {"LinkField":"x_postcode","Ajax":null,"AutoFill":false,"DisplayFields":["x_postcode","x_address1","x_address2","x_address3"],"ParentFields":[],"FilterFields":[],"Options":[]};

// Form object for search
</script>
<script type="text/javascript">

// Write your client script here, no need to add script tags.
</script>
<?php } ?>
<?php
	$bSelectLimit = EW_SELECT_LIMIT;
	if ($bSelectLimit) {
		$staff_list->TotalRecs = $staff->SelectRecordCount();
	} else {
		if ($staff_list->Recordset = $staff_list->LoadRecordset())
			$staff_list->TotalRecs = $staff_list->Recordset->RecordCount();
	}
	$staff_list->StartRec = 1;
	if ($staff_list->DisplayRecs <= 0 || ($staff->Export <> "" && $staff->ExportAll)) // Display all records
		$staff_list->DisplayRecs = $staff_list->TotalRecs;
	if (!($staff->Export <> "" && $staff->ExportAll))
		$staff_list->SetUpStartRec(); // Set up start record position
	if ($bSelectLimit)
		$staff_list->Recordset = $staff_list->LoadRecordset($staff_list->StartRec-1, $staff_list->DisplayRecs);
?>
<p style="white-space: nowrap;"><span id="ewPageCaption" class="ewTitle ewTableTitle"><?php echo $Language->Phrase("TblTypeTABLE") ?><?php echo $staff->TableCaption() ?>&nbsp;&nbsp;</span>
<?php $staff_list->ExportOptions->Render("body"); ?>
</p>
<?php $staff_list->ShowPageHeader(); ?>
<?php
$staff_list->ShowMessage();
?>
<br>
<table cellspacing="0" class="ewGrid"><tr><td class="ewGridContent">
<form name="fstafflist" id="fstafflist" class="ewForm" action="<?php echo ew_CurrentPage() ?>" method="post">
<input type="hidden" name="t" value="staff">
<div id="gmp_staff" class="ewGridMiddlePanel">
<?php if ($staff_list->TotalRecs > 0 || $staff->CurrentAction == "add" || $staff->CurrentAction == "copy") { ?>
<table id="tbl_stafflist" class="ewTable ewTableSeparate">
<?php echo $staff->TableCustomInnerHtml ?>
<thead><!-- Table header -->
	<tr class="ewTableHeader">
<?php

// Render list options
$staff_list->RenderListOptions();

// Render list options (header, left)
$staff_list->ListOptions->Render("header", "left");
?>
<?php if ($staff->id->Visible) { // id ?>
	<?php if ($staff->SortUrl($staff->id) == "") { ?>
		<td><span id="elh_staff_id" class="staff_id"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $staff->id->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div onmousedown="ew_Sort(event,'<?php echo $staff->SortUrl($staff->id) ?>',1);"><span id="elh_staff_id" class="staff_id">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $staff->id->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($staff->id->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($staff->id->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($staff->name->Visible) { // name ?>
	<?php if ($staff->SortUrl($staff->name) == "") { ?>
		<td><span id="elh_staff_name" class="staff_name"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $staff->name->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div onmousedown="ew_Sort(event,'<?php echo $staff->SortUrl($staff->name) ?>',1);"><span id="elh_staff_name" class="staff_name">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $staff->name->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($staff->name->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($staff->name->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($staff->roleId->Visible) { // roleId ?>
	<?php if ($staff->SortUrl($staff->roleId) == "") { ?>
		<td><span id="elh_staff_roleId" class="staff_roleId"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $staff->roleId->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div onmousedown="ew_Sort(event,'<?php echo $staff->SortUrl($staff->roleId) ?>',1);"><span id="elh_staff_roleId" class="staff_roleId">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $staff->roleId->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($staff->roleId->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($staff->roleId->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($staff->active->Visible) { // active ?>
	<?php if ($staff->SortUrl($staff->active) == "") { ?>
		<td><span id="elh_staff_active" class="staff_active"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $staff->active->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div onmousedown="ew_Sort(event,'<?php echo $staff->SortUrl($staff->active) ?>',1);"><span id="elh_staff_active" class="staff_active">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $staff->active->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($staff->active->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($staff->active->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($staff->password->Visible) { // password ?>
	<?php if ($staff->SortUrl($staff->password) == "") { ?>
		<td><span id="elh_staff_password" class="staff_password"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $staff->password->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div onmousedown="ew_Sort(event,'<?php echo $staff->SortUrl($staff->password) ?>',1);"><span id="elh_staff_password" class="staff_password">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $staff->password->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($staff->password->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($staff->password->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($staff->telephone->Visible) { // telephone ?>
	<?php if ($staff->SortUrl($staff->telephone) == "") { ?>
		<td><span id="elh_staff_telephone" class="staff_telephone"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $staff->telephone->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div onmousedown="ew_Sort(event,'<?php echo $staff->SortUrl($staff->telephone) ?>',1);"><span id="elh_staff_telephone" class="staff_telephone">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $staff->telephone->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($staff->telephone->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($staff->telephone->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($staff->addressNo->Visible) { // addressNo ?>
	<?php if ($staff->SortUrl($staff->addressNo) == "") { ?>
		<td><span id="elh_staff_addressNo" class="staff_addressNo"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $staff->addressNo->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div onmousedown="ew_Sort(event,'<?php echo $staff->SortUrl($staff->addressNo) ?>',1);"><span id="elh_staff_addressNo" class="staff_addressNo">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $staff->addressNo->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($staff->addressNo->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($staff->addressNo->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php if ($staff->addressPostcode->Visible) { // addressPostcode ?>
	<?php if ($staff->SortUrl($staff->addressPostcode) == "") { ?>
		<td><span id="elh_staff_addressPostcode" class="staff_addressPostcode"><table class="ewTableHeaderBtn"><thead><tr><td><?php echo $staff->addressPostcode->FldCaption() ?></td></tr></thead></table></span></td>
	<?php } else { ?>
		<td><div onmousedown="ew_Sort(event,'<?php echo $staff->SortUrl($staff->addressPostcode) ?>',1);"><span id="elh_staff_addressPostcode" class="staff_addressPostcode">
			<table class="ewTableHeaderBtn"><thead><tr><td class="ewTableHeaderCaption"><?php echo $staff->addressPostcode->FldCaption() ?></td><td class="ewTableHeaderSort"><?php if ($staff->addressPostcode->getSort() == "ASC") { ?><img src="phpimages/sortup.gif" width="10" height="9" alt="" style="border: 0;"><?php } elseif ($staff->addressPostcode->getSort() == "DESC") { ?><img src="phpimages/sortdown.gif" width="10" height="9" alt="" style="border: 0;"><?php } ?></td></tr></thead></table>
		</span></div></td>		
	<?php } ?>
<?php } ?>		
<?php

// Render list options (header, right)
$staff_list->ListOptions->Render("header", "right");
?>
	</tr>
</thead>
<tbody>
<?php
	if ($staff->CurrentAction == "add" || $staff->CurrentAction == "copy") {
		$staff_list->RowIndex = 0;
		$staff_list->KeyCount = $staff_list->RowIndex;
		if ($staff->CurrentAction == "add")
			$staff_list->LoadDefaultValues();
		if ($staff->EventCancelled) // Insert failed
			$staff_list->RestoreFormValues(); // Restore form values

		// Set row properties
		$staff->ResetAttrs();
		$staff->RowAttrs = array_merge($staff->RowAttrs, array('data-rowindex'=>0, 'id'=>'r0_staff', 'data-rowtype'=>EW_ROWTYPE_ADD));
		$staff->RowType = EW_ROWTYPE_ADD;

		// Render row
		$staff_list->RenderRow();

		// Render list options
		$staff_list->RenderListOptions();
		$staff_list->StartRowCnt = 0;
?>
	<tr<?php echo $staff->RowAttributes() ?>>
<?php

// Render list options (body, left)
$staff_list->ListOptions->Render("body", "left", $staff_list->RowCnt);
?>
	<?php if ($staff->id->Visible) { // id ?>
		<td><span id="el<?php echo $staff_list->RowCnt ?>_staff_id" class="staff_id">
<input type="hidden" name="o<?php echo $staff_list->RowIndex ?>_id" id="o<?php echo $staff_list->RowIndex ?>_id" value="<?php echo ew_HtmlEncode($staff->id->OldValue) ?>">
</span></td>
	<?php } ?>
	<?php if ($staff->name->Visible) { // name ?>
		<td><span id="el<?php echo $staff_list->RowCnt ?>_staff_name" class="staff_name">
<input type="text" name="x<?php echo $staff_list->RowIndex ?>_name" id="x<?php echo $staff_list->RowIndex ?>_name" size="30" maxlength="50" value="<?php echo $staff->name->EditValue ?>"<?php echo $staff->name->EditAttributes() ?>>
<input type="hidden" name="o<?php echo $staff_list->RowIndex ?>_name" id="o<?php echo $staff_list->RowIndex ?>_name" value="<?php echo ew_HtmlEncode($staff->name->OldValue) ?>">
</span></td>
	<?php } ?>
	<?php if ($staff->roleId->Visible) { // roleId ?>
		<td><span id="el<?php echo $staff_list->RowCnt ?>_staff_roleId" class="staff_roleId">
<select id="x<?php echo $staff_list->RowIndex ?>_roleId" name="x<?php echo $staff_list->RowIndex ?>_roleId"<?php echo $staff->roleId->EditAttributes() ?>>
<?php
if (is_array($staff->roleId->EditValue)) {
	$arwrk = $staff->roleId->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($staff->roleId->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " selected=\"selected\"" : "";
		if ($selwrk <> "") $emptywrk = FALSE;
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $arwrk[$rowcntwrk][1] ?>
</option>
<?php
	}
}
?>
</select>
<script type="text/javascript">
fstafflist.Lists["x_roleId"].Options = <?php echo (is_array($staff->roleId->EditValue)) ? ew_ArrayToJson($staff->roleId->EditValue, 1) : "[]" ?>;
</script>
<input type="hidden" name="o<?php echo $staff_list->RowIndex ?>_roleId" id="o<?php echo $staff_list->RowIndex ?>_roleId" value="<?php echo ew_HtmlEncode($staff->roleId->OldValue) ?>">
</span></td>
	<?php } ?>
	<?php if ($staff->active->Visible) { // active ?>
		<td><span id="el<?php echo $staff_list->RowCnt ?>_staff_active" class="staff_active">
<?php
$selwrk = (ew_ConvertToBool($staff->active->CurrentValue)) ? " checked=\"checked\"" : "";
?>
<input type="checkbox" name="x<?php echo $staff_list->RowIndex ?>_active[]" id="x<?php echo $staff_list->RowIndex ?>_active[]" value="1"<?php echo $selwrk ?><?php echo $staff->active->EditAttributes() ?>>
<input type="hidden" name="o<?php echo $staff_list->RowIndex ?>_active[]" id="o<?php echo $staff_list->RowIndex ?>_active[]" value="<?php echo ew_HtmlEncode($staff->active->OldValue) ?>">
</span></td>
	<?php } ?>
	<?php if ($staff->password->Visible) { // password ?>
		<td><span id="el<?php echo $staff_list->RowCnt ?>_staff_password" class="staff_password">
<input type="text" name="x<?php echo $staff_list->RowIndex ?>_password" id="x<?php echo $staff_list->RowIndex ?>_password" size="30" maxlength="50" value="<?php echo $staff->password->EditValue ?>"<?php echo $staff->password->EditAttributes() ?>>
<input type="hidden" name="o<?php echo $staff_list->RowIndex ?>_password" id="o<?php echo $staff_list->RowIndex ?>_password" value="<?php echo ew_HtmlEncode($staff->password->OldValue) ?>">
</span></td>
	<?php } ?>
	<?php if ($staff->telephone->Visible) { // telephone ?>
		<td><span id="el<?php echo $staff_list->RowCnt ?>_staff_telephone" class="staff_telephone">
<input type="text" name="x<?php echo $staff_list->RowIndex ?>_telephone" id="x<?php echo $staff_list->RowIndex ?>_telephone" size="30" maxlength="15" value="<?php echo $staff->telephone->EditValue ?>"<?php echo $staff->telephone->EditAttributes() ?>>
<input type="hidden" name="o<?php echo $staff_list->RowIndex ?>_telephone" id="o<?php echo $staff_list->RowIndex ?>_telephone" value="<?php echo ew_HtmlEncode($staff->telephone->OldValue) ?>">
</span></td>
	<?php } ?>
	<?php if ($staff->addressNo->Visible) { // addressNo ?>
		<td><span id="el<?php echo $staff_list->RowCnt ?>_staff_addressNo" class="staff_addressNo">
<input type="text" name="x<?php echo $staff_list->RowIndex ?>_addressNo" id="x<?php echo $staff_list->RowIndex ?>_addressNo" size="30" value="<?php echo $staff->addressNo->EditValue ?>"<?php echo $staff->addressNo->EditAttributes() ?>>
<input type="hidden" name="o<?php echo $staff_list->RowIndex ?>_addressNo" id="o<?php echo $staff_list->RowIndex ?>_addressNo" value="<?php echo ew_HtmlEncode($staff->addressNo->OldValue) ?>">
</span></td>
	<?php } ?>
	<?php if ($staff->addressPostcode->Visible) { // addressPostcode ?>
		<td><span id="el<?php echo $staff_list->RowCnt ?>_staff_addressPostcode" class="staff_addressPostcode">
<select id="x<?php echo $staff_list->RowIndex ?>_addressPostcode" name="x<?php echo $staff_list->RowIndex ?>_addressPostcode"<?php echo $staff->addressPostcode->EditAttributes() ?>>
<?php
if (is_array($staff->addressPostcode->EditValue)) {
	$arwrk = $staff->addressPostcode->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($staff->addressPostcode->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " selected=\"selected\"" : "";
		if ($selwrk <> "") $emptywrk = FALSE;
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $arwrk[$rowcntwrk][1] ?>
<?php if ($arwrk[$rowcntwrk][2] <> "") { ?>
<?php echo ew_ValueSeparator(1,$staff->addressPostcode) ?><?php echo $arwrk[$rowcntwrk][2] ?>
<?php } ?>
<?php if ($arwrk[$rowcntwrk][3] <> "") { ?>
<?php echo ew_ValueSeparator(2,$staff->addressPostcode) ?><?php echo $arwrk[$rowcntwrk][3] ?>
<?php } ?>
<?php if ($arwrk[$rowcntwrk][4] <> "") { ?>
<?php echo ew_ValueSeparator(3,$staff->addressPostcode) ?><?php echo $arwrk[$rowcntwrk][4] ?>
<?php } ?>
</option>
<?php
	}
}
?>
</select>
<script type="text/javascript">
fstafflist.Lists["x_addressPostcode"].Options = <?php echo (is_array($staff->addressPostcode->EditValue)) ? ew_ArrayToJson($staff->addressPostcode->EditValue, 1) : "[]" ?>;
</script>
<input type="hidden" name="o<?php echo $staff_list->RowIndex ?>_addressPostcode" id="o<?php echo $staff_list->RowIndex ?>_addressPostcode" value="<?php echo ew_HtmlEncode($staff->addressPostcode->OldValue) ?>">
</span></td>
	<?php } ?>
<?php

// Render list options (body, right)
$staff_list->ListOptions->Render("body", "right", $staff_list->RowCnt);
?>
<script type="text/javascript">
fstafflist.UpdateOpts(<?php echo $staff_list->RowIndex ?>);
</script>
	</tr>
<?php
}
?>
<?php
if ($staff->ExportAll && $staff->Export <> "") {
	$staff_list->StopRec = $staff_list->TotalRecs;
} else {

	// Set the last record to display
	if ($staff_list->TotalRecs > $staff_list->StartRec + $staff_list->DisplayRecs - 1)
		$staff_list->StopRec = $staff_list->StartRec + $staff_list->DisplayRecs - 1;
	else
		$staff_list->StopRec = $staff_list->TotalRecs;
}

// Restore number of post back records
if ($objForm) {
	$objForm->Index = -1;
	if ($objForm->HasValue("key_count") && ($staff->CurrentAction == "gridadd" || $staff->CurrentAction == "gridedit" || $staff->CurrentAction == "F")) {
		$staff_list->KeyCount = $objForm->GetValue("key_count");
		$staff_list->StopRec = $staff_list->KeyCount;
	}
}
$staff_list->RecCnt = $staff_list->StartRec - 1;
if ($staff_list->Recordset && !$staff_list->Recordset->EOF) {
	$staff_list->Recordset->MoveFirst();
	if (!$bSelectLimit && $staff_list->StartRec > 1)
		$staff_list->Recordset->Move($staff_list->StartRec - 1);
} elseif (!$staff->AllowAddDeleteRow && $staff_list->StopRec == 0) {
	$staff_list->StopRec = $staff->GridAddRowCount;
}

// Initialize aggregate
$staff->RowType = EW_ROWTYPE_AGGREGATEINIT;
$staff->ResetAttrs();
$staff_list->RenderRow();
$staff_list->EditRowCnt = 0;
if ($staff->CurrentAction == "edit")
	$staff_list->RowIndex = 1;
while ($staff_list->RecCnt < $staff_list->StopRec) {
	$staff_list->RecCnt++;
	if (intval($staff_list->RecCnt) >= intval($staff_list->StartRec)) {
		$staff_list->RowCnt++;

		// Set up key count
		$staff_list->KeyCount = $staff_list->RowIndex;

		// Init row class and style
		$staff->ResetAttrs();
		$staff->CssClass = "";
		if ($staff->CurrentAction == "gridadd") {
			$staff_list->LoadDefaultValues(); // Load default values
		} else {
			$staff_list->LoadRowValues($staff_list->Recordset); // Load row values
		}
		$staff->RowType = EW_ROWTYPE_VIEW; // Render view
		if ($staff->CurrentAction == "edit") {
			if ($staff_list->CheckInlineEditKey() && $staff_list->EditRowCnt == 0) { // Inline edit
				$staff->RowType = EW_ROWTYPE_EDIT; // Render edit
			}
		}
		if ($staff->CurrentAction == "edit" && $staff->RowType == EW_ROWTYPE_EDIT && $staff->EventCancelled) { // Update failed
			$objForm->Index = 1;
			$staff_list->RestoreFormValues(); // Restore form values
		}
		if ($staff->RowType == EW_ROWTYPE_EDIT) // Edit row
			$staff_list->EditRowCnt++;

		// Set up row id / data-rowindex
		$staff->RowAttrs = array_merge($staff->RowAttrs, array('data-rowindex'=>$staff_list->RowCnt, 'id'=>'r' . $staff_list->RowCnt . '_staff', 'data-rowtype'=>$staff->RowType));

		// Render row
		$staff_list->RenderRow();

		// Render list options
		$staff_list->RenderListOptions();
?>
	<tr<?php echo $staff->RowAttributes() ?>>
<?php

// Render list options (body, left)
$staff_list->ListOptions->Render("body", "left", $staff_list->RowCnt);
?>
	<?php if ($staff->id->Visible) { // id ?>
		<td<?php echo $staff->id->CellAttributes() ?>><span id="el<?php echo $staff_list->RowCnt ?>_staff_id" class="staff_id">
<?php if ($staff->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<span<?php echo $staff->id->ViewAttributes() ?>>
<?php echo $staff->id->EditValue ?></span>
<input type="hidden" name="x<?php echo $staff_list->RowIndex ?>_id" id="x<?php echo $staff_list->RowIndex ?>_id" value="<?php echo ew_HtmlEncode($staff->id->CurrentValue) ?>">
<?php } ?>
<?php if ($staff->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span<?php echo $staff->id->ViewAttributes() ?>>
<?php echo $staff->id->ListViewValue() ?></span>
<?php } ?>
</span><a id="<?php echo $staff_list->PageObjName . "_row_" . $staff_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($staff->name->Visible) { // name ?>
		<td<?php echo $staff->name->CellAttributes() ?>><span id="el<?php echo $staff_list->RowCnt ?>_staff_name" class="staff_name">
<?php if ($staff->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<input type="text" name="x<?php echo $staff_list->RowIndex ?>_name" id="x<?php echo $staff_list->RowIndex ?>_name" size="30" maxlength="50" value="<?php echo $staff->name->EditValue ?>"<?php echo $staff->name->EditAttributes() ?>>
<?php } ?>
<?php if ($staff->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span<?php echo $staff->name->ViewAttributes() ?>>
<?php echo $staff->name->ListViewValue() ?></span>
<?php } ?>
</span><a id="<?php echo $staff_list->PageObjName . "_row_" . $staff_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($staff->roleId->Visible) { // roleId ?>
		<td<?php echo $staff->roleId->CellAttributes() ?>><span id="el<?php echo $staff_list->RowCnt ?>_staff_roleId" class="staff_roleId">
<?php if ($staff->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<select id="x<?php echo $staff_list->RowIndex ?>_roleId" name="x<?php echo $staff_list->RowIndex ?>_roleId"<?php echo $staff->roleId->EditAttributes() ?>>
<?php
if (is_array($staff->roleId->EditValue)) {
	$arwrk = $staff->roleId->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($staff->roleId->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " selected=\"selected\"" : "";
		if ($selwrk <> "") $emptywrk = FALSE;
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $arwrk[$rowcntwrk][1] ?>
</option>
<?php
	}
}
?>
</select>
<script type="text/javascript">
fstafflist.Lists["x_roleId"].Options = <?php echo (is_array($staff->roleId->EditValue)) ? ew_ArrayToJson($staff->roleId->EditValue, 1) : "[]" ?>;
</script>
<?php } ?>
<?php if ($staff->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span<?php echo $staff->roleId->ViewAttributes() ?>>
<?php echo $staff->roleId->ListViewValue() ?></span>
<?php } ?>
</span><a id="<?php echo $staff_list->PageObjName . "_row_" . $staff_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($staff->active->Visible) { // active ?>
		<td<?php echo $staff->active->CellAttributes() ?>><span id="el<?php echo $staff_list->RowCnt ?>_staff_active" class="staff_active">
<?php if ($staff->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<?php
$selwrk = (ew_ConvertToBool($staff->active->CurrentValue)) ? " checked=\"checked\"" : "";
?>
<input type="checkbox" name="x<?php echo $staff_list->RowIndex ?>_active[]" id="x<?php echo $staff_list->RowIndex ?>_active[]" value="1"<?php echo $selwrk ?><?php echo $staff->active->EditAttributes() ?>>
<?php } ?>
<?php if ($staff->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span<?php echo $staff->active->ViewAttributes() ?>>
<?php if (ew_ConvertToBool($staff->active->CurrentValue)) { ?>
<input type="checkbox" value="<?php echo $staff->active->ListViewValue() ?>" checked="checked" onclick="this.form.reset();" disabled="disabled">
<?php } else { ?>
<input type="checkbox" value="<?php echo $staff->active->ListViewValue() ?>" onclick="this.form.reset();" disabled="disabled">
<?php } ?></span>
<?php } ?>
</span><a id="<?php echo $staff_list->PageObjName . "_row_" . $staff_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($staff->password->Visible) { // password ?>
		<td<?php echo $staff->password->CellAttributes() ?>><span id="el<?php echo $staff_list->RowCnt ?>_staff_password" class="staff_password">
<?php if ($staff->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<input type="text" name="x<?php echo $staff_list->RowIndex ?>_password" id="x<?php echo $staff_list->RowIndex ?>_password" size="30" maxlength="50" value="<?php echo $staff->password->EditValue ?>"<?php echo $staff->password->EditAttributes() ?>>
<?php } ?>
<?php if ($staff->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span<?php echo $staff->password->ViewAttributes() ?>>
<?php echo $staff->password->ListViewValue() ?></span>
<?php } ?>
</span><a id="<?php echo $staff_list->PageObjName . "_row_" . $staff_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($staff->telephone->Visible) { // telephone ?>
		<td<?php echo $staff->telephone->CellAttributes() ?>><span id="el<?php echo $staff_list->RowCnt ?>_staff_telephone" class="staff_telephone">
<?php if ($staff->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<input type="text" name="x<?php echo $staff_list->RowIndex ?>_telephone" id="x<?php echo $staff_list->RowIndex ?>_telephone" size="30" maxlength="15" value="<?php echo $staff->telephone->EditValue ?>"<?php echo $staff->telephone->EditAttributes() ?>>
<?php } ?>
<?php if ($staff->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span<?php echo $staff->telephone->ViewAttributes() ?>>
<?php echo $staff->telephone->ListViewValue() ?></span>
<?php } ?>
</span><a id="<?php echo $staff_list->PageObjName . "_row_" . $staff_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($staff->addressNo->Visible) { // addressNo ?>
		<td<?php echo $staff->addressNo->CellAttributes() ?>><span id="el<?php echo $staff_list->RowCnt ?>_staff_addressNo" class="staff_addressNo">
<?php if ($staff->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<input type="text" name="x<?php echo $staff_list->RowIndex ?>_addressNo" id="x<?php echo $staff_list->RowIndex ?>_addressNo" size="30" value="<?php echo $staff->addressNo->EditValue ?>"<?php echo $staff->addressNo->EditAttributes() ?>>
<?php } ?>
<?php if ($staff->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span<?php echo $staff->addressNo->ViewAttributes() ?>>
<?php echo $staff->addressNo->ListViewValue() ?></span>
<?php } ?>
</span><a id="<?php echo $staff_list->PageObjName . "_row_" . $staff_list->RowCnt ?>"></a></td>
	<?php } ?>
	<?php if ($staff->addressPostcode->Visible) { // addressPostcode ?>
		<td<?php echo $staff->addressPostcode->CellAttributes() ?>><span id="el<?php echo $staff_list->RowCnt ?>_staff_addressPostcode" class="staff_addressPostcode">
<?php if ($staff->RowType == EW_ROWTYPE_EDIT) { // Edit record ?>
<select id="x<?php echo $staff_list->RowIndex ?>_addressPostcode" name="x<?php echo $staff_list->RowIndex ?>_addressPostcode"<?php echo $staff->addressPostcode->EditAttributes() ?>>
<?php
if (is_array($staff->addressPostcode->EditValue)) {
	$arwrk = $staff->addressPostcode->EditValue;
	$rowswrk = count($arwrk);
	$emptywrk = TRUE;
	for ($rowcntwrk = 0; $rowcntwrk < $rowswrk; $rowcntwrk++) {
		$selwrk = (strval($staff->addressPostcode->CurrentValue) == strval($arwrk[$rowcntwrk][0])) ? " selected=\"selected\"" : "";
		if ($selwrk <> "") $emptywrk = FALSE;
?>
<option value="<?php echo ew_HtmlEncode($arwrk[$rowcntwrk][0]) ?>"<?php echo $selwrk ?>>
<?php echo $arwrk[$rowcntwrk][1] ?>
<?php if ($arwrk[$rowcntwrk][2] <> "") { ?>
<?php echo ew_ValueSeparator(1,$staff->addressPostcode) ?><?php echo $arwrk[$rowcntwrk][2] ?>
<?php } ?>
<?php if ($arwrk[$rowcntwrk][3] <> "") { ?>
<?php echo ew_ValueSeparator(2,$staff->addressPostcode) ?><?php echo $arwrk[$rowcntwrk][3] ?>
<?php } ?>
<?php if ($arwrk[$rowcntwrk][4] <> "") { ?>
<?php echo ew_ValueSeparator(3,$staff->addressPostcode) ?><?php echo $arwrk[$rowcntwrk][4] ?>
<?php } ?>
</option>
<?php
	}
}
?>
</select>
<script type="text/javascript">
fstafflist.Lists["x_addressPostcode"].Options = <?php echo (is_array($staff->addressPostcode->EditValue)) ? ew_ArrayToJson($staff->addressPostcode->EditValue, 1) : "[]" ?>;
</script>
<?php } ?>
<?php if ($staff->RowType == EW_ROWTYPE_VIEW) { // View record ?>
<span<?php echo $staff->addressPostcode->ViewAttributes() ?>>
<?php echo $staff->addressPostcode->ListViewValue() ?></span>
<?php } ?>
</span><a id="<?php echo $staff_list->PageObjName . "_row_" . $staff_list->RowCnt ?>"></a></td>
	<?php } ?>
<?php

// Render list options (body, right)
$staff_list->ListOptions->Render("body", "right", $staff_list->RowCnt);
?>
	</tr>
<?php if ($staff->RowType == EW_ROWTYPE_ADD || $staff->RowType == EW_ROWTYPE_EDIT) { ?>
<script type="text/javascript">
fstafflist.UpdateOpts(<?php echo $staff_list->RowIndex ?>);
</script>
<?php } ?>
<?php
	}
	if ($staff->CurrentAction <> "gridadd")
		$staff_list->Recordset->MoveNext();
}
?>
</tbody>
</table>
<?php } ?>
<?php if ($staff->CurrentAction == "add" || $staff->CurrentAction == "copy") { ?>
<input type="hidden" name="key_count" id="key_count" value="<?php echo $staff_list->KeyCount ?>">
<?php } ?>
<?php if ($staff->CurrentAction == "edit") { ?>
<input type="hidden" name="key_count" id="key_count" value="<?php echo $staff_list->KeyCount ?>">
<?php } ?>
<?php if ($staff->CurrentAction == "") { ?>
<input type="hidden" name="a_list" id="a_list" value="">
<?php } ?>
</div>
</form>
<?php

// Close recordset
if ($staff_list->Recordset)
	$staff_list->Recordset->Close();
?>
<?php if ($staff->Export == "") { ?>
<div class="ewGridLowerPanel">
<?php if ($staff->CurrentAction <> "gridadd" && $staff->CurrentAction <> "gridedit") { ?>
<form name="ewpagerform" id="ewpagerform" class="ewForm" action="<?php echo ew_CurrentPage() ?>">
<table class="ewPager"><tr><td>
<?php if (!isset($staff_list->Pager)) $staff_list->Pager = new cPrevNextPager($staff_list->StartRec, $staff_list->DisplayRecs, $staff_list->TotalRecs) ?>
<?php if ($staff_list->Pager->RecordCount > 0) { ?>
	<table cellspacing="0" class="ewStdTable"><tbody><tr><td><span class="phpmaker"><?php echo $Language->Phrase("Page") ?>&nbsp;</span></td>
<!--first page button-->
	<?php if ($staff_list->Pager->FirstButton->Enabled) { ?>
	<td><a href="<?php echo $staff_list->PageUrl() ?>start=<?php echo $staff_list->Pager->FirstButton->Start ?>"><img src="phpimages/first.gif" alt="<?php echo $Language->Phrase("PagerFirst") ?>" width="16" height="16" style="border: 0;"></a></td>
	<?php } else { ?>
	<td><img src="phpimages/firstdisab.gif" alt="<?php echo $Language->Phrase("PagerFirst") ?>" width="16" height="16" style="border: 0;"></td>
	<?php } ?>
<!--previous page button-->
	<?php if ($staff_list->Pager->PrevButton->Enabled) { ?>
	<td><a href="<?php echo $staff_list->PageUrl() ?>start=<?php echo $staff_list->Pager->PrevButton->Start ?>"><img src="phpimages/prev.gif" alt="<?php echo $Language->Phrase("PagerPrevious") ?>" width="16" height="16" style="border: 0;"></a></td>
	<?php } else { ?>
	<td><img src="phpimages/prevdisab.gif" alt="<?php echo $Language->Phrase("PagerPrevious") ?>" width="16" height="16" style="border: 0;"></td>
	<?php } ?>
<!--current page number-->
	<td><input type="text" name="<?php echo EW_TABLE_PAGE_NO ?>" id="<?php echo EW_TABLE_PAGE_NO ?>" value="<?php echo $staff_list->Pager->CurrentPage ?>" size="4"></td>
<!--next page button-->
	<?php if ($staff_list->Pager->NextButton->Enabled) { ?>
	<td><a href="<?php echo $staff_list->PageUrl() ?>start=<?php echo $staff_list->Pager->NextButton->Start ?>"><img src="phpimages/next.gif" alt="<?php echo $Language->Phrase("PagerNext") ?>" width="16" height="16" style="border: 0;"></a></td>	
	<?php } else { ?>
	<td><img src="phpimages/nextdisab.gif" alt="<?php echo $Language->Phrase("PagerNext") ?>" width="16" height="16" style="border: 0;"></td>
	<?php } ?>
<!--last page button-->
	<?php if ($staff_list->Pager->LastButton->Enabled) { ?>
	<td><a href="<?php echo $staff_list->PageUrl() ?>start=<?php echo $staff_list->Pager->LastButton->Start ?>"><img src="phpimages/last.gif" alt="<?php echo $Language->Phrase("PagerLast") ?>" width="16" height="16" style="border: 0;"></a></td>	
	<?php } else { ?>
	<td><img src="phpimages/lastdisab.gif" alt="<?php echo $Language->Phrase("PagerLast") ?>" width="16" height="16" style="border: 0;"></td>
	<?php } ?>
	<td><span class="phpmaker">&nbsp;<?php echo $Language->Phrase("of") ?>&nbsp;<?php echo $staff_list->Pager->PageCount ?></span></td>
	</tr></tbody></table>
	</td>	
	<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
	<td>
	<span class="phpmaker"><?php echo $Language->Phrase("Record") ?>&nbsp;<?php echo $staff_list->Pager->FromIndex ?>&nbsp;<?php echo $Language->Phrase("To") ?>&nbsp;<?php echo $staff_list->Pager->ToIndex ?>&nbsp;<?php echo $Language->Phrase("Of") ?>&nbsp;<?php echo $staff_list->Pager->RecordCount ?></span>
<?php } else { ?>
	<?php if ($staff_list->SearchWhere == "0=101") { ?>
	<span class="phpmaker"><?php echo $Language->Phrase("EnterSearchCriteria") ?></span>
	<?php } else { ?>
	<span class="phpmaker"><?php echo $Language->Phrase("NoRecord") ?></span>
	<?php } ?>
<?php } ?>
	</td>
</tr></table>
</form>
<?php } ?>
<span class="phpmaker">
<?php if ($Security->IsLoggedIn()) { ?>
<?php if ($staff_list->InlineAddUrl <> "") { ?>
<a class="ewGridLink" href="<?php echo $staff_list->InlineAddUrl ?>"><?php echo $Language->Phrase("InlineAddLink") ?></a>&nbsp;&nbsp;
<?php } ?>
<?php } ?>
</span>
</div>
<?php } ?>
</td></tr></table>
<?php if ($staff->Export == "") { ?>
<script type="text/javascript">
fstafflist.Init();
</script>
<?php } ?>
<?php
$staff_list->ShowPageFooter();
if (EW_DEBUG_ENABLED)
	echo ew_DebugMsg();
?>
<?php if ($staff->Export == "") { ?>
<script type="text/javascript">

// Write your table-specific startup script here
// document.write("page loaded");

</script>
<?php } ?>
<?php include_once "footer.php" ?>
<?php
$staff_list->Page_Terminate();
?>
